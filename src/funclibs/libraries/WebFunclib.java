package libraries;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.MoveTargetOutOfBoundsException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nimbusds.oauth2.sdk.ParseException;
import Launcher.Launcher;
import SeleniumCore.WebActions;


public class WebFunclib extends WebActions 
{
	static Logger logger = Logger.getLogger(WebFunclib.class);
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: launchURL
	// ''@Objective: This Function Open Comporium application and verify its Home Page
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: NA
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= launchURL();
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Himanshu Gosain[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean launchURL() 
	{
		boolean blnFlag = false;
		try
		{
			if(driver.toString().contains("internet explorer"))
			{
				launchIEinPrivateMode();
			}
			// Launching browser
			blnFlag=browser.launchApplication(Launcher.dicCommValues.get("strApplicationURL"));
			if(blnFlag)
			{
				// Verify Comporium home page is displayed
				page("pgeProfilePage").element("txtStreetAddress").waitForElement(60, 1);
				blnFlag = page("pgeProfilePage").element("txtStreetAddress").exist();
				if (!blnFlag) 
					ErrDescription = "Could not open website on browser.";
			}
		}
		catch (Exception e) 
		{
			blnFlag = false;
			logger.error(e.getMessage());
			ErrDescription= "Failed to launch the site on Browser.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: enterAnAddress
	// ''@Objective: This Function enters street address,verifies list of auto-populated addresses, 
	// ''@			select valid address and verify zip-code on Address Search page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: address
	// ''@Param Desc: suggestionList
	// ''@Param Name: isAddressDisplay
	// ''@Param Desc: zip
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= enterAnAddress(strStreetAddressData, 0, false,strZip);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Niraj Kumar[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean enterAnAddress(String strAddress, int intSuggestionList, boolean blnAddressDisplay, String strZip)
	{
		boolean blnFlag = false;
		try
		{
			page("pgeProfilePage").element("txtStreetAddress").waitForElement(60, 1);
			//waitForElement("pgeProfilePage","txtStreetAddress",40);
			blnFlag = page("pgeProfilePage").element("txtStreetAddress").type(strAddress);
			if(blnFlag)
			{
				waitForSync(3);
				//page("pgeHomePage").element("lstSearchAddressList").waitForElement(500,10);
				String strXPath = page("pgeHomePage").element("lstSearchAddressList").GetXpath();
				if(!strXPath.equals(""))
				{
					int AddressListSize = driver.findElements(By.xpath(strXPath)).size();
					if (blnAddressDisplay) 
					{
						if (AddressListSize > 0) 
						{
							if (driver.findElements(By.xpath(strXPath)).get(intSuggestionList).getText().contains(strAddress))
							{
								try
								{
									driver.findElements(By.xpath(strXPath)).get(intSuggestionList).click();
								}
								catch (Exception e)
								{
									((JavascriptExecutor) driver).executeScript("document.getElementsByClassName('suggestion-list-item-container')[0].click();");
								}
							}
							String strZipCode=page("pgeProfilePage").element("txtZipCode").getAttributeValue("value");
							if(!strZipCode.equals(""))
							{
								dicTestData.put("strZipCode", strZipCode);
								blnFlag = page("pgeHomePage").element("btnSubmit").exist();
								if(!blnFlag)
									ErrDescription = "Failed to validate that 'Submit' button is enabled.";
							}
							else
							{
								blnFlag=false;
								ErrDescription = "Failed to fetch auto-populated 'Zip Code'.";
							}
							
						}
						else
						{
							blnFlag=false;
							ErrDescription = "Failed to display auto-populated list of street addresses.";
						}
					}
				}
				else
				{
					blnFlag=false;
					ErrDescription = "Failed to identify auto-populated list of street addresses.";	
				}
			}
			else
				ErrDescription = "Failed to enter Street Address in the address search field.";
		}
		catch (Exception e) 
		{
			blnFlag = false;
			logger.error(e.getMessage());
			ErrDescription= "Failed to enter and select street address.";
		}	
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: clickOnSubmitAddressSearch
	// ''@Objective: This Function clicks on 'Submit' button on Address Search page and verify action performed.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: validData
	// ''@Param Desc: IsPopupDisplay
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= clickOnSubmitAddressSearch(false,false);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Niraj Kumar20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean clickOnSubmitAddressSearch(boolean blnValidData, boolean blnPopupDisplay)
	{
		boolean blnFlag = false;
		try
		{
			//waitForElement("pgeHomePage","btnSubmit",10);
			page("pgeHomePage").element("btnSubmit").waitForElement(30,1);
			blnFlag= page("pgeHomePage").element("btnSubmit").click();
			if (blnValidData) 
			{
				if (blnPopupDisplay) 
				{
					waitForSync(5);
					page("pgeProfilePage").element("eleAccountExistPopUp").waitForElement(60, 1);
					blnFlag = page("pgeHomePage").element("eleAccountExistPopUp").exist();
					if(blnFlag)
					{
						String strPopUpText = page("pgeHomePage").element("eleAccountExistPopUp").getText();
						if (strPopUpText.contains("A Comporium account already exists")) 
						{
							blnFlag=true;
							dicTestData.put("strPopUpText", strPopUpText);
						}
					}
					else 
					{
						blnFlag = false;
						ErrDescription = "Failed to display 'A Comporium account already exists' pop-up.";
					}
				}
				else 
				{
					waitForSync(2);
					blnFlag =page("pgeBrowse").element("eleServiceAddress").exist();
					if(blnFlag)
					{
						page("pgeProfilePage").element("btnChangeAddress").waitForElement(5, 1);
						blnFlag =page("pgeProfilePage").element("btnChangeAddress").exist();
						if(blnFlag)
						{
							blnFlag=page("pgeBrowse").element("btnBundlePlansHighlighted").exist();
							if(blnFlag)
							{
								page("pgeBrowse").element("btnIndividualPlans").waitForElement(5, 1);
								blnFlag=page("pgeBrowse").element("btnIndividualPlans").exist();
								if(blnFlag)
								{
									page("pgeBrowse").element("btnCartIcon").waitForElement(5, 1);
									blnFlag=page("pgeBrowse").element("btnCartIcon").exist();
									if(!blnFlag)
										ErrDescription = "Failed to display 'Cart' section on the Browse page.";
								}
								else
									ErrDescription = "Failed to display 'Individual Plans' button on the Browse page.";
							}
							else
								ErrDescription = "Failed to display highlighted 'Bundle Plans' button on the Browse page.";
						}
						else
							ErrDescription = "Failed to display 'Change Address' button on the Browse page.";
					}
					else
					{
						ErrDescription = "Failed to display selected 'Service Address' on the Browse page.";
					}
				}
			}
			else
			{
				page("pgeHomePage").element("eleAddressNotExistError").waitForElement(5, 1);
				blnFlag = page("pgeHomePage").element("eleAddressNotExistError").exist();
				if(blnFlag)
				{
					String strErrVal = page("pgeHomePage").element("eleAddressNotExistError").getText();
					if(!strErrVal.contains(""))
					{
						blnFlag = false;
						ErrDescription = "Failed to fetch error message from the pop-up.";
					}
					else
					{
						blnFlag=true;
						dicTestData.put("strPopUpText", strErrVal);
					}
				}
				else
					ErrDescription = "Failed to display Error Message pop-up.";
			}
		}
		catch (Exception e) 
		{
			blnFlag = false;
			logger.error(e.getMessage());
			ErrDescription= "Failed to click on 'Submit' button.";
		}
		return blnFlag;
	}

	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: clickOnAddressConformationPopup
	// ''@Objective: This Function click on specified option from 'Existing Address' pop-up and verify navigated page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: locationConfirmation
	// ''@Param Desc: strEnteredStreetAddress
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= clickOnAddressConformationPopup("btnYesLiveHere","");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Niraj Kumar[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean clickOnAddressConformationPopup(String eleCustomerLocation,String strStreetAddress) 
	{
		boolean blnFlag = false;
		try
		{
			page("pgeProfilePage").element(eleCustomerLocation).waitForElement(90, 1);
			if(driver.toString().contains("internet explorer"))
			{
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",driver.findElement(By.xpath(page("pgeProfilePage").element(eleCustomerLocation).GetXpath())));
				blnFlag=true;
			}
			else
			{
				blnFlag = page("pgeProfilePage").element(eleCustomerLocation).click();
			}
			
			if(blnFlag)
			{
				if (eleCustomerLocation == "btnYesLiveHere") 
				{
					waitForSync(5);
					page("pgeMyAccountLogin").element("eleAccountLogin").waitForElement(60,10);
					blnFlag =page("pgeMyAccountLogin").element("eleAccountLogin").exist();
					if(!blnFlag)
						ErrDescription = "Failed to navigate to 'My Account Login' page.";
				} 
				else 
				{
					if (eleCustomerLocation == "btnIamMovingHere") 
					{
						waitForSync(10);
						String strAddress=page("pgeBrowse").element("eleServiceAddress").getText();
						if(strAddress.contains(strStreetAddress))
						{
							page("pgeProfilePage").element("btnChangeAddress").waitForElement(5, 1);
							blnFlag =page("pgeProfilePage").element("btnChangeAddress").exist();
							if(blnFlag)
							{
								blnFlag=page("pgeBrowse").element("btnBundlePlansHighlighted").exist();
								if(!blnFlag)
									ErrDescription = "Failed to display highlighted 'Bundle Plans' button on the Browse page.";
							}
							else
							{
								ErrDescription = "Failed to display 'Change Address' button on the Browse page.";
							}
						}
						else
						{
							blnFlag=false;
							ErrDescription = "Failed to display 'Service Address' section on 'Browse' page.";
						}
					}
				}
			}
			else
				ErrDescription = "Failed to select Location confirmation option from pop-up on 'Browse' page.";
		}
		catch (Exception e)
		{
			blnFlag = false;
			logger.error(e.getMessage());
			ErrDescription= "Failed to click on specified option from 'Existing Customers' pop-up.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: customizePlanAndVerify
	// ''@Objective: This Function clicks on 'Customize' button on 'Browse' page(if blnClick set as true) and verifies fields 
	// ''@			on navigated 'Customize Order' page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: blnClick
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= customizePlanAndVerify(true);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Niraj Kumar[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean customizePlanAndVerify(boolean blnClick)
	{
		boolean blnFlag =false;
		try
		{
			if(blnClick)
			{
				blnFlag=clickAndVerify("pgeBrowse","btnCustomizeBundle","pgeProfilePage","eleCustomizeOrderTitle");
				if(!blnFlag)
					ErrDescription = "Failed to click on 'Customize' button.";
			}
			else
			{
				blnFlag=true;
			}
			
			if(blnFlag)
			{
				page("pgeProfilePage").element("eleCustomizeOrderTitle").waitForElement(50);
				blnFlag=page("pgeProfilePage").element("eleCustomizeOrderTitle").exist();
				if(blnFlag)
				{
					blnFlag=page("pgeCustomizeOrder").element("eleCustomizeBreadcrum").exist();
					if(blnFlag)
					{
						blnFlag=page("pgeCustomizeOrder").element("eleCheckoutBreadcrum2").exist();
						if(blnFlag)
						{
							blnFlag=page("pgeCustomizeOrder").element("eleConfirmBreadcrum3").exist();
							if(blnFlag)
							{
								blnFlag=page("pgeCustomizeOrder").element("elePaymentBreadcrum4").exist();
								if(blnFlag)
								{
									blnFlag=page("pgeCustomizeOrder").element("eleMonthlyCharges").exist();
									if(blnFlag)
									{
										blnFlag=page("pgeCustomizeOrder").element("lnkContinueShopping").exist();
										if(blnFlag)
										{
											blnFlag=page("pgeCustomizeOrder").element("btnContinue").exist();
											if(!blnFlag)
												ErrDescription = "Failed to display 'Continue' button.";
										}
										else
											ErrDescription = "Failed to display 'Continue Shopping' link.";
									}
									else
										ErrDescription = "Failed to display 'Monthly Charges' field.";
								}
								else
									ErrDescription = "Failed to verify 'Payment' breadcrum.";
							}
							else
								ErrDescription = "Failed to verify 'Confirm' breadcrum.";
						}
						else
							ErrDescription = "Failed to verify 'Checkout' breadcrum.";
					}
					else
						ErrDescription = "Failed to verify 'Customize' breadcrum.";
				}
				else
					ErrDescription = "Failed to navigate to 'Customize Order' page.";
			}		
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while clicking and verifying action.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: checkoutPlanAndVerify
	// ''@Objective: This Function clicks on Customize button on 'Browse' page and verify navigated page with fields.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= checkoutPlanAndVerify();
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Niraj Kumar[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean checkoutPlanAndVerify()
	{
		boolean blnFlag = false;
		try
		{
			blnFlag = page("pgeCustomizeOrder").element("btnContinue").click();
			waitForSync(6);
			page("pgeCheckout").element("eleCheckoutBreadcrum").waitForElement(60, 1);
			
			if(blnFlag) 
			{
				blnFlag=verifyElementExist("pgeCheckout","eleCheckoutBreadcrum");
				if(blnFlag)
				{
					blnFlag=verifyElementExist("pgeCheckout","eleBrdcrum2Highlighted");
					if(blnFlag)
					{
						blnFlag=verifyElementExist("pgeCheckout","elePrimaryAccountHolder");
						if(blnFlag)
						{
							blnFlag=verifyElementExist("pgeCheckout","eleBillingAddress");
							if(blnFlag)
							{
								blnFlag=verifyElementExist("pgeCheckout","eleCreditCardInfo");
								if(!blnFlag)
									ErrDescription = "Failed to display 'Credit Check Information' section.";
							}
							else
								ErrDescription = "Failed to display 'Billing Address' section.";
						}
						else
							ErrDescription = "Failed to display 'Primary Account Holder' section.";
					}
					else
						ErrDescription = "Failed to display 'Highlighted Breadcrum'.";
				}
				else
					ErrDescription = "Failed to display 'Highlighted 'Checkout' breadcrum.";
			}
			else
				ErrDescription = "Failed to click on 'Continue' button on 'Customize Order' page.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while clicking and verifying action.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: selectDateAndTimeForInstallation
	// ''@Objective: This Function selects Service Installation date and time on 'Service & Installation' page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= selectDateAndTimeForInstallation();
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean selectDateAndTimeForInstallation()
	{
		boolean blnFlag = false;
		try
		{
			page("pgeServiceSchedule").element("btnSelectDateTime").waitForElement(10, 1);
			blnFlag = page("pgeServiceSchedule").element("btnSelectDateTime").exist();
			if (blnFlag) 
			{
				blnFlag = page("pgeServiceSchedule").element("btnSelectDateTime").click();
				waitForSync(3);
				blnFlag = page("pgeServiceSchedule").element("eleSelectDate").exist();
				if(!blnFlag)
				{
					//page("pgeServiceSchedule").element("btnNextMonth").waitForElement(5);
					blnFlag = page("pgeServiceSchedule").element("btnNextMonth").click();
					waitForSync(2);
					if(blnFlag)
					{
						//page("pgeConfirmOrder").element("eleSelectDate").waitForElement(5);
						blnFlag = page("pgeServiceSchedule").element("eleSelectDate").click();
						waitForSync(2);
					}
				}
				else
				{
					blnFlag = page("pgeServiceSchedule").element("eleSelectDate").click();
					waitForSync(2);
				}
			}
			
			if(blnFlag)
			{
				blnFlag=page("pgeServiceSchedule").element("rdoPreferredTimeOfDay").click();
				if(blnFlag)
				{
					blnFlag=page("pgeServiceSchedule").element("btnOKCalendar").click();
					if(blnFlag)
					{
						waitForSync(2);
						//page("pgeConfirmOrder").element("btnSelectDateTime").waitForElement(10, 1);
						blnFlag=verifyElementExist("pgeServiceSchedule","eleSelectedDateTime");
						if(!blnFlag)
							ErrDescription = "Failed to display seleted Date & Time on 'Schedule' page.";
					}
					else
						ErrDescription = "Failed to click on 'OK' button from Calendar pop-up.";
				}
				else
					ErrDescription = "Failed to select 'Preffered Time of the day' radio-button from Calendar pop-up.";
			}
			else
				ErrDescription = "Failed to select date from the Calendar pop-up.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while selecting Installation Date and Time.";
		}
		return blnFlag;
	}

	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: fillCardDetails
	// ''@Objective: This Function fills all the card details on 'Payment' page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: CardNumber
	// ''@Param Desc: ExpiryDate
	// ''@Param Name: CardHolderName
	// ''@Param Desc: CVV
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= fillCardDetails(CardNumber, ExpiryDate, CardHolderName, CVV);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Niraj Kumar[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean fillCardDetails(String strCardNumber,String strExpiryDate,String strCardHolderName,String strCVV)
	{
		boolean blnFlag = false;
		try
		{
			page("pgeSetUpPayment").element("txtCardNumber").waitForElement(30, 1);
			blnFlag = page("pgeSetUpPayment").element("txtCardNumber").exist();
			if (blnFlag) 
			{
				waitForSync(1);
				blnFlag = page("pgeSetUpPayment").element("txtCardNumber").type(strCardNumber);
				if(blnFlag)
				{
					waitForSync(1);
					blnFlag = page("pgeSetUpPayment").element("txtExpiryDate").type(strExpiryDate);
					if(blnFlag)
					{
						waitForSync(1);
						blnFlag = page("pgeSetUpPayment").element("txtCardHolderName").type(strCardHolderName);
						if(blnFlag)
						{
							waitForSync(1);
							blnFlag = page("pgeSetUpPayment").element("txtCVV").type(strCVV);
							if(blnFlag)
							{
								waitForSync(1);
								blnFlag = page("pgeSetUpPayment").element("btnSubmitOrder").JSClick();
								if(blnFlag)
								{
									waitForSync(7);
									page("pgeThankYou").element("eleServiceInstallTime").waitForElement(30);
									blnFlag=page("pgeThankYou").element("eleOrderNumber").exist();
									if(blnFlag)
									{
										blnFlag= page("pgeThankYou").element("btnBackToHome").exist();
										if(blnFlag)
										{
											blnFlag=page("pgeThankYou").element("eleThankuForOrder").exist();
											if(blnFlag)
											{
												dicTestData.put("strOrderTxt&Number", "'Thank you' page is displayed with following details: '<b>"+"Your service will be installed on: "
														+ page("pgeThankYou").element("eleServiceInstallTime").getText() + ". "
														+ page("pgeThankYou").element("eleOrderNumber").getText()+"</b>'");
													String[] strOrderNumber = page("pgeThankYou").element("eleOrderNumber").getText().split("is");
													dicTestData.put("strOrderNumber",strOrderNumber[1].replaceAll("\\s","").replace(".",""));
											}
											else
												ErrDescription = "Failed to display 'Thank For Your Order' text.";
										}
										else
											ErrDescription = "Failed to display 'Back To Home' button.";
									}
									else
										ErrDescription = "Failed display 'Order Number'.";
								}
								else
									ErrDescription = "Failed to click on 'Submit Order' button.";
							}
							else
								ErrDescription = "Failed to enter CVV.";
						}
						else
							ErrDescription = "Failed to enter Card Holder Name.";
					}
					else
						ErrDescription = "Failed to enter Card Expiry Date..";
				}
				else
					ErrDescription = "Failed to enter Card Number.";
			}
			else
				ErrDescription = "Failed to display Card Number field.";
			waitForSync(3);
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while entering card details.";
		}
		return blnFlag;
	}

	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: enterCheckoutInformation
	// ''@Objective: This Function enters billing address details on Checkout page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: firstName
	// ''@Param Desc: lastNameL
	// ''@Param Name: phoneNumber
	// ''@Param Desc: dob
	// ''@Param Desc: ssn
	// ''@Param Desc: email
	// ''@Param Desc: isCardDetails
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= enterCheckoutInformation(strFirstName, strLastName, strDOB, strPhoneNumber, strSSN, strEmail,true);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Niraj Kumar[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean enterCheckoutInformation(String strFirstName, String strLastName, String strPhoneNumber, String strDob, String strSSN,String strEmail, boolean blnCardDetails)
	{
		boolean blnFlag = false;
		try
		{
			page("pgeProfilePage").element("txtFirstName").waitForElement(30, 1);
			blnFlag = page("pgeProfilePage").element("txtFirstName").exist();
			if (blnFlag) 
			{				
				blnFlag = page("pgeProfilePage").element("txtFirstName").type(strFirstName);
				if(blnFlag)
				{
					blnFlag = page("pgeProfilePage").element("txtLastName").type(strLastName);
					if(blnFlag)
					{
						blnFlag = page("pgeProfilePage").element("txtPhoneNumber").type(strPhoneNumber);
						if(blnFlag)
						{
							blnFlag = page("pgeProfilePage").element("txtDOB").type(strDob);
							if(blnFlag)
							{
								blnFlag = page("pgeProfilePage").element("txtSSN").type(strSSN);
								if(blnFlag)
								{
									blnFlag = page("pgeProfilePage").element("txtEmail").type(strEmail);
									if(blnFlag)
									{
										if (blnCardDetails) 
										{
											page("pgeCheckout").element("chkNoThanksIwillPay").waitForElement(30, 1);
											waitForSync(3);
											blnFlag = page("pgeCheckout").element("chkNoThanksIwillPay").click();
											if(blnFlag)
											{
												blnFlag = page("pgeCheckout").element("btnContinueSubmit").click();
												waitForSync(9);
												//page("pgeServiceSchedule").element("btnSelectDateTime").waitForElement(30, 1);
												if (blnFlag) 
												{
													blnFlag=page("pgeServiceSchedule").element("eleSchedulePageTitle").exist();
													if(blnFlag)
													{
														blnFlag=page("pgeServiceSchedule").element("eleServiceAndInstallation").exist();
														if(!blnFlag)
															ErrDescription = "Failed to display 'Service & Installation' information.";
													}
													else
														ErrDescription = "Failed to navigate to 'Service Installation' page.";
												}
												else
													ErrDescription = "Failed to click on 'Continue' button.";
											}
											else
												ErrDescription = "Failed to select 'No, Thanks I will pay' check-box.";
										}
										else
										{
											blnFlag = page("pgeCheckout").element("btnContinueSubmit").click();
											waitForSync(9);
											page("pgeProfilePage").element("btnBackToHome").waitForElement(30, 1);
											if(blnFlag)
											{
												blnFlag=page("pgeProfilePage").element("btnBackToHome").exist();
												if(!blnFlag)
													ErrDescription = "Failed to display 'Thank you' page.";
											}
											else
												ErrDescription = "Failed to click on 'Continue' button.";
										}
									}
									else
										ErrDescription = "Failed to enter Email Address.";
								}
								else
									ErrDescription = "Failed to enter SSN.";
							}
							else
								ErrDescription = " Failed to enter DOB.";
						}
						else
							ErrDescription = "Failed to enter Phone Number.";
					}
					else
						ErrDescription = "Failed to enter Last Name.";
				}
				else
					ErrDescription = "Failed to enter First Name.";
			}
			else
				ErrDescription = "Failed to display 'Billing Address' section.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while entering Billing Address details.";
		}														
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: enterCheckoutInformation
	// ''@Objective: This Function enters billing address details on Checkout page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: firstName
	// ''@Param Desc: lastNameL
	// ''@Param Name: phoneNumber
	// ''@Param Desc: dob
	// ''@Param Desc: ssn
	// ''@Param Desc: email
	// ''@Param Desc: isCardDetails
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= enterCheckoutInformation(strFirstName, strLastName, strDOB, strPhoneNumber, strSSN, strEmail,true,"pgeSchedule","btnSelectDate);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Niraj Kumar[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean enterCheckoutInformation(String strFirstName, String strLastName, String strPhoneNumber, String strDOB, String strSSN,String strEmail, boolean blnCardDetails,String pgeNewPage,String eleNewElement)
	{
		boolean blnFlag = false;
		try
		{
			page("pgeProfilePage").element("txtFirstName").waitForElement(30, 1);
			blnFlag = page("pgeProfilePage").element("txtFirstName").exist();
			if (blnFlag) 
			{
				blnFlag = page("pgeProfilePage").element("txtFirstName").type(strFirstName);
				if(blnFlag)
				{
					blnFlag = page("pgeProfilePage").element("txtLastName").type(strLastName);
					if(blnFlag)
					{
						blnFlag = page("pgeProfilePage").element("txtPhoneNumber").type(strPhoneNumber);
						if(blnFlag)
						{
							blnFlag = page("pgeProfilePage").element("txtDOB").type(strDOB);
							if(blnFlag)
							{
								blnFlag = page("pgeProfilePage").element("txtSSN").type(strSSN);
								if(blnFlag)
								{
									blnFlag = page("pgeProfilePage").element("txtEmail").type(strEmail);
									if(blnFlag)
									{
										if (blnCardDetails) 
										{
											page("pgeCheckout").element("chkNoThanksIwillPay").waitForElement(30, 1);
											waitForSync(1);
											blnFlag = page("pgeCheckout").element("chkNoThanksIwillPay").click();
											if(blnFlag)
											{
												waitForSync(3);
												blnFlag = page("pgeCheckout").element("btnContinueSubmit").click();
												waitForSync(9);
												if (blnFlag) 
												{
													blnFlag=page(pgeNewPage).element(eleNewElement).exist();	
													if(!blnFlag)
														ErrDescription = "Failed to navigate to the next page on clicking 'Continue' button.";
												}
												else
													ErrDescription = "Failed to click on 'Continue' button.";
											}
											else
												ErrDescription = "Failed to select 'No, Thanks I will pay' check-box.";
										}
										else
										{
											blnFlag = page("pgeCheckout").element("btnContinueSubmit").click();
											waitForSync(9);
											page("pgeProfilePage").element("btnBackToHome").waitForElement(30, 1);
											if(blnFlag)
											{
												blnFlag=page("pgeProfilePage").element("btnBackToHome").exist();
												if(!blnFlag)
													ErrDescription = "Failed to display 'Thank you' page.";
											}
											else
												ErrDescription = "Failed to click on 'Continue' button.";
										}
									}
									else
										ErrDescription = "Failed to enter Email Address.";
								}
								else
									ErrDescription = "Failed to enter SSN.";
							}
							else
								ErrDescription = " Failed to enter DOB.";
						}
						else
							ErrDescription = "Failed to enter Phone Number.";
					}
					else
						ErrDescription = "Failed to enter Last Name.";
				}
				else
					ErrDescription = "Failed to enter First Name.";
			}
			else
				ErrDescription = "Failed to display 'Billing Address' section.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while entering Billing Address details.";
		}														
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: selectIAgreeCheckBoxAndContinue
	// ''@Objective: This Function clicks on 'I agree checkbox' and then click 'continue' button on 'Confirm Order' page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= selectIAgreeCheckBoxAndContinue();
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Niraj Kumar[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean selectIAgreeCheckBoxAndContinue() 
	{
		boolean blnFlag = false;
		try
		{
			blnFlag=page("pgeConfirmOrder").element("chkIagreeTermsConditions").click();
			if(blnFlag)
			{
				waitForSync(2);
				page("pgeConfirmOrder").element("btnContinue").waitForElement(30, 1);
				blnFlag=page("pgeConfirmOrder").element("btnContinue").click();
				if(blnFlag)
				{
					waitForSync(8);
					page("pgeSetUpPayment").element("elePaymentPageTitle").waitForElement(120, 1);
					blnFlag=page("pgeSetUpPayment").element("elePaymentPageTitle").exist();
					if(blnFlag)
					{
						blnFlag=page("pgeSetUpPayment").element("eleWhatIamPayingToday").exist();
						if(blnFlag)
						{
							blnFlag=page("pgeSetUpPayment").element("eleCardPaymentSection").exist();
							if(!blnFlag)
								ErrDescription = "Failed to display 'Credit Card/Debit Card Payment' section.";
						}
						else
							ErrDescription = "Failed to display 'What am I paying today?' section.";
					}
					else
						ErrDescription = "Failed to navigate to 'Set Up Payment' page.";
				}
				else
					ErrDescription = "Failed to click on 'Continue' button";
			}
			else
				ErrDescription = "Failed to click on 'I agree with Terms & Condition' checkbox.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while clicking and verifying action.";
		}
		return blnFlag;
	}

	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: selectViewAllBundlesAndVerify
	// ''@Objective: This Function clicks on 'View All Bundles' checkbox and verify all the bundle plan types available.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= selectViewAllBundlesAndVerify();
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Niraj Kumar[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean selectViewAllBundlesAndVerify()
	{
		boolean blnFlag = false;		
		try
		{
			if(driver.toString().contains("internet explorer"))
			{
				blnFlag=selectCheckBox("pgeBrowse","chkViewBundles","eleImLookingFor","id",true);
			}
			else
			{
				blnFlag = page("pgeBrowse").element("chkViewAllBundles").click();
			}
			if(blnFlag)
			{
				waitForSync(3);
				blnFlag=page("pgeBrowse").element("eleImLookingFor").exist();
				if(blnFlag)
				{
					blnFlag=page("pgeBrowse").element("chkPhonePlan").exist();
					if(blnFlag)
					{
						blnFlag=verifyElementExist("pgeBrowse","chkSecurityPlan");
						if(blnFlag)
						{
							blnFlag=verifyElementExist("pgeBrowse","chkTVPlan");
							if(blnFlag)
							{
								blnFlag=page("pgeBrowse").element("chkInternetPlan").exist();
								if(!blnFlag)
									ErrDescription="Failed to display check-box for 'Internet' plan type.";
							}
							else
								ErrDescription="Failed to display check-box for 'TV' plan type.";
						}
						else
							ErrDescription="Failed to display check-box for 'Security' plan type.";
					}
					else
						ErrDescription="Failed to display check-box for 'Phone' plan type.";
				}
				else
					ErrDescription="Failed to populate 'I am looking for' section.";
			}
			else
				ErrDescription="Failed to select 'View All Bundles' check-box.";	
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while clicking and verifying action.";
		}
		return blnFlag;
	}

	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: navigateBack
	// ''@Objective: This Function navigates back to the previous page from the current page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: eleNewPage
	// ''@Param Desc: eleNewElement
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= navigateBack("pgeHomePage","btnSubmit");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean navigateBack(String eleNewPage,String eleNewElement)
	{
		boolean blnFlag=false;
		
		try
		{
			waitForSync(2);
			driver.navigate().back();
			waitForSync(3);
			if (page(eleNewPage).element(eleNewElement).exist())
			{
				blnFlag=true;
			}
			else
			{
				blnFlag=false;
				ErrDescription="Failed to navigate back to the previous page.";
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while navigating back to the previous page.";
		}
		return blnFlag;
	}

	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: selectAndVerifyBundlePlan
	// ''@Objective: This Function selects and verifies specified bundle plan on Browse page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strBundle
	// ''@Param Desc: blnDeSelect
	// ''@Param Name: chkBundle
	// ''@Param Desc: eleDetails
	// ''@Param Desc: eleBundleName
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= selectAndVerifyBundlePlan("Internet",true,"chkInternetPlan","eleInternetDetails","eleInternetBundleName");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean selectAndVerifyBundlePlan(String strBundle,boolean blnDeSelect,String chkBundle,String eleDetails,String eleBundleName)
	{
		boolean blnFlag = false;
		try
		{
			blnFlag=page("pgeBrowse").element(chkBundle).click();
			waitForSync(5);
			if(blnFlag)
			{
				blnFlag=page("pgeBrowse").element(eleBundleName).exist();
				if(blnFlag)
				{
					blnFlag=page("pgeBrowse").element("eleBundlePrice").exist();
					if(blnFlag)
					{
						blnFlag=page("pgeBrowse").element("lnkViewDetails").exist();
						if(blnFlag)
						{
							blnFlag=page("pgeBrowse").element("btnCustomize").exist();
							if(blnFlag)
							{
								if(blnDeSelect)
									page("pgeBrowse").element(chkBundle).click();
									waitForSync(3);
							}
							else
								ErrDescription="Failed to display 'Customize' button on Browse page.";
						}
						else
							ErrDescription="Failed to display 'View Details' link on Browse page.";
					}
					else
						ErrDescription="Failed to display bundle price on Browse page.";
				}
				else
					ErrDescription="Failed to display bundle plan details for option '"+strBundle+"' on Browse page.";
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while selecting and verifying bundle plans.";
		}
		return blnFlag;
	}

	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: clickAndVerify
	// ''@Objective: This Function clicks on the specified object and verifies multiple fields on navigated page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: eleElement
	// ''@Param Name: pgeNewPage
	// ''@Param Desc: eleNewElement
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= clickAndVerify("pgeBrowse","lnkViewDetails","pgeBrowse","lnkHideDetails","elePlanFeatures");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean clickAndVerify(String pgePage, String eleElement, String pgeNewPage, String... eleNewElement)
	{
		boolean blnFlag = false;
		try
		{      
			blnFlag=page(pgePage).element(eleElement).click();
			if(blnFlag)
			{
				waitForSync(8);
				blnFlag = page(pgeNewPage).element(eleNewElement[0]).exist();
				if(blnFlag)
					blnFlag = page(pgeNewPage).element(eleNewElement[1]).exist();
				if (!blnFlag)
					ErrDescription = "Unable to verify navigated page.";
			} 
			else
				ErrDescription = "Unable to perform click action.";
		}

		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while clicking and verifying action.";
		}
		return blnFlag;
	}

	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: clickAndVerify
	// ''@Objective: This Function clicks on the specified object and verifies navigated page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: eleElement
	// ''@Param Name: pgeNewPage
	// ''@Param Desc: eleNewElement
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= clickAndVerify("pgeMyAccountLogin","btnLogin","pgeAccountOverview","eleAccountSummary");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean clickAndVerify(String pgePage, String eleElement, String pgeNewPage, String eleNewElement)
	{
		boolean blnFlag = false;
		try
		{  
			//waitForElement(pgePage,eleElement,40);
			page(pgePage).element(eleElement).waitForElement(30,1);
			blnFlag=page(pgePage).element(eleElement).click();
			if(blnFlag)
			{
				waitForSync(4);
				blnFlag = verifyElementExist(pgeNewPage,eleNewElement);
				if(!blnFlag)
					ErrDescription = "Unable to verify navigated page.";
			} 
			else
				ErrDescription = "Unable to perform click action.";
		}

		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while clicking and verifying action.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: clickAndVerifyContainsText
	// ''@Objective: This Function clicks and verify that the populated text contains the specified value.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: eleElement
	// ''@Param Name: pgeNewPage
	// ''@Param Desc: eleNewElement
	// ''@Param Desc: strText
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= clickAndVerifyContainsText("pgeBrowse","chkPhonePlan","pgeBrowse","eleSelectedPlanDetails","Phone");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean clickAndVerifyContainsText(String pgePage, String eleElement, String pgeNewPage, String eleNewElement, String strText)
	{
		boolean blnFlag = false;
		String strValue="";
		try
		{      
			blnFlag=page(pgePage).element(eleElement).click();
			if(blnFlag)
			{
				waitForSync(4);
				blnFlag = page(pgeNewPage).element(eleNewElement).exist();
				if(blnFlag)
				{
					strValue = page(pgeNewPage).element(eleNewElement).getText();
					if(strValue.contains(strText))
					{
						blnFlag=true;
					}
					else
					{
						blnFlag=false;
						ErrDescription = "Unable to verify field '"+strText+"' on the navigated page.";
					}
				}
				else
					ErrDescription = "Unable to verify navigated page.";
			} 
			else
				ErrDescription = "Unable to perform click action.";
		}

		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while clicking and verifying text.";
		}
		return blnFlag;
	}

	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: enterDataAndClick
	// ''@Objective: This Function enters specified data and click on the specified object and verifies navigated page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: eleElement
	// ''@Param Name: strData
	// ''@Param Desc: btnClick
	// ''@Param Desc: strNewPage
	// ''@Param Desc: eleNewElement
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= enterDataAndClick("pgeHomePage","txtAptNumber",strAptNumber,"btnSubmit","pgeBrowse","lblAptNumber","btnCartIcon");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean enterDataAndClick(String pgePage, String eleElement, String strData,String btnClick,String strNewPage,String... eleNewElement)
	{
		boolean blnFlag = false;
		try
		{      
			blnFlag=page(pgePage).element(eleElement).type(strData);
			if(blnFlag)
			{
				blnFlag=page(pgePage).element(eleNewElement[0]).waitForElement(10,1);
				if(blnFlag)
				{
					blnFlag=page(pgePage).element(btnClick).click();
					if(blnFlag)
					{
						page(strNewPage).element(eleNewElement[1]).waitForElement(10,1);
						blnFlag = page(strNewPage).element(eleNewElement[1]).exist();
						if (!blnFlag)
							ErrDescription = "Unable to verify navigated page.";
					}
					else
						ErrDescription = "Unable to perform click action.";	
				}
				else
					ErrDescription = "Unable to verify existence of the field.";	
			}
			else
				ErrDescription = "Unable to enter Data in the field.";
		}

		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while entering data and verifying action.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyConfirmOrderPage
	// ''@Objective: This Function verifies fields on Confirm Order page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyConfirmOrderPage();
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyConfirmOrderPage()
	{

		boolean blnFlag = false;
		try
		{
			page("pgeServiceSchedule").element("btnContinue").waitForElement(30);
			blnFlag = page("pgeServiceSchedule").element("btnContinue").click();
			waitForSync(5);
			if(blnFlag)
			{
				//page("pgeConfirmOrder").element("eleConfirmHighlighted").waitForElement(30);
				blnFlag=page("pgeConfirmOrder").element("eleConfirmHighlighted").exist();
				if(blnFlag)
				{
					blnFlag=page("pgeConfirmOrder").element("eleTotalDue").exist();
					if(blnFlag)
					{
						blnFlag=page("pgeConfirmOrder").element("eleAccountDetails").exist();
						if(!blnFlag)
							ErrDescription = "Failed to display Details on 'Confirm Order' page.";
					}
					else
						ErrDescription = "Failed to display 'Total Due' field on 'Confirm Order' page.";
				}
				else
					ErrDescription = "Failed to display highlighted 'Confirm' breadcrum on 'Confirm Order' page.";
			}
			else
				ErrDescription = "Failed to click on 'Continue' button on 'Confirm Order' page.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying fields.";
		}
		return blnFlag;
	}

	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: enterAnAddressWithApt
	// ''@Objective: This Function enters a street address with apartment number on Address Search page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: address
	// ''@Param Desc: zip
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= enterAnAddressWithApt(strStreetAddressData,strZip);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean enterAnAddressWithApt(String strAddress, String strZip)
	{
		boolean blnFlag = false;
		try
		{
			page("pgeProfilePage").element("txtStreetAddress").waitForElement(30,10);
			blnFlag = page("pgeProfilePage").element("txtStreetAddress").exist();
			if (blnFlag)
			{
				blnFlag = page("pgeProfilePage").element("txtStreetAddress").type(strAddress);
				waitForSync(1);
				page("pgeHomePage").element("lstAddressWithApt").waitForElement(60,10);
				if(blnFlag)
				{
					if(page("pgeHomePage").element("lstAddressWithApt").exist())
					{
						blnFlag=page("pgeHomePage").element("lnkAddressWithApt").click();
						if(blnFlag)
						{
							if(page("pgeProfilePage").element("txtZipCode").waitForElement(60,1) && page("pgeProfilePage").element("txtZipCode").exist())
							{
								String strZipCode=page("pgeProfilePage").element("txtZipCode").getAttributeValue("value");
								if(strZipCode.contains(strZip))
								{
									blnFlag=true;
									blnFlag = page("pgeHomePage").element("btnSubmit").exist();
									if(!blnFlag)
									{
										ErrDescription="Failed to verify that 'Submit' button is enabled.";
									}
								}
								else
								{
									blnFlag=false;
									ErrDescription="Failed to display Zip "+strZipCode+" in 'Zip Code' field.";
								}
							}
							else
								ErrDescription="Failed to display 'Zip Code' field.";
						}
						else
							ErrDescription="Failed to select 'Street Address' with apartment number.";
						}
						else
						ErrDescription="Failed to display Street Address with 'Choose Apt#' in auto-populated address list.";	
				}
				else
					ErrDescription="Failed to enter Street Address '"+strAddress+"'.";
			}
			else
				ErrDescription="Failed to display 'Street Address'field on 'Address Search' page.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while performing action.";
		}
		return blnFlag;
	}

	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyCookies
	// ''@Objective: This Function verifies cookies.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyCookies();
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyCookies()
	{
		boolean blnFlag = false;
		try
		{      
			Set<org.openqa.selenium.Cookie> lst = driver.manage().getCookies();
			if (lst.size() == 0)
			{
				blnFlag=true;
			}
			else
			{
				ErrDescription="Failed to validate that the cookies are blank.";
			}
		}

		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying cookies.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifySessionIDCookie
	// ''@Objective: This Function verifies 'SESSION_ID' cookie.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: blnSession
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifySessionIDCookie(false);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifySessionIDCookie(boolean blnSession)
	{
		boolean blnFlag = false;
		String strValue ="";
		
		try
		{    
			if(blnSession)
			{
				strValue = driver.manage().getCookieNamed("SESSION_ID").getValue();
				if(!strValue.equals(""))
				{
					blnFlag=true;
					dicTestData.put("strSessionID", strValue);
				}
				else
				{
					blnFlag=false;
					ErrDescription="Failed to fetch 'Session ID' cookie.";
				}
			}
			else
			{
				waitForSync(4);
				if(driver.toString().contains("internet explorer"))
				{
					driver.manage().deleteAllCookies();
				}
				if (driver.manage().getCookieNamed("SESSION_ID") == null)
				{
					blnFlag=true;	
				}
				else
				{
					blnFlag=false;
					ErrDescription="Failed to verify 'Session ID' cookie is not present.";
				}
			}
		}

		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying cookies.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyEmptyCart
	// ''@Objective: This Function verifies Cart is empty.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyEmptyCart();
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyEmptyCart()
	{
		boolean blnFlag = false;
		String strCount ="";
		
		try
		{   
			waitForElement("pgeBrowse","eleCartCount",40);
			strCount=page("pgeBrowse").element("eleCartCount").getText();
			if(strCount.equals("0"))
			{
				blnFlag = page("pgeBrowse").element("btnCartIcon").click();
				if(blnFlag)
				{
					waitForSync(4);
					blnFlag = page("pgeBrowse").element("eleCartEmptyMsg").exist();
					if(blnFlag)
					{
						blnFlag = page("pgeBrowse").element("btnCartIcon").click();
					}
					else
						ErrDescription = "Unable to verify 'Your Cart is Empty' message.";
				}
				else
					ErrDescription = "Failed to click on 'Cart' icon.";
			}
			else
				ErrDescription = "Failed to verify that cart is empty.";
		}

		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying empty cart.";
		}
		return blnFlag;
	}

	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyStreetAddressUsingAliasName
	// ''@Objective: This Function verifies Street Address on the basis of entered alias name on Address Search page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strAliasName
	// ''@Param Desc: strStreetAddress
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyStreetAddressUsingAliasName(strAliasName,strStreetAddress);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyStreetAddressUsingAliasName(String strAliasName,String strStreetAddress)
	{
		boolean blnFlag = false;
		
		try
		{      
			page("pgeProfilePage").element("txtStreetAddress").waitForElement(30,10);
			blnFlag = page("pgeProfilePage").element("txtStreetAddress").type(strAliasName);
			if(blnFlag)
			{
				waitForSync(2);
				String strXPath = page("pgeHomePage").element("elelstStreetAddress").GetXpath();
				int AddressListSize = driver.findElements(By.xpath(strXPath)).size();
				if (AddressListSize > 0) 
				{
					blnFlag = driver.findElement(By.xpath(strXPath+"["+AddressListSize+"]")).getText().contains(strStreetAddress);
					if(blnFlag)
					{
						try
						{
							driver.findElement(By.xpath(strXPath+"["+AddressListSize+"]")).click();
						}
						catch (Exception e)
						{
							((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(strXPath+"["+AddressListSize+"]")));

						}
						if (page("pgeProfilePage").element("txtZipCode").waitForElement(60,1))
						{
							String strZipCode=page("pgeProfilePage").element("txtZipCode").getAttributeValue("value");
							if(!strZipCode.equals(""))
							{
								dicTestData.put("strZipCode", strZipCode);
								blnFlag = page("pgeHomePage").element("btnSubmit").isVisible();
								if(!blnFlag)
									ErrDescription = "Failed to validate that 'Submit' button is enabled.";
							}
							else
							{
								blnFlag=false;
								ErrDescription = "Failed to fetch Zip Code from 'Zip Code' field.";
							}	
						}
						else
						{
							blnFlag=false;
							ErrDescription = "Failed to populate 'Zip Code' field.";
						}
					}
					else	
						ErrDescription = "Failed to verify auto-populated street address on the basis of alias name.";
				}
				else
				{
					blnFlag=false;
					ErrDescription = "Failed to auto-populate street address.";
				}
			}
			else
				ErrDescription = "Failed to enter street address's alias name in the field.";
		}

		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying street addresses.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: selectBundleAndVerifyPlanTypes
	// ''@Objective: This Function selects a bundle plan and verify plan types populated on 'Browse' page. 
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= selectBundleAndVerifyPlanTypes("pgeBrowse","btnIndividualPlans");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean selectBundleAndVerifyPlanTypes(String page,String element)
	{
		boolean blnFlag = false;
		
		try
		{
			page(page).element(element).waitForElement(30,10);
			blnFlag = page(page).element(element).click();
			if(blnFlag)
			{
				waitForSync(3);
				blnFlag=verifyElementExist("pgeBrowse","chkPhonePlan");
				if(blnFlag)
				{
					blnFlag=verifyElementExist("pgeBrowse","chkSecurityPlan");
					if(blnFlag)
					{
						blnFlag=verifyElementExist("pgeBrowse","chkTVPlan");
						if(blnFlag)
						{
							blnFlag=verifyElementExist("pgeBrowse","chkInternetPlan");
							if(!blnFlag)
								ErrDescription="Failed to display check-box for plan type 'Internet'.";
						}
						else
							ErrDescription="Failed to display check-box for plan type 'TV'.";
					}
					else
						ErrDescription="Failed to display check-box for plan type 'Security'.";
				}
				else
					ErrDescription="Failed to display check-box for plan type 'Phone'.";
			}
			else
				ErrDescription="Failed to select Bundle/Individual plan type.";
		}
		
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying bundle/individual plans.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifymultipleProductsUnderCart
	// ''@Objective: This Function verifies multiple products added under cart on 'Customize Order' page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strPlanType
	// ''@Param Desc: Plan Type added under cart (Internet/TV/Phone/Security)
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifymultipleProductsUnderCart("TV");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifymultipleProductsUnderCart(String strPlanType)
	{

		boolean blnFlag = false;
		String strPlanValue = "";
		
		try
		{
			page("pgeProfilePage").element("btnCartIcon").waitForElement(30,10);
			blnFlag = page("pgeProfilePage").element("btnCartIcon").click();
			if(blnFlag)
			{
				waitForSync(3);
				if(strPlanType.equals("Internet"))
				{
					strPlanValue = page("pgeBrowse").element("eleInternetCartDetails").getText();
					if(strPlanValue.contains("Internet"))
					{
						blnFlag=true;
					}
				}
				if(strPlanType.equals("TV"))
				{
					strPlanValue = page("pgeBrowse").element("eleTVCartDetails").getText();
					if(strPlanValue.contains("Video"))
					{
						blnFlag=true;
					}
				}
				
				if(strPlanType.equals("Phone"))
				{
					strPlanValue = page("pgeBrowse").element("elePhoneCartDetails").getText();
					if(strPlanValue.contains("Landline"))
					{
						blnFlag=true;
					}
				}
				
				if(strPlanType.equals("Security"))
				{
					strPlanValue = page("pgeProfilePage").element("btnCartIcon").getText();
					if(strPlanValue.contains("Internet"))
					{
						blnFlag=true;
					}
				}
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying cart items.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyPrimaryAccountHolderFields
	// ''@Objective: This Function fields under 'Primary Account Holder' section on 'Checkout' page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyPrimaryAccountHolderFields();
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyPrimaryAccountHolderFields()
	{
		boolean blnFlag = false;
		try
		{  
			page("pgeCheckout").element("txtFirstName").waitForElement(30,10);
			blnFlag=page("pgeCheckout").element("txtFirstName").exist();
			if(blnFlag)
			{
				blnFlag=page("pgeCheckout").element("txtLastName").exist();
				if(blnFlag)
				{
					blnFlag=page("pgeCheckout").element("txtPhoneNumber").exist();
					if(blnFlag)
					{
						blnFlag=page("pgeCheckout").element("txtDOB").exist();
						if(blnFlag)
						{
							blnFlag=page("pgeCheckout").element("txtSSN").exist();
							if(blnFlag)
							{
								blnFlag=page("pgeCheckout").element("txtEmailAddress").exist();
								if(!blnFlag)
									ErrDescription="Failed to verify 'Email Address' field.";
							}
							else
								ErrDescription="Failed to verify 'Social Security Number' field.";
						}
						else
							ErrDescription="Failed to verify 'Date of Birth' field.";
					}
					else
						ErrDescription="Failed to verify 'Phone Number' field.";
				}
				else
					ErrDescription="Failed to verify 'Last Name' field.";
			}
			else
				ErrDescription="Failed to verify 'First Name' field.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying fields.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyPrefilledBillingAddressFields
	// ''@Objective: This Function verifies pre-filled data under 'Billing Address' section on Checkout page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyPrefilledBillingAddressFields(strStreetNumber,strStreetName,strBillingCity,strZip);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyPrefilledBillingAddressFields(String strStreetNumber,String strStreetName,String strBillingCity,String strBillingZip)
	{
		boolean blnFlag = false;
		String strValue="";
		try
		{     
			page("pgeCheckout").element("txtStreetNumber").waitForElement(30,10);
			blnFlag=page("pgeCheckout").element("txtStreetNumber").exist();
			if(blnFlag)
			{
				strValue = page("pgeCheckout").element("txtStreetNumber").getAttributeValue("value");
				if(strValue.equals(strStreetNumber))
				{
					blnFlag=page("pgeCheckout").element("txtStreetName").exist();
					if(blnFlag)
					{
						strValue = page("pgeCheckout").element("txtStreetName").getAttributeValue("value");
						if(strValue.equals(strStreetName))
						{
							blnFlag=page("pgeCheckout").element("txtApartmentNum").exist();
							if(blnFlag)
							{
								blnFlag=page("pgeCheckout").element("txtPOBox").exist();
								if(blnFlag)
								{
									blnFlag=page("pgeCheckout").element("txtBillingCity").exist();
									if(blnFlag)
									{
										strValue = page("pgeCheckout").element("txtBillingCity").getAttributeValue("value");
										if(strValue.equals(strBillingCity))
										{
											blnFlag=page("pgeCheckout").element("drpBillingState").isSelected();
											if(blnFlag)
											{
												blnFlag=page("pgeCheckout").element("txtBillingZip").exist();
												if(blnFlag)
												{
													strValue = page("pgeCheckout").element("txtBillingZip").getAttributeValue("value");
													if(strValue.equals(strBillingZip))
													{
														blnFlag=true;
													}
													else
													{
														blnFlag=false;
														ErrDescription="Failed to verify pre-filled 'ZipCode' field.";
													}
												}
												else
													ErrDescription="Failed to display 'ZipCode' field.";	
											}
											else
												ErrDescription="Failed to verify pre-selected 'State' drop-down field.";
										}
										else
										{
											blnFlag=false;
											ErrDescription="Failed to verify pre-filled 'City' field.";
										}
									}
									else
										ErrDescription="Failed to display 'City' field.";
								}
								else
									ErrDescription="Failed to display 'P.O Box' field.";
							}
							else
								ErrDescription="Failed to display 'Apartment #' field.";
						}
						else
						{
							blnFlag=false;
							ErrDescription="Failed to verify pre-filled 'Street Name' field.";
						}
					}
					else
						ErrDescription="Failed to display 'Street Name' field.";	
				}
				else
				{
					blnFlag=false;
					ErrDescription="Failed to verify pre-filled 'Street Number' field.";
				}
			}
			else
				ErrDescription="Failed to display 'Street Number' field.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying pre-filled data.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: enterDataAndVerifyErrorMessage
	// ''@Objective: This Function enters specified data in a field and verifies error message on submitting the data.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= enterDataAndVerifyErrorMessage("pgeCheckout","txtAuthUserFirstName","a","","errAuthUserFirstName",strErrorMsg,"ValidData");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean enterDataAndVerifyErrorMessage(String pgePage, String eleElement, String strValidData,String strInvalidData,String eleErrorMessage,String strErrorMsg,String strDataType)
	{
		boolean blnFlag = false;
		try
		{   
			page(pgePage).element(eleElement).waitForElement(30,10);
			if(strDataType.equals("ValidData"))
			{
				blnFlag=page(pgePage).element(eleElement).type(strValidData);
				if(blnFlag)
				{
					blnFlag=page(pgePage).element(eleErrorMessage).waitForElement(60,1);
					if(blnFlag)
					{
							blnFlag = page(pgePage).element(eleErrorMessage).exist();
							if (!blnFlag)
							{
								ErrDescription = "Unable to verify Error Message '"+strErrorMsg+"'.";
							}
					}
					else
						ErrDescription = "Failed to display Error Message'"+strErrorMsg+"'.";	
				}
				else
					ErrDescription = "Failed to enter data in the text-field.";
			}
			else if(strDataType.equals("InvalidData"))
			{
				blnFlag=page(pgePage).element(eleElement).type(strInvalidData);
				if(blnFlag)
				{
					String strValue = page(pgePage).element(eleElement).getAttributeValue("value");
					if(strValue.equals(""))
					{
						blnFlag=page(pgePage).element(eleElement).type(strValidData);
						if(blnFlag)
						{
							strValue = page(pgePage).element(eleElement).getAttributeValue("value");
							if(!strValue.equals(""))
							{
								blnFlag=true;
							}
							else
							{
								blnFlag=false;
								ErrDescription = "Failed to verify allowed data in the field.";	
							}
						}
						else
							ErrDescription = "Failed to enter allowed characters in the field.";	
					}
					else
					{
						blnFlag=false;
						ErrDescription = "Failed to verify if invalid data is allowed in the field.";
					}		
				}
				else
					ErrDescription = "Failed to enter invalid characters in the field.";
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying error message.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyDateOfBirthFormat
	// ''@Objective: This Function verifies format of the specified 'Date Of Birth' value.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyDateOfBirthFormat(strDOB);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyDateOfBirthFormat(String strDOB) throws ParseException, java.text.ParseException
	{
		boolean blnFlag = false;
		String strValue="";
		try
		{
			page("pgeCheckout").element("txtDOB").waitForElement(30,10);
			blnFlag=page("pgeCheckout").element("txtDOB").type(strDOB);
			if(blnFlag)
			{
				strValue = page("pgeCheckout").element("txtDOB").getAttributeValue("value");
			     
				if(!strValue.equals(""))
				{
					 SimpleDateFormat format = new SimpleDateFormat("mm-dd-yyyy");
					 if(format.parse(strValue) != null)
					 {
						 blnFlag=true;
					 }
					 else
					 {
						 blnFlag=false;
						 ErrDescription = "Failed to verify format of 'Date Of Birth' field.";
					 } 
				}
				else
				{
					blnFlag=false;
					 ErrDescription = "Failed to fetch 'Date Of Birth' value.";		
				}				 
			}
			else
				ErrDescription = "Failed to enter value in 'Date Of Birth' field.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying date format.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyExpirationDateFormat
	// ''@Objective: This Function verifies format of the specified expiration date.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyExpirationDateFormat(strDate);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyExpirationDateFormat(String strDate) throws ParseException, java.text.ParseException
	{
		boolean blnFlag = false;
		String strValue="";
		try
		{
			page("pgeSetUpPayment").element("txtExpiryDate").waitForElement(30,10);
			blnFlag=page("pgeSetUpPayment").element("txtExpiryDate").type(strDate);
			if(blnFlag)
			{
				strValue = page("pgeSetUpPayment").element("txtExpiryDate").getAttributeValue("value");
			     
				if(!strValue.equals(""))
				{
					 SimpleDateFormat format = new SimpleDateFormat("mm/yy");
					 if(format.parse(strValue) != null)
					 {
						 blnFlag=true;
					 }
					 else
					 {
						 blnFlag=false;
						 ErrDescription = "Failed to verify format of 'Expiration Date' field.";
					 } 
				}
				else
				{
					blnFlag=false;
					 ErrDescription = "Failed to fetch 'Expiration Date' value.";		
				}				 
			}
			else
				ErrDescription = "Failed to enter value in 'Expiration Date' field.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying date format.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: EnterCardAndVerifyLogo
	// ''@Objective: This Function enters card number and verify populated card logo on 'Payment' page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= EnterCardAndVerifyLogo("pgeSetUpPayment","txtCardNumber","eleVISACardLogo",strVisaCardNum,strCardType1);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean EnterCardAndVerifyLogo(String pgePage, String eleElement,String eleNewElement, String strCardNumber,String strCardType)
	{
		boolean blnFlag = false;
		try
		{     
			page(pgePage).element(eleElement).waitForElement(30,10);
			blnFlag=page(pgePage).element(eleElement).type(strCardNumber);
			if(blnFlag)
			{
				waitForSync(2);
				blnFlag = page(pgePage).element(eleNewElement).exist();
				if (!blnFlag)
				{
					ErrDescription = "Unable to verify logo for '"+strCardType+"' card type.";
				}
			}
			else
				ErrDescription = "Failed to enter card number in the field.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying card logo.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyAddAuthorizedUserFields
	// ''@Objective: This Function verifies fields under 'Add Authorized User' section' section on 'Payment' page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyAddAuthorizedUserFields();
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyAddAuthorizedUserFields()
	{
		boolean blnFlag = false;
		try
		{     
			page("pgeCheckout").element("btnAddUser").waitForElement(30,10);
			blnFlag=page("pgeCheckout").element("btnAddUser").click();
			if(blnFlag)
			{
				waitForSync(1);
				blnFlag = page("pgeCheckout").element("lblAddAuthorizedUser").exist();
				if (blnFlag)
				{
					blnFlag = page("pgeCheckout").element("txtAuthUserFirstName").exist();
					if(blnFlag)
					{
						blnFlag = page("pgeCheckout").element("txtAuthUserLastName").exist();
						if(blnFlag)
						{
							blnFlag = page("pgeCheckout").element("drpPrimaryRelationship").exist();
							if(blnFlag)
							{
								blnFlag = page("pgeCheckout").element("btnDeleteUser").exist();
								if(!blnFlag)
								{
									ErrDescription = "Failed to display 'Delete' button.";
								}
							}
							else
								ErrDescription = "Failed to display 'Relationship To Primary' drop-down.";
						}
						else
							ErrDescription = "Failed to display 'Last Name' field under 'Add Authorized User' section.";
					}
					else
						ErrDescription = "Failed to display 'First Name' field under 'Add Authorized User' section.";
				}
				else
					ErrDescription = "Failed to display text 'Give authority to a family member or friend to make changes to your account.'.";
			}
			else
				ErrDescription = "Failed to click on 'Add User' button.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying Fields.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyRelationshipToPrimaryValues
	// ''@Objective: This Function verifies values under 'Relationship to Primary' drop-down on 'Payment' page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyRelationshipToPrimaryValues("pgeCheckout","drpPrimaryRelationship",strRelationValues);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyRelationshipToPrimaryValues(String pgePage,String eleElement,String strValues)
	{
		boolean blnFlag = false;
		
		try
		{   
			page(pgePage).element(eleElement).waitForElement(30,10);
			List<WebElement> lstValues =page(pgePage).element(eleElement).getAllDropdownOptions();
			String[] lstDrpValues = strValues.split(",");
			
			if(!(lstValues.isEmpty() && (lstDrpValues.length==0)))
			{
				for(int i=0;i<lstDrpValues.length;i++)
				{
					blnFlag=lstValues.get(i).getText().equals(lstDrpValues[i]);
					if(!blnFlag)
					{
						blnFlag=lstValues.get(i+1).getText().equals(lstDrpValues[i]);
						if(!blnFlag)
						{
							blnFlag=lstValues.get(i+2).getText().equals(lstDrpValues[i]);
							if(!blnFlag)
								ErrDescription=ErrDescription + "Failed to display value '"+lstDrpValues[i]+"' in drop-down.";
						}
					}
				}
			}
			else
			{
				blnFlag=false;
				ErrDescription = "Failed to display any values in the drop-down.";
			}
			
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying dropdown values.";
		}
		return blnFlag;
	}

	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: AddAuthorizedUserAsAccountHolder
	// ''@Objective: This Function enters all the required details under 'Add Authorized User' section and clicks on 'Add User' button.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= AddAuthorizedUserAsAccountHolder(strFirstName,strLastName,strPrimaryRelation[0]);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean AddAuthorizedUserAsAccountHolder(String strFirstName,String strLastName,String strRelationship)
	{
		boolean blnFlag = false;
		String strUserName = "";
		
		try
		{  
			blnFlag=page("pgeCheckout").element("txtAuthUserFirstName").type(strFirstName);
			if(blnFlag)
			{
				blnFlag=page("pgeCheckout").element("txtAuthUserLastName").type(strLastName);
				if(blnFlag)
				{
					blnFlag=page("pgeCheckout").element("drpPrimaryRelationship").select(strRelationship,"byvalue",false);
					if(driver.toString().contains("internet explorer"))
					{
						blnFlag=true;
					}
					if(blnFlag)
					{
						waitForSync(1);
						blnFlag=page("pgeCheckout").element("btnAddUserIcon").click();
						if(blnFlag)
						{
							waitForSync(1);
							blnFlag =!(page("pgeCheckout").element("txtAuthUserFirstName").exist() && page("pgeCheckout").element("txtAuthUserLastName").exist());
							if(blnFlag)
							{
								strUserName=page("pgeCheckout").element("eleAddedAuthUserInfo").getText();
								if(!strUserName.equals(""))
								{
									if(strUserName.contains(strFirstName) && strUserName.contains(strLastName))
									{
										blnFlag=true;
									}
									else
									{
										blnFlag=false;
										ErrDescription = "Failed to display First Name,Last Name of the added user.";
									}
								}
								else
								{
									blnFlag=false;
									ErrDescription = "Failed to fetch displayed First Name,Last Name of the added user.";
								}
							}
							else
								ErrDescription = "Failed to collapse 'Add Authorized User' section after adding the user.";
						}
						else
							ErrDescription = "Failed to click on '+' icon to add the user.";
					}
					else
						ErrDescription = "Failed to select value '"+strRelationship+"' from 'Relationship To Primary' drop-down.";
				}
				else
					ErrDescription = "Failed to enter 'Last Name' of the authorized user.";
			}
			else
				ErrDescription = "Failed to enter 'First Name' of the authorized user.";
			
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription = "Error occured while adding a authorized user.";
		}
		return blnFlag;
	}

	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyAdddedAuthUserDetails
	// ''@Objective: This Function verifies the added details of the authorized user.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyAdddedAuthUserDetails(strFirstName,strLastName,strPrimaryRelation[0]);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyAdddedAuthUserDetails(String strFirstName,String strLastName,String strPrimaryDrpValue)
	{
		boolean blnFlag = false;
		
		try
		{     
			blnFlag=page("pgeCheckout").element("btnAddUserIcon").click();
			if(blnFlag)
			{
				waitForSync(2);
				blnFlag=getValueFromField("pgeCheckout","txtAuthUserFirstName").equals(strFirstName) && getValueFromField("pgeCheckout","txtAuthUserLastName").equals(strLastName);
				if(blnFlag)
				{
					blnFlag=page("pgeCheckout").element("drpPrimaryRelationship").getSelectedOptions().get(0).getText().equals(strPrimaryDrpValue);
					if(!blnFlag)
					{
						ErrDescription = "Failed to verify previously selected value '"+strPrimaryDrpValue+"' from 'Relationship To Primary' drop-down.";
					}
				}
				else
					ErrDescription = "Failed to verify previously selected User's First Name '"+strFirstName+"' and Last Name '"+strLastName+"' values.";
			}
			else
				ErrDescription = "Failed to click on '+' icon to verify added user details.";
			
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription = "Error occured while verifying authorized user details.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: getValueFromField
	// ''@Objective: This Function fetches the populated value from a field.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= getValueFromField("pgeCheckout","txtAuthUserFirstName");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public String getValueFromField(String pgePage,String eleElement)
	{
		String strValue="";
		
		try
		{   
			String strXPath = page(pgePage).element(eleElement).GetXpath();
			if(!strXPath.equals(""))
			{
				strValue=driver.findElement(By.xpath(strXPath)).getAttribute("value");
				if(strValue.equals(""))
				{
					strValue="";
					ErrDescription = "Failed to fetch value from the text-field.";	
				}
			}
			else
			{
				strValue="";
				ErrDescription = "Failed to fetch XPath of the text-field object.";
			}	
		}
		catch (Exception e)
		{
			logger.error(e.getMessage());
			ErrDescription = "Error occured while fetching the values/text.";
		}
		return strValue;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyMonthlyChargesForSelectedPlan
	// ''@Objective: This Function verifies monthly charges and plan details for selected plan on 'Customize Order' page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: NumOfPlans
	// ''@Param Desc: Number of plans to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyMonthlyChargesForSelectedPlan(3);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyMonthlyChargesForSelectedPlan(int intNumOfPlans) 
	{
		boolean blnFlag = false;
		String strXpath = "";
		String strPlanName= "";
		String strPlanCharge="";
		String strTotalCharge="";
			
		try
		{
			strXpath = page("pgeCustomizeOrder").element("eleSelectedPlans").GetXpath();
			int lstPlanSize = driver.findElements(By.xpath(strXpath)).size();
			
			if(lstPlanSize==intNumOfPlans)
			{
				strPlanName=page("pgeCustomizeOrder").element("eleSelectedPlanName").getText();
				if(!strPlanName.equals(""))
				{
					strPlanCharge=page("pgeCustomizeOrder").element("eleSelectedPlanCharge").getText();
					if(!strPlanCharge.equals(""))
					{
						strTotalCharge=page("pgeCustomizeOrder").element("eleTotalMonthlyCharges").getText();
						if(strTotalCharge.contains(strPlanCharge))
						{
							blnFlag=true;
						}
						else
						{
							blnFlag=false;
							ErrDescription="Selected Plan '"+strPlanName+"' charges are not matching with 'Total Monthly Charges'.";
						}	
					}
					else
					{
						blnFlag=false;
						ErrDescription="Failed to display selected Plan '"+strPlanName+"' charges.";
					}
				}
				else
				{
					blnFlag=false;
					ErrDescription="Failed to display selected plan under 'Monthly Charges' section.";
				}
			}
			else
			{
				blnFlag=false;
				ErrDescription="More than one plan is showing as selected under 'Monthly Charges' section.";		
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription = "Error occured while verifying monthly charges";
		}
		return blnFlag;	
	}
			
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyMonthlyChargesForSelectedPlan
	// ''@Objective: This Function verifies monthly charges for a selected plan on Customize Order page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyMonthlyChargesForSelectedPlan();
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyMonthlyChargesForSelectedPlan() 
	{
		boolean blnFlag = false;
		String strXpath = "";
		
		try
		{
			strXpath = page("pgeCustomizeOrder").element("eleSelectedPlans").GetXpath();
			int lstPlanSize = driver.findElements(By.xpath(strXpath)).size();
			
			if(lstPlanSize==1)
			{
				blnFlag=true;
			}
			else if(lstPlanSize==0)
			{
				blnFlag=false;
				ErrDescription="No plans are appearing under 'Total Monthly Charges' section.";
			}
			else if(lstPlanSize>0)
			{
				blnFlag=false;
				ErrDescription="More than one selected plans are appearing under 'Total Monthly Charges' section.";
			}	
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription = "Error occured while verifying monthly charges";
		}
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifySelectedPlansUnderMonthlyCharges
	// ''@Objective: This Function verifies selected plans and monthky charges on the basis of number of plans and plan names.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyMonthlyChargesForSelectedPlan(2,"TV,Internet");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifySelectedPlansUnderMonthlyCharges(int intNumOfPlans,String strValues) 
	{
		boolean blnFlag = false;
		try
		{
			String strXpath = "";
			String[] lstValues = strValues.split(",");
				
			strXpath = page("pgeCustomizeOrder").element("eleSelectedPlans").GetXpath();
			int lstPlanSize = driver.findElements(By.xpath(strXpath)).size();
			
			if(lstPlanSize==intNumOfPlans)
			{
				List<WebElement> lstPlans = driver.findElements(By.xpath(strXpath));
				if(!lstPlans.isEmpty())
				{
					for(int i=0;i<lstPlanSize;i++)
					{
						blnFlag=lstPlans.get(i).getText().contains(lstValues[i]);
						if(!blnFlag)
						{
							ErrDescription="Failed to display selected Plan '"+lstPlans.get(i).getText()+"' under 'Monthly Charges' section.";
						}
					}
				}
				else
				{
					blnFlag=false;
					ErrDescription="Failed to fetch selected plans '"+strValues+"' from 'Monthly Charges' section.";
				}
			}
			else
			{
				blnFlag=false;
				ErrDescription="Failed to display selected plans under 'Monthly Charges' section.";		
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription = "Error occured while verifying monthly charges";
		}
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyTextContains
	// ''@Objective: This Function verifies if a field contains specified text.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyTextContains("pgeBrowse","eleSelectedPlanDetails","Internet");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyTextContains(String pgePage, String eleElement, String strText)
	{
		boolean blnFlag = false;
		String strValue="";
		try
		{      
				blnFlag = page(pgePage).element(eleElement).exist();
				if(blnFlag)
				{
					strValue = page(pgePage).element(eleElement).getText();
					if(strValue.contains(strText))
					{
						blnFlag=true;
					}
					else
					{
						blnFlag=false;
						ErrDescription = "Unable to verify field '"+strText+"' on the navigated page.";
					}
				}
				else
					ErrDescription = "Failed to display field from where the text has to be fetched and validated.";
		}

		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying text.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: clickSubmitAndVerifyPopup
	// ''@Objective: This Function clicks on 'Submit' button and verifies existing account pop-up on 'Address Search' page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= clickSubmitAndVerifyPopup(true);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[30th Nov,2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean clickSubmitAndVerifyPopup(boolean blnPopupDisplay)
	{
		boolean blnFlag = false;
		
		try
		{
			blnFlag= page("pgeHomePage").element("btnSubmit").click();
			if (blnFlag) 
			{
				waitForSync(4);
				if (blnPopupDisplay) 
				{
					page("pgeProfilePage").element("eleAccountExistPopUp").waitForElement(3, 1);
					blnFlag = page("pgeHomePage").element("eleAccountExistPopUp").exist();
					if(!blnFlag)
							ErrDescription = "Failed to display 'A Comporium account already exists' pop-up.";
				}
				else 
				{
					page("pgeProfilePage").element("btnChangeAddress").waitForElement(5, 1);
					blnFlag=page("pgeProfilePage").element("btnChangeAddress").exist();
					if(!blnFlag)
					{
						ErrDescription = "Failed to navigate to 'Browse' page.";
					}
				}
			}
			else
				ErrDescription = "Failed to click on 'Submit' button.";
		}
		
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while clicking and verifying action.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyMaxLengthLimit
	// ''@Objective: This Function verifies if the maximum length of a field matches with the specified maximum length.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyMaxLengthLimit("pgeCheckout","txtAuthUserFirstName",strLongFirstName,strMaxFirstLength);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[30th Nov,2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyMaxLengthLimit(String pgePage,String eleElement,String strInputVal,String strMaxLength)
	{

		boolean blnFlag = false;
		String strEnteredValue="";
		int strLength=0;
		int intMaxLength=Integer.parseInt(strMaxLength);
		
		try
		{
			blnFlag= page(pgePage).element(eleElement).type(strInputVal);
			waitForSync(2);
			if(blnFlag)
			{
				strEnteredValue=page(pgePage).element(eleElement).getAttributeValue("value");
				if(!strEnteredValue.isEmpty())
				{
					strLength = strEnteredValue.length();
					if(strLength==intMaxLength)
					{
						blnFlag=true;
					}
					else
					{
						blnFlag=false;
						ErrDescription = "Failed to verify maximum length limit of '"+strMaxLength+"' characters.";
					}
				}
				else
				{
					blnFlag=false;
					ErrDescription = "Failed to fetch entered characters from the field.";
				}
			}
			else
				ErrDescription = "Failed to enter characters in the text-field.";
		}
		
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying maximum length limit.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: selectCheckBox
	// ''@Objective: This Function selects a checkbox on the basis of attribute specified as input parameter.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= selectCheckBox("pgeBrowse","chkViewBundles","eleImLookingFor","id",true);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[30th Nov,2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean selectCheckBox(String pgePage,String eleElement,String eleNewElement,String strAttribute,boolean blnScrollPage)
	{
		boolean blnFlag = false;
		
		try
		{
			Actions action=new Actions(driver);
			if(blnScrollPage)
			{
				action.sendKeys(Keys.PAGE_DOWN).build().perform();
				waitForSync(3);
				blnFlag = verifyElementExist(pgePage,eleElement);
				if(!blnFlag)
					action.sendKeys(Keys.PAGE_DOWN).build().perform();
					waitForSync(3);
			}
			if(driver.toString().contains("internet explorer"))
			{
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",driver.findElement(By.xpath(page(pgePage).element(eleElement).GetXpath())));
				blnFlag=true;
			}
			else
			{
				String strAttributeVal = page(pgePage).element(eleElement).getAttributeValue(strAttribute);
				if(!strAttributeVal.isEmpty())
				{
					if(strAttribute.equals("id"))
					{
						try
						{
							action.moveToElement(driver.findElement(By.id(strAttributeVal))).click().build().perform();
							waitForSync(5);
						}
						catch(MoveTargetOutOfBoundsException e)
						{
							action.sendKeys(Keys.PAGE_DOWN).build().perform();
							try
							{
								waitForSync(3);
								action.moveToElement(driver.findElement(By.id(strAttributeVal))).click().build().perform();
								waitForSync(5);
							}
							catch(MoveTargetOutOfBoundsException ex)
							{
								action.sendKeys(Keys.PAGE_DOWN).build().perform();
								waitForSync(2);
								action.moveToElement(driver.findElement(By.id(strAttributeVal))).click().build().perform();
								waitForSync(5);
							}
						}
					}
				}
				else
				{
					blnFlag=false;
					ErrDescription = "Failed to fetch attributes of the checkbox field.";
				}
			}
		waitForSync(3);
		blnFlag= page(pgePage).element(eleNewElement).exist();
		if(!blnFlag)
		{
			ErrDescription = "Failed to verify checkbox selection.";
		}
		if(blnScrollPage)
		{
			action.sendKeys(Keys.PAGE_UP).build().perform();
			waitForSync(2);
		}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while selecting the checkbox.";
		}
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: loginExistingCustomer
	// ''@Objective: This Function logs in into an application as an existing customer with specified username,password.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= loginExistingCustomer("41237892","comporium#1");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean loginExistingCustomer(String strUsername,String strPassword)
	{
		boolean blnFlag = false;
		try
		{    
			waitForSync(1);
			blnFlag=page("pgeMyAccountLogin").element("txtUserName").type(strUsername);
			if(blnFlag)
			{
				waitForSync(1);
				blnFlag=page("pgeMyAccountLogin").element("txtPassword").type(strPassword);
				if(blnFlag)
				{
					waitForSync(1);
					blnFlag=page("pgeMyAccountLogin").element("btnLogin").click();
					if(blnFlag)
					{
						waitForSync(6);
						blnFlag=page("pgeAccountOverview").element("lnkAddUpgradeService").exist();
						if(!blnFlag)
						{
							blnFlag=page("pgeCustomerSummary").element("eleAccountSummary").exist();
							if(!blnFlag)
								ErrDescription = "Failed to navigate to 'My Account Overview/Summary' page.";
						}
					}
					else
						ErrDescription="Failed to click on 'Login' button.";
				}
				else
					ErrDescription = "Failed to enter Account's password in 'Password' field.";
			}
			else
				ErrDescription = "Failed to enter Account's Username in 'User Name' field.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while logging into as an existing customer.";
		}
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyExistsAndClick
	// ''@Objective: This Function verifies if field exists and then clicks on it.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyExistsAndClick("pgeAccountOverview","lnkAddUpgradeService","pgeAccountOverview","btnShopUpgrades","pgeCustomizeOrder","lnkReturnToAccSummary");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyExistsAndClick(String pgePage,String eleElement,String pgePage1,String eleElement1,String pgeNewPage,String eleNewElement)
	{
		boolean blnFlag = false;
		try
		{    
			blnFlag=page(pgePage).element(eleElement).exist();
			if(blnFlag)
			{
				blnFlag=page(pgePage).element(eleElement).click();
				if(blnFlag)
				{
					waitForSync(4);
					blnFlag=page(pgeNewPage).element(eleNewElement).exist();
					if(!blnFlag)
						ErrDescription = "Failed to navigate to the next page.";
				}
			}
			else
			{
				blnFlag=page(pgePage1).element(eleElement1).click();
				if(blnFlag)
				{
					waitForSync(4);
					blnFlag=page(pgeNewPage).element(eleNewElement).exist();
					if(!blnFlag)
						ErrDescription = "Failed to navigate to the next page.";
				}
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying existance and clicking.";
		}
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: navigateToURL
	// ''@Objective: This Function accesses specified URL and verifies navigated page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= navigateToURL(strURL,"pgeMyAccountLogin","txtUserName");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean navigateToURL(String strURL,String pgePage,String eleElement)
	{
		boolean blnFlag = false;
		try
		{   
			if(driver.toString().contains("internet explorer"))
			{
				launchIEinPrivateMode();
			}
			driver.get(strURL);
			waitForSync(4);
			blnFlag=page(pgePage).element(eleElement).exist();
			if(!blnFlag)
				ErrDescription = "Failed to navigate to the URL.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while navigating to URL.";
		}
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyCheckboxSelection
	// ''@Objective: This Function clicks verifies checkbox selection on the basis of specified attribute 'id' of the checkbox field.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: eleElement
	// ''@Param Name: pgeNewPage
	// ''@Param Desc: eleNewElement
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyCheckboxSelection("pgeMyAccountLogin","btnLogin","pgeAccountOverview","eleAccountSummary");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyCheckboxSelection(String pgePage, String eleElement,boolean blnSelect,String strID)
	{
		boolean blnFlag = false;
		try
		{      
			if(blnSelect)
			{
				blnFlag=driver.findElement(By.id(strID)).isSelected();
				if(!blnFlag)
					ErrDescription = "Unable to verify if the specified check-box is selected.";
			}
			
			if(!blnSelect)
			{
				blnFlag=!driver.findElement(By.id(strID)).isSelected();
				if(!blnFlag)
					ErrDescription = "Unable to verify if the specified check-box is not selected.";
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying check-box selection.";
		}
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: customClick
	// ''@Objective: This Function clicks on the specified object and verifies navigated page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: eleElement
	// ''@Param Name: pgeNewPage
	// ''@Param Desc: eleNewElement
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= customClick("pgeBrowse","lnkViewDetails","pgeBrowse","lnkHideDetails");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean customClick(String strXpath,String pgePage,String eleElement)
	{
		boolean blnFlag = false;

		try
		{     
			driver.findElement(By.xpath(strXpath)).click();
			waitForSync(4);
			blnFlag = page(pgePage).element(eleElement).exist();
			if (!blnFlag)
				ErrDescription = "Unable to verify navigated page.";
		}

		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while clicking and verifying action.";
		}
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: fillBillAddressForExstCustomers
	// ''@Objective: This Function enters billing address details on Checkout page for existing customers.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: firstName
	// ''@Param Desc: lastNameL
	// ''@Param Name: phoneNumber
	// ''@Param Desc: dob
	// ''@Param Desc: ssn
	// ''@Param Desc: email
	// ''@Param Desc: isCardDetails
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= fillBillAddressForExstCustomers(strFirstName, strLastName, strDOB, strPhoneNumber, strSSN, strEmail,true);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean fillBillAddressForExstCustomers(String strFirstName, String strLastName, String strPhoneNumber, String strDOB,String strEmail)
	{
		boolean blnFlag = false;
		String strValue ="";
		try
		{
			blnFlag = page("pgeCheckout").element("txtFullName").type(strFirstName+" "+strLastName);
			if(blnFlag)
			{
				strValue = page("pgeCheckout").element("txtFullName").getAttributeValue("value");
				if(!strValue.equals(strFirstName+" "+strLastName) || strValue.equals(null))
				{
					blnFlag = page("pgeCheckout").element("txtPhoneNumber").type(strPhoneNumber);
					if(blnFlag)
					{
						strValue = page("pgeCheckout").element("txtPhoneNumber").getAttributeValue("value");
						if(!strValue.equals(strPhoneNumber) || strValue.equals(null))
						{
							blnFlag = page("pgeCheckout").element("txtDOB").type(strDOB);
							if(blnFlag)
							{
								strValue = page("pgeCheckout").element("txtDOB").getAttributeValue("value");
								if(!strValue.equals(strDOB) || strValue.equals(null))
								{
									blnFlag = page("pgeCheckout").element("txtSSN").type("342324343");
									if(blnFlag)
									{
										strValue = page("pgeCheckout").element("txtSSN").getAttributeValue("value");
										if(!strValue.equals("123") || strValue.equals(null))
										{
											blnFlag = page("pgeCheckout").element("txtEmailAddress").type(strEmail);
											if(blnFlag)
											{
												strValue = page("pgeCheckout").element("txtEmailAddress").getAttributeValue("value");
												if(strValue.equals(strEmail) || strValue.equals(null))
												{
													blnFlag=true;
												}
											}
											else
												ErrDescription = "Failed to enter Email Address.";
										}
									}
									else
										ErrDescription = "Failed to enter Social Security Number.";
								}
							}
							else
								ErrDescription = "Failed to enter DOB.";
						}
					}
					else
						ErrDescription = "Failed to enter Phone Number.";
				}
			}
			else
				ErrDescription = "Failed to enter First Name and Last Name in 'Full Name' field.";				
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while entering Billing Address details.";
		}														
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: customSelectFromList
	// ''@Objective: This Function selects specified value from a list.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: eleNewPage
	// ''@Param Desc: eleNewElement
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= ccustomSelectFromList(strObj1Xpath,strSelectBy,strValue);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean customSelectFromList(String strXpath,String strSelectBy,String strValue)
	{
		boolean blnFlag=false;
		
		try
		{
			Select dropdown = new Select(driver.findElement(By.xpath(strXpath)));
			if(strSelectBy.contains("ByValue"))
			{
				dropdown.selectByValue(strValue);
			}
			
			waitForSync(2);
			
			String strSelectedVal = dropdown.getFirstSelectedOption().getText();
			if(strSelectedVal.equals(strValue))
			{
				blnFlag=true;
			}
			else
			{
				blnFlag=false;
				ErrDescription="Failed to select the specified option.";
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while selecting a value from the list.";
		}
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyEmptyCart
	// ''@Objective: This Function verifies Cart is empty.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strXpath
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyEmptyCart(objDeleteCart);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyEmptyCart(String strXpath)
	{
		boolean blnFlag = false;
		Actions action=new Actions(driver);
		try
		{      
			blnFlag = page("pgeBrowse").element("btnDeleteCart").click();
			if(blnFlag)
			{
				waitForSync(3);
				blnFlag = page("pgeBrowse").element("btnCartIcon").click();
				if(blnFlag)
				{
					WebElement element = driver.findElement(By.xpath(page("pgeBrowse").element("eleCartEmptyMsg").GetXpath()));
					action.moveToElement(element).build().perform();
					waitForSync(3);
					blnFlag = page("pgeBrowse").element("eleCartEmptyMsg").exist();
					if(!blnFlag)
						ErrDescription = "Unable to verify 'Your Cart is Empty' message.";
				}
				else
					ErrDescription = "Failed to click on 'Cart' icon.";
			}
			else
				ErrDescription = "Failed to click on 'x' button to empty the cart.";
		}

		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying empty cart.";
		}
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyCartItems
	// ''@Objective: This Function verifies items added in the cart.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyCartItems(strCartItems,strPlanType1,1);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyCartItems(String strXpath,String strPlanType, int intNumOfPlans)
	{
		boolean blnFlag = false;
		try
		{   
			blnFlag=page("pgeBrowse").element("btnCartIcon").click();
			if(blnFlag)
			{
				waitForSync(1);
				if(blnFlag)
				{
					int intCartSize = driver.findElements(By.xpath(strXpath)).size();
					if(intCartSize==intNumOfPlans)
					{
						blnFlag=true;
					}
					else
					{
						blnFlag=false;
						ErrDescription = "Failed to display added plan '"+strPlanType+"' under Cart.";
					}
				}
				else
				{
					blnFlag=false;
					ErrDescription = "Failed to identify added cart-item.";
				}
			}
			else
				ErrDescription = "Failed to click on 'Cart' icon.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying empty cart.";
		}
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: clickHoverAndVerify
	// ''@Objective: This Function clicks on the specified object, hovers on the populated field and verifies it.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: eleElement
	// ''@Param Name: pgeNewPage
	// ''@Param Desc: eleNewElement
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= clickHoverAndVerify("pgeMyAccountLogin","btnLogin","pgeAccountOverview","eleAccountSummary");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean clickHoverAndVerify(String pgePage, String eleElement,String pgeNewPage, String eleNewElement)
	{
		boolean blnFlag = false;
		Actions action=new Actions(driver);
		
		try
		{      
			blnFlag=page(pgePage).element(eleElement).click();
			if(blnFlag)
			{
				if(driver.toString().contains("FirefoxDriver"))
				{	
					blnFlag=verifyElementExist(pgeNewPage,eleNewElement);
				}
				else
				{
					WebElement element = driver.findElement(By.xpath(page(pgeNewPage).element(eleNewElement).GetXpath()));
					action.moveToElement(element).build().perform();
					blnFlag = page(pgeNewPage).element(eleNewElement).exist();
					if(!blnFlag)
					{
						blnFlag=page(pgePage).element(eleElement).click();
						if(blnFlag)
						{
							blnFlag=verifyElementExist(pgeNewPage,eleNewElement);
						}
					}
					waitForSync(2);
				}
				if (!blnFlag)
					ErrDescription = "Unable to verify navigated page.";		
			} 
			else
				ErrDescription = "Unable to perform click action.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while clicking and verifying action.";
		}
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: navigateToThankyouPageWithUncheckCredit
	// ''@Objective: This Function navigates to 'Thank You' page without checking the 'Credit' checkbox.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= navigateToThankyouPageWithUncheckCredit();
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean navigateToThankyouPageWithUncheckCredit()
	{
		boolean blnFlag = false;
		try
		{
			page("pgeThankYou").element("eleThankYouRequest").waitForElement(30);
			blnFlag=page("pgeThankYou").element("eleThankYouRequest").exist();
			if(blnFlag)
			{
				blnFlag=page("pgeThankYou").element("eleContactForRequest").exist();
				if(!blnFlag)
					ErrDescription = "Failed to display 'We will contact you within 24 business hours to complete your request.' message.";
			}		
			else
				ErrDescription = "Failed to display 'Thank You! We have received your request!' message.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while navigating to 'Thank you' page.";
		}
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifySelection
	// ''@Objective: This Function verifies selection/de-selection of the specified field.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: CardNumber
	// ''@Param Desc: ExpiryDate
	// ''@Param Name: CardHolderName
	// ''@Param Desc: CVV
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifySelection("pgeServiceSchedule","rdoPreferredTimeOfDay",false);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifySelection(String strPage, String strElement,boolean blnSelect)
	{
		boolean blnFlag = false;
		String strXpath = "";
		try
		{
			strXpath = page(strPage).element(strElement).GetXpath();
			if(blnSelect)
			{
				if(!strXpath.isEmpty())
				{
					blnFlag=driver.findElement(By.xpath(strXpath)).isSelected();
					if(!blnFlag)
						ErrDescription="Failed to verify if element is selected.";
				}
				else
					ErrDescription="Failed to display element on the page.";
			}
			else
			{
				if(!strXpath.isEmpty())
				{
					blnFlag=!driver.findElement(By.xpath(strXpath)).isSelected();
					if(!blnFlag)
						ErrDescription="Failed to verify if element is not selected.";
				}
				else
					ErrDescription="Failed to display element on the page.";
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying element's selection.";
		}	
	return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyEnabled
	// ''@Objective: This Function verifies if the field is enabled/disabled.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: CardNumber
	// ''@Param Desc: ExpiryDate
	// ''@Param Name: CardHolderName
	// ''@Param Desc: CVV
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyEnabled("pgeConfirmOrder","btnContinue",false);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyEnabled(String strPage, String strElement,boolean blnEnabled)
	{
		boolean blnFlag = false;
		String strXpath = "";
		try
		{
			strXpath = page(strPage).element(strElement).GetXpath();
			if(blnEnabled)
			{
				if(!strXpath.isEmpty())
				{
					blnFlag=driver.findElement(By.xpath(strXpath)).isEnabled();
					if(!blnFlag)
						ErrDescription="Failed to verify if element is enabled.";
				}
				else
					ErrDescription="Failed to display element on the page.";
			}
			else
			{
				if(!strXpath.isEmpty())
				{
					blnFlag=!driver.findElement(By.xpath(strXpath)).isEnabled();
					if(!blnFlag)
						ErrDescription="Failed to verify if element is disabled.";
				}
				else
					ErrDescription="Failed to display element on the page.";
			}
			
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying if element is enabled/disabled.";
		}	
	return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyTextMatches
	// ''@Objective: This Function verifies if the text fetched from two different fields, matches or not.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= vverifyTextMatches("pgeConfirmOrder","eleTotalDueAmount","pgeConfirmOrder","eleTotalDueToday$");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyTextMatches(String pgePage, String eleElement,String pgePage1, String eleElement1)
	{
		boolean blnFlag = false;
		String strValue1="";
		String strValue2="";
		try
		{      
				strValue1 = page(pgePage).element(eleElement).getText();
				if(!strValue1.isEmpty())
				{
					strValue2 = page(pgePage1).element(eleElement1).getText();
					if(!strValue2.isEmpty())
					{
						if(strValue1.contains(strValue2))
						{
							blnFlag=true;
						}
						else
							ErrDescription = "Failed to verify if the fetched values matches.";
					}
					else
						ErrDescription = "Unable to fetch the text from the second field.";
				}
				else
					ErrDescription = "Unable to fetch the text from the first field.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying text matches.";
		}
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyMonthlyChargesOnPlanUpgrade
	// ''@Objective: This Function verifies updated plan name,updated plan charges on a plan upgrade.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyMonthlyChargesOnPlanUpgrade("pgeCustomizeOrder","chkUpgradeEliteAddOn","eleSelectedPlanName",strPlanName,false,"clickCheckBox");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyMonthlyChargesOnPlanUpgrade(String pgePage,String eleElement,String eleUpdatedPlan,String strUpdatedPlanName,boolean blnScroll,String strAction) 
	{
		boolean blnFlag = false;
		String strPlanName= "";
		String strOneTimeCharge = "";
		String strUpdatedOneTimeChrg="";
		String strPlanCharge="";
		String strUpdatedPlanChrg="";
		Actions action=new Actions(driver);
		
		try
		{
			strPlanName=page("pgeCustomizeOrder").element("eleSelectedPlanName").getText();
			if(strPlanName.equals(""))
			{
				blnFlag=clickAndVerify("pgeCustomizeOrder","btnExpandCharges","pgeCustomizeOrder","eleSelectedPlanName");
				strPlanName=page("pgeCustomizeOrder").element("eleSelectedPlanName").getText();
			}
			if(!strPlanName.equals(""))
			{
				strPlanCharge=page("pgeCustomizeOrder").element("eleTotalMonthlyCharges").getText().replace("$","");
				waitForSync(1);
				if(!strPlanCharge.equals(""))
				{
					strOneTimeCharge=page("pgeCustomizeOrder").element("eleOneTimePlanCharges").getText().replace("$","");
					waitForSync(1);
					if(!strOneTimeCharge.equals(""))
					{
						if(strAction.equals("selectFromList"))
						{
							action.sendKeys(Keys.PAGE_DOWN).build().perform();
							waitForSync(2);
							Select oSelect = new Select(driver.findElement(By.xpath(page(pgePage).element(eleElement).GetXpath())));
							oSelect.selectByValue("1");
							waitForSync(1);
							blnFlag=true;
						}
						else if(strAction.equals("clickCheckBox"))
						{
							if(driver.toString().contains("internet explorer"))
							{
								blnFlag=clickAndVerify(pgePage,eleElement,pgePage,"eleTaxes&Fees");
							}
							else
							{
								blnFlag=selectCheckBox(pgePage,eleElement,"eleTaxes&Fees","id",blnScroll);
							}
						}
						waitForSync(3);
						if(blnFlag)
						{
							strPlanName=page("pgeCustomizeOrder").element(eleUpdatedPlan).getText();
							waitForSync(1);
							if(strPlanName.contains(strUpdatedPlanName))
							{
								strUpdatedPlanChrg=page("pgeCustomizeOrder").element("eleTotalMonthlyCharges").getText().replace("$","");
								waitForSync(1);
								dicTestData.put("strMonthlyCharges", strUpdatedPlanChrg);
								if(Double.parseDouble(strUpdatedPlanChrg) > Double.parseDouble(strPlanCharge))	
								{
									strUpdatedOneTimeChrg=page("pgeCustomizeOrder").element("eleOneTimePlanCharges").getText().replace("$","");
									waitForSync(1);
									dicTestData.put("strOneTimeCharges", strUpdatedOneTimeChrg);
									if(Double.parseDouble(strUpdatedOneTimeChrg) > Double.parseDouble(strOneTimeCharge))
									{
										blnFlag=true;
									}
									else
									{
										blnFlag=false;
										ErrDescription="Failed to update Plan's One Time charges on plan upgrade.";
									}
								}
								else
								{
									blnFlag=false;
									ErrDescription="Failed to update Plan's Monthly charges on plan upgrade.";
								}
							}
							else
							{
								blnFlag=false;
								ErrDescription="Failed to update selected plan's name.";
							}
						}
						else
							ErrDescription="Failed to select Add-On/Feature/Equipment to upgrade the selected plan.";
					}
					else
						ErrDescription="Failed to display selected plan's One Time Charges.";
				}
				else
					ErrDescription="Failed to display selected plan's Monthly Charges.";
		}
		else
			ErrDescription="Failed to display selected plan's Name.";
	}
	catch (Exception e)
	{
		blnFlag=false;
		logger.error(e.getMessage());
		ErrDescription="Error occurred while verifying plan upgrade changes.";
	}							
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyChargesOnSecurityPlanUpgrade
	// ''@Objective: This Function verifies updated Plan Name and Charges on upgrading Security Plan on Customize Order page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyChargesOnSecurityPlanUpgrade("pgeCustomizeOrder","lstDoorLockSecurityEquip","eleReadyHomeSecPlan",strUpdatedPlanName,strSelectValue,strSelectType,false);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyChargesOnSecurityPlanUpgrade(String pgePage,String eleElement,String eleUpdatedPlan,String strUpdatedPlanName,String strSelectValue,String strSelectType,boolean deselect) 
	{
		boolean blnFlag = false;
		String strPlanName= "";
		String strOneTimeCharge = "";
		String strUpdatedOneTimeChrg="";
		
		try
		{
			strPlanName=page("pgeCustomizeOrder").element("eleSelectedPlan").getText();
			if(strPlanName.equals(""))
			{
				blnFlag=clickAndVerify("pgeCustomizeOrder","btnExpandCharges","pgeCustomizeOrder","eleSelectedPlanName");
				strPlanName=page("pgeCustomizeOrder").element("eleSelectedPlanName").getText();
			}
			if(!strPlanName.equals(""))
			{
				strOneTimeCharge=page("pgeCustomizeOrder").element("eleOneTimePlanCharges").getText().replace("$","");
				waitForSync(1);
				if(!strOneTimeCharge.equals(""))
				{
					blnFlag=page(pgePage).element(eleElement).select(strSelectValue,strSelectType,deselect);
					if(blnFlag)
					{
						waitForSync(5);
						strPlanName=page("pgeCustomizeOrder").element(eleUpdatedPlan).getText();
						if(strPlanName.contains(strUpdatedPlanName))
						{
							strUpdatedOneTimeChrg=page("pgeCustomizeOrder").element("eleOneTimePlanCharges").getText().replace("$","");
							waitForSync(1);
							dicTestData.put("strOneTimeCharges", strUpdatedOneTimeChrg);
							if(Double.parseDouble(strUpdatedOneTimeChrg) > Double.parseDouble(strOneTimeCharge))
							{
								blnFlag=true;
							}
							else
							{
								blnFlag=false;
								ErrDescription="Failed to update Plan's One Time charges on plan upgrade.";
							}	
						}
						else
						{
							blnFlag=false;
							ErrDescription="Failed to update selected plan's name.";
						}
					}
					else
						ErrDescription="Failed to select Add-On/Feature/Equipment to upgrade the selected plan.";
				}
				else
					ErrDescription="Failed to display selected plan's One Time Charges.";
			}
			else
				ErrDescription="Failed to display selected plan's Name.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying plan upgrade changes.";
		}							
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: selectFromList
	// ''@Objective: This Function selects specified value from a list on the specified selection type.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: Page on which list exists
	// ''@Param Name: eleElement
	// ''@Param Desc: List Element
	// ''@Param Name: strSelectValue
	// ''@Param Desc: Value to be selected
	// ''@Param Name: strSelectType
	// ''@Param Desc: Selection Type ByValue/ByVisibleText..
	// ''@Param Name: pgeNewPage
	// ''@Param Desc: Navigated page on selection
	// ''@Param Name: eleNewElement
	// ''@Param Desc: Element on the navigated page
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= selectFromList("pgeCustomizeOrder","lstDoorLockSecurityEquip",strSelectValue,strSelectType,"pgeCustomizeOrder","eleSelectedDoorEquip");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean selectFromList(String pgePage,String eleElement,String strSelectValue,String strSelectType,String pgeNewPage,String eleNewElement) 
	{
		boolean blnFlag = false;
		
		try
		{
			blnFlag=page(pgePage).element(eleElement).select(strSelectValue,strSelectType,false);
			if(!blnFlag)
				blnFlag=customSelectFromList(pgePage,eleElement,eleNewElement,strSelectType,strSelectValue);
			
			if(blnFlag)
			{
				waitForSync(4);
				blnFlag = page(pgeNewPage).element(eleNewElement).exist();
				if(!blnFlag)
					ErrDescription="Failed to verify the selection of value '"+strSelectValue+"' from the list.";
				
			}
			else
				ErrDescription="Failed to select value '"+strSelectValue+"' from the list.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while selecting from list.";
		}							
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: selectDateAndTimeForInstallation
	// ''@Objective: This Function selects Service Installation date and time from the active/current month on 'Service & Installation' page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: Page on which Calendar exists
	// ''@Param Name: eleElement
	// ''@Param Desc: Select Date & Time Button
	// ''@Param Name: eleNewElement
	// ''@Param Desc: Field populated on Data/Time Selection
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= selectDateAndTimeForInstallation("pgeServiceSchedule","btnSelectDateForSecurity","eleSelectedDateForSecurity");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean selectDateAndTimeForInstallation(String pgePage,String eleElement,String eleNewElement)
	{
		boolean blnFlag = false;
		try
		{
			//blnFlag = page(pgePage).element(eleElement).click();
			blnFlag = verifyExistsAndClick(pgePage,eleElement,pgePage,eleNewElement,"pgeServiceSchedule","btnOKCalendar");
			if(blnFlag)
			{
				waitForSync(3);
				blnFlag = page("pgeServiceSchedule").element("eleSelectDate").exist();
				if(!blnFlag)
				{
					//page("pgeServiceSchedule").element("btnNextMonth").waitForElement(5);
					blnFlag = page("pgeServiceSchedule").element("btnNextMonth").click();
					waitForSync(1);
					if(blnFlag)
					{
						//page("pgeConfirmOrder").element("eleSelectDate").waitForElement(5);
						blnFlag = page("pgeServiceSchedule").element("eleSelectDate").click();
						waitForSync(1);
					}
				}
				else
				{
					blnFlag = page("pgeServiceSchedule").element("eleSelectDate").click();
					waitForSync(1);
				}
				
				if(blnFlag)
				{
					blnFlag=page("pgeServiceSchedule").element("rdoPreferredTimeOfDay").click();
					if(blnFlag)
					{
						blnFlag=page("pgeServiceSchedule").element("btnOKCalendar").click();
						if(blnFlag)
						{
							//page("pgeConfirmOrder").element("btnSelectDateTime").waitForElement(10, 1);
							waitForSync(1);
							blnFlag = verifyElementExist(pgePage,eleNewElement);
							if(!blnFlag)
								ErrDescription = "Failed to display the selected date and time on Schedule page.";
						}
						else
							ErrDescription = "Failed to click on 'OK' button from Calendar pop-up.";
					}
					else
						ErrDescription = "Failed to select 'Preffered Time of the day' radio-button from Calendar pop-up.";
				}
				else
					ErrDescription = "Failed to select date from the Calendar pop-up.";
			}
			else
				ErrDescription = "Failed to click on 'Select Date & Time' button.";
			
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while selecting Installation Date and Time.";
		}
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyKickOutsPageForNewCustomers
	// ''@Objective: This Function verifies 'KickOuts' page for New Customers.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strHeaderText
	// ''@Param Desc: Page Header/OOOPs Error Message to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyKickOutsPageForNewCustomers(strErrMessage);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Nov-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyKickOutsPageForNewCustomers(String strHeaderText)
	{
		boolean blnFlag =false;
		String strText = "";
		try
		{
			strText=page("pgeKickOuts").element("eleHeaderSection").getText();
			waitForSync(1);
			if(strHeaderText.contains(strText))
			{
				blnFlag=page("pgeCheckout").element("txtFirstName").exist();
				if(blnFlag)
				{
					blnFlag=page("pgeCheckout").element("txtLastName").exist();
					if(blnFlag)
					{
						blnFlag=page("pgeCheckout").element("txtPhoneNumber").exist();
						if(blnFlag)
						{
							blnFlag=page("pgeCheckout").element("txtEmailAddress").exist();
							if(blnFlag)
							{
								blnFlag=page("pgeKickOuts").element("btnSubmit").exist();
								if(!blnFlag)
									ErrDescription = "Failed to display 'Submit' button.";
							}
							else
								ErrDescription = "Failed to display 'Email Address' field.";
						}
						else
							ErrDescription = "Failed to display 'Phone Number' field.";
					}
					else
						ErrDescription = "Failed to display 'Last Name' field.";
				}
				else
					ErrDescription = "Failed to display 'First Name' field.";
			}
			else
				ErrDescription = "Failed to display text "+strHeaderText+" on kick-outs page.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying fields.";
		}
	return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: enterKickOutsInfo
	// ''@Objective: This Function enters data in all the fields on KickOuts/OOOPs page for Existing/New Customers.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strFirstName
	// ''@Param Desc: First Name of the Customer
	// ''@Param Name: strLastName
	// ''@Param Desc: Last Name of the Customer
	// ''@Param Name: strPhoneNumber
	// ''@Param Desc: Phone Number of the Customer
	// ''@Param Name: strEmailAddress
	// ''@Param Desc: Email Address of the Customer
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= enterKickOutsInfo(strFirstName,strLastName,strPhoneNumber,strEmailAddress);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Nov-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean enterKickOutsInfo(String strFirstName,String strLastName,String strPhoneNumber,String strEmailAddress)
	{
		boolean blnFlag =false;

		try
		{
			blnFlag=page("pgeCheckout").element("txtFirstName").type(strFirstName);
			if(blnFlag)
			{
				blnFlag=page("pgeCheckout").element("txtLastName").type(strLastName);
				if(blnFlag)
				{
					blnFlag=page("pgeCheckout").element("txtPhoneNumber").type(strPhoneNumber);
					if(blnFlag)
					{
						blnFlag=page("pgeCheckout").element("txtEmailAddress").type(strEmailAddress);
						if(!blnFlag)
							ErrDescription = "Failed to enter data in Email Address field.";
						}
						else
							ErrDescription = "Failed to enter data in 'Phone Number' field.";
					}
					else
						ErrDescription = "Failed to enter data in 'Last Name' field.";
				}
				else
					ErrDescription = "Failed to enter data in 'First Name' field";
			
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying fields.";
		}
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyKickOutsPageForExstCustomers
	// ''@Objective: This Function verifies 'KickOuts' page for Existing Customers.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strHeaderText
	// ''@Param Desc: Page Header to be verified
	// ''@Param Name: strCallRepText
	// ''@Param Desc: Call Representative Text to be verified
	// ''@Param Name: eleOOOPsError
	// ''@Param Desc: OOOPs Error Message to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyKickOutsPageForExstCustomers(strErrMessage,strCallRepresentative,"lblOOOPsErrMessage1");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Nov-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyKickOutsPageForExstCustomers(String strHeaderText,String strCallRepText,String eleOOOPsError)
	{
		boolean blnFlag =false;
		String strText = "";
		try
		{
			strText=page("pgeKickOuts").element(eleOOOPsError).getText();
			waitForSync(1);
			if(strHeaderText.contains(strText))
			{
				strText=page("pgeKickOuts").element("eleCallRepresentative").getText();
				waitForSync(1);
				if(strText.contains(strCallRepText))
				{
					blnFlag=page("pgeKickOuts").element("lnkChatNow").exist();
					if(!blnFlag)
						ErrDescription = "Failed to display 'Chat now' field.";
				}
				else
					ErrDescription = "Failed to display 'Call' field with representative's number.";
			}
			else
				ErrDescription = "Failed to display text "+strHeaderText+" on kick-outs page.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying fields.";
		}
	return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyTelephoneServicesAndCharges
	// ''@Objective: This Function verifies selected Services and their $ amount when 'Phone' plan is added/upgraded on Customize Order page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strPlanAction
	// ''@Param Desc: 'Phone' plan to be added/upgraded
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyTelephoneServicesAndCharges("AddPlan");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyTelephoneServicesAndCharges(String strPlanAction)
	{
		boolean blnFlag = false;
		try
		{   
			if(strPlanAction.equals("AddPlan"))
			{
				blnFlag = page("pgeCustomizeOrder").element("eleEmergencyService").exist();
				if(blnFlag)
				{
					blnFlag = page("pgeCustomizeOrder").element("eleEmergencyService$").exist();
					if(blnFlag)
					{
						blnFlag = page("pgeCustomizeOrder").element("eleDualRelayService").exist();
						if(blnFlag)
						{
							blnFlag = page("pgeCustomizeOrder").element("eleDualRelayService$").exist();
							if(blnFlag)
							{
								blnFlag = page("pgeCustomizeOrder").element("eleFedEndUserCharge").exist();
								if(blnFlag)
								{
									blnFlag = page("pgeCustomizeOrder").element("eleFedEndUserCharge$").exist();
									if(blnFlag)
									{
										blnFlag = page("pgeCustomizeOrder").element("eleLongDistance").exist();
										if(blnFlag)
										{
											blnFlag = page("pgeCustomizeOrder").element("eleLongDistance$").exist();
											if(blnFlag)
											{
												blnFlag = page("pgeCustomizeOrder").element("elePrivateNumChrgPlan").exist();
												if(blnFlag)
												{
													blnFlag = page("pgeCustomizeOrder").element("elePrivateNumCharges").exist();
													if(!blnFlag)
														ErrDescription = "Failed to display $ amount for 'Private Number Charges' service.";
												}
												else
													ErrDescription = "Failed to display 'Private Number Charges' service.";
											}
											else
												ErrDescription = "Failed to display $ amount for 'Long Distance Restriction' services.";
										}
										else
											ErrDescription = "Failed to display 'Long Distance Restriction' services.";
									}
									else
										ErrDescription = "Failed to display $ amount for 'Federal End User Charge' services.";
								}
								else
									ErrDescription = "Failed to display 'Federal End User Charge' services.";
							}
							else
								ErrDescription = "Failed to display $ amount for 'Dual Party Relay Services'.";
						}
						else
							ErrDescription = "Failed to display 'Dual Party Relay Services'.";
					}
					else
						ErrDescription = "Failed to display $ amount for 'Emergency Service Fee (E911)' services.";
				}
				else
					ErrDescription = "Failed to display 'Emergency Service Fee (E911)' services.";
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying fields.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifySecurityServicesAndCharges
	// ''@Objective: This Function verifies security services and charges on Security Plan Upgrade/Add.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifySecurityServicesAndCharges("AddPlan");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifySecurityServicesAndCharges(String strPlanAction)
	{
		boolean blnFlag = false;
		try
		{   
			if(strPlanAction.equals("AddPlan"))
			{
				blnFlag = page("pgeCustomizeOrder").element("eleDoorContact1stFree").exist();
				if(blnFlag)
				{
					blnFlag = page("pgeCustomizeOrder").element("eleDoorContact1stFree$").exist();
					if(blnFlag)
					{
						blnFlag = page("pgeCustomizeOrder").element("eleDoorContact3rdFree").exist();
						if(blnFlag)
						{
							blnFlag = page("pgeCustomizeOrder").element("eleDoorContact3rdFree$").exist();
							if(blnFlag)
							{
								blnFlag = page("pgeCustomizeOrder").element("eleMotionDectFree").exist();
								if(blnFlag)
								{
									blnFlag = page("pgeCustomizeOrder").element("eleMotionDectFree$").exist();
									if(blnFlag)
									{
										blnFlag = page("pgeCustomizeOrder").element("eleTouchScreenFree").exist();
										if(blnFlag)
										{
											blnFlag = page("pgeCustomizeOrder").element("eleTouchScreenFree$").exist();
											if(blnFlag)
											{
												blnFlag = page("pgeCustomizeOrder").element("eleLightBulb1stFree").exist();
												if(blnFlag)
												{
													blnFlag = page("pgeCustomizeOrder").element("eleLightBulb1stFree$").exist();
													if(blnFlag)
													{
														blnFlag = page("pgeCustomizeOrder").element("eleDoorContact2ndFree").exist();
														if(blnFlag)
														{
															blnFlag = page("pgeCustomizeOrder").element("eleDoorContact2ndFree$").exist();
															if(blnFlag)
															{
																blnFlag = page("pgeCustomizeOrder").element("eleLightBulb2ndFree").exist();
																if(blnFlag)
																{
																	blnFlag = page("pgeCustomizeOrder").element("eleLightBulb2ndFree$").exist();
																	if(!blnFlag)
																		ErrDescription = "Failed to display $ amount for 'Light Bulb 2nd free' service.";
																}
																else
																	ErrDescription = "Failed to display 'Light Bulb 2nd free' service.";
															}
															else
																ErrDescription = "Failed to display $ amount for 'Door/Window Contact 2nd free' service.";
														}
														else
															ErrDescription = "Failed to display 'Door/Window Contact 2nd free' service.";
													}
													else
														ErrDescription = "Failed to display $ amount for 'Light Bulb 1st free' service.";
												}
												else
													ErrDescription = "Failed to display 'Light Bulb 1st free' service.";
											}
											else
												ErrDescription = "Failed to display $ amount for 'Touchscreen free' service.";
										}
										else
											ErrDescription="Failed to display 'Touchscreen free' service.";
									}
									else
										ErrDescription = "Failed to display $ amount for 'Motion Detector free' service.";
								}
								else
									ErrDescription="Failed to display 'Motion Detector free' service.";
							}
							else
								ErrDescription = "Failed to display $ amount for 'Door/Window Contact 3rd free' service.";
						}
						else
							ErrDescription="Failed to display 'Door/Window Contact 3rd free' service.";
					}
					else
						ErrDescription = "Failed to display $ amount for 'Door/Window Contact 1st free' service.";
				}
				else
					ErrDescription="Failed to display 'Door/Window Contact 1st free' service.";
			}
			if(strPlanAction.equals("AddSecEquipments"))
			{
				blnFlag = page("pgeConfirmOrder").element("eleDoorContact1stFree").exist();
				if(blnFlag)
				{
					blnFlag = page("pgeConfirmOrder").element("eleDoorContact1stFree$").exist();
					if(blnFlag)
					{
						blnFlag = page("pgeConfirmOrder").element("eleDoorContact3rdFree").exist();
						if(blnFlag)
						{
							blnFlag = page("pgeConfirmOrder").element("eleDoorContact3rdFree$").exist();
							if(!blnFlag)
								ErrDescription = "Failed to display $ amount for 'Light Bulb 2nd free' service.";
						}
						else
							ErrDescription = "Failed to display 'Light Bulb 2nd free' service.";
					}
					else
						ErrDescription = "Failed to display $ amount for 'Door/Window Contact 2nd free' service.";
				}
				else
					ErrDescription = "Failed to display 'Door/Window Contact 2nd free' service.";
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying fields.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: clickSecurityAndVerify
	// ''@Objective: This Function clicks on an element and verifies checkbox selection on a page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: Page on which Element exists
	// ''@Param Name: eleElement
	// ''@Param Desc: Element to be clicked
	// ''@Param Name: pgeNewPage
	// ''@Param Desc: Navigated Page on which CheckBox exists
	// ''@Param Name: eleNewElement
	// ''@Param Desc: Checkbox to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= clickSecurityAndVerify("pgeCustomerSummary","btnTVAddService","pgeBrowse","chkTVPlan");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean clickSecurityAndVerify(String pgePage, String eleElement, String pgeNewPage, String eleNewElement)
	{
		boolean blnFlag = false;
		try
		{      
			blnFlag=page(pgePage).element(eleElement).click();
			if(blnFlag)
			{
				waitForSync(4);
				blnFlag = page(pgeNewPage).element(eleNewElement).exist();
				if (!blnFlag)
				{
					blnFlag=driver.findElements(By.xpath((page("pgeBrowse").element("chkSecurityPlan").GetXpath()))).size()>0;
					if(!blnFlag)
						ErrDescription = "Unable to verify navigated page.";
				}		
			} 
			else
				ErrDescription = "Unable to perform click action.";
		}

		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while clicking and verifying action.";
		}
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyTelevisionServicesAndCharges
	// ''@Objective: This Function verifies selected Services and their $ amount when 'TV' plan is added/upgraded on Customize Order page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strPlanAction
	// ''@Param Desc: 'TV' plan to be added/upgraded
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyTelevisionServicesAndCharges("UpgradePlan");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyTelevisionServicesAndCharges(String strPlanAction)
	{
		boolean blnFlag = false;
		try
		{   
			if(strPlanAction.equals("UpgradePlan"))
			{
				blnFlag = page("pgeConfirmOrder").element("eleMoxiWholeHomeHD").exist();
				if(blnFlag)
				{
					blnFlag = page("pgeConfirmOrder").element("eleMoxiWholeHomeHD$").exist();
					if(blnFlag)
					{
						blnFlag = page("pgeConfirmOrder").element("eleMoxiMediaPlayerEquip").exist();
						if(blnFlag)
						{
							blnFlag = page("pgeConfirmOrder").element("eleMoxiMediaPlayerEquip$").exist();
							if(!blnFlag)
								ErrDescription = "Failed to display $ amount for 'Additional Moxi Media Media Player' equipment.";
						}
						else
							ErrDescription = "Failed to display 'Additional Moxi Media Media Player' equipment.";
					}
					else
						ErrDescription = "Failed to display $ amount for 'Moxi Whole Home HD DVR Digital Variety' plan.";
				}
				else
					ErrDescription = "Failed to display 'Moxi Whole Home HD DVR Digital Variety' plan.";
			}
			
			if(strPlanAction.equals("UpgradeHDDVRDigPlan"))
			{
				blnFlag = page("pgeConfirmOrder").element("eleHDdigitalDVR").exist();
				if(blnFlag)
				{
					blnFlag = page("pgeConfirmOrder").element("eleHDdigitalDVR$").exist();
					if(blnFlag)
					{
						blnFlag = page("pgeConfirmOrder").element("ele1stHDDVRConverter").exist();
						if(blnFlag)
						{
							blnFlag = page("pgeConfirmOrder").element("ele1stHDDVRConverter$").exist();
							if(!blnFlag)
								ErrDescription = "Failed to display $ amount for '1st HD DVR Converter' equipment.";
						}
						else
							ErrDescription = "Failed to display '1st HD DVR Converter' equipment.";
					}
					else
						ErrDescription = "Failed to display $ amount for 'HD DVR Digital Variety' plan.";
				}
				else
					ErrDescription = "Failed to display 'HD DVR Digital Variety' plan.";
			}
			
			if(strPlanAction.equals("AddPlan"))
			{
				blnFlag = page("pgeConfirmOrder").element("eleHDdigitalVariety").exist();
				if(blnFlag)
				{
					blnFlag = page("pgeConfirmOrder").element("eleHDdigitalVariety$").exist();
					if(blnFlag)
					{
						blnFlag = page("pgeConfirmOrder").element("eleHDdigitalConverter").exist();
						if(blnFlag)
						{
							blnFlag = page("pgeConfirmOrder").element("eleHDdigitalConverter$").exist();
							if(blnFlag)
							{
								blnFlag = page("pgeConfirmOrder").element("eleLocalStationChrg").exist();
								if(blnFlag)
								{
									blnFlag = page("pgeConfirmOrder").element("eleLocalStationChrg$").exist();
									if(blnFlag)
									{
										blnFlag = page("pgeConfirmOrder").element("eleConverterAdditional").exist();
										if(blnFlag)
										{
											blnFlag = page("pgeConfirmOrder").element("eleConverterAdditional$").exist();
											if(blnFlag)
											{
												blnFlag = page("pgeConfirmOrder").element("eleRegularFeeFCC").exist();
												if(blnFlag)
												{
													blnFlag = page("pgeConfirmOrder").element("eleRegularFeeFCC$").exist();
													if(blnFlag)
													{
														blnFlag = page("pgeConfirmOrder").element("eleOffOnVideoPack").exist();
														if(blnFlag)
														{
															blnFlag = page("pgeConfirmOrder").element("eleOffOnVideoPack$").exist();
															if(!blnFlag)
																ErrDescription = "Failed to display $ amount for '$25 Off Any Video Package for 6 months' service.";
														}
														else
															ErrDescription = "Failed to display '$25 Off Any Video Package for 6 months' service.";
													}
													else
														ErrDescription = "Failed to display $ amount for 'FCC Regulatory Fee' service.";
												}
												else
													ErrDescription = "Failed to display 'FCC Regulatory Fee' service.";
											}
											else
												ErrDescription = "Failed to display 'Converter - Additional Digital' service.";
										}
										else
											ErrDescription = "Failed to display $ amount for 'Converter - Additional Digital' service.";
									}
									else
										ErrDescription = "Failed to display $ amount for 'Local Station Charge' service.";
								}
								else
									ErrDescription = "Failed to display 'Local Station Charge' service.";
							}
							else
								ErrDescription = "Failed to display $ amount for 'HD Digital Converter' service.";
						}
						else
							ErrDescription = "Failed to display 'HD Digital Converter' service.";
					}
					else
						ErrDescription = "Failed to display $ amount for 'HD Digital Variety' service.";
				}
				else
					ErrDescription = "Failed to display 'HD Digital Variety' service.";				
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying fields.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: customSelectFromList
	// ''@Objective: This Function selects specified value from a list using 'ByValue' selectType on a page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: page on which list exists
	// ''@Param Name: eleElement
	// ''@Param Desc: List from where value is selected
	// ''@Param Name: eleNewElement
	// ''@Param Desc: Element that populates on list selection
	// ''@Param Name: strSelectBy
	// ''@Param Desc: SelectType specification 'ByValue/ByVisibleText'
	// ''@Param Name: strValue
	// ''@Param Desc: Value to be selected from List
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= customSelectFromList("pgeCustomizeOrder","lstAddHDdigConverter","eleSelectedHDconverter","ByValue","1");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[25-Nov-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean customSelectFromList(String pgePage,String eleElement,String eleNewElement,String strSelectBy,String strValue)
	{
		boolean blnFlag=false;
		
		try
		{
			Select dropdown = new Select(driver.findElement(By.xpath(page(pgePage).element(eleElement).GetXpath())));
			if(strSelectBy.equals("ByValue"))
			{
				dropdown.selectByValue(strValue);
			}
			
			page(pgePage).element(eleElement).waitForElement(5,1);
			waitForSync(5);
			blnFlag=page(pgePage).element(eleNewElement).exist();
				if(!blnFlag)
					ErrDescription="Failed to select the specified option.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while selecting a value from the list.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyCheckboxSelection
	// ''@Objective: This Function verifies specified checkbox's selection and de-selection using attribute 'id' .
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: Page on which CheckBox exists
	// ''@Param Name: eleElement
	// ''@Param Desc: CheckBox to be verified
	// ''@Param Name: blnSelect
	// ''@Param Desc: True/False to verify if checkbox is selected/de-selected
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus=verifyCheckboxSelection("pgeBrowse","chkTVPlan",true);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Dec-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyCheckboxSelection(String pgePage, String eleElement,boolean blnSelect)
	{
		boolean blnFlag = false;
		try
		{      
			if(blnSelect)
			{
				blnFlag=page(pgePage).element(eleElement).isSelected();
				if(!blnFlag)
					blnFlag=driver.findElement(By.xpath(page(pgePage).element(eleElement).getAttributeValue("id"))).isSelected();
					if(!blnFlag)
						ErrDescription = "Unable to verify if the specified check-box is selected.";
			}
			
			if(!blnSelect)
			{
				blnFlag=!page(pgePage).element(eleElement).isSelected();
				if(!blnFlag)
					blnFlag=!driver.findElement(By.id(page(pgePage).element(eleElement).getAttributeValue("id"))).isSelected();
					if(!blnFlag)
						ErrDescription = "Unable to verify if the specified check-box is not selected.";
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying check-box selection.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyElementExist
	// ''@Objective: This Function verifies if element exists on a page by verifying it's size, greater than zero.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: page on which the element exists
	// ''@Param Name: eleElement
	// ''@Param Desc: element to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyElementExist("pgeMyAccountLogin","btnLogin");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[30-Dec-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyElementExist(String pgePage, String eleElement)
	{
		boolean blnFlag = false;
		try
		{      
			blnFlag=page(pgePage).element(eleElement).exist();
			if(!blnFlag)
			{
				blnFlag=driver.findElements(By.xpath((page(pgePage).element(eleElement).GetXpath()))).size()>0;
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying element's existence.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: deleteAllCookies
	// ''@Objective: This Function deletes all cookies from Internet Explorer browser.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= deleteAllCookies("");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[01-Jan-2019]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public void deleteAllCookies()
	{
		try
		{
			if(driver.toString().contains("internet explorer"))
			{
				driver.manage().deleteAllCookies();
			}
		}
		catch (Exception e)
		{
			logger.error(e.getMessage());
			ErrDescription="Error occurred while deleting cookies.";
		}
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: enterAddressAndSubmitByEnterKey
	// ''@Objective: This Function enters street address and submit the address by hitting Enter Key on Address Search page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strAddress
	// ''@Param Desc: Street Address to be searched/Submitted
	// ''@Param Name: pgePage,eleElement
	// ''@Param Desc: navigated page when address is submitted
	// ''@Param Name: blnErrorMessage
	// ''@Param Desc: True/False whether to verify Error Message
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= enterAddressAndSubmitByEnterKey(strStreetAddress1,"pgeProfilePage","eleAccountExistPopUp",false);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Jan-2019]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean enterAddressAndSubmitByEnterKey(String strAddress,String pgePage,String eleElement,boolean blnErrorMessage)
	{
		boolean blnFlag = false;
		Actions action = new Actions(driver);
		
		try
		{  
			page("pgeProfilePage").element("txtStreetAddress").click();
			blnFlag=page("pgeProfilePage").element("txtStreetAddress").type(strAddress);
			if(blnFlag && !blnErrorMessage)
			{
				page("pgeHomePage").element("lstSearchAddressList").waitForElement(60, 1);
				blnFlag = page("pgeHomePage").element("lstSearchAddressList").exist();
			}
			if(blnFlag)
			{
				// Sendkeys using Action class object
				action.sendKeys(Keys.ENTER).build().perform();
				page(pgePage).element(eleElement).waitForElement(60, 1);
				blnFlag = page(pgePage).element(eleElement).exist();
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while clicking and verifying address.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyBundlePlanServicesAndCharges
	// ''@Objective: This Function verifies selected Services and their $ amount when a bundle plan is added/upgraded on Customize Order page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strPlanAction
	// ''@Param Desc: 'Bundle' plan to be added/upgraded
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyBundlePlanServicesAndCharges(strBundlePlans,"AddPlan");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyBundlePlanServicesAndCharges(String strPlanType,String strPlanAction)
	{
		boolean blnFlag = false;
		try
		{   
			if(strPlanAction.equals("AddPlan"))
			{
				blnFlag = page("pgeConfirmOrder").element("eleMoxiWholeHomeHD").exist();
				if(blnFlag)
				{
					blnFlag = page("pgeConfirmOrder").element("eleMoxiWholeHomeHD$").exist();
					if(blnFlag)
					{
						blnFlag = page("pgeConfirmOrder").element("eleLocalStationChrg").exist();
						if(blnFlag)
						{
							blnFlag = page("pgeConfirmOrder").element("eleLocalStationChrg$").exist();
							if(blnFlag)
							{
								blnFlag = page("pgeConfirmOrder").element("eleRegularFeeFCC").exist();
								if(blnFlag)
								{
									blnFlag = page("pgeConfirmOrder").element("eleRegularFeeFCC$").exist();
									if(blnFlag)
									{
										blnFlag = page("pgeCustomizeOrder").element("eleEmergencyService").exist();
										if(blnFlag)
										{
											blnFlag = page("pgeCustomizeOrder").element("eleEmergencyService$").exist();
											if(blnFlag)
											{
												blnFlag = page("pgeCustomizeOrder").element("eleDualRelayService").exist();
												if(blnFlag)
												{
													blnFlag = page("pgeCustomizeOrder").element("eleDualRelayService$").exist();
													if(blnFlag)
													{
														blnFlag = page("pgeCustomizeOrder").element("eleFedEndUserCharge").exist();
														if(blnFlag)
														{
															blnFlag = page("pgeCustomizeOrder").element("eleFedEndUserCharge$").exist();
															if(blnFlag)
															{
																blnFlag = page("pgeCustomizeOrder").element("eleLongDistance").exist();
																if(blnFlag)
																{
																	blnFlag = page("pgeCustomizeOrder").element("eleLongDistance$").exist();
																	if(blnFlag)
																	{
																		blnFlag = page("pgeConfirmOrder").element("eleZipStrInternet").exist();
																		if(blnFlag)
																		{
																			blnFlag = page("pgeConfirmOrder").element("eleZipStrInternet$").exist();
																			if(blnFlag)
																			{
																				blnFlag = page("pgeConfirmOrder").element("eleVoicePlus").exist();
																				if(blnFlag)
																				{
																					blnFlag = page("pgeConfirmOrder").element("eleVoicePlus$").exist();
																					if(blnFlag)
																					{
																						blnFlag = page("pgeConfirmOrder").element("eleBundleSavings").exist();
																						if(blnFlag)
																						{
																							blnFlag = page("pgeConfirmOrder").element("eleBundleSavings$").exist();
																							if(!blnFlag)
																								ErrDescription="Failed to display $ amount for 'Bundle Savings' service.";
																						}
																						else
																							ErrDescription="Failed to display 'Bundle Savings' service.";
																					}
																					else
																						ErrDescription="Failed to display $ amount for 'Voice Plus' service.";
																				}
																				else
																					ErrDescription="Failed to display 'Voice Plus' service.";
																			}
																			else
																				ErrDescription="Failed to display $ amount for 'Zipstream Internet' service.";
																		}
																		else
																			ErrDescription="Failed to display 'Zipstream Internet' service.";
																	}
																	else
																		ErrDescription = "Failed to display $ amount for 'Long Distance Restriction' service.";
																}
																else
																	ErrDescription = "Failed to display 'Long Distance Restriction' service.";
															}
															else
																ErrDescription = "Failed to display $ amount for 'Federal End User Charge' service.";
														}
														else
															ErrDescription = "Failed to display 'Federal End User Charge' service.";
													}
													else
														ErrDescription = "Failed to display $ amount for 'Dual Party Relay Service' service.";
												}
												else
													ErrDescription = "Failed to display 'Dual Party Relay Service' service.";
											}
											else
												ErrDescription = "Failed to display $ amount for 'Emergency Service Fee (E911)' service.";
										}
										else
											ErrDescription = "Failed to display 'Emergency Service Fee (E911)' service.";
									}
									else
										ErrDescription = "Failed to display $ amount for 'FCC Regulatory Fee' service.";
								}
								else
									ErrDescription = "Failed to display 'FCC Regulatory Fee' service.";
							}
							else
								ErrDescription = "Failed to display $ amount for 'Local Station Charge' service.";
						}
						else
							ErrDescription = "Failed to display 'Local Station Charge' service.";
					}
					else
						ErrDescription = "Failed to display $ amount for 'Moxi Whole Home HD DVR Digital Variety' service.";
				}
				else
					ErrDescription = "Failed to display 'Moxi Whole Home HD DVR Digital Variety' service.";						
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying fields.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: selectViewAllBundles
	// ''@Objective: This Function clicks on 'View All Bundles' checkbox and verify populated fields.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= selectViewAllBundles();
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean selectViewAllBundles()
	{
		boolean blnFlag = false;		
		try
		{
			if(driver.toString().contains("internet explorer"))
			{
				blnFlag=selectCheckBox("pgeBrowse","chkViewBundles","eleImLookingFor","id",true);
			}
			else
			{
				blnFlag = page("pgeBrowse").element("chkViewAllBundles").click();
			}
			if(blnFlag)
			{
				waitForSync(3);
				blnFlag=page("pgeBrowse").element("eleImLookingFor").exist();
				if(!blnFlag)
					ErrDescription="Failed to display 'I am Looking for' section.";
			}
			else
				ErrDescription="Failed to select 'View All Bundles' check-box.";	
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while clicking and verifying action.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: waitForSync
	// ''@Objective: This Function waits for the specified amount of time in milliseconds.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= waitForSync(1);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public void waitForSync(int intWaitTime)
	{		
		try
		{
			Thread.sleep(intWaitTime*1000);
		}
		catch (Exception e)
		{
			logger.error(e.getMessage());
			ErrDescription="Error occured while waiting for an element.";
		}
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyExistsAndClick
	// ''@Objective: This Function verifies if field exists and then clicks on it.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyExistsAndClick("pgeAccountOverview","lnkAddUpgradeService","pgeAccountOverview","btnShopUpgrades","pgeCustomizeOrder","lnkReturnToAccSummary");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyExistsAndClick(String pgePage,String eleElement,String pgeNewPage,String eleNewElement)
	{
		boolean blnFlag = false;
		try
		{    
			blnFlag=page(pgePage).element(eleElement).exist();
			if(blnFlag)
			{
				blnFlag=page(pgePage).element(eleElement).click();
				if(blnFlag)
				{
					waitForSync(4);
					blnFlag=page(pgeNewPage).element(eleNewElement).exist();
					if(!blnFlag)
						ErrDescription = "Failed to navigate to the next page.";
				}
			}
			else
			{
				blnFlag=true;
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying existance and clicking.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyServiceChargesOnConfirmOrder
	// ''@Objective: This Function verifies selected Services and their $ amount when a plan is added/upgraded on Confirm Order page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strPlanAction
	// ''@Param Desc: 'Phone' plan to be added/upgraded
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyServiceChargesOnConfirmOrder("pgeConfirmOrder","",strService[0],intServicePosition);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyServiceChargesOnConfirmOrder(String pgePage,String eleService,String strService,int intServicePosition,int intChargePosition)
	{
		boolean blnFlag = false;
		String strXpath = "";
		String strText = "";
		
		try
		{   
			strXpath = page(pgePage).element(eleService).GetXpath();
			if(!strXpath.isEmpty())
			{
				strText = driver.findElement(By.xpath(strXpath+"["+intServicePosition+"]")).getText();
				if(strText.contains(strService))
				{
					strXpath = strXpath+"["+intServicePosition+"]//following::div["+intChargePosition+"]";
					blnFlag = driver.findElement(By.xpath(strXpath)).getText().contains("$");
					if(!blnFlag)
						ErrDescription="Failed to verify charges($ amount) of the selected services.";
				}
				else
					ErrDescription="Failed to display selected Services on 'Confirm Order' page.";
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying services and charges.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyServiceChargesOnConfirmOrder
	// ''@Objective: This Function verifies selected Services and their $ amount when a plan is added/upgraded on Confirm Order page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strPlanAction
	// ''@Param Desc: 'Phone' plan to be added/upgraded
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyServiceChargesOnConfirmOrder("pgeConfirmOrder","",strService[0],intServicePosition);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyServiceChargesOnConfirmOrder(String pgePage,String eleService,String strService)
	{
		boolean blnFlag = false;
		String strXpath = "";
		String strText = "";
		
		try
		{   
			strXpath = page(pgePage).element(eleService).GetXpath();
			if(!strXpath.isEmpty())
			{
				//driver.findElements(By.xpath(strXpath)).
				List<WebElement> listOfElements = driver.findElements(By.xpath(strXpath));
				if(!listOfElements.isEmpty())
				{
					for(WebElement element: listOfElements)
					{
						strText = element.getText();
						if(strText.contains(strService))
						{
							strXpath = "//*[text()='"+strText+"']//following::div[2]";
							blnFlag = driver.findElement(By.xpath(strXpath)).getText().contains("$");
							if(!blnFlag)
								ErrDescription="Failed to verify charges($ amount) of the selected services.";
							break;
						}
					}
				}
				else
					ErrDescription="Failed to display selected Services on 'Confirm Order' page.";
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying services and charges.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: clickAndVerify
	// ''@Objective: This Function clicks on the specified object and verifies navigated page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: eleElement
	// ''@Param Name: pgeNewPage
	// ''@Param Desc: eleNewElement
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= clickAndVerify("pgeMyAccountLogin","btnLogin","pgeAccountOverview","eleAccountSummary");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean clickJSAndVerify(String pgePage, String eleElement, String pgeNewPage, String eleNewElement)
	{
		boolean blnFlag = false;
		try
		{   
			blnFlag=page(pgePage).element(eleElement).JSClick();
			if(blnFlag)
			{
				waitForSync(4);
				blnFlag = page(pgeNewPage).element(eleNewElement).exist();
				if(!blnFlag)
				{
					blnFlag=driver.findElements(By.xpath((page(pgeNewPage).element(eleNewElement).GetXpath()))).size()>0;
					if(!blnFlag)
						ErrDescription = "Unable to verify navigated page.";
				}	
			} 
			else
				ErrDescription = "Unable to perform click action.";
		}

		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while clicking and verifying action.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyServiceChargesOnConfirmOrder
	// ''@Objective: This Function verifies selected Services and their $ amount when a plan is added/upgraded on Confirm Order page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strPlanAction
	// ''@Param Desc: 'Phone' plan to be added/upgraded
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyServiceChargesOnConfirmOrder("pgeConfirmOrder","",strService[0],intServicePosition);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyMultipleServicesAndCharges(String pgePage,String eleService,String strService)
	{
		boolean blnFlag = false;
		String strXpath = "";
		String strText = "";
		int i=0;
		String [] strServices = strService.split(",");
		
		try
		{   
			strXpath = page(pgePage).element(eleService).GetXpath();
			if(!strXpath.isEmpty())
			{
				//driver.findElements(By.xpath(strXpath)).
				List<WebElement> listOfElements = driver.findElements(By.xpath(strXpath));
				if(!listOfElements.isEmpty())
				{
					for(i=0;i<strServices.length;i++)
					{
						for(WebElement element: listOfElements)
						{
							strText = element.getText();
							if(strText.contains(strServices[i]))
							{
								strXpath = "//*[text()='"+strText+"']//following::div[2]";
								blnFlag = driver.findElement(By.xpath(strXpath)).getText().contains("$");
								if(!blnFlag)
								{
									strXpath = "//*[text()='"+strText+"']//following::div[1]";
									blnFlag = driver.findElement(By.xpath(strXpath)).getText().contains("$");
								}
								if(blnFlag)
										break;
							}
						}
					}
					if(!blnFlag)
						ErrDescription="Failed to display charges with $ amount for selected Services.";
				}
				else
					ErrDescription="Failed to display selected Services.";
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying services and charges.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyServiceChargesOnConfirmOrder
	// ''@Objective: This Function verifies selected Services and their $ amount when a plan is added/upgraded on Confirm Order page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strPlanAction
	// ''@Param Desc: 'Phone' plan to be added/upgraded
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyServiceChargesOnConfirmOrder("pgeConfirmOrder","",strService[0],intServicePosition);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean customizeBundleIndividualPlan(String strParentPlan,String strSubPlan,String strCustomizePlan)
	{
		boolean blnFlag = false;

		try
		{  
		   switch(strParentPlan)
		   {
		     case "Individual":
		     blnFlag=clickAndVerify("pgeBrowse","btnIndividualPlans","pgeBrowse","eleImLookingFor");
		     if(blnFlag && strSubPlan.equals("Internet"))
		     {
		    	 blnFlag=clickAndVerifyContainsText("pgeBrowse","chkInternetPlan","pgeBrowse","eleSelectedPlanHeader",strSubPlan);
		     }
		     if(blnFlag && strSubPlan.equals("TV"))
		     {
		    	 blnFlag=clickAndVerifyContainsText("pgeBrowse","chkTVPlan","pgeBrowse","eleSelectedPlanHeader",strSubPlan);
		     }
		     if(blnFlag && strSubPlan.equals("Security"))
		     {
		    	 blnFlag=clickAndVerifyContainsText("pgeBrowse","chkSecurityPlan","pgeBrowse","eleSelectedPlanHeader",strSubPlan);
		     }
		     if(blnFlag && strSubPlan.equals("Phone"))
		     {
		    	 blnFlag=clickAndVerifyContainsText("pgeBrowse","chkPhonePlan","pgeBrowse","eleSelectedPlanHeader",strSubPlan);
		     }
		     break;

		     case "BundleWithInternet":
		     blnFlag=selectViewAllBundlesAndVerify();
		     if(blnFlag)
		     {
		    	 blnFlag=page("pgeBrowse").element("chkInternetPlan").click();
		    	 waitForSync(3);
		    	 if(blnFlag && strSubPlan.equals("TV"))
		    	 {
		    		 blnFlag=page("pgeBrowse").element("chkTVPlan").click();
		    		 if(blnFlag)
		    		 {
		    			 waitForSync(2);
		    			 blnFlag=verifyTextContains("pgeBrowse","eleSelectedPlanDetails","Internet") && verifyTextContains("pgeBrowse","eleSelectedPlanDetails",strSubPlan);
		    		 }
		    	 }
		    	 if(blnFlag && strSubPlan.equals("Security"))
		    	 {
		    		 blnFlag=page("pgeBrowse").element("chkSecurityPlan").click();
		    		 if(blnFlag)
		    		 {
		    			 waitForSync(2);
		    			 blnFlag=verifyTextContains("pgeBrowse","eleSelectedPlanDetails","Internet") && verifyTextContains("pgeBrowse","eleSelectedPlanDetails",strSubPlan);
		    		 }
		    	 }
		    	 if(blnFlag && strSubPlan.equals("Phone"))
		    	 {
		    		 blnFlag=page("pgeBrowse").element("chkPhonePlan").click();
		    		 if(blnFlag)
		    		 {
		    			 waitForSync(2);
		    			 blnFlag=verifyTextContains("pgeBrowse","eleSelectedPlanDetails","Internet") && verifyTextContains("pgeBrowse","eleSelectedPlanDetails",strSubPlan);
		    		 }
		    	 }
		     }
		     break;
		     
		     case "BundleWithTV":
		     blnFlag=selectViewAllBundlesAndVerify();
		     if(blnFlag)
		     {
		    	 blnFlag=page("pgeBrowse").element("chkTVPlan").click();
		    	 if(blnFlag && strSubPlan.equals("Internet"))
		    	 {
		    		 blnFlag=page("pgeBrowse").element("chkInternetPlan").click();
		    		 if(blnFlag)
		    		 {
		    			 waitForSync(2);
		    			 blnFlag=verifyTextContains("pgeBrowse","eleSelectedPlanDetails","TV") && verifyTextContains("pgeBrowse","eleSelectedPlanDetails",strSubPlan);
		    		 }
		    	 }
		    	 if(blnFlag && strSubPlan.equals("Security"))
		    	 {
		    		 blnFlag=page("pgeBrowse").element("chkSecurityPlan").click();
		    		 if(blnFlag)
		    		 {
		    			 waitForSync(2);
		    			 blnFlag=verifyTextContains("pgeBrowse","eleSelectedPlanDetails","TV") && verifyTextContains("pgeBrowse","eleSelectedPlanDetails",strSubPlan);
		    		 }
		    	 }
		    	 if(blnFlag && strSubPlan.equals("Phone"))
		    	 {
		    		 blnFlag=page("pgeBrowse").element("chkPhonePlan").click();
		    		 if(blnFlag)
		    		 {
		    			 waitForSync(2);
		    			 blnFlag=verifyTextContains("pgeBrowse","eleSelectedPlanDetails","TV") && verifyTextContains("pgeBrowse","eleSelectedPlanDetails",strSubPlan);
		    		 }
		    	 }
		     }
		     break;
			 
		     case "BundleWithSecurity":
		     blnFlag=selectViewAllBundlesAndVerify();
		     if(blnFlag)
		     {
		    	 blnFlag=page("pgeBrowse").element("chkSecurityPlan").click();
		    	 if(blnFlag && strSubPlan.equals("Internet"))
		    	 {
		    		 blnFlag=page("pgeBrowse").element("chkInternetPlan").click();
		    		 if(blnFlag)
		    		 {
		    			 waitForSync(2);
		    			 blnFlag=verifyTextContains("pgeBrowse","eleSelectedPlanDetails","Security") && verifyTextContains("pgeBrowse","eleSelectedPlanDetails",strSubPlan);
		    		 }
		    	 }
		    	 if(blnFlag && strSubPlan.equals("TV"))
		    	 {
		    		 blnFlag=page("pgeBrowse").element("chkTVPlan").click();
		    		 if(blnFlag)
		    		 {
		    			 waitForSync(2);
		    			 blnFlag=verifyTextContains("pgeBrowse","eleSelectedPlanDetails","Security") && verifyTextContains("pgeBrowse","eleSelectedPlanDetails",strSubPlan);
		    		 }
		    	 }
		    	 if(blnFlag && strSubPlan.equals("Phone"))
		    	 {
		    		 blnFlag=page("pgeBrowse").element("chkPhonePlan").click();
		    		 if(blnFlag)
		    		 {
		    			 waitForSync(2);
		    			 blnFlag=verifyTextContains("pgeBrowse","eleSelectedPlanDetails","Security") && verifyTextContains("pgeBrowse","eleSelectedPlanDetails",strSubPlan);
		    		 }
		    	 }
		     }
		     break;
			
		     case "BundleWithPhone":
		     blnFlag=selectViewAllBundlesAndVerify();
		     if(blnFlag)
		     {
		    	 blnFlag=page("pgeBrowse").element("chkPhonePlan").click();
		    	 if(blnFlag && strSubPlan.equals("Internet"))
		    	 {
		    		 blnFlag=page("pgeBrowse").element("chkInternetPlan").click();
		    		 if(blnFlag)
		    		 {
		    			 waitForSync(2);
		    			 blnFlag=verifyTextContains("pgeBrowse","eleSelectedPlanDetails","Phone") && verifyTextContains("pgeBrowse","eleSelectedPlanDetails",strSubPlan);
		    		 }
		    	 }
		    	 if(blnFlag && strSubPlan.equals("TV"))
		    	 {
		    		 blnFlag=page("pgeBrowse").element("chkTVPlan").click();
		    		 if(blnFlag)
		    		 {
		    			 waitForSync(2);
		    			 blnFlag=verifyTextContains("pgeBrowse","eleSelectedPlanDetails","Phone") && verifyTextContains("pgeBrowse","eleSelectedPlanDetails",strSubPlan);
		    		 }
		    	 }
		    	 if(blnFlag && strSubPlan.equals("Security"))
		    	 {
		    		 blnFlag=page("pgeBrowse").element("chkSecurityPlan").click();
		    		 if(blnFlag)
		    		 {
		    			 waitForSync(2);
		    			 blnFlag=verifyTextContains("pgeBrowse","eleSelectedPlanDetails","Phone") && verifyTextContains("pgeBrowse","eleSelectedPlanDetails",strSubPlan);
		    		 }
		    	 }
		     }
		     break;
		     
		     default:
		     System.out.println("Not in the list");
		     break;
		   }
		   
		   if(blnFlag)
		   {
			   if(driver.toString().contains("internet explorer"))
				   ((JavascriptExecutor) driver).executeScript("arguments[0].click();",driver.findElement(By.xpath("//*[@class='title']//span[contains(text(),'"+strCustomizePlan+"')]//following::div[@class='bottom-box']//button[text()='Customize']")));
			   else
				   driver.findElement(By.xpath("//*[@class='title']//span[contains(text(),'"+strCustomizePlan+"')]//following::div[@class='bottom-box']//button[text()='Customize']")).click();
			   waitForSync(5);
			   blnFlag=verifyElementExist("pgeCustomizeOrder","eleCustomizeBreadcrum");
			   if(!blnFlag)
				   ErrDescription="Failed to navigate to 'Customize Order' page.";
		   }
		   else
			   ErrDescription="Failed to selected Bundle/Individual Plan on 'Browse' page.";
		   
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while selecting Individual and Bundle Plans.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyExistsAndSelect
	// ''@Objective: This Function verifies if field exists and then selects a value from it.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strURL
	// ''@Param Desc: Application URL
	// ''@Param Name: strPageName
	// ''@Param Desc: Page Name which has to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyExistsAndClick("pgeAccountOverview","lnkAddUpgradeService","2","ByValue",true,"pgeCustomizeOrder","lnkReturnToAccSummary");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyExistsAndSelect(String pgePage,String eleElement,String strSelectValue,String strSelectType,boolean deselect,String pgeNewPage,String eleNewElement)
	{
		boolean blnFlag = false;
		try
		{    
			blnFlag=page(pgePage).element(eleElement).exist();
			if(blnFlag)
			{
				blnFlag=page(pgePage).element(eleElement).select(strSelectValue,strSelectType,deselect);
				if(blnFlag)
				{
					waitForSync(4);
					blnFlag=page(pgeNewPage).element(eleNewElement).exist();
					if(!blnFlag)
						ErrDescription = "Failed to navigate to the next page.";
				}
			}
			else
			{
				blnFlag=true;
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying existance and selecting.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: selectDateAndTimeForInstallation
	// ''@Objective: This Function selects Service Installation date and time from the active/current month on 'Service & Installation' page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: Page on which Calendar exists
	// ''@Param Name: eleElement
	// ''@Param Desc: Select Date & Time Button
	// ''@Param Name: eleNewElement
	// ''@Param Desc: Field populated on Data/Time Selection
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= selectDateAndTimeForInstallation("pgeConfirmOrder","btnSelectDateForSecurity","eleSelectedDateForSecurity");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean selectDateAndTimeForInstallation(String pgePage,String eleElement1,String eleElement2,String eleNewElement)
	{
		boolean blnFlag = false;
		try
		{
			//blnFlag = page(pgePage).element(eleElement).click();
			blnFlag = verifyExistsAndClick(pgePage,eleElement1,pgePage,eleElement2,"pgeServiceSchedule","btnOKCalendar");
			if(blnFlag)
			{
				waitForSync(3);
				blnFlag = page("pgeServiceSchedule").element("eleSelectDate").exist();
				if(!blnFlag)
				{
					page("pgeServiceSchedule").element("btnNextMonth").waitForElement(5);
					blnFlag = page("pgeServiceSchedule").element("btnNextMonth").click();
					waitForSync(1);
					if(blnFlag)
					{
						page("pgeServiceSchedule").element("eleSelectDate").waitForElement(5);
						blnFlag = page("pgeServiceSchedule").element("eleSelectDate").click();
						waitForSync(1);
					}
				}
				else
				{
					blnFlag = page("pgeServiceSchedule").element("eleSelectDate").click();
					waitForSync(1);
				}
				
				if(blnFlag)
				{
					blnFlag=page("pgeServiceSchedule").element("rdoPreferredTimeOfDay").click();
					if(blnFlag)
					{
						blnFlag=page("pgeServiceSchedule").element("btnOKCalendar").click();
						if(blnFlag)
						{
							page("pgeServiceSchedule").element("btnSelectDateTime").waitForElement(10, 1);
							blnFlag = page(pgePage).element(eleNewElement).exist();
							if(!blnFlag)
								ErrDescription = "Failed to display the selected date and time on Schedule page.";
						}
						else
							ErrDescription = "Failed to click on 'OK' button from Calendar pop-up.";
					}
					else
						ErrDescription = "Failed to select 'Preffered Time of the day' radio-button from Calendar pop-up.";
				}
				else
					ErrDescription = "Failed to select date from the Calendar pop-up.";
			}
			else
				ErrDescription = "Failed to click on 'Select Date & Time' button.";
			
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while selecting Installation Date and Time.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyServiceChargesOnConfirmOrder
	// ''@Objective: This Function verifies selected Services and their $ amount when a plan is added/upgraded on Confirm Order page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strPlanAction
	// ''@Param Desc: 'Phone' plan to be added/upgraded
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyServiceChargesOnConfirmOrder("pgeConfirmOrder","",strService[0],intServicePosition);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyMultipleServicesChargesOnCustomize(String pgePage,String eleService,String strService)
	{
		boolean blnFlag = false;
		String strXpath = "";
		String strText = "";
		int i=0;
		String [] strServices = strService.split(",");
		
		try
		{   
			strXpath = page(pgePage).element(eleService).GetXpath();
			if(!strXpath.isEmpty())
			{
				//driver.findElements(By.xpath(strXpath)).
				List<WebElement> listOfElements = driver.findElements(By.xpath(strXpath));
				if(!listOfElements.isEmpty())
				{
					for(i=0;i<strServices.length;i++)
					{
						for(WebElement element: listOfElements)
						{
							strXpath = page(pgePage).element(eleService).GetXpath();
							strText = element.getText();
							if(strText.contains(strServices[i]))
							{
								strXpath = strXpath+"[text()='"+strText+"']//following::div[1]";
								blnFlag = driver.findElement(By.xpath(strXpath)).getText().contains("$");
								if(blnFlag)
										break;
							}
						}
					}
					if(!blnFlag)
						ErrDescription="Failed to display charges with $ amount for selected Services.";
				}
				else
					ErrDescription="Failed to display selected Services.";
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying services and charges.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyServiceChargesOnConfirmOrder
	// ''@Objective: This Function verifies selected Services and their $ amount when a plan is added/upgraded on Confirm Order page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strPlanAction
	// ''@Param Desc: 'Phone' plan to be added/upgraded
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyServiceChargesOnConfirmOrder("pgeConfirmOrder","",strService[0],intServicePosition);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean selectMultipleFeatures(String pgePage,String strService)
	{
		boolean blnFlag = false;
		int i=0;
		String strXpath = "";
		String [] strServices = strService.split(",");
		Actions action=new Actions(driver);
		
		try
		{   
			if(driver.toString().contains("internet explorer"))
			{
				action.sendKeys(Keys.PAGE_DOWN).build().perform();
				for(i=0;i<strServices.length;i++)
				{
					strXpath = "//*[text()='"+strServices[i]+"']//following::input[1]";
					WebElement ele = driver.findElement(By.xpath(strXpath));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();",ele);
					waitForSync(3);
					blnFlag=true;
				}
			}
			else
			{
				for(i=0;i<strServices.length;i++)
				{
					strXpath = "//*[text()='"+strServices[i]+"']//following::input[1]";
					driver.findElement(By.xpath(strXpath)).click();
					waitForSync(3);
					blnFlag=true;
				}
			}
			if(!blnFlag)
				ErrDescription="Failed to select all the equipments.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying services and charges.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: getTextAndSaveDictionaryVal
	// ''@Objective: This Function fetches from the element and saves it to a dictionary with specified keyname.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: Page on which element exists
	// ''@Param Name: eleElement
	// ''@Param Desc: Element from which text has to be fetched
	// ''@Param Name: strKeyName
	// ''@Param Desc: Key Name of the dictionary object
	// ''@Param Name: strReplaceChar
	// ''@Param Desc: Characters to be replaced if any, from the fetched text
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= getTextAndSaveDictionaryVal("pgeCustomizeOrder","eleSubTotalCharges$","strSubTotalCharges","$");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean getTextAndSaveDictionaryVal(String pgePage,String eleElement,String strKeyName,String strReplaceChar)
	{
		boolean blnFlag = false;
		String strText ="";
		
		try
		{   
			if(strReplaceChar.isEmpty())
				strText=page(pgePage).element(eleElement).getText();
			else
				strText=page(pgePage).element(eleElement).getText().replace(strReplaceChar,"");
			
			if(!strText.isEmpty())
			{
				waitForSync(1);
				dicTestData.put(strKeyName, strText);
				blnFlag=true;
			}
			else
				ErrDescription="Failed to fetch text from the element";
			
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while fetching and saving the text.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: fill_BillingAddress
	// ''@Objective: This Function enters billing address details on Checkout page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: firstName
	// ''@Param Desc: lastNameL
	// ''@Param Name: phoneNumber
	// ''@Param Desc: dob
	// ''@Param Desc: ssn
	// ''@Param Desc: email
	// ''@Param Desc: isCardDetails
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= fill_BillingAddress(strFirstName, strLastName, strDOB, strPhoneNumber, strSSN, strEmail,true);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Niraj Kumar[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean fill_BillingAddress(String strFirstName, String strLastName, String strPhoneNumber, String strDOB, String strSSN,String strEmail, boolean blnCardDetails,boolean blnMulltipleIterations)
	{
		boolean blnFlag = false;
		try
		{
			if(blnMulltipleIterations)
			{
				String[] strName = strFirstName.split(" ");
				
				SimpleDateFormat format = new SimpleDateFormat("ddMMMYYYY");
				strFirstName= strName[0]+format.format(new Date())+" "+strName[1];
				dicTestData.put("strLocation",strName[1]);
				
				SimpleDateFormat format1 = new SimpleDateFormat("hhmm");
				strLastName = strName[0]+format.format(new Date())+" "+format1.format(new Date());
			}
			page("pgeProfilePage").element("txtFirstName").waitForElement(30, 1);
			blnFlag = page("pgeProfilePage").element("txtFirstName").exist();
			if (blnFlag) 
			{				
				blnFlag = page("pgeProfilePage").element("txtFirstName").type(strFirstName);
				if(blnFlag)
				{
					blnFlag = page("pgeProfilePage").element("txtLastName").type(strLastName);
					if(blnFlag)
					{
						blnFlag = page("pgeProfilePage").element("txtPhoneNumber").type(strPhoneNumber);
						if(blnFlag)
						{
							blnFlag = page("pgeProfilePage").element("txtDOB").type(strDOB);
							if(blnFlag)
							{
								blnFlag = page("pgeProfilePage").element("txtSSN").type(strSSN);
								if(blnFlag)
								{
									blnFlag = page("pgeProfilePage").element("txtEmail").type(strEmail);
									if(blnFlag)
									{
										if (blnCardDetails) 
										{
											page("pgeCheckout").element("chkNoThanksIwillPay").waitForElement(30, 1);
											waitForSync(3);
											blnFlag = page("pgeCheckout").element("chkNoThanksIwillPay").click();
											if(blnFlag)
											{
												blnFlag = page("pgeCheckout").element("btnContinueSubmit").click();
												waitForSync(9);
												//page("pgeServiceSchedule").element("btnSelectDateTime").waitForElement(30, 1);
												if (blnFlag) 
												{
													blnFlag=page("pgeServiceSchedule").element("eleSchedulePageTitle").exist();
													if(blnFlag)
													{
														blnFlag=page("pgeServiceSchedule").element("eleServiceAndInstallation").exist();
														if(!blnFlag)
															ErrDescription = "Failed to display 'Service & Installation' information.";
													}
													else
														ErrDescription = "Failed to navigate to 'Service Installation' page.";
												}
												else
													ErrDescription = "Failed to click on 'Continue' button.";
											}
											else
												ErrDescription = "Failed to select 'No, Thanks I will pay' check-box.";
										}
										else
										{
											blnFlag = page("pgeCheckout").element("btnContinueSubmit").click();
											waitForSync(9);
											page("pgeProfilePage").element("btnBackToHome").waitForElement(30, 1);
											if(blnFlag)
											{
												blnFlag=page("pgeProfilePage").element("btnBackToHome").exist();
												if(!blnFlag)
													ErrDescription = "Failed to display 'Thank you' page.";
											}
											else
												ErrDescription = "Failed to click on 'Continue' button.";
										}
									}
									else
										ErrDescription = "Failed to enter Email Address.";
								}
								else
									ErrDescription = "Failed to enter SSN.";
							}
							else
								ErrDescription = " Failed to enter DOB.";
						}
						else
							ErrDescription = "Failed to enter Phone Number.";
					}
					else
						ErrDescription = "Failed to enter Last Name.";
				}
				else
					ErrDescription = "Failed to enter First Name.";
			}
			else
				ErrDescription = "Failed to display 'Billing Address' section.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while entering Billing Address details.";
		}														
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: launchIEinPrivateMode
	// ''@Objective: This Function launches a new IE driver instance (after closing the current driver instance) after 
	// '' 			clearing all the cookies,cache and history (same as launching in private mode)
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= launchIEinPrivateMode();
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	@SuppressWarnings("deprecation")
	public void launchIEinPrivateMode()
	{
		try
		{
			driver.close();
			waitForSync(1);
			System.setProperty("webdriver.ie.driver", "./src/test/resources/drivers/IEDriverServer.exe");
	        DesiredCapabilities dc = DesiredCapabilities.internetExplorer();
	        dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
	        dc.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,true);
	        driver = new InternetExplorerDriver(dc);
		}
		catch (Exception e) 
		{
			logger.error(e.getMessage());
			ErrDescription= "Failed to launch the site on Browser.";
		}
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: selectFromMultipleFeatures
	// ''@Objective: This Function selects values from multiple lists/dropdowns
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: Page on which Features exists
	// ''@Param Name: strService
	// ''@Param Desc: List of features
	// ''@Param Name: strSelectBy
	// ''@Param Desc: Selection Option
	// ''@Param Name: strValue
	// ''@Param Desc: Value to be selected from each list
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= selectFromMultipleFeatures("pgeCustomizeOrder",strServices,strSelectType,strSelectValue);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean selectFromMultipleFeatures(String pgePage,String strService,String strSelectBy,String strValue)
	{
		boolean blnFlag = false;
		int i=0;
		String strXpath = "";
		String [] strServices = strService.split(",");
		Actions action=new Actions(driver);
		
		try
		{   
			if(driver.toString().contains("internet explorer") || driver.toString().contains("FirefoxDriver"))
			{
				action.sendKeys(Keys.PAGE_DOWN).build().perform();
			}
			for(i=0;i<strServices.length;i++)
			{
				strXpath = "//*[text()='"+strServices[i]+"']//following::select[1]";
				blnFlag=customSelectFromList(strXpath,strSelectBy,strValue);
				waitForSync(4);
			}
			if(!blnFlag)
				ErrDescription="Failed to select all the equipments.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while selecting features.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: logoutExistingCustomers
	// ''@Objective: This Function logs out existing customer from 'shopautomate' application and quits the driver instance 
	// ''		when running on IE browser.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= logoutExistingCustomers();
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean logoutExistingCustomers()
	{
		boolean blnFlag = false;

		try
		{
			blnFlag = clickAndVerify("pgeAccountOverview","lnkLogout","pgeMyAccountLogin","txtUserName");
			if(!blnFlag)
				ErrDescription="Failed to logout user from application.";
			
			if(driver.toString().contains("internet explorer"))
			{
				driver.quit();
			}
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while logging-out as existing customer.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: sendEmail
	// ''@Objective: This Function sends specified file (as an attachment to specified recipients via email (smtp authentication)
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strFilePath
	// ''@Param Desc: Path/Location of the file
	// ''@Param Name: strFileName
	// ''@Param Desc: Name of the attachment
	// ''@Param Name: strFromUser
	// ''@Param Desc: Email Address of the sender
	// ''@Param Name: strFromPassword
	// ''@Param Desc: Password of the sender
	// ''@Param Name: strToUser
	// ''@Param Desc: Email Address of the recipient
	// ''@Param Name: subject
	// ''@Param Desc: Subject of the Email
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= sendEmail(strZipFile,strFileName,strFromUser,strFromPassword,strToUser,strSubject);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public static void sendEmail(String strFilePath,String strFileName,String strFromUser,String strFromPassword,String[] strToUser,String subject)
	{
        String strBody = "<b>Please find attached file with generated Order Numbers for different locations for 'New Customers'</b>.";
		try
		{
			Properties props = System.getProperties();
	        String host = "smtp.gmail.com";
	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.smtp.host", host);
	        props.put("mail.smtp.user", strFromUser);
	        props.put("mail.smtp.password", strFromPassword);
	        props.put("mail.smtp.port", "587");
	        props.put("mail.smtp.auth", "true");
	
	        Session session = Session.getDefaultInstance(props);
	        MimeMessage message = new MimeMessage(session);
	        

            message.setFrom(new InternetAddress(strFromUser));
            InternetAddress[] toAddress = new InternetAddress[strToUser.length];

            // To get the array of addresses
            for( int i = 0; i < strToUser.length; i++ ) {
                toAddress[i] = new InternetAddress(strToUser[i]);
            }

            for( int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            message.setSubject(subject);
            message.setText(strBody);
            
            BodyPart messageBodyPart = new MimeBodyPart();
            MimeMultipart multipart = new MimeMultipart();
            DataSource dataSource = new FileDataSource(strFilePath);
            messageBodyPart.setDataHandler(new DataHandler(dataSource));
            messageBodyPart.setFileName(strFileName);
            multipart.addBodyPart(messageBodyPart);
            //messageBodyPart.setText(body);

            message.setContent(multipart);
            
            Transport transport = session.getTransport("smtp");
            transport.connect(host, strFromUser, strFromPassword);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
		}
		catch (Exception e)
		{
			logger.error(e.getMessage());
		}
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: writeDataToExcel
	// ''@Objective: This Function updates specified data in specified file.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strFilePath
	// ''@Param Desc: Path/Location of the file
	// ''@Param Name: strValues
	// ''@Param Desc: Data to be updated in the file
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus=writeDataToExcel(strFilePath,dicTestOrders);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean writeDataToExcel(String strFilePath,HashMap<String,List<String>> strValues)
	{
		boolean blnFlag = false;
		int rowCount=0;

		try
		{
			FileInputStream in = new FileInputStream(new File(strFilePath));
			 // Blank workbook 
	        XSSFWorkbook workbook = new XSSFWorkbook(in); 

	        XSSFSheet  sheet = workbook.getSheet("TestOrders");

	        rowCount = sheet.getPhysicalNumberOfRows();
     
            // Iterate over data and write to sheet 
            Set<String> keyset = strValues.keySet();

            for (String key : keyset) 
            {
                List<String> objArr = strValues.get(key);
                if(!objArr.isEmpty())
                {
                	blnFlag=true;
                	for(String str : objArr)
	                {
                		String [] strVal = str.split(" ");
	                	Row row = sheet.createRow(rowCount);
	                	Cell cell = row.createCell(0);
	                	Cell cell1 = row.createCell(1);
	                	cell.setCellValue(strVal[0]);
	                	cell1.setCellValue(strVal[1]);
	                	rowCount++;
	                }
                }
                else
                	ErrDescription="Failed to update data in Data File.";   
            }     
	        FileOutputStream out = new FileOutputStream(new File(strFilePath)); 
	        workbook.write(out); 
	        out.close();
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while updating data in specified file.";
		}
	    return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: addDictionaryValues
	// ''@Objective: This Function adds values to specified Keys in dictionary object.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strStringMap
	// ''@Param Desc: Hashmap/Dictionary Object to which Key,Value pair is added
	// ''@Param Name: strValue
	// ''@Param Desc: Value to be added
	// ''@Param Name: strKeyName
	// ''@Param Desc: Key to be added
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= addDictionaryValues(dicTestOrders,strOrderNumWithLocation,"strOrderNumber");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean addDictionaryValues(HashMap<String,List<String>> strStringMap,String strValue,String strKeyName)
	{
		boolean blnFlag = false;
		List<String> list;

		try
		{
			if(strStringMap.containsKey(strKeyName))
			{
			    // if the key has already been used,
			    // we'll just grab the array list and add the value to it
			    list = strStringMap.get(strKeyName);
			    list.add(strValue);
			} 
			else 
			{
			    // if the key hasn't been used yet,
			    // we'll create a new ArrayList<String> object, add the value
			    // and put it in the array list with the new key
			    list = new ArrayList<String>();
			    list.add(strValue);
			    strStringMap.put(strKeyName, list);
			}
			if(!list.isEmpty())
			{
				blnFlag=true;
			}
			else
				ErrDescription="Failed to add values to specified key.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while adding values to specified dictionary Keys.";
		}
		return blnFlag;
	}
			
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: compressFile
	// ''@Objective: This Function compresses specified file with the specified file name.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strFilePath
	// ''@Param Desc: Path/Location of the file
	// ''@Param Name: strZipFileName
	// ''@Param Desc: File name of the compressed file
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= compressFile(strFilePath,"TestOrders.zip");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean compressFile(String strFilePath,String strZipFileName)
	{
		boolean blnFlag = false;
		try
		{
			File file = new File(strFilePath);
	     
	      //create ZipOutputStream to write to the zip file
            FileOutputStream fos = new FileOutputStream(strZipFileName);
            ZipOutputStream zos = new ZipOutputStream(fos);
            //add a new Zip Entry to the ZipOutputStream
            ZipEntry ze = new ZipEntry(file.getName());
            zos.putNextEntry(ze);
            //read the file and write to ZipOutputStream
            FileInputStream fis = new FileInputStream(file);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = fis.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }
            
            //Close the zip entry to write to zip file
            zos.closeEntry();
            //Close resources
            zos.close();
            fis.close();
            fos.close();
            blnFlag=true;
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while compressing a file.";
		}
		return blnFlag;
	}
		
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: cleanExcelData
	// ''@Objective: This Function cleans data from specified excel sheet by removing data from all the filled rows.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strFilePath
	// ''@Param Desc: Path/Location of the file
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= cleanExcelData(strFilePath);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public static void cleanExcelData(String strFilePath)
	{
		int rowCount = 0;
		try
		{  
			FileInputStream in = new FileInputStream(new File(strFilePath));
			 // Blank workbook 
	        XSSFWorkbook workbook = new XSSFWorkbook(in); 

	        XSSFSheet  sheet = workbook.getSheet("TestOrders");
	        rowCount = sheet.getPhysicalNumberOfRows();
	        
	        for(int i =1;i<rowCount;i++)
	        {
	        	Row row = sheet.createRow(i);
	        	sheet.removeRow(row);
	        }
	        
	        FileOutputStream out = new FileOutputStream(new File(strFilePath)); 
            workbook.write(out); 
            out.close();
		}
		catch (Exception e)
		{
			logger.error(e.getMessage());
		}	
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: clickAndVerifyNotExists
	// ''@Objective: This Function clicks on the specified object and verifies an element should not exist on navigated page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: eleElement
	// ''@Param Name: pgeNewPage
	// ''@Param Desc: eleNewElement
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= clickAndVerifyNotExists("pgeMyAccountLogin","btnLogin","pgeAccountOverview","eleAccountSummary");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean clickAndVerifyNotExists(String pgePage, String eleElement, String pgeNewPage, String eleNewElement)
	{
		boolean blnFlag = false;
		try
		{   
			blnFlag=page(pgePage).element(eleElement).click();
			if(blnFlag)
			{
				waitForSync(4);
				blnFlag = !verifyElementExist(pgeNewPage,eleNewElement);
				if(!blnFlag)
					ErrDescription = "Failed to verify element does not exist on the navigated page.";
			}
			else
				ErrDescription = "Unable to perform click action.";
		}
	
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while clicking and verifying action.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyElementsText
	// ''@Objective: This Function verifies if a field contains specified text.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: Page Name which has to be verified
	// ''@Param Name: eleElement
	// ''@Param Desc: Element whose text has to be verified
	// ''@Param Name: strText
	// ''@Param Desc: Text to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyElementsText("pgeBrowse","eleSelectedPlanDetails","Internet");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyElementsText(String pgePage, String eleElement, String strText)
	{
		boolean blnFlag = false;

		try
		{      
			int intSize = driver.findElements(By.xpath(page(pgePage).element(eleElement).GetXpath())).size();
			if(intSize>0)
			{
				List<WebElement> element = driver.findElements(By.xpath(page(pgePage).element(eleElement).GetXpath()));
				for(int i=0;i<=intSize;i++)
				{
					blnFlag=element.get(i).getText().contains(strText);
					if(blnFlag)
						break;
				}
			}
			if(!blnFlag)
						ErrDescription = "Unable to verify text '"+strText+"' on the navigated page.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying text.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: clickAndVerifyText
	// ''@Objective: This Function clicks on the specified object and verifies populated element's text.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: eleElement
	// ''@Param Name: pgeNewPage
	// ''@Param Desc: eleNewElement
	// ''@Param Desc: strText
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= clickAndVerifyText("pgeMyAccountLogin","btnLogin","eleAccountSummary","Account Summary");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean clickAndVerifyText(String pgePage, String eleElement,String eleNewElement,String strText)
	{
		boolean blnFlag = false;
		try
		{   
			blnFlag=page(pgePage).element(eleElement).click();
			if(blnFlag)
			{
				waitForSync(4);
				blnFlag = verifyElementsText(pgePage,eleNewElement,strText);
				if(!blnFlag)
					ErrDescription = "Unable to verify navigated page.";	
			} 
			else
				ErrDescription = "Unable to perform click action.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while clicking and verifying Text.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: clickElementAndVerify
	// ''@Objective: This Function clicks on the specified object and if clicks fails,click the same element using javascriptexecutor 
	// ''@			and verifies populated element's text.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: eleElement
	// ''@Param Name: pgeNewPage
	// ''@Param Desc: eleNewElement
	// ''@Param Desc: strText
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= clickElementAndVerify("pgeMyAccountLogin","btnLogin","eleAccountSummary","Account Summary");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean clickElementAndVerify(String pgePage, String eleElement, String pgeNewPage, String... eleNewElement)
	{
		boolean blnFlag = false;
		try
		{   
			blnFlag=page(pgePage).element(eleElement).click();
			if(!blnFlag)
			{
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",driver.findElement(By.xpath(page(pgePage).element(eleElement).GetXpath())));
				blnFlag=true;
			}
			if(blnFlag)
			{
				waitForSync(4);
				blnFlag = verifyElementExist(pgeNewPage,eleNewElement[0]);
				if(blnFlag)
					blnFlag = verifyElementExist(pgeNewPage,eleNewElement[1]);
				if(!blnFlag)
					ErrDescription = "Unable to verify navigated page.";	
			}
			else
				ErrDescription = "Unable to perform click action.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while clicking and verifying Text.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: enterDataAndClick
	// ''@Objective: This Function enters specified data and click on the specified object and verifies navigated page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: eleElement
	// ''@Param Name: strData
	// ''@Param Desc: btnClick
	// ''@Param Desc: strNewPage
	// ''@Param Desc: eleNewElement
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= enterDataAndClick("pgeHomePage","txtAptNumber",strAptNumber,"btnSubmit","pgeBrowse","lblAptNumber","btnCartIcon");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean enterDataAndClick(String pgePage, String eleElement, String strData,String btnClick,String strNewPage,String eleNewElement)
	{
		boolean blnFlag = false;
		try
		{      
			blnFlag=page(pgePage).element(eleElement).type(strData);
			if(blnFlag)
			{
				waitForSync(2);
				blnFlag=page(pgePage).element(btnClick).click();
				if(blnFlag)
				{
					waitForSync(4);
					blnFlag = verifyElementExist(strNewPage,eleNewElement);
					if (!blnFlag)
						ErrDescription = "Unable to verify navigated page.";
				}
				else
					ErrDescription = "Unable to perform click action.";	
			}
			else
				ErrDescription = "Unable to enter Data in the field.";
		}

		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while entering data and verifying action.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: waitForSync
	// ''@Objective: This Function waits for the specified amount of time in milliseconds.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= waitForSync(1);
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean waitForElement(String pgePage,String eleElement,int intWaitTime)
	{	
		boolean blnFlag = false;
		try
		{
			WebDriverWait wait=new WebDriverWait(driver,intWaitTime);
			// Wait till the element is not visible
			WebElement element=wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(page(pgePage).element(eleElement).GetXpath())));
			blnFlag=element.isDisplayed();

		}
		catch (Exception e)
		{
			logger.error(e.getMessage());
			ErrDescription="Error occured while waiting for an element.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyInformationModalElements
	// ''@Objective: This Function verifies if Information modal pop-up contains Images or Video or both on Browse page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name:N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyInformationModalElements();
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[30-Dec-2018]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyInformationModalElements()
	{
		boolean blnFlag = false;
		try
		{ 
			blnFlag=verifyElementExist("pgeBrowse","eleModalVideo") || verifyElementExist("pgeBrowse","eleModalImage");
			if(!blnFlag)
				ErrDescription="Failed to display Images,Videos on Information modal pop-up.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying element's existence on Information Modal.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifymultipleProductsUnderCart
	// ''@Objective: This Function verifies multiple products added under cart on 'Customize Order' page.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: strPlanType
	// ''@Param Desc: Plan Type added under cart (Internet/TV/Phone/Security)
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifymultipleProductsUnderCart("TV");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifymultipleProductsUnderCart(String pgePage,String eleElement,String strPlanType,String strProductName)
	{

		boolean blnFlag = false;
		
		try
		{
			page("pgeProfilePage").element("btnCartIcon").waitForElement(30,10);
			blnFlag = page("pgeProfilePage").element("btnCartIcon").click();
			if(blnFlag)
			{
				waitForSync(3);
				if(strPlanType.equals("Phone"))
				{
					blnFlag=verifyElementsText(pgePage,eleElement,strProductName);
					if(!blnFlag)
					{
						ErrDescription="Failed to verify product '"+strProductName+"' under 'Cart' for plan '"+strPlanType+"'.";
					}
					else
					{
						blnFlag = page("pgeProfilePage").element("btnCartIcon").click();
						waitForSync(3);
					}
				}
			}
			else
				ErrDescription="Failed to click on 'Cart' icon.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying cart items.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyEmptyCart
	// ''@Objective: This Function verifies Cart is empty.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: N/A
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyEmptyCart();
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean EmptyCart()
	{
		boolean blnFlag = false;
		String strCount ="";
		
		try
		{   
			waitForElement("pgeBrowse","eleCartCount",40);
			strCount=page("pgeBrowse").element("eleCartCount").getText();
			if(!strCount.equals("0"))
			{
				blnFlag = clickAndVerify("pgeBrowse","btnCartIcon","pgeBrowse","btnDeleteCart");
				if(blnFlag)
				{
					blnFlag = clickAndVerify("pgeBrowse","btnDeleteCart","pgeBrowse","btnCartIcon");
					if(blnFlag)
					{
						blnFlag = clickAndVerify("pgeBrowse","btnCartIcon","pgeBrowse","eleCartEmptyMsg");
						if(!blnFlag)
							ErrDescription = "Unable to verify 'Your Cart is Empty' message.";
					}
					else
						ErrDescription = "Failed to click on 'Delete' icon.";
				}
				else
					ErrDescription = "Failed to click on 'Cart' icon.";
			}
			else
				ErrDescription = "Failed to display any products in Cart.";
		}

		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying empty cart.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyElementsText
	// ''@Objective: This Function verifies if a field contains specified text.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: Page Name which has to be verified
	// ''@Param Name: eleElement
	// ''@Param Desc: Element whose text has to be verified
	// ''@Param Name: strText
	// ''@Param Desc: Text to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyElementsText("pgeBrowse","eleSelectedPlanDetails","Internet");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyMultipleElementsText(String pgePage, String eleElement,String strText)
	{
		boolean blnFlag = false;
		String[] lstValues = strText.split(",");

		try
		{      
			int intSize = driver.findElements(By.xpath(page(pgePage).element(eleElement).GetXpath())).size();
			if(intSize>0)
			{
				List<WebElement> element = driver.findElements(By.xpath(page(pgePage).element(eleElement).GetXpath()));
				for(int i=0;i<=intSize-1;i++)
				{
					blnFlag=element.get(i).getText().contains(lstValues[i]);
				}
			}
			if(!blnFlag)
				ErrDescription = "Unable to verify text on the navigated page.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying text.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: verifyElementsText
	// ''@Objective: This Function verifies if a field contains specified text.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Param Name: pgePage
	// ''@Param Desc: Page Name which has to be verified
	// ''@Param Name: eleElement
	// ''@Param Desc: Element whose text has to be verified
	// ''@Param Name: strText
	// ''@Param Desc: Text to be verified
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= verifyElementsText("pgeBrowse","eleSelectedPlanDetails","Internet");
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Neha Chauhan[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean verifyMultipleElements(String pgePage, String eleElement)
	{
		boolean blnFlag = false;

		try
		{  
			List<WebElement> element = driver.findElements(By.xpath(page(pgePage).element(eleElement).GetXpath()));
			if(element.size()>0)
			{
				for(int i=0;i<=element.size()-1;i++)
				{
					blnFlag=element.get(i).isDisplayed();
				}
			}
			if(!blnFlag)
				ErrDescription = "Unable to verify elements on the navigated page.";
		}
		catch (Exception e)
		{
			blnFlag=false;
			logger.error(e.getMessage());
			ErrDescription="Error occurred while verifying text.";
		}
		return blnFlag;
	}
	
	// ''@###########################################################################################################################
	// ''@Function ID:
	// ''@Function Name: NavigateFromConfirmToThankYou
	// ''@Objective: This Function Clicks the "I Agree" Checkbox and Clicks on Continue button.
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Return Desc:
	// ''@ Success - True
	// ''@ Failure - False
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Example: blnStatus= NavigateFromConfirmToThankYou();
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Created by[Date]: Niraj Kumar[20-Oct-2016]
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@Reviewed by[Date]:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@History Notes:
	// ''@---------------------------------------------------------------------------------------------------------------------------
	// ''@###########################################################################################################################
	public boolean navigateFromConfirmToThankYou()
	{
		boolean blnFlag = false;
		try
		{
			page("pgeConfirmOrder").element("btnContinue").waitForElement(30, 1);
			blnFlag=verifyElementExist("pgeConfirmOrder", "chkIagreeTermsConditions");
			if(blnFlag)
			{	
				blnFlag=clickAndVerify("pgeConfirmOrder", "chkIagreeTermsConditions", "pgeConfirmOrder", "btnContinue");
				if(!blnFlag)
					ErrDescription = "Failed to click on 'I agree with Terms & Condition' checkbox.";
			}
			blnFlag=clickAndVerify("pgeConfirmOrder", "btnContinue", "pgeThankYou", "eleThankuOrder");
			if(blnFlag)
			{
				blnFlag=verifyElementExist("pgeThankYou", "eleThankuOrder");
				if(blnFlag)
				{
					dicTestData.put("strOrderTxt&Number", "'Thank you' page is displayed with following details: '<b>"+page("pgeThankYou").element("eleThankuOrder").getText()
							+ page("pgeThankYou").element("eleOrderNumber").getText()+"</b>'");
					String[] strOrderNumber = page("pgeThankYou").element("eleOrderNumber").getText().split("is");
					dicTestData.put("strOrderNumber",strOrderNumber[1].replaceAll("\\s","").replace(".",""));
				}
				else
					ErrDescription = "Failed to display 'Thank For Your Order' text.";

			}
			else
				ErrDescription = "Failed to click on 'Continue' button";
	}
	catch (Exception e)
	{
		blnFlag=false;
		logger.error(e.getMessage());
		ErrDescription="Error occurred while Navigating from Confirm to Thank You Page.";
	}														
	return blnFlag;
	}
}

