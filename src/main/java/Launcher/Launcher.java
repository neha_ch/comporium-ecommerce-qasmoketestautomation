package Launcher;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.testng.TestNG;
import org.testng.collections.Lists;

import Excel.ExcelRead;
import Utils.ReadProperty;
import Utils.CreateTestNGFile;
import Utils.Slack;
import Utils.mail;
import libraries.WebFunclib;
import Reporter.Report;

public class Launcher extends LauncherBase{
	static Logger logger = Logger.getLogger(Launcher.class);
	public static void main(String[] args) throws Exception {
		SimpleDateFormat  dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
		SimpleDateFormat  dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.ENGLISH);		
		ExcelRead excelRead = new ExcelRead();	
		CreateTestNGFile testng = new CreateTestNGFile();
		Date date = new Date();
		Report report = new Report(null,null);
		boolean blnflag=false;
		//@ Initialize log4j properties
		PropertyConfigurator.configure("src/test/resources/log4j.properties");
		logger.info("Automation Execution Begins");
		//@ Read Property File
		ReadProperty.propertyFile("src/test/resources/framework-config.properties");
		if (ReadProperty.dictProjectVar.size() == 0) {
			logger.error("Error while storing project property config data");
			return ;
		}		
		// Creating Hashmap of config file 
		dicConfig = excelRead.exlDictionary(ReadProperty.dictProjectVar.get("config") + "Config.xlsx","Config", 1, 2);		
		if (dicConfig.size() == 0) {
			logger.error("Error while storing excel config data");
			return ;
		}
		//@ create project execution date and time 
		Launcher.dicConfig.put("ProjectStartTime", dateFormat.format(date));
		Launcher.dicConfig.put("ProjectStartDateTime", dateTimeFormat.format(date));
		//@ Report Initialization
		report.intializeProject();
		logger.info("Reading TestSuite.");
		// Reading Suite(s) file and creating Hashmap
		dicTestSuite = excelRead.exlReadTestSuite(dicConfig.get("TestSuiteFileName"));
//		excelRead.exlDictionary(fileName, sheetName, keyValueColumnIndex)
		if (dicTestSuite.size() == 0) {
			logger.error("Error while storing TestSuite data OR Make Test case with 'Y' status ");
			return;
		}		
		logger.info("Creating testng.xml");
		// Creating TestNG xml
		blnflag = testng.createTestNGFile(dicConfig,dicTestSuite,false);
		excelRead.updateExcel("testuitereset","","");
		logger.info("Reading CommonValues from MasterTestData.");
		// Creating Hashmap of Common values sheet   
		dicCommValues = excelRead.exlDictionary(ReadProperty.dictProjectVar.get("testdata")+ dicConfig.get("MasterTestDataPath"),"CommonValues", 1, 2);
		//Got the Raw data for Report
		///@ New Report//////////
		Report.initializeNewReport();
		try {
			if(blnflag)
			{
				TestNG testngsuite = new TestNG();
				File file=new File(ReadProperty.dictProjectVar.get("TestNG"));
				List<String> suites = Lists.newArrayList();
				String files[] = file.list();
				// Adding all xmls to Suite
				for(String suiteFile : files)
				{
					suites.add(ReadProperty.dictProjectVar.get("TestNG")+suiteFile);
				}
				if(suites.size()>=1)
				{
					// Start execution
					testngsuite.setTestSuites(suites);
					testngsuite.run();
					date = new Date();	
					Launcher.dicConfig.put("ProjectEndTime", dateFormat.format(date));
					Launcher.dicConfig.put("ProjectEndDateTime", dateTimeFormat.format(date));
				}
				else
				{
					logger.error(String.format("No testng's xml file found"));
					blnflag=false;
				}
				Report.newReport();
				Report.reportDicProcess();
				report.createSummaryReportData();
			}
			else
			{
				logger.error(String.format("Issue while creating Project Default configuration"));
			}
			///@ End New Report/////////
			Report.EndNewReport();
			if(dicConfig.get("IsMultipleIterations").equalsIgnoreCase("Yes"))
			{
				String strZipFile = "./src/data/Data/TestDataOrders.zip";
				String strFileName = "TestData_OrderNumbersWithLocations.zip";
				String strFromUser = "comporiumautomation";
				String strFromPassword = "Pyramid123";
				String strSubject = "New Order Numbers generated through Automation";
				String[] strToUser = { "neha.chauhan@pyramidconsultinginc.com","ruchit.talwandi@pyramidconsultinginc.com"};
				String strFilePath = "./src/data/Data/TestDataOrders.xlsx";
				//WebFunclib.sendEmail(strZipFile,strFileName,strFromUser,strFromPassword,strToUser,"New Order Numbers generated through Automation");
				WebFunclib.sendEmail(strZipFile,strFileName,strFromUser,strFromPassword,strToUser,strSubject);
				WebFunclib.cleanExcelData(strFilePath);
			}
			if(dicConfig.get("SendSummaryMail").equalsIgnoreCase("Yes")){
				mail sendmail= new mail();
				sendmail.sendFromGmail();
			}
			if(dicConfig.get("IsSlackEnable").equalsIgnoreCase("Yes")){
				Slack sendMessage= new Slack();
				sendMessage.sendSlackMessage();
			}
		}
		catch(Exception e) {
			logger.error(String.format("Issue while executing testng.\n Error is :%s",e.getMessage()));
			e.printStackTrace();
		}
	}
}