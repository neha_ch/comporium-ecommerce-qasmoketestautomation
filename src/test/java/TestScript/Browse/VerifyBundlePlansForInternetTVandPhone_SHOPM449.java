package TestScript.Browse;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import Launcher.Launcher;
import libraries.WebFunclib;
public class VerifyBundlePlansForInternetTVandPhone_SHOPM449 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;
	
	@Test
	public void VerifyBundlePlansForInternetTVandPhone(ITestContext testContext) throws InterruptedException 
	{
		// ########################################################################################################
		// # Test Case ID: SHOPM-449
		// # Test Case: VerifyBundlePlansForInternetTVandPhone
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify plan details when Internet,TV and Phone plans are selected as bundle plans on Browse page.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-19-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################
		
		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strPlanNames = dicTestData.get("strPlanNames");
		
		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium account exists, Street Address: '<b>"+strStreetAddressData+"</b>'";
		strActualResult = "'Zip code: '<b>"+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}	

		// ########################################################################################################
		// Step 4 - "Click on 'No, I'm Moving here'
		// "Click on 'No, I'm Moving here'
		// ########################################################################################################
		strStepDesc = "Click on 'No, I'm Moving here' from the pop-up.";
		strActualResult = "Fields 'Selected Service Address', 'Change Address','highlighted Bundle Plans','Individual Plans' and 'Cart section' are displayed.";
		blnStepRC = clickOnAddressConformationPopup("btnIamMovingHere",strStreetAddressData);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}		
		
		// ########################################################################################################
		// Step 5 - Click on 'View All Bundles' check-box
		// Click on 'View All Bundles' check-box
		// ########################################################################################################
		strStepDesc = "Click on 'View All Bundles' check-box and verify 'I am looking for:' section.";
		strActualResult = "Check-boxes for each bundle type '<b>TV,Internet,Phone</b>' and '<b>Security</b>' are displayed in 'I am looking for' section.";
		blnStepRC = selectViewAllBundlesAndVerify();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 6 - Select 'Internet' plan type
		// Select 'Internet' plan type
		// ########################################################################################################
		strStepDesc = "Select '<b>Internet</b>' plan type on 'Browse' page.";
		strActualResult = "Plans for only 'Internet' are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerify("pgeBrowse","chkInternetPlan","pgeBrowse","eleInternetPlanDetails");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 7 - Select 'TV' plan type
		// Select 'TV' plan type
		// ########################################################################################################
		strStepDesc = "Select '<b>TV</b>' plan type on 'Browse' page.";
		strActualResult = "Plans for only 'TV' are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerifyText("pgeBrowse","chkTVPlan","elePlanPackageTitle","TV");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}

		// ########################################################################################################
		// Step 8 - Select 'Phone' plan type
		// Select 'Security' plan type
		// ########################################################################################################
		strStepDesc = "Select '<b>Phone</b>' plan type on 'Browse' page.";
		strActualResult = "Plans for 'Phone' plan type are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerifyText("pgeBrowse","chkPhonePlan","elePlanPackageTitle","Phone");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 9 - Verify 'Bundle Plan' with Internet,TV and Security
		// Verify 'Bundle Plan' with Internet,TV and Security
		// ########################################################################################################
		strStepDesc = "Verify populated bundle plan contains an 'Internet' plan.";
		strActualResult = "Populated Bundle Plans contains an 'Internet' plan on the 'Browse' page.";
		blnStepRC=verifyTextContains("pgeBrowse","eleSelectedPlanDetails","Internet");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 10 - Verify 'Bundle Plan' with Internet,TV and Security
		// Verify 'Bundle Plan' with Internet,TV and Security
		// ########################################################################################################
		strStepDesc = "Verify bundle plan populated contains a 'TV' plan.";
		strActualResult = "Populated Bundle Plans contains a 'TV' plan on the 'Browse' page.";
		blnStepRC=verifyTextContains("pgeBrowse","eleSelectedPlanDetails","TV");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 11 - Click on 'Customize' button
		// Click on 'Customize' button for the bundle Plan
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button for the bundle plan on 'Browse' page.";
		strActualResult = "'<b>Internet,Televison and Phone Package details</b>' are displayed on 'Customize Order' page.";
		blnStepRC=clickAndVerify("pgeBrowse","btnCustomize","pgeCustomizeOrder","eleInternetPlanHeader","eleTVPlanHeader");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 12 - Verify 'Internet' plan details
		//  Verify 'Internet' plan details
		// ########################################################################################################
		strStepDesc = "Verify 'Internet' plan details populated on 'Customize Order' page.";
		strActualResult = "'Internet' plan details are populated on 'Customize Order' page.";
		blnStepRC= page("pgeCustomizeOrder").element("eleInternetPlanHeader").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 13 - Verify 'Equipment' for Internet plan
		//  Verify 'Equipment' for Internet plan
		// ########################################################################################################
		strStepDesc = "Verify 'Equipment' section for 'Internet' plan type.";
		strActualResult = "'Equipments' section is displayed for 'Internet' plan type.";
		blnStepRC= page("pgeCustomizeOrder").element("eleEquipmentSection").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 14 - Verify 'Television' plan details
		//  Verify 'Television' plan details
		// ########################################################################################################
		strStepDesc = "Verify 'Television' plan details populated on 'Customize Order' page.";
		strActualResult = "'Television' plan details are populated on 'Customize Order' page.";
		blnStepRC= page("pgeCustomizeOrder").element("eleTVPlanHeader").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 15 - Verify 'Phone' plan details
		//  Verify 'Phone' plan details
		// ########################################################################################################
		strStepDesc = "Verify 'Phone' plan details populated on 'Customize Order' page.";
		strActualResult = "'Phone' plan details are populated on 'Customize Order' page.";
		blnStepRC= page("pgeCustomizeOrder").element("elePhonePlanHeader").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 16 - Verify selected 'Phone' Features
		//  Verify 'Phone' features
		// ########################################################################################################
		strStepDesc = "Verify selected 'Phone' plan's features populated under 'Telephone' section.";
		strActualResult = "Selected Phone plan's features are populated under 'Telephone' section.";
		blnStepRC= page("pgeCustomizeOrder").element("elePhonePlanFeatures").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 17 - Verify Charges for 'Internet,TV Plans'
		//  Verify 'Internet,TV Plans'
		// ########################################################################################################
		strStepDesc = "Verify Charges for 'Internet','TV' and 'Phone' plans are showing under 'Monthly Charges' section.";
		strActualResult = "Charges for only 'Internet','TV' and 'Phone' plans are displayed under 'Monthly Charges' section.";
		blnStepRC=verifySelectedPlansUnderMonthlyCharges(3,strPlanNames); 
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 18 - Verify 'Taxes & Fees'
		//  Verify 'Taxes & Fees'
		// ########################################################################################################
		strStepDesc = "Verify 'Taxes & Fees' section under 'One Time Charges' section.";
		strActualResult = "'Taxes & Fees' section is populated under 'One Time Charges' section.";
		blnStepRC= page("pgeCustomizeOrder").element("eleTaxes&Fees").exist();
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 19 - Verify 'Taxes & Fees'
		//  Verify 'Taxes & Fees'
		// ########################################################################################################
		strStepDesc = "Verify 'Taxes & Fees' Charges under 'One Time Charges' section.";
		strActualResult = "'Taxes & Fees' Charges is populated under 'One Time Charges' section.";
		blnStepRC= page("pgeCustomizeOrder").element("eleTaxesCharges").exist();
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}
