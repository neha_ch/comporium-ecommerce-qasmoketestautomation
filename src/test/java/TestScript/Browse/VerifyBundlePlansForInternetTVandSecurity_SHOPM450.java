package TestScript.Browse;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import Launcher.Launcher;
import libraries.WebFunclib;
public class VerifyBundlePlansForInternetTVandSecurity_SHOPM450 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;
	
	@Test
	public void VerifyBundlePlansForInternetTVandSecurity(ITestContext testContext) throws InterruptedException 
	{
		// ########################################################################################################
		// # Test Case ID: SHOPM-450
		// # Test Case: VerifyBundlePlansForInternetTVandSecurity
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify plan details when Internet,TV and Security is selected as bundle plans on Browse page.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-19-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################
		
		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strPlanNames = dicTestData.get("strPlanNames");
		
		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium account exists, Street Address: '<b>"+strStreetAddressData+"</b>'";
		strActualResult = "'Zip code: '<b>"+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}	

		// ########################################################################################################
		// Step 4 - "Click on 'No, I'm Moving here'
		// "Click on 'No, I'm Moving here'
		// ########################################################################################################
		strStepDesc = "Click on 'No, I'm Moving here' from the pop-up.";
		strActualResult = "Fields 'Selected Service Address', 'Change Address','highlighted Bundle Plans','Individual Plans' and 'Cart section' are displayed.";
		blnStepRC = clickOnAddressConformationPopup("btnIamMovingHere",strStreetAddressData);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}		
		
		// ########################################################################################################
		// Step 5 - Click on 'View All Bundles' check-box
		// Click on 'View All Bundles' check-box
		// ########################################################################################################
		strStepDesc = "Click on 'View All Bundles' check-box and verify 'I am looking for:' section.";
		strActualResult = "Check-boxes for each bundle type '<b>TV,Internet,Phone</b>' and '<b>Security</b>' are displayed in 'I am looking for' section.";
		blnStepRC = selectViewAllBundlesAndVerify();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 6 - Select 'Internet' plan type
		// Select 'Internet' plan type
		// ########################################################################################################
		strStepDesc = "Select '<b>Internet</b>' plan type on 'Browse' page.";
		strActualResult = "Plans for only 'Internet' are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerify("pgeBrowse","chkInternetPlan","pgeBrowse","eleInternetPlanDetails");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 7 - Select 'TV' plan type
		// Select 'TV' plan type
		// ########################################################################################################
		strStepDesc = "Select '<b>TV</b>' plan type on 'Browse' page.";
		strActualResult = "Plans for only 'TV' are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerify("pgeBrowse","chkTVPlan","pgeBrowse","eleTVPlanDetails");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 8 - Select 'Security' plan type
		// Select 'Security' plan type
		// ########################################################################################################
		strStepDesc = "Select '<b>Security</b>' plan type on 'Browse' page.";
		strActualResult = "Plans for only 'Security' are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerify("pgeBrowse","chkSecurityPlan","pgeBrowse","eleSecurityPlanDetails");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 9 - Verify 'Bundle Plan' with Internet,TV and Security
		// Verify 'Bundle Plan' with Internet,TV and Security
		// ########################################################################################################
		strStepDesc = "Verify bundle plan populated contains a 'TV' plan.";
		strActualResult = "Populated Bundle Plans contains a 'TV' plan on the 'Browse' page.";
		blnStepRC=verifyTextContains("pgeBrowse","eleSelectedPlanDetails","TV");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 10 - Verify 'Bundle Plan' with Internet,TV and Security
		// Verify 'Bundle Plan' with Internet,TV and Security
		// ########################################################################################################
		strStepDesc = "Verify populated bundle plan contains an 'Internet' plan.";
		strActualResult = "Populated Bundle Plans contains an 'Internet' plan on the 'Browse' page.";
		blnStepRC=verifyTextContains("pgeBrowse","eleSelectedPlanDetails","Internet");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 11 - Click on 'Customize' button
		// Click on 'Customize' button for an Internet Standard Plan
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button for an Internet Standard Plan on 'Browse' page.";
		strActualResult = "'<b>Internet,Security and Televison Package details</b>' are displayed on 'Customize Order' page.";
		blnStepRC=clickAndVerify("pgeBrowse","btnCustomTVIntSecBundle","pgeCustomizeOrder","eleTVinternetSecurityPlans","eleTVPlanHeader");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 12 - Verify 'Internet' plan add-ons
		//  Verify 'Internet' plan add-ons
		// ########################################################################################################
		strStepDesc = "Verify '<b>Upgrade to Elite</b>' add-on populated under 'Internet' section.";
		strActualResult = "'Upgrade to Elite' add-on is populated under 'Internet' section.";
		blnStepRC= page("pgeCustomizeOrder").element("eleUpgradeEliteAddOn").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 13 - Verify 'Internet' plan add-ons
		//  Verify 'Internet' plan add-ons
		// ########################################################################################################
		strStepDesc = "Verify 'Upgrade to Ultra' add-on populated under 'Internet' section.";
		strActualResult = "'Upgrade to Ultra' add-on is populated under 'Internet' section.";
		blnStepRC= page("pgeCustomizeOrder").element("eleUpgradeUltraAddOn").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 14 - Verify 'Television' plan add-ons
		//  Verify 'Television' plan add-ons
		// ########################################################################################################
		strStepDesc = "Verify 'Premium Channel' section under 'Television' section.";
		strActualResult = "'Premium Channel' section is displayed under 'Television' section.";
		blnStepRC= page("pgeCustomizeOrder").element("elePremiumChannels").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 15 - Verify 'Television' plan add-ons
		//  Verify 'Television' plan add-ons
		// ########################################################################################################
		strStepDesc = "Verify 'Premium Channel' add-ons populated under 'Television' section.";
		strActualResult = "'Premium Channel' add-ons are populated under 'Television' section.";
		blnStepRC= page("pgeCustomizeOrder").element("elePremiumAddOns").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 16 - Verify 'Equipments' for TV plan
		//  Verify 'Equipments' for TV plan
		// ########################################################################################################
		strStepDesc = "Verify 'Equipments' section for 'TV' plan type.";
		strActualResult = "'Equipments' section is displayed for 'TV' plan type.";
		blnStepRC= page("pgeCustomizeOrder").element("eleTVEquipments").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 17 - Verify 'Equipments' for Security plan
		//  Verify 'Equipments' for Security plan
		// ########################################################################################################
		strStepDesc = "Verify 'Equipments' section for 'Security' plan type.";
		strActualResult = "'Equipments' section is displayed for 'Security' plan type.";
		blnStepRC= page("pgeCustomizeOrder").element("eleSecurityEquipments").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 18 - Verify Charges for 'Internet,TV and Security Plans'
		//  Verify 'Internet,TV and Security Plans'
		// ########################################################################################################
		strStepDesc = "Verify Charges for 'Internet','TV' and 'Security' plans are showing under 'Monthly Charges' section.";
		strActualResult = "Charges for only 'Internet','TV' and 'Security' plans are displayed under 'Monthly Charges' section.";
		blnStepRC=verifySelectedPlansUnderMonthlyCharges(3,strPlanNames); 
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 19 - Verify 'Taxes & Fees'
		//  Verify 'Taxes & Fees'
		// ########################################################################################################
		strStepDesc = "Verify 'Taxes & Fees' section under 'One Time Charges' section.";
		strActualResult = "'Taxes & Fees' section is populated under 'One Time Charges' section.";
		blnStepRC= page("pgeCustomizeOrder").element("eleTaxes&Fees").exist();
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 20 - Verify 'Taxes & Fees'
		//  Verify 'Taxes & Fees'
		// ########################################################################################################
		strStepDesc = "Verify 'Taxes & Fees' Charges under 'One Time Charges' section.";
		strActualResult = "'Taxes & Fees' Charges is populated under 'One Time Charges' section.";
		blnStepRC= page("pgeCustomizeOrder").element("eleTaxesCharges").exist();
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}
