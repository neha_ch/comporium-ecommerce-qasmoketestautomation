package TestScript.Browse;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import Launcher.Launcher;
import libraries.WebFunclib;
public class VerifyBrowsePageWhenMovingInBundlePlans_SHOPM440 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;
	
	@Test
	public void VerifyBrowsePageWhenMovingInBundlePlans(ITestContext testContext) throws InterruptedException 
	{
		// ########################################################################################################
		// # Test Case ID: SHOPM-440
		// # Test Case: VerifyBrowsePageWhenMovingInBundlePlans
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify bundle plans on Browse page.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-19-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################
		
		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		
		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{	    
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium account exists, Street Address: '<b>"+strStreetAddressData+"</b>'";
		strActualResult = "'Zip code: '<b>"+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}	

		// ########################################################################################################
		// Step 4 - "Click on 'No, I'm Moving here'
		// "Click on 'No, I'm Moving here'
		// ########################################################################################################
		strStepDesc = "Click on 'No, I'm Moving here' from the pop-up.";
		strActualResult = "Fields 'Selected Service Address', 'Change Address','highlighted Bundle Plans','Individual Plans' and 'Cart section' are displayed.";
		blnStepRC = clickOnAddressConformationPopup("btnIamMovingHere",strStreetAddressData);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}		
		
		// ########################################################################################################
		// Step 5 - Click on 'View All Bundles' check-box
		// Click on 'View All Bundles' check-box
		// ########################################################################################################
		strStepDesc = "Click on 'View All Bundles' check-box and verify 'I am looking for:' section.";
		strActualResult = "Check-boxes for each bundle type '<b>TV,Internet,Phone</b>' and '<b>Security</b>' are displayed in 'I am looking for' section.";
		blnStepRC = selectViewAllBundlesAndVerify();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}

		// ########################################################################################################
		// Step 6 - Click on each bundle type and verify details
		// Click on each bundle type and verify details
		// ########################################################################################################
		strStepDesc = "Select all plan type ony by one and verify details for each on 'Browse' page.";
		strActualResult = "Fields 'Bundle Name,Bundle Price,Bundle Details,View Details link' and 'Customize button' are displayed for each bundle plan.";
		blnStepRC=selectAndVerifyBundlePlan("Internet",true,"chkInternetPlan","eleInternetDetails","eleInternetBundleName");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 6 - Click on each bundle type and verify details
		// Click on each bundle type and verify details
		// ########################################################################################################
		strStepDesc = "Select all plan type ony by one and verify details for each on 'Browse' page.";
		strActualResult = "Fields 'Bundle Name,Bundle Price,Bundle Details,View Details link' and 'Customize button' are displayed for each bundle plan.";
		blnStepRC=selectAndVerifyBundlePlan("TV",true,"chkTVPlan","eleTVDetails","eleTVBundleName");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 6 - Click on each bundle type and verify details
		// Click on each bundle type and verify details
		// ########################################################################################################
		strStepDesc = "Select all plan type ony by one and verify details for each on 'Browse' page.";
		strActualResult = "Fields 'Bundle Name,Bundle Price,Bundle Details,View Details link' and 'Customize button' are displayed for each bundle plan.";
		blnStepRC=selectAndVerifyBundlePlan("Security",true,"chkSecurityPlan","eleSecurityDetails","eleSecurityBundleName");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 6 - Click on each bundle type and verify details
		// Click on each bundle type and verify details
		// ########################################################################################################
		strStepDesc = "Select all plan type ony by one and verify details for each on 'Browse' page.";
		strActualResult = "Fields 'Bundle Name,Bundle Price,Bundle Details,View Details link' and 'Customize button' are displayed for each bundle plan.";
		blnStepRC=selectAndVerifyBundlePlan("Phone",true,"chkPhonePlan","elePhoneDetails","elePhoneBundleName");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 7 - Click on 'View Details' link
		// Click on 'View Details' link
		// ########################################################################################################
		strStepDesc = "Click on 'View Details' link for any plan on 'Browse' page.";
		strActualResult = "Plan Features are displayed with 'Hide Details' link for the selected plan.";
		blnStepRC=clickAndVerify("pgeBrowse","lnkViewDetails","pgeBrowse","lnkHideDetails","elePlanFeatures");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}
