package TestScript.Schedule;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import Launcher.Launcher;
import libraries.WebFunclib;
public class VerifySchedulingOfSecurityProducts_SHOPM470 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;

	@Test
	public void VerifySchedulingOfSecurityProducts(ITestContext testContext) throws InterruptedException 
	{
		// ########################################################################################################
		// # Test Case ID: SHOPM-470
		// # Test Case: VerifySchedulingOfSecurityProducts
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify the flow when two security products are scheduled.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################
		
		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strFirstName = dicTestData.get("strFirstName");
		String strLastName = dicTestData.get("strLastName");
		String strDOB = dicTestData.get("strDOB");
		String strPhoneNumber = dicTestData.get("strPhoneNumber");
		String strEmail = dicTestData.get("strEmail");
		String strSSN = dicTestData.get("strSSN");
		String strSelectValue = dicTestData.get("strSelectValue");
		String strSelectType = dicTestData.get("strSelectType");
		
		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium account exists, Street Address: '<b>"+strStreetAddressData+"</b>'";
		strActualResult = "'Zip code: '<b>"+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		strActualResult = "Fields 'Selected Service Address','Change Address','highlighted Bundle Plans','Individual Plans' and 'Cart section' are displayed.";
		blnStepRC = clickOnSubmitAddressSearch(true,false);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 4 - Click on 'View All Bundles' check-box
		// Click on 'View All Bundles' check-box
		// ########################################################################################################
		strStepDesc = "Click on 'View All Bundles' check-box and verify 'I am looking for:' section.";
		strActualResult = "Check-boxes for each bundle type '<b>TV,Internet,Phone</b>' and '<b>Security</b>' are displayed in 'I am looking for' section.";
		blnStepRC = selectViewAllBundlesAndVerify();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 5 - Select 'Security' plan type
		// Select 'Security' plan type
		// ########################################################################################################
		strStepDesc = "Select '<b>Security</b>' plan type on 'Browse' page.";
		strActualResult = "Plans for only 'Security' are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerifyContainsText("pgeBrowse","chkSecurityPlan","pgeBrowse","eleSelectedPlanDetails","Security");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 6 - Click on 'Customize' button
		// Click on 'Customize' button to add a plan
		// ########################################################################################################
		strStepDesc = "Select a Bundle Plan by clicking on 'Customize' button on the 'Browse' page.";
		strActualResult = "'Customize Order' Page is displayed with following fields : highlighted 'Customize' breadcrum with Step-Number,Checkout,Confirm,Payment breadcrums,"
				+ "Recommended Upgraded Equipment Section,Monthly Charges Section,Product Section(Internet) and Continue,Continue Shopping buttons.";
		blnStepRC = customizePlanAndVerify(true);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 7 - Verify 'Equipments' for Security plan
		//  Verify 'Equipments' for Security plan
		// ########################################################################################################
		strStepDesc = "Verify 'Equipments' section for 'Security' plan type.";
		strActualResult = "'Equipments' section is displayed for 'Security' plan type.";
		blnStepRC= page("pgeCustomizeOrder").element("eleSecurityEquipments").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 8 - Select an Equipment
		//  Select an Equipment
		// ########################################################################################################
		strStepDesc = "Select a Security Equipment with a price from the list of equipments.";
		strActualResult = "A security equipment with a price is selected.";
		blnStepRC=selectFromList("pgeCustomizeOrder","lstDoorLockSecurityEquip",strSelectValue,strSelectType,"pgeCustomizeOrder","eleSelectedDoorEquip");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,"Failed to select Security Equipment.", "Fail");
		}
		
		// ########################################################################################################
		// Step 9 - Select an Equipment
		//  Select an Equipment
		// ########################################################################################################
		strStepDesc = "Select another Security Equipment with a price from the list of equipments.";
		strActualResult = "Another security equipment with a price is selected.";
		blnStepRC=selectFromList("pgeCustomizeOrder","lstSmokeDetSecurityEquip",strSelectValue,strSelectType,"pgeCustomizeOrder","eleSelectedSmokeEquip");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,"Failed to select Security Equipment.", "Fail");
		}
				
		// ########################################################################################################
		// Step 10 - Click on 'Continue' button
		// Click on 'Continue' button
		// ########################################################################################################
		strStepDesc = "Click on 'Continue' button on 'Customized Order' page.";
		strActualResult = "'Checkout Page' is displayed with following fields 'Highlighted 'Checkout' section,'Primary Account Holder','Billing Address' and 'Credit Check Information' sections.";
		blnStepRC = checkoutPlanAndVerify();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
	
		// ########################################################################################################
		// Step 11 - Enter Billing Address details
		// Enter Billing Address details for existing customer
		// ########################################################################################################
		strStepDesc = "Enter all details in Primary Account Holder section, select 'No Thanks' checkbox and then Click Continue.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC=enterCheckoutInformation(strFirstName, strLastName,strPhoneNumber, strDOB, strSSN, strEmail,true,"pgeServiceSchedule","btnSelectDateTime");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 12 - Select Installation Date and Time
		// Select Installation Date and Time
		// ########################################################################################################
		strStepDesc = "Click on 'Select Date and Time' button,select an available date and Preferred Time of Day and click on OK.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC =selectDateAndTimeForInstallation();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	
		// ########################################################################################################
		// Step 13 - Select Installation Date and Time for Security Products
		// Select Installation Date and Time for Security Products
		// ########################################################################################################
		strStepDesc = "Click on 'Select Date and Time for Security Products' button, select Date & Time for installation.";
		strActualResult = "'Service & Installation' page is displayed with selected date and time to schedule installation for security products.";
		blnStepRC =selectDateAndTimeForInstallation("pgeServiceSchedule","btnSelectDateForSecurity","eleSelectedDateForSecurity");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}
