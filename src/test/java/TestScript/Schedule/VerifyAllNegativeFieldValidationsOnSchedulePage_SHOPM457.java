package TestScript.Schedule;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import Launcher.Launcher;
import libraries.WebFunclib;
public class VerifyAllNegativeFieldValidationsOnSchedulePage_SHOPM457 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;

	@Test
	public void VerifyAllNegativeFieldValidationsOnSchedulePage(ITestContext testContext) throws InterruptedException 
	{
		// ########################################################################################################
		// # Test Case ID: SHOPM-457
		// # Test Case: VerifyAllNegativeFieldValidationsOnSchedulePage
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify all negative field validations on Schedule Page.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################
		
		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strFirstName = dicTestData.get("strFirstName");
		String strLastName = dicTestData.get("strLastName");
		String strDOB = dicTestData.get("strDOB");
		String strPhoneNumber = dicTestData.get("strPhoneNumber");
		String strEmail = dicTestData.get("strEmail");
		String strSSN = dicTestData.get("strSSN");
		String strPopUpText = dicTestData.get("strPopUpText");
		String strErrorMessage = dicTestData.get("strErrorMessage");
		
		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium account exists, Street Address: '<b>"+strStreetAddressData+"</b>'";
		strActualResult = "'Zip code: '<b>"+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 4 - "Click on 'No, I'm Moving here'
		// "Click on 'No, I'm Moving here'
		// ########################################################################################################
		strStepDesc = "Click on 'No, I'm Moving here' from the pop-up.";
		strActualResult = "Fields 'Selected Service Address', 'Change Address','highlighted Bundle Plans','Individual Plans' and 'Cart section' are displayed.";
		blnStepRC = clickOnAddressConformationPopup("btnIamMovingHere",strStreetAddressData);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 5 - Click on 'Customize' button
		// Click on 'Customize' button to add a plan
		// ########################################################################################################
		strStepDesc = "Select a Bundle Plan by clicking on 'Customize' button on the 'Browse' page.";
		strActualResult = "'Customize Order' Page is displayed with following fields : highlighted 'Customize' breadcrum with Step-Number,Checkout,Confirm,Payment breadcrums,"
				+ "Recommended Upgraded Equipment Section,Monthly Charges Section,Product Section(Internet) and Continue,Continue Shopping buttons.";
		blnStepRC = customizePlanAndVerify(true);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 6 - Click on 'Continue' button
		// Click on 'Continue' button
		// ########################################################################################################
		strStepDesc = "Click on 'Continue' button on 'Customized Order' page.";
		strActualResult = "'Checkout Page' is displayed with following fields 'Highlighted 'Checkout' section,'Primary Account Holder','Billing Address' and 'Credit Check Information' sections.";
		blnStepRC = checkoutPlanAndVerify();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
	
		// ########################################################################################################
		// Step 7 - Enter Billing Address details
		// Enter Billing Address details for existing customer
		// ########################################################################################################
		strStepDesc = "Enter all details in Primary Account Holder section, select 'No Thanks' checkbox and then Click Continue.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC =enterCheckoutInformation(strFirstName, strLastName,strPhoneNumber,strDOB, strSSN, strEmail,true);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 8 - Click on Continue without selecting any details
		// Click on Continue without selecting any details
		// ########################################################################################################
		strStepDesc = "Click on Continue without selecting Date, Time and Phone Number.";
		strActualResult = "Error message '<b>"+strPopUpText+"</b>' pop-up is displayed.";
		blnStepRC =clickHoverAndVerify("pgeServiceSchedule","btnContinue","pgeServiceSchedule","eleErrorMessage");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 9 - Click on 'Select Date and Time'
		// Click on 'Select Date and Time'
		// ########################################################################################################
		strStepDesc = "Click on 'Select Date and Time' button.";
		strActualResult = "Calendar Pop Up is displayed on Schedule page.";
		blnStepRC =clickAndVerify("pgeServiceSchedule","btnSelectDateTime","pgeServiceSchedule","rdoPreferredTimeOfDay");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 10 - Verify 'OK' button
		// Verify 'OK' button
		// ########################################################################################################
		strStepDesc = "Verify 'OK' button without 'Preferred Time of Day' selection.";
		strActualResult = "OK button on the Calendar pop-up is disabled when 'Preferred Time of Day' is not selected.";
		blnStepRC=verifyEnabled("pgeServiceSchedule","btnOKCalendar",false);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 11 - Close the pop-up
		// Close the pop-up
		// ########################################################################################################
		strStepDesc = "Click on 'Cancel' button on the Calendar pop-up.";
		strActualResult = "Calendar Pop-Up is closed and Service & Installation page is displayed.";
		blnStepRC= clickAndVerify("pgeServiceSchedule","btnCancelPopUp","pgeServiceSchedule","btnSelectDateTime");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 12 - Click on 'Select Date and Time'
		// Click on 'Select Date and Time'
		// ########################################################################################################
		strStepDesc = "Click on 'Select Date and Time' button.";
		strActualResult = "Calendar Pop Up is displayed on Schedule page.";
		blnStepRC =clickAndVerify("pgeServiceSchedule","btnSelectDateTime","pgeServiceSchedule","rdoPreferredTimeOfDay");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 13 - Select 'I am Flexible' checkbox
		// Select 'I am Flexible' checkbox
		// ########################################################################################################
		strStepDesc = "Select 'I am Flexible' checkbox on Calendar Pop-Up.";
		strActualResult = "'I am Flexible' checkbox is selected on Calendar Pop-Up.";
		blnStepRC =clickAndVerify("pgeServiceSchedule","chkIamFlexible","pgeServiceSchedule","btnNextMonth");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 14 - Verify 'OK' button
		// Verify 'OK' button
		// ########################################################################################################
		strStepDesc = "Verify 'OK' button without 'Preferred Time of Day' selection.";
		strActualResult = "OK button on the Calendar pop-up is disabled when 'I'm Flexible' checkbox is selected.";
		blnStepRC=verifyEnabled("pgeServiceSchedule","btnOKCalendar",false);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 15 - Close the pop-up
		// Close the pop-up
		// ########################################################################################################
		strStepDesc = "Click on 'Cancel' button on the Calendar pop-up.";
		strActualResult = "Calendar Pop-Up is closed and Service & Installation page is displayed.";
		blnStepRC= clickAndVerify("pgeServiceSchedule","btnCancelPopUp","pgeServiceSchedule","btnSelectDateTime");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 16 - Enter invalid Phone Number
		// Enter invalid Phone Number
		// ########################################################################################################
		strStepDesc = "Enter invalid Phone Number and verify error message.";
		strActualResult = "Error Message '<b>"+strErrorMessage+"</b>' is displayed on entering invalid Phone Number.";
		blnStepRC=enterDataAndVerifyErrorMessage("pgeServiceSchedule","txtPhoneNumber","22","","elePhoneError",strErrorMessage,"ValidData");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 17 - Select Installation Date and Time
		// Select Installation Date and Time
		// ########################################################################################################
		strStepDesc = "Click on 'Select Date and Time' button,select an available date and Preferred Time of Day and click on OK.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC =selectDateAndTimeForInstallation();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 18 - Click on 'Select Date and Time'
		// Click on 'Select Date and Time'
		// ########################################################################################################
		strStepDesc = "Click on 'Select Date and Time' button.";
		strActualResult = "Calendar Pop Up is displayed on Schedule page.";
		blnStepRC =clickAndVerify("pgeServiceSchedule","btnSelectDateTime","pgeServiceSchedule","rdoPreferredTimeOfDay");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 19 - Select another Date 
		// Select another Date
		// ########################################################################################################
		strStepDesc = "Select another Date on the Calendar Pop-Up.";
		strActualResult = "Another Date is selected from the Calendar Pop-Up.";
		//blnStepRC=clickNextMonthAndSelectDate();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 20 - Verify 'Preferred Time of Day' selection
		// Verify 'Preferred Time of Day' selection
		// ########################################################################################################
		strStepDesc = "Verify 'Preferred Time of Day' is reset to no selection on Calendar Pop-up.";
		strActualResult = "'Preferred Time of Day' is reset to no selection on Calendar Pop-up.";
		//blnStepRC=verifySelection("pgeServiceSchedule","rdoPreferredTimeOfDay",false);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 21 - Close the pop-up
		// Close the pop-up
		// ########################################################################################################
		strStepDesc = "Click on 'Cancel' button on the Calendar pop-up.";
		strActualResult = "Calendar Pop-Up is closed and Service & Installation page is displayed.";
		blnStepRC= clickAndVerify("pgeServiceSchedule","btnCancelPopUp","pgeServiceSchedule","btnSelectDateTime");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 22 - Verify selected date
		// Verify selected date
		// ########################################################################################################
		strStepDesc = "Verify selected Date & Time on 'Schedule' page.";
		strActualResult = "'<b>Selected Date and Time</b>' is populated on 'Schedule' page below 'Select Date & Time' button.";
		blnStepRC = page("pgeServiceSchedule").element("eleSelectedDateTime").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}
