package TestScript.AddressSearch;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import Launcher.Launcher;
import libraries.WebFunclib;

public class VerifySessionIDandEmptyCart_SHOPM459 extends WebFunclib {
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;

	@Test
	public void VerifySessionIDandEmptyCart(ITestContext testContext) throws InterruptedException 
	{
		// ########################################################################################################
		// # Test Case ID: SHOPM-459
		// # Test Case: VerifySessionIDandEmptyCart
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify 'Session ID' cookie on Address search page and empty cart on 'Browse' page.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// #########################################################################################################

		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("str_Zip");
		String strChangeAddressMsg = dicTestData.get("strPopUpMessage");
		
		// ########################################################################################################
		// Step 1 - Launch the browser and verify cookies
		// Launch the browser and verify cookies
		// ########################################################################################################
		strStepDesc = "Launch the browser and verify cookies are blank.";
		strActualResult = "Cookies are blank after successful launch of browser.";
		blnStepRC = verifyCookies();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 2 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 3 - Verify Session ID cookie.
		// Verify Session ID
		// ########################################################################################################
		strStepDesc = "Verify 'SESSION_ID' cookie.";
		strActualResult = "'SESSION_ID' cookie is not present.";
		blnStepRC = verifySessionIDCookie(false);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 4 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter a street address where Comporium Service exists, Street Address: '<b>"+ dicTestData.get("strStreetAddress") + "</b>'";
		strActualResult = "'Zip code: '<b>"+""+dicTestData.get("strZip")+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 5 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 6 - Click on 'No, I'm Moving here' from the pop-up
		// Click on 'No, I'm Moving here' from the pop-up
		// ########################################################################################################
		strStepDesc = "Click on 'No, I'm Moving here' from the pop-up.";
		strActualResult = "User is navigated to 'Browse' page.";
		blnStepRC = clickOnAddressConformationPopup("btnIamMovingHere",strStreetAddressData);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 7 - Verify Session ID cookie.
		// Verify Session ID
		// ########################################################################################################
		strStepDesc = "Verify 'SESSION_ID' cookie is present.";
		blnStepRC = verifySessionIDCookie(true);
		strActualResult = "'SESSION_ID' cookie is present with ID: '<b>"+dicTestData.get("strSessionID")+"</b>'.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 8 - Click on Cart.
		// Click on Cart
		// ########################################################################################################
		strStepDesc = "Click on Cart icon on 'Browse' page.";
		strActualResult = "Cart is empty and a message '<b>Your cart is empty</b>' is displayed.";
		blnStepRC=verifyEmptyCart();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 9 - Click on 'Change Address' link.
		// Click on 'Change Address' link
		// ########################################################################################################
		strStepDesc = "Click on 'Change Address' link on 'Browse' page.";
		strActualResult = "A Pop up with message '<b>"+strChangeAddressMsg+"</b>' is displayed.";
		blnStepRC=clickAndVerify("pgeProfilePage","btnChangeAddress","pgeBrowse","eleChangeAddressPopUp");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 10 - Click on 'Continue' button
		// Click on 'Continue' button
		// ########################################################################################################
		strStepDesc = "Click on 'Continue' button on the pop-up.";
		strActualResult = "User is navigated to 'Address Search' Page'";
		blnStepRC=clickAndVerify("pgeBrowse","btnPopUpContinue","pgeHomePage","btnSubmit");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}