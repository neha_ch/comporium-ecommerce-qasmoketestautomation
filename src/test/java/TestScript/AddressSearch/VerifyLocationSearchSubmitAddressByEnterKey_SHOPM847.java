
package TestScript.AddressSearch;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import Launcher.Launcher;
import libraries.WebFunclib;

public class VerifyLocationSearchSubmitAddressByEnterKey_SHOPM847 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;

	@Test
	public void VerifyLocationSearchSubmitAddressByEnterKey(ITestContext testContext) throws InterruptedException 
	{
		// ########################################################################################################
		// # Test Case ID: SHOPM-847
		// # Test Case: VerifyLocationSearchSubmitAddressByEnterKey
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify Location Search: Automatically submit once address is selected when user 
		// #		Hits Enter on the keyboard.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################
		
		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddress1 = dicTestData.get("strStreetAddress");
		String strStreetAddress2 = dicTestData.get("strStreetAddress2");
		String strStreetAddress3 = dicTestData.get("strStreetAddress3");
		String strExstCustPopUp = dicTestData.get("strPopUpMessage");
		String strErrorMessage = dicTestData.get("strErrorMessage");

		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium account exists,Street Address: '<b>"+strStreetAddress1+ "</b>' "
				+ "and submit the address by hitting Enter key.";
		strActualResult = "Existing Account Pop up with message '<b>"+strExstCustPopUp+"</b>' is displayed.";
		blnStepRC=enterAddressAndSubmitByEnterKey(strStreetAddress1,"pgeHomePage","eleAccountExistPopUp",false);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}

		// ########################################################################################################
		// Step 3 - Close the existing account pop-up
		// Close the existing account pop-up
		// ########################################################################################################
		strStepDesc = "Close the Existing Account pop-up on 'Address Search' page.";
		strActualResult = "Existing Account pop-up is closed.";
		blnStepRC=clickAndVerify("pgeHomePage","btnCloseExstAccPopUp","pgeProfilePage","txtStreetAddress");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 4 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium account exists and has an Apartment #,Street Address:'<b>"+strStreetAddress2+ "</b>' "
				+ "and submit the address by hitting Enter key.";
		strActualResult = "'<b>Apartment # (Optional)</b>' field is displayed on 'Address Search' page.";
		blnStepRC=enterAddressAndSubmitByEnterKey(strStreetAddress2,"pgeHomePage","txtAptNumber",false);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 5 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an invalid address,Street Address: '<b>"+strStreetAddress3+ "</b>' "
				+ "and submit the address by hitting Enter key.";
		strActualResult = "Error Message '<b>"+strErrorMessage+"</b>' is displayed.";
		blnStepRC=enterAddressAndSubmitByEnterKey(strStreetAddress3,"pgeHomePage","eleAddressNotExistError",true);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}

	}
}