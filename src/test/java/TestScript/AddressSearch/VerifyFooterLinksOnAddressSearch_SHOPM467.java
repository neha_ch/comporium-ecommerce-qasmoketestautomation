package TestScript.AddressSearch;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import Launcher.Launcher;
import libraries.WebFunclib;

public class VerifyFooterLinksOnAddressSearch_SHOPM467 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;

	@Test
	public void VerifyFooterLinksOnAddressSearch(ITestContext testContext) throws InterruptedException 
	{
		// ########################################################################################################
		// # Test Case ID: SHOPM-467
		// # Test Case: VerifyFooterLinksOnAddressSearch
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify links in the footer section on Address search page.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-19-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################
		
		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		
		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Click on 'Offer Disclosures' link in the footer section.
		// Click on Offer Disclosures
		// ########################################################################################################
		strStepDesc = "Click on '<b>Offer Disclosures</b>' link in the footer section.";
		strActualResult = "'<b>Offer Disclosures</b>' page is displayed.";
		blnStepRC = clickAndVerify("pgeHomePage","lnkOfferDisclosure","pgeCommon","lblOfferDisclosure");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 3 - Navigate back to the Address Search page.
		// Navigate back
		// ########################################################################################################
		strStepDesc = "Navigate back to the Address Search page.";
		strActualResult = "'Address Search' page is displayed.";
		blnStepRC = navigateBack("pgeHomePage","btnSubmit");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 4 - Click on 'Terms & Conditions' link.
		// Click on Terms & Conditions
		// ########################################################################################################
		strStepDesc = "Click on '<b>Terms & Conditions</b>' link.";
		strActualResult = "'<b>Terms & Conditions</b>' page is displayed.";
		blnStepRC = clickAndVerify("pgeHomePage","lnkTermsConditions","pgeCommon","lblTerms&Conditions");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 5 - Navigate back to the Address Search page.
		// Navigate back
		// ########################################################################################################
		strStepDesc = "Navigate back to the Address Search page.";
		strActualResult = "'Address Search' page is displayed.";
		blnStepRC = navigateBack("pgeHomePage","btnSubmit");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 6 - Click on 'Service Agreements' link.
		// Click on Service Agreements
		// ########################################################################################################
		strStepDesc = "Click on '<b>Service Agreements</b>' link.";
		strActualResult = "'<b>Service Agreements</b>' page is displayed.";
		blnStepRC = clickAndVerify("pgeHomePage","lnkServiceAgreements","pgeCommon","lblServiceAgreements");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 7 - Navigate back to the Address Search page.
		// Navigate back
		// ########################################################################################################
		strStepDesc = "Navigate back to the Address Search page.";
		strActualResult = "'Address Search' page is displayed.";
		blnStepRC = navigateBack("pgeHomePage","btnSubmit");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 8 - Click on 'Privacy Policy' link.
		// Click on Privacy Policy
		// ########################################################################################################
		strStepDesc = "Click on '<b>Privacy Policy</b>' link.";
		strActualResult = "'<b>Privacy Policy</b>' page is displayed.";
		blnStepRC = clickAndVerify("pgeHomePage","lnkPrivacyPolicy","pgeCommon","lblPrivacyPolicy");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 9 - Navigate back to the Address Search page.
		// Navigate back
		// ########################################################################################################
		strStepDesc = "Navigate back to the Address Search page.";
		strActualResult = "'Address Search' page is displayed.";
		blnStepRC = navigateBack("pgeHomePage","btnSubmit");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 10 - Click on 'Linking Policy' link.
		// Click on Linking Policy
		// ########################################################################################################
		strStepDesc = "Click on '<b>Linking Policy</b>' link.";
		strActualResult = "'<b>Linking Policy</b>' page is displayed.";
		blnStepRC = clickAndVerify("pgeHomePage","lnkLinkingPolicy","pgeCommon","lblLinkingPolicy");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 11 - Navigate back to the Address Search page.
		// Navigate back
		// ########################################################################################################
		strStepDesc = "Navigate back to the Address Search page.";
		strActualResult = "'Address Search' page is displayed.";
		blnStepRC = navigateBack("pgeHomePage","btnSubmit");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 12 - Click on 'Internet Acceptable Use Policy' link.
		// Click on Internet Acceptable Use Policy
		// ########################################################################################################
		strStepDesc = "Click on '<b>Internet Acceptable Use Policy</b>' link.";
		strActualResult = "'<b>Internet Acceptable Use Policy</b>' page is displayed.";
		blnStepRC = clickAndVerify("pgeHomePage","lnkAcceptableUsePolicy","pgeCommon","lblInternetUsePolicy");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 13 - Navigate back to the Address Search page.
		// Navigate back
		// ########################################################################################################
		strStepDesc = "Navigate back to the Address Search page.";
		strActualResult = "'Address Search' page is displayed.";
		blnStepRC = navigateBack("pgeHomePage","btnSubmit");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 14 - Click on 'Site Map' link.
		// Click on Site Map
		// ########################################################################################################
		strStepDesc = "Click on '<b>Site Map</b>' link.";
		strActualResult = "'<b>Sitemap</b>' page is displayed.";
		blnStepRC = clickAndVerify("pgeHomePage","lnkSiteMap","pgeCommon","lblSiteMap");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 15 - Navigate back to the Address Search page.
		// Navigate back
		// ########################################################################################################
		strStepDesc = "Navigate back to the Address Search page.";
		strActualResult = "'Address Search' page is displayed.";
		blnStepRC = navigateBack("pgeHomePage","btnSubmit");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 16 - Click on 'Find a Retail Location' link.
		// Click on Find a Retail Location
		// ########################################################################################################
		strStepDesc = "Click on '<b>Find a Retail Location</b>' link.";
		strActualResult = "'<b>Find a Store</b>' page is displayed.";
		blnStepRC = clickAndVerify("pgeHomePage","lnkFindRetailLocation","pgeCommon","lblFindStore");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 17 - Navigate back to the Address Search page.
		// Navigate back
		// ########################################################################################################
		strStepDesc = "Navigate back to the Address Search page.";
		strActualResult = "'Address Search' page is displayed.";
		blnStepRC = navigateBack("pgeHomePage","btnSubmit");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 18 - Click on 'Customer Support' link.
		// Click on Customer Support
		// ########################################################################################################
		strStepDesc = "Click on '<b>Customer Support</b>' link.";
		strActualResult = "'<b>Technical Support</b>' page is displayed.";
		blnStepRC = clickAndVerify("pgeHomePage","lnkCustomerSupport","pgeCommon","lblTechnicalSupport");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 19 - Navigate back to the Address Search page.
		// Navigate back
		// ########################################################################################################
		strStepDesc = "Navigate back to the Address Search page.";
		strActualResult = "'Address Search' page is displayed.";
		blnStepRC = navigateBack("pgeHomePage","btnSubmit");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 20  - Click on 'Careers Page' link.
		// Click on Careers Page
		// ########################################################################################################
		strStepDesc = "Click on '<b>Careers Page</b>' link.";
		strActualResult = "'<b>Careers</b>' page is displayed.";
		blnStepRC = clickAndVerify("pgeHomePage","lnkCareersPage","pgeCommon","lblWorkAtComporium");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}