package TestScript.SanityReg;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import Launcher.Launcher;
import libraries.WebFunclib;
public class VerifySanityRegressionFlow_SHOPM314 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;
	
	@Test
	public void VerifySanityRegressionFlow(ITestContext testContext) throws InterruptedException 
	{
		
		// ########################################################################################################
		// # Test Case ID: SHOPM314
		// # Test Case: VerifySanityRegressionFlow
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify Sanity flow for new and customer customers.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################

		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strStreetAddress2 = dicTestData.get("strStreetAddress2");
		String strZip2 = dicTestData.get("strZip2");
		String strFirstName = dicTestData.get("strFirstName");
		String strLastName = dicTestData.get("strLastName");
		String strDOB = dicTestData.get("strDOB");
		String strPhoneNumber = dicTestData.get("strPhoneNumber");
		String strEmail = dicTestData.get("strEmail");
		String strSSN = dicTestData.get("strSSN");
		String CardNumber = dicTestData.get("CardNumber");
		String ExpiryDate = dicTestData.get("ExpiryDate");
		String CardHolderName = dicTestData.get("CardHolderName");
		String CVV = dicTestData.get("CVV");

		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter an address where No Comporium Service exists
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where No Comporium Service exists, Street Address: '<b>"+ dicTestData.get("strStreetAddress") + "</b>'";
		strActualResult = "'Zip code: '<b>"+""+dicTestData.get("strZip")+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		strActualResult = "Fields 'Selected Service Address','Change Address','highlighted Bundle Plans','Individual Plans' and 'Cart section' are displayed.";
		blnStepRC = clickOnSubmitAddressSearch(true,false);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 4 - Navigate back to the Address Search page.
		// Navigate back to enter another street address
		// ########################################################################################################
		strStepDesc = "Navigate back to the Address Search page.";
		strActualResult = "'Address Search' page is displayed.";
		blnStepRC = navigateBack("pgeProfilePage","txtStreetAddress");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 5 - Enter an address where Comporium Service exists
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium Service exists, Street Address: '<b>"+ dicTestData.get("strStreetAddress") + "</b>'";
		strActualResult = "'Zip code: '<b>"+""+dicTestData.get("strZip")+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddress2, 0,true,strZip2);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}

		// ########################################################################################################
		// Step 6 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 7 - Click on 'No I am moving here'.
		// Click on 'No I am moving here' for new customers
		// ########################################################################################################
		strStepDesc = "Click on 'No, I'm Moving here' from the pop-up.";
		strActualResult = "Fields 'Selected Service Address', 'Change Address','highlighted Bundle Plans','Individual Plans' and 'Cart section' are displayed.";
		blnStepRC = clickOnAddressConformationPopup("btnIamMovingHere",strStreetAddress2);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
	
		// ########################################################################################################
		// Step 8 - Click on 'Individual Plans'.
		//  Click on 'Individual Plans'.
		// ########################################################################################################
		strStepDesc = "Click on 'Individual Plans' button on 'Browse' page.";
		strActualResult = "Individual Plans should be displayed on 'Browse' page.";
		blnStepRC = clickAndVerify("pgeBrowse","btnIndividualPlans","pgeBrowse","chkInternetPlan");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 9 - Select a Plan by clicking on 'Customize' button
		//  Click on 'Customize' button.
		// ########################################################################################################
		strStepDesc = "Select an Individual Plan by clicking on 'Customize' button on the 'Browse' page.";
		strActualResult = "Individual Plans should be displayed on 'Browse' page.";
		blnStepRC = customizePlanAndVerify(true);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
	
		// ########################################################################################################
		// Step 10 - Click on 'Continue' button.
		//  Click on 'Continue' button.
		// ########################################################################################################
		strStepDesc = "Click on 'Continue' button on 'Customized Order' page.";
		strActualResult = "'Checkout' page is displayed.";
		blnStepRC = checkoutPlanAndVerify();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 11 - Enter Billing Address details
		//  Enter Billing Address details
		// ########################################################################################################
		strStepDesc = "Enter all details in Primary Account Holder section, select 'No Thanks' checkbox and then Click Continue.";
		strActualResult = "'Checkout' page is displayed.";
		blnStepRC = enterCheckoutInformation(strFirstName, strLastName, strDOB, strPhoneNumber, strSSN, strEmail,true);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 12 - Select Installation Date & Time
		//  Select Installation Date & Time
		// ########################################################################################################
		strStepDesc = "Click on 'Select Date and Time' button,select an available date and Preferred Time of Day and click on OK.";
		strActualResult = "'Confirm Order' page is displayed.";
		blnStepRC = selectDateAndTimeForInstallation();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 13 - Click on 'Continue' button.
		//  Click on 'Continue' button.
		// ########################################################################################################
		strStepDesc = "Click on Continue button on 'Service & Installation' page.";
		strActualResult = "'Confirm Order' page is displayed with fields 'Highlighted Confirm breadcrum,Total Due and Account Details' sections.";
		blnStepRC = verifyConfirmOrderPage();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 14 - Click on 'I Agree' checkbox and click 'Continue'.
		//  Click on 'Continue' button.
		// ########################################################################################################
		strStepDesc = "Click on 'I agree checkbox' and click 'Continue' button on 'Confirm Order' page.";
		strActualResult = "'Set Up Payment' page is displayed.";
		blnStepRC = selectIAgreeCheckBoxAndContinue();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
	
		// ########################################################################################################
		// Step 15 - Fill Card Details
		//   Fill Card Details.
		// ########################################################################################################
		strStepDesc = "Fill 'Credit card' details and click on Submit Order button.";
		strActualResult = "Order should be placed.";
		blnStepRC = fillCardDetails(CardNumber, ExpiryDate, CardHolderName, CVV);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,dicTestData.get("strOrderTxt&Number"),"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}
