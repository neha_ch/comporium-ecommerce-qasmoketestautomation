package TestScript.Checkout;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.nimbusds.oauth2.sdk.ParseException;

import Launcher.Launcher;
import libraries.WebFunclib;
public class VerifyFieldsValidationOnCheckoutPage_SHOPM455 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;
	
	@Test
	public void VerifyFieldsValidationOnCheckoutPage(ITestContext testContext) throws InterruptedException, ParseException, java.text.ParseException 
	{

		// ########################################################################################################
		// # Test Case ID: SHOPM-455
		// # Test Case: VerifyFieldsValidationOnCheckoutPage
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify fields and and fields validations on 'Checkout' page.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################

		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strStreetNumber = dicTestData.get("strStreetNumber");
		String strStreetName = dicTestData.get("strStreetName");
		String strBillingCity = dicTestData.get("strBillingCity");
		String strFirstName = dicTestData.get("strFirstName");
		String strErrorMsg = dicTestData.get("strErrorMsg");
		//String strLastName = dicTestData.get("strLastName");
		String strPhoneNumber = dicTestData.get("strPhoneNumber");
		String strErrorMsg1 = dicTestData.get("strErrorMsg1");
		String strSSN = dicTestData.get("strSSN");
		String strErrorMsg2 = dicTestData.get("strErrorMsg2");
		String strDOB = dicTestData.get("strDOB");
		String strEmail = dicTestData.get("strEmail");
		String strErrorMsg3 = dicTestData.get("strErrorMsg3");
		//String strInvalidCharacters = dicTestData.get("strInvalidCharacters");
		String strValidPhoneNum = dicTestData.get("strValidPhoneNum");
		String strValidSSN = dicTestData.get("strValidSSN");

		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter a street address where Comporium Service exists, Street Address: '<b>"+ strStreetAddressData + "</b>'";
		strActualResult = "'Zip code: '<b>"+""+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 4 - Click on 'No, I'm Moving here' from the pop-up
		// Click on 'No, I'm Moving here' from the pop-up
		// ########################################################################################################
		strStepDesc = "Click on 'No, I'm Moving here' from the pop-up.";
		strActualResult = "User is navigated to 'Browse' page.";
		blnStepRC = clickOnAddressConformationPopup("btnIamMovingHere",strStreetAddressData);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 5 - Click on 'Individual Plans' on 'Browse' page.
		// Click on 'Individual Plans'
		// ########################################################################################################
		strStepDesc = "Click on 'Individual Plans' on 'Browse' page.";
		strActualResult = "All the individual plans are displayed.";
		blnStepRC=selectBundleAndVerifyPlanTypes("pgeBrowse","btnIndividualPlans");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 6 - Click on Customize button for any Plan
		// Click on Customize button for any Plan
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button for any Plan on 'Browse' page.";
		strActualResult = "User is navigated to 'Customize Order' page.";
		blnStepRC=clickAndVerify("pgeBrowse","btnCustomize","pgeCustomizeOrder","lnkContinueShopping");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 7 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on Continue button on 'Customize Order' page.";
		strActualResult = "User is navigated to 'Checkout' page.";
		blnStepRC =checkoutPlanAndVerify();
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 8 - Verify the Primary Account Holder section
		// Verify the Primary Account Holder section
		// ########################################################################################################
		strStepDesc = "Verify fields under 'Primary Account Holder' section on 'Checkout' page.";
		strActualResult = "Fields '<b>First Name,Last Name,Phone Number,Date Of Birth (MM-DD-YYYY),Social Security Number </b>' and '<b>Email Address</b>' are displayed.";
		blnStepRC=verifyPrimaryAccountHolderFields();
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 9 - Verify Comporium Billing Address section
		// Verify Comporium Billing Address section
		// ########################################################################################################
		strStepDesc = "Verify fields under 'Comporium Billing Address' section on 'Checkout' page.";
		strActualResult = "Fields 'Street Number,Street Name,Apartment#, P.O. Box,City, State and Zipcode are displayed with pre-filled data from selected street address '<b>"+strStreetAddressData+"</b>'.";
		blnStepRC=verifyPrefilledBillingAddressFields(strStreetNumber,strStreetName,strBillingCity,strZip);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 10 - Enter Alphanumeric and Special Characters
		// Enter Alphanumeric and Special Characters
		// ########################################################################################################
		strStepDesc = "Enter Alphanumeric and Special Characters in 'First Name' field.";
		strActualResult = "User is not be able to enter anything other than Alphabets in 'First Name' field.";
		//blnStepRC=EnterDataAndVerifyErrorMessage("pgeCheckout","txtFirstName",strFirstName,strInvalidCharacters,"","","InvalidData");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
				
		}
		
		// ########################################################################################################
		// Step 11 - Enter Alphanumeric and Special Characters
		// Enter Alphanumeric and Special Characters
		// ########################################################################################################
		strStepDesc = "Enter Alphanumeric and Special Characters in 'Last Name' field.";
		strActualResult = "User is not be able to enter anything other than Alphabets in 'Last Name' field.";
		//blnStepRC=EnterDataAndVerifyErrorMessage("pgeCheckout","txtLastName",strLastName,strInvalidCharacters,"","","InvalidData");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
				
		}
		
		// ########################################################################################################
		// Step 12 - Enter only 1 alphabet
		// Enter only 1 alphabet as First name
		// ########################################################################################################
		strStepDesc = "Enter only 1 alphabet in 'First Name' field and verify error message.";
		strActualResult = "Error Message '<b>"+strErrorMsg+"</b>' is displayed.";
		blnStepRC=enterDataAndVerifyErrorMessage("pgeCheckout","txtFirstName","a","","eleNameError",strErrorMsg,"ValidData");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
		
		// ########################################################################################################
		// Step 13 - Enter only 1 alphabet
		// Enter only 1 alphabet as Last name
		// ########################################################################################################
		strStepDesc = "Enter only 1 alphabet in 'Last Name' field and verify error message.";
		strActualResult = "Error Message '<b>"+strErrorMsg+"</b>' is displayed.";
		blnStepRC=enterDataAndVerifyErrorMessage("pgeCheckout","txtLastName","b","","eleNameError",strErrorMsg,"ValidData");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
		
		// ########################################################################################################
		// Step 14 - Enter Alphanumeric and Special Characters
		// Enter Alphanumeric and Special Characters
		// ########################################################################################################
		strStepDesc = "Enter Alphanumeric and Special Characters in 'Phone Number' field.";
		strActualResult = "User is not be able to enter anything other than numbers in 'Phone Number' field.";
		blnStepRC=enterDataAndVerifyErrorMessage("pgeCheckout","txtPhoneNumber",strValidPhoneNum,strFirstName,"","","InvalidData");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");		
		}
		
		// ########################################################################################################
		// Step 15 - Enter less than 10 digit Phone Number
		// Enter less than 10 digit Phone Number
		// ########################################################################################################
		strStepDesc = "Enter a phone number less than 10 digits and verify error message.";
		strActualResult = "Error Message '<b>"+strErrorMsg1+"</b>' is displayed.";
		blnStepRC=enterDataAndVerifyErrorMessage("pgeCheckout","txtPhoneNumber",strPhoneNumber,"","elePhoneNumError",strErrorMsg1,"ValidData");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
		
		// ########################################################################################################
		// Step 16 - Enter Date Of Birth
		// Enter Date Of Birth
		// ########################################################################################################
		strStepDesc = "Enter Date Of Birth and verify the format.";
		strActualResult = "DOD is entered only in MM-DD-YYYY format.";
		blnStepRC=verifyDateOfBirthFormat(strDOB);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
		
		// ########################################################################################################
		// Step 17 - Enter Alphanumeric and Special Characters
		// Enter Alphanumeric and Special Characters in SSN field
		// ########################################################################################################
		strStepDesc = "Enter Alphanumeric and Special Characters in 'Social Security Number' field.";
		strActualResult = "User is not be able to enter anything other than numbers in 'Social Security Number' field.";
		blnStepRC=enterDataAndVerifyErrorMessage("pgeCheckout","txtSSN",strValidSSN,strFirstName,"","","InvalidData");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
		
		// ########################################################################################################
		// Step 18 - Enter less than 10 digit SSN
		// Enter less than 10 digit SSN
		// ########################################################################################################
		strStepDesc = "Enter a Social Security number less than 10 digits and verify error message.";
		strActualResult = "Error Message '<b>"+strErrorMsg2+"</b>' is displayed.";
		blnStepRC=enterDataAndVerifyErrorMessage("pgeCheckout","txtSSN",strSSN,"","eleSSNumError",strErrorMsg2,"ValidData");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
		
		// ########################################################################################################
		// Step 19 - Enter invalid email address
		// Enter invalid email address
		// ########################################################################################################
		strStepDesc = "Enter an invalid email address and verify error message.";
		strActualResult = "Error Message '<b>"+strErrorMsg3+"</b>' is displayed.";
		blnStepRC=enterDataAndVerifyErrorMessage("pgeCheckout","txtEmailAddress",strEmail,"","eleInvalidEmailError",strErrorMsg3,"ValidData");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
	}
}
