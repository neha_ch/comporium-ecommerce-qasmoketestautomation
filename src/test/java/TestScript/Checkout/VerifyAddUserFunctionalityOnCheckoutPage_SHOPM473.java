package TestScript.Checkout;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.nimbusds.oauth2.sdk.ParseException;

import Launcher.Launcher;
import libraries.WebFunclib;
public class VerifyAddUserFunctionalityOnCheckoutPage_SHOPM473 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;
	
	@Test
	public void VerifyAddUserFunctionalityOnCheckoutPage(ITestContext testContext) throws InterruptedException, ParseException, java.text.ParseException 
	{

		// ########################################################################################################
		// # Test Case ID: SHOPM-473
		// # Test Case: VerifyAddUserFunctionalityOnCheckoutPage
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify 'Add user' functionality under 'Primary Account Holder' section on 'Checkout' page.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################

		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strFirstName = dicTestData.get("strFirstName");
		String strLongFirstName = dicTestData.get("strLongFirstName");
		String strErrorMsg = dicTestData.get("strErrorMsg");
		String strLastName = dicTestData.get("strLastName");
		String strLongLastName = dicTestData.get("strLongLastName");
		String strRelationValues = dicTestData.get("strRelationshipValues");
		String strMaxFirstLength = dicTestData.get("strMaxFirstLength");
		String strMaxLastLength = dicTestData.get("strMaxLastLength");
		String[] strPrimaryRelation = strRelationValues.split(",");
		
		// ###############################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ###############################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ###############################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ###############################################################################################################
		strStepDesc = "Enter a street address where Comporium Service exists, Street Address: '<b>"+ strStreetAddressData + "</b>'";
		strActualResult = "'Zip code: '<b>"+""+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ###############################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ###############################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ###############################################################################################################
		// Step 4 - Click on 'No, I'm Moving here' from the pop-up
		// Click on 'No, I'm Moving here' from the pop-up
		// ###############################################################################################################
		strStepDesc = "Click on 'No, I'm Moving here' from the pop-up.";
		strActualResult = "User is navigated to 'Browse' page.";
		blnStepRC = clickOnAddressConformationPopup("btnIamMovingHere",strStreetAddressData);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ###############################################################################################################
		// Step 5 - Click on 'Individual Plans' on 'Browse' page.
		// Click on 'Individual Plans'
		// ###############################################################################################################
		strStepDesc = "Click on 'Individual Plans' on 'Browse' page.";
		strActualResult = "All the individual plans are displayed.";
		blnStepRC=selectBundleAndVerifyPlanTypes("pgeBrowse","btnIndividualPlans");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ###############################################################################################################
		// Step 6 - Click on Customize button for any Plan
		// Click on Customize button for any Plan
		// ###############################################################################################################
		strStepDesc = "Click on 'Customize' button for any Plan on 'Browse' page.";
		strActualResult = "User is navigated to 'Customize Order' page.";
		blnStepRC=clickAndVerify("pgeBrowse","btnCustomize","pgeCustomizeOrder","lnkContinueShopping");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ###############################################################################################################
		// Step 7 - Click on Continue button
		// Click on Continue button
		// ###############################################################################################################
		strStepDesc = "Click on Continue button on 'Customize Order' page.";
		strActualResult = "User is navigated to 'Checkout' page.";
		blnStepRC =checkoutPlanAndVerify();
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ###############################################################################################################
		// Step 8 - Verify the Primary Account Holder section
		// Verify the Primary Account Holder section
		// ###############################################################################################################
		strStepDesc = "Verify fields under 'Primary Account Holder' section on 'Checkout' page.";
		strActualResult = "Fields '<b>First Name,Last Name,Phone Number,Date Of Birth (MM-DD-YYYY),Social Security Number </b>' "
				+ "and '<b>Email Address</b>' are displayed.";
		blnStepRC=verifyPrimaryAccountHolderFields();
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ###############################################################################################################
		// Step 9 - Click on Add user
		// Click on Add user
		// ###############################################################################################################
		strStepDesc = "Click on 'Add user' button under 'Add Authorized User' section and verify fields.";
		strActualResult = "'Add User' section is expanded and fields 'First Name,Last Name,Relationship to Primary drop-down "
				+ "and Delete button is displayed.";
		blnStepRC=verifyAddAuthorizedUserFields();
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ###############################################################################################################
		// Step 10 - Verify 'Relationship to Primary'
		// Verify 'Relationship to Primary' values
		// ###############################################################################################################
		strStepDesc = "Verify values under 'Relationship to Primary' drop-down.";
		strActualResult = "Values '<b>"+strRelationValues+"</b>' are listed under 'Relationship to Primary' drop-down.";
		blnStepRC=verifyRelationshipToPrimaryValues("pgeCheckout","drpPrimaryRelationship",strRelationValues);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");		
		}
		
		// ###############################################################################################################
		// Step 11 - Enter only 1 character
		// Enter only 1 character as First name
		// ###############################################################################################################
		strStepDesc = "Enter only 1 character in 'First Name' field and verify error message.";
		strActualResult = "Error Message '<b>"+strErrorMsg+"</b>' is displayed.";
		blnStepRC=enterDataAndVerifyErrorMessage("pgeCheckout","txtAuthUserFirstName","a","","errAuthUserFirstName",strErrorMsg,"ValidData");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
		
		// ###############################################################################################################
		// Step 12 - Enter only 1 character
		// Enter only 1 character as Last name
		// ###############################################################################################################
		strStepDesc = "Enter only 1 character in 'Last Name' field and verify error message.";
		strActualResult = "Error Message '<b>"+strErrorMsg+"</b>' is displayed.";
		blnStepRC=enterDataAndVerifyErrorMessage("pgeCheckout","txtAuthUserLastName","a","","errAuthUserLastName",strErrorMsg,"ValidData");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
		
		// ###############################################################################################################
		// Step 13 - Enter 10 characters
		// Enter 10 characters as First name
		// ###############################################################################################################
		strStepDesc = "Verify maximum length limit for 'First Name' field.";
		strActualResult = "'First Name' field is not accepting more than 10 characters.";
		blnStepRC=verifyMaxLengthLimit("pgeCheckout","txtAuthUserFirstName",strLongFirstName,strMaxFirstLength);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
		
		// ###############################################################################################################
		// Step 14 - Enter 10 characters
		// Enter 10 characters as Last name
		// ###############################################################################################################
		strStepDesc = "Verify maximum length limit for 'Last Name' field.";
		strActualResult = "'Last Name' field is not accepting more than 10 characters.";
		blnStepRC=verifyMaxLengthLimit("pgeCheckout","txtAuthUserLastName",strLongLastName,strMaxLastLength);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
				
		// ###############################################################################################################
		// Step 15 - Enter authorized user details
		// Enter authorized user details
		// ###############################################################################################################
		strStepDesc = "Enter First Name, Last Name, select '<b>"+strPrimaryRelation[0]+"</b>' as Primary Relationship "
				+ "and click on '+' icon to add the user.";
		strActualResult = "'Add Authorized user' section is collapsed and values First Name '<b>"+strFirstName+"</b>',Last Name '<b>"+strLastName+"</b>' are displayed.";
		blnStepRC=AddAuthorizedUserAsAccountHolder(strFirstName,strLastName,strPrimaryRelation[0]);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
		
		// ###############################################################################################################
		// Step 16 - Expand the 'Add User' section
		// Expand the 'Add User' section
		// ###############################################################################################################
		strStepDesc = "Expand the 'Add User' section by clicking '+' icon.";
		strActualResult = "'Add User' section is expanded and previously selected user's First Name,Last Name and "
				+ "Primary Relationship value '<b>"+strPrimaryRelation[0]+"</b>' are retained.";
		blnStepRC=verifyAdddedAuthUserDetails(strFirstName,strLastName,strPrimaryRelation[0]);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");		
		}
		
		// ###############################################################################################################
		// Step 17 - Delete the added user
		// Delete the added user
		// ###############################################################################################################
		strStepDesc = "Click on 'Delete' button and verify action performed.";
		strActualResult = "Added User is deleted from 'Add Authorized User' section.";
		blnStepRC=clickAndVerify("pgeCheckout","btnDeleteUser","pgeCheckout","btnAddUser");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");		
		}
		
		// ###############################################################################################################
		// Step 18 - Click on Add user
		// Click on Add user
		// ###############################################################################################################
		strStepDesc = "Click on 'Add user' button under 'Add Authorized User' section and verify fields.";
		strActualResult = "'Add User' section is expanded and fields 'First Name,Last Name,Relationship to Primary drop-down "
				+ "and Delete button are displayed.";
		blnStepRC=verifyAddAuthorizedUserFields();
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ###############################################################################################################
		// Step 19 - Enter authorized user details
		// Enter authorized user details
		// ###############################################################################################################
		strStepDesc = "Enter First Name, Last Name, select '<b>"+strPrimaryRelation[1]+"</b>' as Primary Relationship and "
				+ "click on '+' icon to add the user.";
		strActualResult = "'Add Authorized user' section is collapsed and values First Name '<b>"+strFirstName+"</b>',Last Name '<b>"+strLastName+ "</b>' are displayed.";
		blnStepRC=AddAuthorizedUserAsAccountHolder(strFirstName,strLastName,strPrimaryRelation[1]);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
		
		// ###############################################################################################################
		// Step 20 - Expand the 'Add User' section
		// Expand the 'Add User' section
		// ###############################################################################################################
		strStepDesc = "Expand the 'Add User' section by clicking '+' icon.";
		strActualResult = "'Add User' section is expanded and previously selected user's First Name,Last Name and "
				+ "Primary Relationship value '<b>"+strPrimaryRelation[1]+"</b>' are retained.";
		blnStepRC=verifyAdddedAuthUserDetails(strFirstName,strLastName,strPrimaryRelation[1]);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");		
		}
		
		// ###############################################################################################################
		// Step 21 - Delete the added user
		// Delete the added user
		// ###############################################################################################################
		strStepDesc = "Click on 'Delete' button and verify action performed.";
		strActualResult = "Added User is deleted from 'Add Authorized User' section.";
		blnStepRC=clickAndVerify("pgeCheckout","btnDeleteUser","pgeCheckout","btnAddUser");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");		
		}
		
		// ###############################################################################################################
		// Step 22 - Click on Add user
		// Click on Add user
		// ###############################################################################################################
		strStepDesc = "Click on 'Add user' button under 'Add Authorized User' section and verify fields.";
		strActualResult = "'Add User' section is expanded and fields 'First Name,Last Name,Relationship to Primary drop-down "
				+ "and Delete button are displayed.";
		blnStepRC=verifyAddAuthorizedUserFields();
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ###############################################################################################################
		// Step 23 - Enter authorized user details
		// Enter authorized user details
		// ###############################################################################################################
		strStepDesc = "Enter First Name, Last Name, select '<b>"+strPrimaryRelation[2]+"</b>' as Primary Relationship and click on '+' icon to add the user.";
		strActualResult = "'Add Authorized user' section is collapsed and values First Name '<b>"+strFirstName+"</b>',Last Name '<b>"+strLastName+"</b>' are displayed.";
		blnStepRC=AddAuthorizedUserAsAccountHolder(strFirstName,strLastName,strPrimaryRelation[2]);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
		
		// ###############################################################################################################
		// Step 24 - Expand the 'Add User' section
		// Expand the 'Add User' section
		// ###############################################################################################################
		strStepDesc = "Expand the 'Add User' section by clicking '+' icon.";
		strActualResult = "'Add User' section is expanded and previously selected user's First Name,Last Name and "
				+ "Primary Relationship '<b>"+strPrimaryRelation[2]+"</b>' are retained.";
		blnStepRC=verifyAdddedAuthUserDetails(strFirstName,strLastName,strPrimaryRelation[2]);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");		
		}
		
		// ###############################################################################################################
		// Step 25 - Delete the added user
		// Delete the added user
		// ###############################################################################################################
		strStepDesc = "Click on 'Delete' button and verify action performed.";
		strActualResult = "Added User is deleted from 'Add Authorized User' section.";
		blnStepRC=clickAndVerify("pgeCheckout","btnDeleteUser","pgeCheckout","btnAddUser");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");		
		}
		
		// ###############################################################################################################
		// Step 26 - Click on Add user
		// Click on Add user
		// ###############################################################################################################
		strStepDesc = "Click on 'Add user' button under 'Add Authorized User' section and verify fields.";
		strActualResult = "'Add User' section is expanded and fields 'First Name,Last Name,Relationship to Primary drop-down"
				+ " and Delete button are displayed.";
		blnStepRC=verifyAddAuthorizedUserFields();
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ###############################################################################################################
		// Step 27 - Enter authorized user details
		// Enter authorized user details
		// ###############################################################################################################
		strStepDesc = "Enter First Name, Last Name, select '<b>"+strPrimaryRelation[3]+"</b>' as Primary Relationship and click on '+' icon to add the user.";
		strActualResult = "'Add Authorized user' section is collapsed and values First Name '<b>"+strFirstName+"</b>',Last Name '<b>"+strLastName+"</b>' are displayed.";
		blnStepRC=AddAuthorizedUserAsAccountHolder(strFirstName,strLastName,strPrimaryRelation[3]);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
		
		// ###############################################################################################################
		// Step 28 - Expand the 'Add User' section
		// Expand the 'Add User' section
		// ###############################################################################################################
		strStepDesc = "Expand the 'Add User' section by clicking '+' icon.";
		strActualResult = "'Add User' section is expanded and previously selected user's First Name,Last Name and "
				+ "Primary Relationship '<b>"+strPrimaryRelation[3]+"</b>' are retained.";
		blnStepRC=verifyAdddedAuthUserDetails(strFirstName,strLastName,strPrimaryRelation[3]);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");		
		}
		
		// ###############################################################################################################
		// Step 29 - Delete the added user
		// Delete the added user
		// ###############################################################################################################
		strStepDesc = "Click on 'Delete' button and verify action performed.";
		strActualResult = "Added User is deleted from 'Add Authorized User' section.";
		blnStepRC=clickAndVerify("pgeCheckout","btnDeleteUser","pgeCheckout","btnAddUser");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");		
		}
	}
}
