package TestScript.CustomizeAddToCart;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import Launcher.Launcher;
import libraries.WebFunclib;
public class VerifyBundleIndividualPlansDeletionFromCart_SHOPM453 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;
	
	@Test
	public void VerifyBundleIndividualPlansDeletionFromCart(ITestContext testContext) throws InterruptedException 
	{

		// ########################################################################################################
		// # Test Case ID: SHOPM-453
		// # Test Case: VerifyBundleIndividualPlansDeletionFromCart
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify deletion of Bundle and Individual plans from Cart.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################

		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strPlanType1 = dicTestData.get("strPlanType1");
		String objDeleteCart = dicTestData.get("XpathDeleteButton");
		
		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter a street address where Comporium Service exists, Street Address: '<b>"+ strStreetAddressData + "</b>'";
		strActualResult = "'Zip code: '<b>"+""+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 4 - "Click on 'No, I'm Moving here'
		// "Click on 'No, I'm Moving here'
		// ########################################################################################################
		strStepDesc = "Click on 'No, I'm Moving here' from the pop-up.";
		strActualResult = "Fields 'Selected Service Address', 'Change Address','highlighted Bundle Plans','Individual Plans' and 'Cart section' are displayed.";
		blnStepRC = clickOnAddressConformationPopup("btnIamMovingHere",strStreetAddressData);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 4 - Click on 'Individual Plans'
		// Click on 'Individual Plans'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Individual Plans</b>' on 'Browse' page.";
		strActualResult = "Check-boxes for each bundle type '<b>TV,Internet,Phone</b>' and '<b>Security</b>' are displayed in 'I am looking for' section.";
		blnStepRC=selectBundleAndVerifyPlanTypes("pgeBrowse","btnIndividualPlans");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 5 - Select 'Internet' plan type
		// Select 'Internet' plan type
		// ########################################################################################################
		strStepDesc = "Select '<b>Internet</b>' plan type on 'Browse' page.";
		strActualResult = "Plans for only 'Internet' are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerify("pgeBrowse","chkInternetPlan","pgeBrowse","eleInternetPlanHeader");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 6 - Select 'TV' plan type
		// Select 'TV' plan type
		// ########################################################################################################
		strStepDesc = "Select '<b>TV</b>' plan type on 'Browse' page.";
		strActualResult = "Plans for only 'TV' are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerify("pgeBrowse","chkTVPlan","pgeBrowse","eleTVPlanTypeHeader");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 7 - Click on 'Customize' button
		// Click on 'Customize' button for an Internet Standard Plan
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button for a TV Plan on 'Browse' page.";
		strActualResult = "'<b>TV Package details</b>' are displayed along-with 'Equipment' section on 'Customize Order' page.";
		blnStepRC=clickAndVerify("pgeBrowse","btnHDTVPlanCustomize","pgeCustomizeOrder","eleTVPlanType","eleTVequipmentSection");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 8 - Click on Continue Shopping button
		// Click on Continue Shopping button
		// ########################################################################################################
		strStepDesc = "Click on Continue Shopping button on 'Customize Order' page.";
		strActualResult = "User is navigated to 'Browse' page.";
		blnStepRC=clickAndVerify("pgeCustomizeOrder","lnkContinueShopping","pgeProfilePage","btnChangeAddress");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 9 - Click on Cart icon and verify added plans
		// Click on Cart icon and verify added plans
		// ########################################################################################################
		strStepDesc = "Verify plans added under cart on clicking 'Cart' icon on 'Browse' page.";
		strActualResult = "Selected plans '<b>"+strPlanType1+"</b>' are successfully added under Cart.";
		blnStepRC=verifymultipleProductsUnderCart(strPlanType1);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 10 - Click on 'x' button
		// Click on 'x' button
		// ########################################################################################################
		strStepDesc = "Click on 'x' button and verify 'Cart is Empty' message.";
		strActualResult = "'<b>Your Cart is empty</b>' message is displayed under 'Cart' section..";
		blnStepRC=verifyEmptyCart(objDeleteCart);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 11 - Click on 'Customize' button
		// Click on 'Customize' button
		// ########################################################################################################
		strStepDesc = "Click on 'Customize Order' button from the cart";
		strActualResult = "User should be navigated to 'Browse' page.";
		blnStepRC=clickAndVerify("pgeProfilePage","btnCustomizeOrder","pgeBrowse","btnIndividualPlans");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 12 - Click on 'View All Bundles' check-box and verify 'I am looking for:' section.
		// Click on 'View All Bundles' check-box
		// ########################################################################################################
		strStepDesc = "Click on '<b>View All Bundles</b>' check-box and verify 'I am looking for:' section.";
		strActualResult = "Check-boxes for each bundle type '<b>TV,Internet,Phone</b>' and '<b>Security</b>' are displayed in 'I am looking for' section.";
		blnStepRC =selectViewAllBundlesAndVerify();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 13 - Select 'Internet' plan type
		// Select 'Internet' plan type
		// ########################################################################################################
		strStepDesc = "Select 'Internet' plan type on 'Browse' page.";
		strActualResult = "Plans for only 'Internet' are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerifyContainsText("pgeBrowse","chkInternetPlan","pgeBrowse","eleSelectedPlanDetails","Internet");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 14 - Select 'TV' plan type
		// Select 'TV' plan type
		// ########################################################################################################
		strStepDesc = "Select 'TV' plan type on 'Browse' page.";
		strActualResult = "Plans for only 'TV' are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerifyContainsText("pgeBrowse","chkTVPlan","pgeBrowse","eleSelectedPlanDetails","TV");		
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 15 - Click on Customize button for any Plan
		// Click on Customize button for any Plan
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button for any Plan on 'Browse' page.";
		strActualResult = "User is navigated to 'Customize Order' page.";
		blnStepRC=clickAndVerify("pgeBrowse","btnCustomize","pgeCustomizeOrder","lnkContinueShopping");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 16 - Click on 'x' button
		// Click on 'x' button
		// ########################################################################################################
		strStepDesc = "Click on 'x' button to delete the selected plans on 'Customize Order' page.";
		strActualResult = "User is navigated to 'Browse' page.";
		blnStepRC=clickAndVerify("pgeCustomizeOrder","btnRemovePlans","pgeBrowse","btnIndividualPlans");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 17 - Verify empty cart
		// Verify empty cart
		// ########################################################################################################
		strStepDesc = "Verify Cart is empty on Browse page.";
		strActualResult = "'<b>Your Cart is empty</b>' message is displayed under 'Cart' section.";
		blnStepRC=verifyEmptyCart();
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}