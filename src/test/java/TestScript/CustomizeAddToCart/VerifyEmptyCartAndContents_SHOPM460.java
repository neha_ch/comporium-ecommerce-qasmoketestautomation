package TestScript.CustomizeAddToCart;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import Launcher.Launcher;
import libraries.WebFunclib;

public class VerifyEmptyCartAndContents_SHOPM460 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;

	@Test
	public void VerifyEmptyCartAndContents(ITestContext testContext) throws InterruptedException 
	{
		// ########################################################################################################
		// # Test Case ID: SHOPM-460
		// # Test Case: VerifyEmptyCartAndContents
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify empty cart and other related information on 'Browse' page.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################

		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("str_Zip");
		
		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter a street address where Comporium Service exists, Street Address: '<b>"+ dicTestData.get("strStreetAddress") + "</b>'";
		strActualResult = "'Zip code: '<b>"+""+dicTestData.get("strZip")+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 4 - Click on 'No, I'm Moving here' from the pop-up
		// Click on 'No, I'm Moving here' from the pop-up
		// ########################################################################################################
		strStepDesc = "Click on 'No, I'm Moving here' from the pop-up.";
		strActualResult = "User is navigated to 'Browse' page.";
		blnStepRC = clickOnAddressConformationPopup("btnIamMovingHere",strStreetAddressData);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 5 - Click on Cart.
		// Click on Cart
		// ########################################################################################################
		strStepDesc = "Click on Cart icon and verify its content on 'Browse' page.";
		strActualResult = "'MY CART' heading along-with '<b>My Cart is Empty</b>' message is displayed.";
		blnStepRC=clickAndVerify("pgeBrowse","btnCartIcon","pgeBrowse","eleCartEmptyMsg","eleMyCartContents");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 6 - Click on 'Customize Order' button
		// Click on 'Customize Order' button
		// ########################################################################################################
		strStepDesc = "Click on 'Customize Order' button from 'My Cart' section.";
		strActualResult = "User remains on the 'Browse' page on clicking '<b>Customize Order</b>' button.";
		blnStepRC=clickAndVerify("pgeProfilePage","btnCustomizeOrder","pgeProfilePage","btnChangeAddress");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}