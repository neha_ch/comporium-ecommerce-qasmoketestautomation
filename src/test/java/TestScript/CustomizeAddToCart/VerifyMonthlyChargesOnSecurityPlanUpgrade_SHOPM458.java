package TestScript.CustomizeAddToCart;

import org.testng.ITestContext;
import org.testng.annotations.Test;
import Launcher.Launcher;
import libraries.WebFunclib;
public class VerifyMonthlyChargesOnSecurityPlanUpgrade_SHOPM458 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;
	
	@Test
	public void VerifyMonthlyChargesOnSecurityPlanUpgrade(ITestContext testContext) throws InterruptedException 
	{
		// ########################################################################################################
		// # Test Case ID: SHOPM-458
		// # Test Case: VerifyMonthlyChargesOnSecurityPlanUpgrade
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify changes to monthly charges on upgrading a Security Plan on Customize Order page.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-19-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################
		
		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strPlanName = dicTestData.get("strPlanType1");
		String strSelectValue=dicTestData.get("strSelectValue");
		String strSelectType=dicTestData.get("strSelectType");
		String strServices = dicTestData.get("strCartItems");
		String strFirstName = dicTestData.get("strFirstName");
		String strLastName = dicTestData.get("strLastName");
		String strPhoneNumber = dicTestData.get("strPhoneNumber");;
		String strSSN = dicTestData.get("strSSN");
		String strDOB = dicTestData.get("strDOB");
		String strEmail = dicTestData.get("strEmail");
		
		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium account exists, Street Address: '<b>"+strStreetAddressData+"</b>'";
		strActualResult = "'Zip code: '<b>"+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}	

		// ########################################################################################################
		// Step 4 - "Click on 'No, I'm Moving here'
		// "Click on 'No, I'm Moving here'
		// ########################################################################################################
		strStepDesc = "Click on 'No, I'm Moving here' from the pop-up.";
		strActualResult = "Fields 'Selected Service Address', 'Change Address','highlighted Bundle Plans','Individual Plans' and 'Cart section' are displayed.";
		blnStepRC = clickOnAddressConformationPopup("btnIamMovingHere",strStreetAddressData);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}		
		
		// ########################################################################################################
		// Step 5 - Click on 'Individual Plans'
		// Click on 'Individual Plans'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Individual Plans</b>' on 'Browse' page.";
		strActualResult = "Check-boxes for each bundle type '<b>TV,Internet,Phone</b>' and '<b>Security</b>' are displayed in 'I am looking for' section.";
		blnStepRC=selectBundleAndVerifyPlanTypes("pgeBrowse","btnIndividualPlans");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 6 - Select 'Security' plan type
		// Select 'Security' plan type
		// ########################################################################################################
		strStepDesc = "Select '<b>Security</b>' plan type on 'Browse' page.";
		strActualResult = "Plans for only 'Security' are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerify("pgeBrowse","chkSecurityPlan","pgeBrowse","eleSecurityPlanHeader");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 7 - Click on 'Customize' button
		// Click on 'Customize' button for a Security Plan
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button for a Security Plan on 'Browse' page.";
		strActualResult = "'<b>Security Plan details</b>' are displayed on 'Customize Order' page.";
		blnStepRC=clickAndVerify("pgeBrowse","btnCustomizeSecurityPlan","pgeCustomizeOrder","eleSecurityPlanHeader");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 8 - Verify 'Equipments' for Security plan
		//  Verify 'Equipments' for Security plan
		// ########################################################################################################
		strStepDesc = "Verify 'Equipments' section for 'Security' plan type.";
		strActualResult = "'Equipments' section is displayed for 'Security' plan type.";
		blnStepRC= page("pgeCustomizeOrder").element("eleSecurityEquipments").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 9 - Verify Charges for 'Security Plans'
		//  Verify 'Security Plans'
		// ########################################################################################################
		strStepDesc = "Verify Charges for 'Security' plans are showing under 'Monthly Charges' section.";
		strActualResult = "Charges for only 'Security' plans are displayed under 'Monthly Charges' section.";
		blnStepRC=verifySelectedPlansUnderMonthlyCharges(1,strPlanName); 
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 10 - Select an Equipment
		//  Select an Equipment
		// ########################################################################################################
		strStepDesc = "Select a Security Equipment from the list of equipments and verify One Time Charges.";
		strActualResult = "'Sub Total' under One Time Charges are updated when a security equipment is selected.";
		blnStepRC=selectFromList("pgeCustomizeOrder","lstDoorLockSecurityEquip",strSelectValue,strSelectType,"pgeCustomizeOrder","eleOneTimeServices");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 11 - Select an Equipment
		//  Select an Equipment
		// ########################################################################################################
		strStepDesc = "Select another Security Equipment from the list of equipments and verify One Time Charges.";
		strActualResult = "'Sub Total' under One Time Charges are updated when a security equipment is selected.";
		blnStepRC=selectFromList("pgeCustomizeOrder","lstSmokeDetSecurityEquip",strSelectValue,strSelectType,"pgeCustomizeOrder","eleOneTimeServices");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 12 - Verify selected feature services
		//  Verify selected feature services
		// ########################################################################################################
		strStepDesc = "Verify services '<b>"+strServices+"</b>' populated under Additional Monthly Charges section on 'Customize Order' page.";
		strActualResult = "Selected Services with $ amount are displayed under 'Additional Monthly Charges' section.";
		blnStepRC=verifyMultipleServicesChargesOnCustomize("pgeCustomizeOrder","eleOneTimeServices",strServices);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 12 - Fetch One Time Charges
		//  Fetch One Time Charges
		// ########################################################################################################
		strStepDesc = "Fetch 'One Time Charges' on 'Customize Order' page.";
		strActualResult = "'One Time Charges' are fetched from 'Customize Order' page.";
		blnStepRC=getTextAndSaveDictionaryVal("pgeCustomizeOrder","eleOneTimePlanCharges","strOneTimeCharges","$");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 13 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on Continue button on 'Customize Order' page.";
		strActualResult = "User is navigated to 'Checkout' page.";
		blnStepRC =checkoutPlanAndVerify();
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 14 - Verify One Time Charges
		// Verify One Time Charges
		// ########################################################################################################
		strStepDesc = "Verify One Time Charges on 'Checkout' page.";
		strActualResult = "One Time Charges is same as on Customize Order page.";
		
		blnStepRC =page("pgeCustomizeOrder").element("eleOneTimePlanCharges").getText().replace("$","").equals(dicTestData.get("strOneTimeCharges"));
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,"Failed to verify One Time Charges on Checkout page.", "Fail");
		}
		
		// ########################################################################################################
		// Step 15 - Enter Billing Address details
		// Enter Billing Address details for existing customer
		// ########################################################################################################
		strStepDesc = "Enter all details in Primary Account Holder section, select 'No Thanks' checkbox and then Click Continue.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC=enterCheckoutInformation(strFirstName, strLastName,strPhoneNumber, strDOB, strSSN, strEmail,true,"pgeServiceSchedule","eleSchedulePageTitle");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}
