package TestScript.CustomizeAddToCart;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import Launcher.Launcher;
import libraries.WebFunclib;
public class VerifyCutomizedCartWithIndividualPlans_SHOPM463 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;
	
	@Test
	public void VerifyCutomizedCartWithIndividualPlans(ITestContext testContext) throws InterruptedException 
	{

		// ########################################################################################################
		// # Test Case ID: SHOPM-463
		// # Test Case: VerifyCutomizedCartWithIndividualPlans
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify Customized Cart by adding individual plans one by one.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################

		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strPlanType1 = dicTestData.get("strPlanType1");
		String strPlanType2 = dicTestData.get("strPlanType2");
		String strPlanType3 = dicTestData.get("strPlanType3");
		String strPlanType4 = dicTestData.get("strPlanType4");
		String strCartItems = dicTestData.get("strCartItems");
		String strErrorMessage = dicTestData.get("strErrorMessage");

		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter a street address where Comporium Service exists, Street Address: '<b>"+ strStreetAddressData + "</b>'";
		strActualResult = "'Zip code: '<b>"+""+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		strActualResult = "User is navigated to 'Browse' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,false);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 4 - Click on 'Individual Plans'
		// Click on 'Individual Plans'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Individual Plans</b>' on 'Browse' page.";
		strActualResult = "Check-boxes for each bundle type '<b>TV,Internet,Phone</b>' and '<b>Security</b>' are displayed in 'I am looking for' section.";
		blnStepRC=selectBundleAndVerifyPlanTypes("pgeBrowse","btnIndividualPlans");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 5 - Select 'Internet' plan type
		// Select 'Internet' plan type
		// ########################################################################################################
		strStepDesc = "Select '<b>Internet</b>' plan type on 'Browse' page.";
		strActualResult = "Plans for only 'Internet' are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerify("pgeBrowse","chkInternetPlan","pgeBrowse","eleInternetPlanHeader");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 6 - Click on 'Customize' button
		// Click on 'Customize' button for an Internet Standard Plan
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button for an Internet Standard Plan on 'Browse' page.";
		strActualResult = "'<b>Internet Standard Package details</b>' are displayed along-with 'Recommended Equipment Upgrade' section on 'Customize Order' page.";
		blnStepRC=clickAndVerify("pgeBrowse","btnStdInternetCustomize","pgeCustomizeOrder","eleInternetPlanType","eleEquipmentSection");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 7 - Click on Continue Shopping button
		// Click on Continue Shopping button
		// ########################################################################################################
		strStepDesc = "Click on Continue Shopping button on 'Customize Order' page.";
		strActualResult = "User is navigated to 'Browse' page.";
		blnStepRC=clickAndVerify("pgeCustomizeOrder","lnkContinueShopping","pgeProfilePage","btnChangeAddress");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 8 - Click on Cart icon and verify added plans
		// Click on Cart icon and verify added plans
		// ########################################################################################################
		strStepDesc = "Verify plans added under cart on clicking 'Cart' icon on 'Browse' page.";
		strActualResult = "Selected plans '<b>"+strPlanType1+"</b>' are successfully added under Cart.";
		blnStepRC=verifyCartItems(strCartItems,strPlanType1,1);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 9 - Click on 'Individual Plans'
		// Click on 'Individual Plans'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Individual Plans</b>' on 'Browse' page.";
		strActualResult = "Check-boxes for each bundle type '<b>TV,Internet,Phone</b>' and '<b>Security</b>' are displayed in 'I am looking for' section.";
		blnStepRC=selectBundleAndVerifyPlanTypes("pgeBrowse","btnIndividualPlans");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 10 - Select 'TV' plan type
		// Select 'TV' plan type
		// ########################################################################################################
		strStepDesc = "Select '<b>TV</b>' plan type on 'Browse' page.";
		strActualResult = "Plans for only 'TV' are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerify("pgeBrowse","chkTVPlan","pgeBrowse","eleTVPlanTypeHeader");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 11 - Click on 'Customize' button
		// Click on 'Customize' button for an Internet Standard Plan
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button for a TV Plan on 'Browse' page.";
		strActualResult = "'<b>TV Package details</b>' are displayed along-with 'Equipment' section on 'Customize Order' page.";
		blnStepRC=clickAndVerify("pgeBrowse","btnHDTVPlanCustomize","pgeCustomizeOrder","eleTVPlanType","eleTVequipmentSection");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 12 - Click on Continue Shopping button
		// Click on Continue Shopping button
		// ########################################################################################################
		strStepDesc = "Click on Continue Shopping button on 'Customize Order' page.";
		strActualResult = "User is navigated to 'Browse' page.";
		blnStepRC=clickAndVerify("pgeCustomizeOrder","lnkContinueShopping","pgeProfilePage","btnChangeAddress");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 13 - Click on Cart icon and verify added plans
		// Click on Cart icon and verify added plans
		// ########################################################################################################
		strStepDesc = "Verify plans added under cart on clicking 'Cart' icon on 'Browse' page.";
		strActualResult = "Selected individual plans '<b>"+strPlanType1+"</b>' and '<b>"+strPlanType2+"</b>' are successfully added under Cart.";
		blnStepRC=verifyCartItems(strCartItems,strPlanType1+strPlanType2,2);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 14 - Click on 'Individual Plans'
		// Click on 'Individual Plans'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Individual Plans</b>' on 'Browse' page.";
		strActualResult = "Check-boxes for each bundle type '<b>TV,Internet,Phone</b>' and '<b>Security</b>' are displayed in 'I am looking for' section.";
		blnStepRC=selectBundleAndVerifyPlanTypes("pgeBrowse","btnIndividualPlans");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 15 - Select 'Security' plan type
		// Select 'Security' plan type
		// ########################################################################################################
		strStepDesc = "Select '<b>Security</b>' plan type on 'Browse' page.";
		strActualResult = "Plans for only 'Security' are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerify("pgeBrowse","chkSecurityPlan","pgeBrowse","eleSecurityPlanHeader");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 16 - Click on 'Customize' button
		// Click on 'Customize' button for a Security Plan
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button for a Security Plan on 'Browse' page.";
		strActualResult = "'<b>Security Plan details</b>' are displayed on 'Customize Order' page.";
		blnStepRC=clickAndVerify("pgeBrowse","btnCustomizeSecurityPlan","pgeCustomizeOrder","eleSecurityPlanHeader");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}		
				
		// ########################################################################################################
		// Step 17 - Click on Continue Shopping button
		// Click on Continue Shopping button
		// ########################################################################################################
		strStepDesc = "Click on Continue Shopping button on 'Customize Order' page.";
		strActualResult = "User is navigated to 'Browse' page.";
		blnStepRC=clickAndVerify("pgeCustomizeOrder","lnkContinueShopping","pgeProfilePage","btnChangeAddress");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 18 - Click on Cart icon and verify added plans
		// Click on Cart icon and verify added plans
		// ########################################################################################################
		strStepDesc = "Verify plans added under cart on clicking 'Cart' icon on 'Browse' page.";
		strActualResult = "Selected individual plans '<b>"+strPlanType1+"</b>','<b>"+strPlanType2+"</b>' and '<b>"+strPlanType3+"</b>' are successfully added under Cart.";
		blnStepRC=verifyCartItems(strCartItems,strPlanType1+strPlanType2+strPlanType3,3);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}		
		
		// ########################################################################################################
		// Step 19 - Click on 'Individual Plans'
		// Click on 'Individual Plans'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Individual Plans</b>' on 'Browse' page.";
		strActualResult = "Check-boxes for each bundle type '<b>TV,Internet,Phone</b>' and '<b>Security</b>' are displayed in 'I am looking for' section.";
		blnStepRC=selectBundleAndVerifyPlanTypes("pgeBrowse","btnIndividualPlans");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 20 - Select 'Phone' plan type
		// Select 'Phone' plan type
		// ########################################################################################################
		strStepDesc = "Select '<b>Phone</b>' plan type on 'Browse' page.";
		strActualResult = "Plans for only 'Phone' are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerify("pgeBrowse","chkPhonePlan","pgeBrowse","elePhonePlanTypeHeader");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 21 - Click on 'Customize' button
		// Click on 'Customize' button for a Phone Plan
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button on 'Browse' page.";
		strActualResult = "'User is navigated to 'Customize Order' page.";
		blnStepRC=clickAndVerify("pgeBrowse","btnCustomize","pgeCustomizeOrder","elePhonePlanHeader");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 22 - Click on Continue Shopping button
		// Click on Continue Shopping button
		// ########################################################################################################
		strStepDesc = "Click on Continue Shopping button on 'Customize Order' page.";
		strActualResult = "User is navigated to 'Browse' page.";
		blnStepRC=clickAndVerify("pgeCustomizeOrder","lnkContinueShopping","pgeProfilePage","btnChangeAddress");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 23 - Click on Cart icon and verify added plans
		// Click on Cart icon and verify added plans
		// ########################################################################################################
		strStepDesc = "Verify plans added under cart on clicking 'Cart' icon on 'Browse' page.";
		strActualResult = "Selected individual plans '<b>"+strPlanType1+"</b>','<b>"+strPlanType2+"</b>','<b>"+strPlanType3+"</b>' and '<b>"+strPlanType4+"</b>' are successfully added under Cart.";
		blnStepRC=verifyCartItems(strCartItems,strPlanType1+strPlanType2+strPlanType3+strPlanType4,4);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}		
				
		// ########################################################################################################
		// Step 24 - Click on 'Individual Plans'
		// Click on 'Individual Plans'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Individual Plans</b>' on 'Browse' page.";
		strActualResult = "Check-boxes for each bundle type '<b>TV,Internet,Phone</b>' and '<b>Security</b>' are displayed in 'I am looking for' section.";
		blnStepRC=selectBundleAndVerifyPlanTypes("pgeBrowse","btnIndividualPlans");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 25 - Click on 'Customize'
		// Click on 'Customize'
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button for an Internet plan on 'Browse' page.";
		strActualResult = "Error Message <b>'"+strErrorMessage+"'</b> is displayed successfully.";
		blnStepRC=clickHoverAndVerify("pgeBrowse","btnStdInternetCustomize","pgeBrowse","eleErrorMessage");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 26 - Click on 'Customize'
		// Click on 'Customize'
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button for a TV plan on 'Browse' page.";
		strActualResult = "Error Message <b>'"+strErrorMessage+"'</b> is displayed successfully.";
		blnStepRC=clickHoverAndVerify("pgeBrowse","btnHDTVPlanCustomize","pgeBrowse","eleErrorMessage");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 27 - Click on 'Customize'
		// Click on 'Customize'
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button for a Security plan on 'Browse' page.";
		strActualResult = "Error Message <b>'"+strErrorMessage+"'</b> is displayed successfully.";
		blnStepRC=clickHoverAndVerify("pgeBrowse","btnCustomizeSecurity","pgeBrowse","eleErrorMessage");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 28 - Click on 'Customize'
		// Click on 'Customize'
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button for a Phone plan on 'Browse' page.";
		strActualResult = "Error Message <b>'"+strErrorMessage+"'</b> is displayed successfully.";
		blnStepRC=clickHoverAndVerify("pgeBrowse","btnCustomizePhone","pgeBrowse","eleErrorMessage");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}