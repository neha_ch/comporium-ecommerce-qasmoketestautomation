package TestScript.CustomizeAddToCart;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import Launcher.Launcher;
import libraries.WebFunclib;
public class VerifyAlertOnUpgradingStdTVPlanToMoxiPlanForPBTI_SHOPM955 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;
	
	@Test
	public void VerifyAlertOnUpgradingStdTVPlanToMoxiPlanForPBTI(ITestContext testContext) throws InterruptedException 
	{
		// ########################################################################################################
		// # Test Case ID: SHOPM-955
		// # Test Case: VerifyAlertOnUpgradingStdTVPlanToMoxiPlanForPBTI
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify alert when a Standard Telephone plan is upgraded to Moxi Plan on Customize Order page for PBTI Customers
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-19-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################
		
		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strAlertText1 = dicTestData.get("strErrorMessage");
		String strAlertText2 = dicTestData.get("strAlertPopUp");
		String strUpdatedPlanName = dicTestData.get("strPlanType1");
		
		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium account exists, Street Address: '<b>"+strStreetAddressData+"</b>'";
		strActualResult = "'Zip code: '<b>"+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}	

		// ########################################################################################################
		// Step 4 - "Click on 'No, I'm Moving here'
		// "Click on 'No, I'm Moving here'
		// ########################################################################################################
		strStepDesc = "Click on 'No, I'm Moving here' from the pop-up.";
		strActualResult = "Fields 'Selected Service Address', 'Change Address','highlighted Bundle Plans','Individual Plans' and 'Cart section' are displayed.";
		blnStepRC = clickOnAddressConformationPopup("btnIamMovingHere",strStreetAddressData);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}		
		
		// ########################################################################################################
		// Step 5 - Click on 'Individual Plans'
		// Click on 'Individual Plans'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Individual Plans</b>' on 'Browse' page.";
		strActualResult = "Check-boxes for each bundle type '<b>TV,Internet,Phone</b>' and '<b>Security</b>' are displayed in 'I am looking for' section.";
		blnStepRC=selectBundleAndVerifyPlanTypes("pgeBrowse","btnIndividualPlans");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 6 - Select 'Internet' plan type
		// Select 'Internet' plan type
		// ########################################################################################################
		strStepDesc = "Select '<b>Internet</b>' plan type on 'Browse' page.";
		strActualResult = "Plans for only 'Internet' are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerify("pgeBrowse","chkInternetPlan","pgeBrowse","eleInternetPlanHeader");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 7 - Click on 'Customize' button
		// Click on 'Customize' button for an Internet Standard Plan
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button for an Internet Standard Plan on 'Browse' page.";
		strActualResult = "'<b>Internet Standard Package details</b>' are displayed on 'Customize Order' page.";
		blnStepRC=clickAndVerify("pgeBrowse","btnStdInternetCustomize","pgeCustomizeOrder","eleInternetPlanType");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 8 - Verify Plan Name
		// Verify Plan Name
		// ########################################################################################################
		strStepDesc = "Verify selected plan's name under 'Monthly Charges' section.";
		strActualResult = "Selected plan's name '<b>Standard Internet</b>' is displayed under 'Monthly Charges' section.";
		blnStepRC=verifyTextContains("pgeCustomizeOrder","eleSelectedPlanName",strUpdatedPlanName);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 9 - Click on Continue Shopping button
		// Click on Continue Shopping button
		// ########################################################################################################
		strStepDesc = "Click on Continue Shopping button on 'Customize Order' page.";
		strActualResult = "User is navigated to 'Browse' page.";
		blnStepRC=clickAndVerify("pgeCustomizeOrder","lnkContinueShopping","pgeProfilePage","btnChangeAddress");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 10 - Click on 'Individual Plans'
		// Click on 'Individual Plans'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Individual Plans</b>' on 'Browse' page.";
		strActualResult = "Check-boxes for each bundle type '<b>TV,Internet,Phone</b>' and '<b>Security</b>' are displayed in 'I am looking for' section.";
		blnStepRC=selectBundleAndVerifyPlanTypes("pgeBrowse","btnIndividualPlans");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 11 - Select 'TV' plan type
		// Select 'TV' plan type
		// ########################################################################################################
		strStepDesc = "Select '<b>TV</b>' plan type on 'Browse' page.";
		strActualResult = "Plans for only 'TV' are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerify("pgeBrowse","chkTVPlan","pgeBrowse","eleTVPlanTypeHeader");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 12 - Click on 'Customize' button
		// Click on 'Customize' button
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button for a TV Plan on 'Browse' page.";
		strActualResult = "'<b>TV Package details</b>' are displayed along-with 'Equipment' section on 'Customize Order' page.";
		blnStepRC=clickAndVerify("pgeBrowse","btnHDTVPlanCustomize","pgeCustomizeOrder","eleTVPlanType","eleTVequipmentSection");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 13 - Remove 'Internet' plan
		// Remove 'Internet' plan
		// ########################################################################################################
		strStepDesc = "Click on 'X' for the Internet plan on 'Customize Order' page.";
		strActualResult = "Internet Plan is removed from Customize page.";
		blnStepRC=!clickAndVerify("pgeCustomizeOrder","btnRemoveInternetPlan","pgeCustomizeOrder","eleInternetPlanType");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 14 - Select Upgrade To Moxi
		// Select Upgrade To Moxi
		// ########################################################################################################
		strStepDesc = "Select '<b>Upgrade to Moxi Whole Home HD DVR Digital Variety</b>' plan under 'Television' section.";
		strActualResult = "Alert Pop-Up '<b>"+strAlertText1+"</b>' is displayed on 'Customize Order' page.";
		blnStepRC=clickHoverAndVerify("pgeCustomizeOrder","chkTVmaxiHomeHDPlan","pgeCustomizeOrder","eleHighSpeedIntAlert");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 15 - Click on Continue Shopping button
		// Click on Continue Shopping button
		// ########################################################################################################
		strStepDesc = "Click on Continue Shopping button on 'Customize Order' page.";
		strActualResult = "User is navigated to 'Browse' page.";
		blnStepRC=clickAndVerify("pgeCustomizeOrder","lnkContinueShopping","pgeProfilePage","btnChangeAddress");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 16 - Click on 'Individual Plans'
		// Click on 'Individual Plans'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Individual Plans</b>' on 'Browse' page.";
		strActualResult = "Check-boxes for each bundle type '<b>TV,Internet,Phone</b>' and '<b>Security</b>' are displayed in 'I am looking for' section.";
		blnStepRC=selectBundleAndVerifyPlanTypes("pgeBrowse","btnIndividualPlans");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 17 - Select 'Internet' plan type
		// Select 'Internet' plan type
		// ########################################################################################################
		strStepDesc = "Select '<b>Internet</b>' plan type on 'Browse' page.";
		strActualResult = "Plans for only 'Internet' are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerify("pgeBrowse","chkInternetPlan","pgeBrowse","eleInternetPlanHeader");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 18 - Click on 'Customize' button
		// Click on 'Customize' button for an Internet Standard Plan
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button for an Internet Standard Plan on 'Browse' page.";
		strActualResult = "'<b>Internet Standard Package details</b>' are displayed on 'Customize Order' page.";
		blnStepRC=clickAndVerify("pgeBrowse","btnStdInternetCustomize","pgeCustomizeOrder","eleInternetPlanType");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 19 - Select a TV Plan
		// Select a TV Plan
		// ########################################################################################################
		strStepDesc = "Select '<b>Upgrade to Moxi Whole Home HD DVR Digital Variety</b>' plan under 'Television' section.";
		strActualResult = "Alert Pop-Up '<b>"+strAlertText2+"</b>' is not displayed on 'Customize Order' page.";
		blnStepRC=!clickAndVerify("pgeCustomizeOrder","chkTVmaxiHomeHDPlan","pgeCustomizeOrder","eleInternetAlertPopUp");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 20 - Remove 'Internet' plan
		// Remove 'Internet' plan
		// ########################################################################################################
		strStepDesc = "Click on 'X' for the Internet plan on 'Customize Order' page.";
		strActualResult = "Alert Pop-Up '<b>"+strAlertText1+"</b>' is displayed on 'Customize Order' page.";
		blnStepRC=clickHoverAndVerify("pgeCustomizeOrder","btnRemoveInternetPlan","pgeCustomizeOrder","eleHighSpeedIntAlert");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}
