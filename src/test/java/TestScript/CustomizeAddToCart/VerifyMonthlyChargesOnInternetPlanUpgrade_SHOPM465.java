package TestScript.CustomizeAddToCart;

import org.testng.ITestContext;
import org.testng.annotations.Test;
import Launcher.Launcher;
import libraries.WebFunclib;
public class VerifyMonthlyChargesOnInternetPlanUpgrade_SHOPM465 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;
	
	@Test
	public void VerifyMonthlyChargesOnInternetPlanUpgrade(ITestContext testContext) throws InterruptedException 
	{
		// ########################################################################################################
		// # Test Case ID: SHOPM-465
		// # Test Case: VerifyMonthlyChargesOnInternetPlanUpgrade
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify changes to monthly charges on upgrading an Internet Plan on Customize Order page.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-19-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################
		
		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strPlanName = dicTestData.get("strPlanType1");
		String strUpdatedPlanName = dicTestData.get("strPlanType2");
		String strFirstName = dicTestData.get("strFirstName");
		String strLastName = dicTestData.get("strLastName");
		String strPhoneNumber = dicTestData.get("strPhoneNumber");;
		String strSSN = dicTestData.get("strSSN");
		String strDOB = dicTestData.get("strDOB");
		String strEmail = dicTestData.get("strEmail");
		
		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium account exists, Street Address: '<b>"+strStreetAddressData+"</b>'";
		strActualResult = "'Zip code: '<b>"+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}	

		// ########################################################################################################
		// Step 4 - "Click on 'No, I'm Moving here'
		// "Click on 'No, I'm Moving here'
		// ########################################################################################################
		strStepDesc = "Click on 'No, I'm Moving here' from the pop-up.";
		strActualResult = "Fields 'Selected Service Address', 'Change Address','highlighted Bundle Plans','Individual Plans' and 'Cart section' are displayed.";
		blnStepRC = clickOnAddressConformationPopup("btnIamMovingHere",strStreetAddressData);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}		
		
		// ########################################################################################################
		// Step 5 - Click on 'Individual Plans'
		// Click on 'Individual Plans'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Individual Plans</b>' on 'Browse' page.";
		strActualResult = "Check-boxes for each bundle type '<b>TV,Internet,Phone</b>' and '<b>Security</b>' are displayed in 'I am looking for' section.";
		blnStepRC=selectBundleAndVerifyPlanTypes("pgeBrowse","btnIndividualPlans");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 6 - Select 'Internet' plan type
		// Select 'Internet' plan type
		// ########################################################################################################
		strStepDesc = "Select '<b>Internet</b>' plan type on 'Browse' page.";
		strActualResult = "Plans for only 'Internet' are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerify("pgeBrowse","chkInternetPlan","pgeBrowse","eleInternetPlanHeader");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 7 - Click on 'Customize' button
		// Click on 'Customize' button for an Internet Standard Plan
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button for an Internet Standard Plan on 'Browse' page.";
		strActualResult = "'<b>Internet Standard Package details</b>' are displayed along-with 'Recommended Equipment Upgrade' section on 'Customize Order' page.";
		blnStepRC=clickAndVerify("pgeBrowse","btnStdInternetCustomize","pgeCustomizeOrder","eleInternetPlanType","eleEquipmentSection");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 8 - Verify 'Internet' plan add-ons
		//  Verify 'Internet' plan add-ons
		// ########################################################################################################
		strStepDesc = "Verify '<b>Upgrade to Elite</b>' add-on populated under 'Internet' section.";
		strActualResult = "'Upgrade to Elite' add-on is populated under 'Internet' section.";
		blnStepRC= page("pgeCustomizeOrder").element("eleUpgradeEliteAddOn").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 9 - Verify 'Internet' plan add-ons
		//  Verify 'Internet' plan add-ons
		// ########################################################################################################
		strStepDesc = "Verify 'Upgrade to Ultra' add-on populated under 'Internet' section.";
		strActualResult = "'Upgrade to Ultra' add-on is populated under 'Internet' section.";
		blnStepRC= page("pgeCustomizeOrder").element("eleUpgradeUltraAddOn").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 10 - Verify 'Internet charges'
		//  Verify 'Internet charges'
		// ########################################################################################################
		strStepDesc = "Verify only Internet Charges under 'Monthly Charges' section.";
		strActualResult = "Only Internet charges are displayed under 'Monthly Charges' section.";
		blnStepRC= verifyMonthlyChargesForSelectedPlan(1);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 11 - Upgrade the Internet Plan
		//  Upgrade the Internet Plan
		// ########################################################################################################
		strStepDesc = "Select 'Upgrade to Elite' add-on and verify Monthly and One Time Charges after upgrade.";
		strActualResult = "Plan name and charges are updated in Monthly Charges and One Time Charges section under Internet.";
		blnStepRC=verifyMonthlyChargesOnPlanUpgrade("pgeCustomizeOrder","chkUpgradeEliteAddOn","eleSelectedPlanName",strPlanName,false,"clickCheckBox");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 12 - Upgrade the Internet Plan
		//  Upgrade the Internet Plan
		// ########################################################################################################
		strStepDesc = "Select an equipment and verify Monthly and One Time Charges after upgrade.";
		strActualResult = "Plan name and charges are updated in Monthly Charges and One Time Charges section under Internet.";
		blnStepRC=verifyMonthlyChargesOnPlanUpgrade("pgeCustomizeOrder","chkUpgradeEquipment","elePlanEquipmentUpdate",strUpdatedPlanName,true,"clickCheckBox");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 13 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on Continue button on 'Customize Order' page.";
		strActualResult = "User is navigated to 'Checkout' page.";
		blnStepRC =checkoutPlanAndVerify();
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 14 - Enter Billing Address details
		// Enter Billing Address details for existing customer
		// ########################################################################################################
		strStepDesc = "Enter all details in Primary Account Holder section, select 'No Thanks' checkbox and then Click Continue.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC =enterCheckoutInformation(strFirstName, strLastName,strPhoneNumber, strDOB, strSSN, strEmail,true);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}
