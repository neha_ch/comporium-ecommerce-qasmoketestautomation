package TestScript.CustomizeAddToCart;

import org.testng.ITestContext;
import org.testng.annotations.Test;
import Launcher.Launcher;
import libraries.WebFunclib;
public class VerifyMonthlyChargesOnTelephonePlanUpgrade_SHOPM454 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;
	
	@Test
	public void VerifyMonthlyChargesOnTelephonePlanUpgrade(ITestContext testContext) throws InterruptedException 
	{
		// ########################################################################################################
		// # Test Case ID: SHOPM-454
		// # Test Case: VerifyMonthlyChargesOnTelephonePlanUpgrade
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify changes to monthly charges on upgrading a Telephone Plan on Customize Order page.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-19-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################
		
		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strPlanName = dicTestData.get("strPlanType1");
		String strUpdatedPlanName  = dicTestData.get("strPlanType2");
		String strFirstName = dicTestData.get("strFirstName");
		String strLastName = dicTestData.get("strLastName");
		String strPhoneNumber = dicTestData.get("strPhoneNumber");;
		String strSSN = dicTestData.get("strSSN");
		String strDOB = dicTestData.get("strDOB");
		String strEmail = dicTestData.get("strEmail");
		
		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium account exists, Street Address: '<b>"+strStreetAddressData+"</b>'";
		strActualResult = "'Zip code: '<b>"+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}	

		// ########################################################################################################
		// Step 4 - "Click on 'No, I'm Moving here'
		// "Click on 'No, I'm Moving here'
		// ########################################################################################################
		strStepDesc = "Click on 'No, I'm Moving here' from the pop-up.";
		strActualResult = "Fields 'Selected Service Address', 'Change Address','highlighted Bundle Plans','Individual Plans' and 'Cart section' are displayed.";
		blnStepRC = clickOnAddressConformationPopup("btnIamMovingHere",strStreetAddressData);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}		
		
		// ########################################################################################################
		// Step 5 - Click on 'Individual Plans'
		// Click on 'Individual Plans'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Individual Plans</b>' on 'Browse' page.";
		strActualResult = "Check-boxes for each bundle type '<b>TV,Internet,Phone</b>' and '<b>Security</b>' are displayed in 'I am looking for' section.";
		blnStepRC=selectBundleAndVerifyPlanTypes("pgeBrowse","btnIndividualPlans");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 6 - Select 'Phone' plan type
		// Select 'Phone' plan type
		// ########################################################################################################
		strStepDesc = "Select '<b>Phone</b>' plan type on 'Browse' page.";
		strActualResult = "Plans for only 'Phone' are displayed on the 'Browse' page.";
		blnStepRC=clickAndVerify("pgeBrowse","chkPhonePlan","pgeBrowse","elePhonePlanTypeHeader");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 7 - Click on 'Customize' button
		// Click on 'Customize' button for a Phone Plan
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button on 'Browse' page.";
		strActualResult = "'User is navigated to 'Customize Order' page.";
		blnStepRC=clickAndVerify("pgeBrowse","btnCustomize","pgeCustomizeOrder","elePhonePlanHeader");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 8 - Verify 'Phone' plan add-ons
		//  Verify 'Phone' plan add-ons
		// ########################################################################################################
		strStepDesc = "Verify add-ons populated under 'Telephone' section.";
		strActualResult = "Add-ons are populated under 'Telephone' section.";
		blnStepRC= page("pgeCustomizeOrder").element("elePhonePlanAddOns").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 9 - Verify selected 'Phone' Features
		//  Verify 'Phone' features
		// ########################################################################################################
		strStepDesc = "Verify selected 'Phone' plan's features populated under 'Telephone' section.";
		strActualResult = "Selected Phone plan's features are populated under 'Telephone' section.";
		blnStepRC= page("pgeCustomizeOrder").element("elePhonePlanFeatures").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 10 - Verify 'Phone charges'
		//  Verify 'Phone charges'
		// ########################################################################################################
		strStepDesc = "Verify only Phone Charges under 'Monthly Charges' section.";
		strActualResult = "Only Phone charges are displayed under 'Monthly Charges' section.";
		blnStepRC=verifySelectedPlansUnderMonthlyCharges(1,strPlanName);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 11 - Upgrade the Telephone Plan
		//  Upgrade the Telephone Plan
		// ########################################################################################################
		strStepDesc = "Upgrade the plan by selecting an Add-On and verify Monthly and One Time Charges after upgrade.";
		strActualResult = "Plan name and charges are updated in Monthly Charges and One Time Charges section under Phone.";
		blnStepRC=verifyMonthlyChargesOnPlanUpgrade("pgeCustomizeOrder","chkPhonePlanAddOns","eleSelectedPlanName",strUpdatedPlanName,false,"clickCheckBox");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 12 - Upgrade the Telephone Plan
		//  Upgrade the Telephone Plan
		// ########################################################################################################
		strStepDesc = "Select a Phone feature and verify Monthly and One Time Charges after upgrade.";
		strActualResult = "Plan name and charges are updated in Monthly Charges and One Time Charges section under Phone.";
		blnStepRC=verifyMonthlyChargesOnPlanUpgrade("pgeCustomizeOrder","chkPhonePlanFeature","eleSelectedPlanName",strUpdatedPlanName,true,"clickCheckBox");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 13 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on Continue button on 'Customize Order' page.";
		strActualResult = "User is navigated to 'Checkout' page.";
		blnStepRC =checkoutPlanAndVerify();
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 14 - Verify Monthly Charges
		// Verify Monthly Charges
		// ########################################################################################################
		strStepDesc = "Verify Monthly Charges on 'Checkout' page.";
		strActualResult = "Monthly Charges is same as on Customize Order page.";
		blnStepRC =page("pgeCustomizeOrder").element("eleTotalMonthlyCharges").getText().replace("$","").equals(dicTestData.get("strMonthlyCharges"));
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,"Failed to verify Monthly Charges on Checkout page.", "Fail");
		}
		
		// ########################################################################################################
		// Step 15 - Verify One Time Charges
		// Verify One Time Charges
		// ########################################################################################################
		strStepDesc = "Verify One Time Charges on 'Checkout' page.";
		strActualResult = "One Time Charges is same as on Customize Order page.";
		blnStepRC =page("pgeCustomizeOrder").element("eleOneTimePlanCharges").getText().replace("$","").equals(dicTestData.get("strOneTimeCharges"));
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,"Failed to verify One Time Charges on Checkout page.", "Fail");
		}
		
		// ########################################################################################################
		// Step 16 - Enter Billing Address details
		// Enter Billing Address details for existing customer
		// ########################################################################################################
		strStepDesc = "Enter all details in Primary Account Holder section, select 'No Thanks' checkbox and then Click Continue.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC =enterCheckoutInformation(strFirstName, strLastName,strPhoneNumber, strDOB, strSSN, strEmail,true);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}
