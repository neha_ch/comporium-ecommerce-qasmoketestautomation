package TestScript.CustomerSummary;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import libraries.WebFunclib;
public class VerifyVIDCoretypeForFTMLByUpgradingTVtoMoxi_SHOPM1004 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;
	
	@Test
	public void VerifyVIDCoretypeForFTMLByUpgradingTVtoMoxi(ITestContext testContext) throws InterruptedException 
	{
		
		// ########################################################################################################
		// # Test Case ID: SHOPM1004
		// # Test Case: VerifyVIDCoretypeForFTMLByUpgradingTVtoMoxi
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify VID CoreType for Fort Mills location for a customer, by upgrading a TV plan.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################

		//Getting data from master test data file
		String strURL = dicTestData.get("strURL");
		String strStreetAddress = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strExstUsername = dicTestData.get("strExstUsername");
		String strExstPassword = dicTestData.get("strExstPassword");
		String strPopUpText = dicTestData.get("strPopUpText");
		String strTelevisionTile = dicTestData.get("strText");
		String strUpdatedEquipName = dicTestData.get("strUpdatedEquipName");
		String strSelectValue = dicTestData.get("strSelectValue");
		String strSelectType = dicTestData.get("strSelectType");
		
		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> Address Search, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address Search' Page is displayed.";
		blnStepRC = navigateToURL(strURL,"pgeProfilePage","txtStreetAddress");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium account exists, Street Address: '<b>"+strStreetAddress+ "</b>'";
		strActualResult = "'Zip code: '<b>"+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddress, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 4 - Click on 'Yes I live here' from the pop-up
		//Click on 'Yes I live here' from the pop-up
		// ########################################################################################################
		strStepDesc = "Click on 'Yes I live here' from the pop-up.";
		strActualResult = "User is navigated to 'My Account Login' page.";
		blnStepRC = clickOnAddressConformationPopup("btnYesLiveHere","");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 5 - Enter Account username and password
		//Enter Account username and password
		// ########################################################################################################
		strStepDesc = "Login as '<b>FTML existing customer with VID coretype</b>' from My Account login page.";
		strActualResult = "User is navigated to 'My Account Overview' page for account number '<b>"+strExstUsername+"</b>'.";
		blnStepRC = loginExistingCustomer(strExstUsername,strExstPassword);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 6 - Click on 'Add/Upgrade Service'
		//Click on 'Add/Upgrade Service'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Add/Upgrade Service</b>' link and verify navigated page.";
		strActualResult = "User is navigated to 'Customer Summary' page.";
		blnStepRC = verifyExistsAndClick("pgeAccountOverview","lnkAddUpgradeService","pgeAccountOverview","btnShopUpgrades");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 7 - Verify Television Tile
		// Verify Television Tile
		// ########################################################################################################
		strStepDesc = "Verify Television Tile on 'Customer Summary' page.";
		strActualResult = "Television Tile with text '"+strTelevisionTile+"' is displayed.";
		blnStepRC = verifyTextContains("pgeCustomerSummary","eleTelevisionTile",strTelevisionTile);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 8 - Click on 'Shop Upgrades'
		// Click on 'Shop Upgrades'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Shop Upgrades</b>' button for 'Television' and verify navigated page.";
		strActualResult = "User is navigated to 'Customize Order' page.";
		blnStepRC = clickAndVerify("pgeAccountOverview","btnShopUpgrades","pgeCustomizeOrder","eleTVPlanHeader");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 9 - Select a TV equipment
		// Select a TV equipment
		// ########################################################################################################
		strStepDesc = "Select '<b>Add Moxi Media Player (HD DVR)</b>' Equipment.";
		strActualResult = "'Add Moxi Media Player (HD DVR)' equipment is selected on 'Customize Order' page.";
		blnStepRC=customSelectFromList("pgeCustomizeOrder","lstTVmaxiMediaEquip","eleSelectedTVEquip",strSelectType,strSelectValue);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 10 - Verify Equipment Name
		// Verify Equipment Name
		// ########################################################################################################
		strStepDesc = "Verify '<b>Additional Moxi Media Media Player</b>' equipment under 'Additional Monthly Charges' section.";
		strActualResult = "'Additional Moxi Media Media Player' equipment is displayed under 'Additional Monthly Charges' section.";
		blnStepRC=verifyTextContains("pgeCustomizeOrder","eleSelectedTVEquip",strUpdatedEquipName);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 11 - Verify Equipment Charges
		// Verify Equipment Charges
		// ########################################################################################################
		strStepDesc = "Verify equipment charges under 'Additional Monthly Charges' section.";
		strActualResult = "Selected equipment's Charges are displayed under 'Additional Monthly Charges' section.";
		blnStepRC=verifyTextContains("pgeCustomizeOrder","eleSelectedTVEquip$","$");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 12 - Fetch Sub-Total
		// Fetch Sub-Total Charges
		// ########################################################################################################
		strStepDesc = "Fetch 'Sub Total' on 'Customize Order' page.";
		strActualResult = "'Sub-Total Charges' are fetched from 'Customize Order' page.";
		blnStepRC=getTextAndSaveDictionaryVal("pgeCustomizeOrder","eleSubTotalCharges$","strSubTotalCharges","$");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 13 - Click on 'Continue' button
		// Click on 'Continue' button
		// ########################################################################################################
		strStepDesc = "Click on 'Continue' button on 'Customized Order' page.";
		strActualResult = "User is navigated to 'Schedule' page.";
		blnStepRC=clickAndVerify("pgeCheckout","btnContinue","pgeServiceSchedule","eleSchedulePageTitle");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 14 - Select Installation Date and Time
		// Select Installation Date and Time
		// ########################################################################################################
		strStepDesc = "Click on 'Select Date and Time' button,select an available date and Preferred Time of Day and click on OK.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC =selectDateAndTimeForInstallation();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 15 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on Continue button on 'Service & Installation' page.";
		strActualResult = "'Confirm Order' page is displayed with fields 'Highlighted Confirm breadcrum,Total Due and Account Details' sections.";
		blnStepRC =verifyConfirmOrderPage();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 16 - Verify Total Due Charges
		// Verify Total Due Charges
		// ########################################################################################################
		strStepDesc = "Verify 'Total Due' Charges on 'Checkout' page.";
		strActualResult = "'Total Due' Charges is same as on Customize Order page.";
		blnStepRC =page("pgeConfirmOrder").element("eleTotalDueToday$").getText().replace("$","").equals(dicTestData.get("strSubTotalCharges"));
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,"Failed to verify One Time Charges on Checkout page.", "Fail");
		}
				
		// ########################################################################################################
		// Step 17 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on 'Continue' button on 'Confirm Order' page.";
		strActualResult = "'Thank you' page is displayed.";
		//blnStepRC =CheckIAgreeCheckBoxAndClickOnContinue();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			//return;
		}
		
		// ########################################################################################################
		// Step 18 - Click on 'Go To My Account'.
		//  Click on 'Go To My Account' button.
		// ########################################################################################################
		strStepDesc = "Click on 'Go To My Account' button on 'Customer Summary' page.";
		strActualResult = "Pop-Up '<b>"+strPopUpText+"</b>' is displayed.";
		blnStepRC = clickAndVerify("pgeBrowse","btnGoToMyAccount","pgeCustomerSummary","eleAreYouSureToLeavePopUp");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 19 - Click on 'Leave Page'.
		//  Click on 'Leave Page' button.
		// ########################################################################################################
		strStepDesc = "Click on 'Leave Page' button from the pop-up.";
		strActualResult = "'My Account Overview' page is displayed.";
		blnStepRC = clickAndVerify("pgeCustomerSummary","btnLeavePage","pgeAccountOverview","lnkAddUpgradeService");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 20 - Logout from the session.
		//  Logout from the application.
		// ########################################################################################################
		strStepDesc = "Click on 'Logout' button to logout from the application.";
		strActualResult = "Account is logged out successfully.";
		blnStepRC = clickAndVerify("pgeAccountOverview","lnkLogout","pgeMyAccountLogin","txtUserName");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}
