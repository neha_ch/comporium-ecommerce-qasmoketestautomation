package TestScript.CustomerSummary;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import libraries.WebFunclib;
public class VerifyINTCoretypeForFTMLByAddingTV_SHOPM799 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;
	
	@Test
	public void VerifyINTCoretypeForRKHLByUpgradingTV(ITestContext testContext) throws InterruptedException 
	{
		
		// ########################################################################################################
		// # Test Case ID: SHOPM799
		// # Test Case: VerifyINTCoretypeForFTMLByAddingTV
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify INT CoreType for Fort Mills location for a customer, by adding a TV plan.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################

		//Getting data from master test data file
		String strURL = dicTestData.get("strURL");
		String strStreetAddress = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strExstUsername = dicTestData.get("strExstUsername");
		String strExstPassword = dicTestData.get("strExstPassword");
		String strPopUpText = dicTestData.get("strPopUpText");
		String strTelevisionTile = dicTestData.get("strText");
		String strServices = dicTestData.get("strServices");
		String strUpdatedPlanName = dicTestData.get("strUpdatedPlanName");
		String strUpdatedEquipName = dicTestData.get("strUpdatedEquipName");
		String strSelectValue = dicTestData.get("strSelectValue");
		String strSelectType = dicTestData.get("strSelectType");
		String strFirstName = dicTestData.get("strFirstName");
		String strLastName = dicTestData.get("strLastName");
		String strDOB = dicTestData.get("strDOB");
		String strPhoneNumber = dicTestData.get("strPhoneNumber");
		String strEmail = dicTestData.get("strEmail");
		String CardNumber = dicTestData.get("CardNumber");
		String ExpiryDate = dicTestData.get("ExpiryDate");
		String CardHolderName = dicTestData.get("CardHolderName");
		String CVV = dicTestData.get("CVV");
		
		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> Address Search, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address Search' Page is displayed.";
		blnStepRC = navigateToURL(strURL,"pgeProfilePage","txtStreetAddress");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium account exists, Street Address: '<b>"+strStreetAddress+ "</b>'";
		strActualResult = "'Zip code: '<b>"+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddress, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 4 - Click on 'Yes I live here' from the pop-up
		//Click on 'Yes I live here' from the pop-up
		// ########################################################################################################
		strStepDesc = "Click on 'Yes I live here' from the pop-up.";
		strActualResult = "User is navigated to 'My Account Login' page.";
		blnStepRC = clickOnAddressConformationPopup("btnYesLiveHere","");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 5 - Enter Account username and password
		//Enter Account username and password
		// ########################################################################################################
		strStepDesc = "Login as '<b>FTML existing customer with INT coretype</b>' from My Account login page.";
		strActualResult = "User is navigated to 'My Account Overview' page for account number '<b>"+strExstUsername+"</b>'.";
		blnStepRC = loginExistingCustomer(strExstUsername,strExstPassword);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 6 - Click on 'Add/Upgrade Service'
		//Click on 'Add/Upgrade Service'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Add/Upgrade Service</b>' link and verify navigated page.";
		strActualResult = "User is navigated to 'Customer Summary' page.";
		blnStepRC = verifyExistsAndClick("pgeAccountOverview","lnkAddUpgradeService","pgeCustomerSummary","btnTVAddService");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 7 - Verify Television Tile
		// Verify Television Tile
		// ########################################################################################################
		strStepDesc = "Verify Television Tile on 'Customer Summary' page.";
		strActualResult = "Television Tile with text '"+strTelevisionTile+"' is displayed.";
		blnStepRC = verifyTextContains("pgeCustomerSummary","eleExpTheBestTvTile",strTelevisionTile);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 8 - Click on 'Add Service'
		// Click on 'Add Service'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Add Service</b>' button for 'Television' and verify navigated page.";
		strActualResult = "User is navigated to 'Browse' page.";
		blnStepRC = clickSecurityAndVerify("pgeCustomerSummary","btnTVAddService","pgeBrowse","chkTVPlan");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 9 - Verify 'TV' plan check-box
		// Verify 'TV' plan check-box
		// ########################################################################################################
		strStepDesc = "Verify 'TV' plan check-box is selected on 'Browse' page.";
		strActualResult = "'TV' plan type is selected on 'Browse' page.";
		blnStepRC = verifyCheckboxSelection("pgeBrowse","chkTVPlan",true);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 10 - Verify 'TV' plans
		// Verify 'TV' plans
		// ########################################################################################################
		strStepDesc = "Verify only 'TV' Plans on Browse page.";
		strActualResult = "Plans for only 'TV' are displayed on the 'Browse' page.";
		blnStepRC=page("pgeBrowse").element("eleTVPlanTypeHeader").exist();
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 11 - Click on Customize button
		// Click on Customize button
		// ########################################################################################################
		strStepDesc = "Click on Customize button for '<b>HD Digital Variety - 200+ Channels</b>' plan.";
		strActualResult = "User is navigated to 'Customize Order' page.";
		blnStepRC=clickAndVerify("pgeBrowse","btnCustomizeHD200ChTV","pgeCustomizeOrder","chkHDdigitalVariety");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 12 - Verify HD Digital Variety plan
		// Verify HD Digital Variety plan
		// ########################################################################################################
		strStepDesc = "Verify '<b>HD Digital Variety</b>' plan on 'Customize Order' page.";
		strActualResult = "Plan 'HD Digital Variety' is selected on 'Customize Order' page.";
		blnStepRC=verifyCheckboxSelection("pgeCustomizeOrder","chkHDdigitalVariety",true);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 13 - Verify Upgrade to Moxi Whole Home HD DVR Digital Variety plan
		// Verify Upgrade to Moxi Whole Home HD DVR Digital Variety plan
		// ########################################################################################################
		strStepDesc = "Verify 'Upgrade to Moxi Whole Home HD DVR Digital Variety' plan on 'Customize Order' page.";
		strActualResult = "Plan '<b>Upgrade to Moxi Whole Home HD DVR Digital Variety</b>' is not selected on 'Customize Order' page.";
		blnStepRC=verifyCheckboxSelection("pgeCustomizeOrder","chkTVmaxiHomeHDPlan",false);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 14 - Verify Upgrade to HD DVR Digital Variety plan
		// Verify Upgrade to HD DVR Digital Variety plan
		// ########################################################################################################
		strStepDesc = "Verify 'Upgrade to HD DVR Digital Variety' plan on 'Customize Order' page.";
		strActualResult = "Plan '<b>Upgrade to HD DVR Digital Variety</b>' is not selected on 'Customize Order' page.";
		blnStepRC=verifyCheckboxSelection("pgeCustomizeOrder","chkUpgradeHDdigitalDVR",false);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 15 - Verify Plan Name
		// Verify Plan Name
		// ########################################################################################################
		strStepDesc = "Verify selected plan's name under 'Additional Monthly Charges' section.";
		strActualResult = "Selected plan's name '<b>Moxi Whole Home HD DVR Digital Variety</b>' is displayed under 'Additional Monthly Charges' section.";
		blnStepRC=verifyTextContains("pgeCustomizeOrder","eleSelectedTVPlan",strUpdatedPlanName);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 16 - Verify Plan Charges
		// Verify Plan Charges
		// ########################################################################################################
		strStepDesc = "Verify plan charges under 'Additional Monthly Charges' section.";
		strActualResult = "Selected Plan's Charges are displayed under 'Additional Monthly Charges' section.";
		blnStepRC=verifyTextContains("pgeCustomizeOrder","eleSelectedTVPlan$","$");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 17 - Select a TV equipment
		// Select a TV equipment
		// ########################################################################################################
		strStepDesc = "Select '<b>Add High Definition Digital Converter(s)</b>' Equipment.";
		strActualResult = "'Add High Definition Digital Converter (s)' equipment is selected on 'Customize Order' page.";
		blnStepRC=customSelectFromList("pgeCustomizeOrder","lstAddHDdigConverter","eleSelectedHDconverter",strSelectType,strSelectValue);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 18 - Verify Equipment Name
		// Verify Equipment Name
		// ########################################################################################################
		strStepDesc = "Verify selected equipment's name under 'Additional Monthly Charges' section.";
		strActualResult = "Selected equipment's Name '<b>Additional HD Digital Converter</b>' equipment is displayed under 'Additional Monthly Charges' section.";
		blnStepRC=verifyTextContains("pgeCustomizeOrder","eleSelectedHDconverter",strUpdatedEquipName);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 19 - Verify Equipment Charges
		// Verify Equipment Charges
		// ########################################################################################################
		strStepDesc = "Verify equipment charges under 'Additional Monthly Charges' section.";
		strActualResult = "Selected equipment's Charges are displayed under 'Additional Monthly Charges' section.";
		blnStepRC=verifyTextContains("pgeCustomizeOrder","eleSelectedHDconverter$","$");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 20 - Click on 'Continue' button
		// Click on 'Continue' button
		// ########################################################################################################
		strStepDesc = "Click on 'Continue' button on 'Customized Order' page.";
		strActualResult = "User is navigated to 'Checkout' page.";
		blnStepRC = clickAndVerify("pgeCheckout","btnContinue","pgeCheckout","eleCheckoutBreadcrum");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
	
		// ########################################################################################################
		// Step 21 - Enter Billing Address details
		// Enter Billing Address details for existing customer
		// ########################################################################################################
		strStepDesc = "Enter all details in Primary Account Holder section on 'Checkout' page.";
		strActualResult = "All the billing address details are entered on 'Checkout' page.";
		blnStepRC =fillBillAddressForExstCustomers(strFirstName, strLastName,strPhoneNumber,strDOB,strEmail);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 22 - Click 'No,Thanks' checkbox
		// Click 'No,Thanks' checkbox
		// ########################################################################################################
		strStepDesc = "Click on 'No,Thanks I will pay' checkbox on 'Checkout' page.";
		strActualResult = "'No,Thanks I will pay' checkbox is selected.";
		blnStepRC = clickAndVerify("pgeCheckout","chkNoThanksIwillPay","pgeCheckout","txtDOB");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 23 - Click on 'Continue' button
		// Click on 'Continue' button
		// ########################################################################################################
		strStepDesc = "Click on 'Continue' button on 'Checkout' page.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC = clickAndVerify("pgeCheckout","btnContinueSubmit","pgeServiceSchedule","btnSelectDateTime");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
					
		// ########################################################################################################
		// Step 24 - Select Installation Date and Time
		// Select Installation Date and Time
		// ########################################################################################################
		strStepDesc = "Click on 'Select Date and Time' button,select an available date and Preferred Time of Day and click on OK.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC =selectDateAndTimeForInstallation();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 25 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on Continue button on 'Service & Installation' page.";
		strActualResult = "'Confirm Order' page is displayed with fields 'Highlighted Confirm breadcrum,Total Due and Account Details' sections.";
		blnStepRC =verifyConfirmOrderPage();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 26 - Verify selected feature services
		//  Verify selected feature services
		// ########################################################################################################
		strStepDesc = "Verify services populated under Additional Monthly Charges section on 'Confirm Order' page.";
		strActualResult = "Services '<b>"+strServices+"</b>' are displayed under 'Additional Monthly Charges' section.";
		blnStepRC=verifyTelevisionServicesAndCharges("AddPlan");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 27 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on 'I agree checkbox' and click 'continue' button on 'Confirm Order' page.";
		strActualResult = "'Set Up Payment' page is displayed with following fields: 'What am I paying today?' ,'Credit Card/Debit Card Payment section','Submit Order button' and 'Monthly Charges section'.";
		blnStepRC =selectIAgreeCheckBoxAndContinue();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 28 - Fill 'Credit card' details
		// Fill 'Credit card' details
		// ########################################################################################################
		strStepDesc = "Fill 'Credit card' details and click on Submit Order button.";
		strActualResult = "'Thank you' page is displayed.";
		blnStepRC =fillCardDetails(CardNumber, ExpiryDate, CardHolderName, CVV);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,dicTestData.get("strOrderTxt&Number"), "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 29 - Click on 'Go To My Account'.
		//  Click on 'Go To My Account' button.
		// ########################################################################################################
		strStepDesc = "Click on 'Go To My Account' button on 'Customer Summary' page.";
		strActualResult = "Pop-Up '<b>"+strPopUpText+"</b>' is displayed.";
		blnStepRC = clickAndVerify("pgeBrowse","btnGoToMyAccount","pgeCustomerSummary","eleAreYouSureToLeavePopUp");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 30 - Click on 'Leave Page'.
		//  Click on 'Leave Page' button.
		// ########################################################################################################
		strStepDesc = "Click on 'Leave Page' button from the pop-up.";
		strActualResult = "'My Account Overview' page is displayed.";
		blnStepRC = clickAndVerify("pgeCustomerSummary","btnLeavePage","pgeAccountOverview","lnkAddUpgradeService");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 31 - Logout from the session.
		//  Logout from the application.
		// ########################################################################################################
		strStepDesc = "Click on 'Logout' button to logout from the application.";
		strActualResult = "Account is logged out successfully.";
		blnStepRC = clickAndVerify("pgeAccountOverview","lnkLogout","pgeMyAccountLogin","txtUserName");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}
