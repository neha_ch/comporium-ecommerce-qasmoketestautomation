package TestScript.CustomerSummary;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import libraries.WebFunclib;
public class VerifyBREVwithSubTypeHSIMODEMByUpgradingPhoneAllFeatures_SHOPM910 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;
	
	@Test
	public void VerifyBREVwithSubTypeHSIMODEMByUpgradingPhoneAllFeatures(ITestContext testContext) throws InterruptedException 
	{
		
		// ########################################################################################################
		// # Test Case ID: SHOPM910
		// # Test Case: VerifyBREVwithSubTypeHSIMODEMByUpgradingPhoneAllFeatures
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify BREV customers with subtype HSI-MODEM can upgrade Telephone plan by adding all features.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################

		//Getting data from master test data file
		String strURL = dicTestData.get("strURL");
		String strStreetAddress = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strExstUsername = dicTestData.get("strExstUsername");
		String strExstPassword = dicTestData.get("strExstPassword");
		String strPopUpText = dicTestData.get("strPopUpText");
		String strTelephoneTile = dicTestData.get("strText");
		String strServices = dicTestData.get("strServices");
		
		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> Address Search, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address Search' Page is displayed.";
		blnStepRC = navigateToURL(strURL,"pgeProfilePage","txtStreetAddress");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium account exists, Street Address: '<b>"+strStreetAddress+ "</b>'";
		strActualResult = "'Zip code: '<b>"+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddress, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 4 - Click on 'Yes I live here' from the pop-up
		//Click on 'Yes I live here' from the pop-up
		// ########################################################################################################
		strStepDesc = "Click on 'Yes I live here' from the pop-up.";
		strActualResult = "User is navigated to 'My Account Login' page.";
		blnStepRC = clickOnAddressConformationPopup("btnYesLiveHere","");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 5 - Enter Account username and password
		//Enter Account username and password
		// ########################################################################################################
		strStepDesc = "Login as '<b>BREV customer with subtype HSI-MODEM</b>' from My Account login page.";
		strActualResult = "User is navigated to 'My Account Overview' page for account number '<b>"+strExstUsername+"</b>'.";
		blnStepRC = loginExistingCustomer(strExstUsername,strExstPassword);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 6 - Click on 'Add/Upgrade Service'
		//Click on 'Add/Upgrade Service'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Add/Upgrade Service</b>' link and verify navigated page.";
		strActualResult = "User is navigated to 'Customer Summary' page.";
		blnStepRC = verifyExistsAndClick("pgeAccountOverview","lnkAddUpgradeService","pgeAccountOverview","btnPhoneShopUpgrades");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 7 - Verify Telephone Tile
		// Verify Telephone Tile
		// ########################################################################################################
		strStepDesc = "Verify Telephone Tile on 'Customer Summary' page.";
		strActualResult = "Telephone Tile with text '"+strTelephoneTile+"' is displayed.";
		blnStepRC = verifyTextContains("pgeCustomerSummary","eleBasicPhoneTile",strTelephoneTile);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 8 - Click on 'Shop Upgrades'
		// Click on 'Shop Upgrades'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Shop Upgrades</b>' button for 'Telephone' and verify navigated page.";
		strActualResult = "User is navigated to 'Customize Order' page.";
		blnStepRC = clickAndVerify("pgeAccountOverview","btnPhoneShopUpgrades","pgeCustomizeOrder","elePhonePlanHeader");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 9 - Verify Local Telephone Service
		// Verify Service
		// ########################################################################################################
		strStepDesc = "Verify '<b>Local Telephone Service</b>' under 'Telephone' section.";
		strActualResult = "'<b>Local Telephone Service</b>' is displayed under 'Telephone' section.";
		blnStepRC=verifyElementExist("pgeCustomizeOrder","elePhonePlanAddOns");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 10 - Verify Local Telephone Service Amount
		// Verify Service Charges
		// ########################################################################################################
		strStepDesc = "Verify '<b>Local Telephone Service Charges</b>' with $ amount under 'Telephone' section.";
		strActualResult = "'<b>Local Telephone Service Charges</b>' is displayed under 'Telephone' section.";
		blnStepRC=verifyElementExist("pgeCustomizeOrder","eleLocalPhoneService$");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 11 - Verify Upgrade To Voice Plus Unlimited Service
		// Verify Service
		// ########################################################################################################
		strStepDesc = "Verify '<b>Upgrade To Voice Plus Unlimited</b>' under 'Telephone' section.";
		strActualResult = "'<b>Upgrade To Voice Plus Unlimited</b>' is displayed under 'Telephone' section.";
		blnStepRC=verifyElementExist("pgeCustomizeOrder","chkPhonePlanAddOns");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 12 - Verify Upgrade To Voice Plus Service
		// Verify Service
		// ########################################################################################################
		strStepDesc = "Verify '<b>Upgrade To Voice Plus</b>' under 'Telephone' section.";
		strActualResult = "'<b>Upgrade To Voice Plus</b>' is displayed under 'Telephone' section.";
		blnStepRC=verifyElementExist("pgeCustomizeOrder","chkUpgradeVoicePlus");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 13 - Verify Multiple Features
		// Verify Feature
		// ########################################################################################################
		strStepDesc = "Verify '<b>Multiple Features</b>' under 'Telephone' section.";
		strActualResult = "'<b>Multiple Features</b>' are displayed under 'Telephone' section.";
		blnStepRC=verifyElementExist("pgeCustomizeOrder","elePhonePlanFeatures");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 14 - Verify Res-Per-Call Blocking
		// Verify Feature
		// ########################################################################################################
		strStepDesc = "Verify '<b>Res-Per-Call Blocking</b>' feature under 'Telephone' section.";
		strActualResult = "'<b>Res-Per-Call Blocking</b>' feature is displayed under 'Telephone' section.";
		blnStepRC=verifyElementExist("pgeCustomizeOrder","chkResPerCallBlocking");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 15 - Verify Vocation Rate
		// Verify Vocation Rate
		// ########################################################################################################
		strStepDesc = "Verify '<b>Vocation Rate</b>' feature under 'Telephone' section.";
		strActualResult = "'<b>Vocation Rate</b>' feature is displayed under 'Telephone' section.";
		blnStepRC=verifyElementExist("pgeCustomizeOrder","eleVocationRateFeature");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 16 - Select all the features
		// Select all the features
		// ########################################################################################################
		strStepDesc = "Select multiple features '<b>"+strServices+"</b>' on 'Customize Orde' page.";
		strActualResult = "All the features are selected from 'Features' section.";
		blnStepRC=selectMultipleFeatures("pgeCustomizeOrder",strServices);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 17 - Verify selected feature services
		//  Verify selected feature services
		// ########################################################################################################
		strStepDesc = "Verify service '<b>"+strServices+"</b>' populated under Additional Monthly Charges section on 'Customize Order' page.";
		strActualResult = "Selected Services with $ amount are displayed under 'Additional Monthly Charges' section.";
		blnStepRC=verifyMultipleServicesChargesOnCustomize("pgeCustomizeOrder","eleAdditionalMnthServices",strServices);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 18 - Click on 'Continue' button
		// Click on 'Continue' button
		// ########################################################################################################
		strStepDesc = "Click on 'Continue' button on 'Checkout' page.";
		strActualResult = "User is navigated to 'Confirm Order' page.";
		blnStepRC = clickAndVerify("pgeCustomizeOrder","btnContinue","pgeConfirmOrder","eleConfirmHighlighted");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 19 - Verify selected feature services
		//  Verify selected feature services
		// ########################################################################################################
		strStepDesc = "Verify services '<b>"+strServices+"</b>' populated under Additional Monthly Charges section on 'Confirm Order' page.";
		strActualResult = "Selected Services with $ amount are displayed under 'Additional Monthly Charges' section.";
		blnStepRC=verifyMultipleServicesAndCharges("pgeConfirmOrder","eleServiceFeature",strServices);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 20 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on 'I agree checkbox' and click 'continue' button on 'Confirm Order' page.";
		strActualResult = "'Thank you' page is displayed.";
		//blnStepRC =CheckIAgreeCheckBoxAndClickOnContinue();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			//return;
		}
				
		// ########################################################################################################
		// Step 21 - Click on 'Go To My Account'.
		//  Click on 'Go To My Account' button.
		// ########################################################################################################
		strStepDesc = "Click on 'Go To My Account' button on 'Customer Summary' page.";
		strActualResult = "Pop-Up '<b>"+strPopUpText+"</b>' is displayed.";
		blnStepRC = clickAndVerify("pgeBrowse","btnGoToMyAccount","pgeCustomerSummary","eleAreYouSureToLeavePopUp");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 22 - Click on 'Leave Page'.
		//  Click on 'Leave Page' button.
		// ########################################################################################################
		strStepDesc = "Click on 'Leave Page' button from the pop-up.";
		strActualResult = "'My Account Overview' page is displayed.";
		blnStepRC = clickAndVerify("pgeCustomerSummary","btnLeavePage","pgeAccountOverview","lnkAddUpgradeService");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 23 - Logout from the session.
		//  Logout from the application.
		// ########################################################################################################
		strStepDesc = "Click on 'Logout' button to logout from the application.";
		strActualResult = "Account is logged out successfully.";
		blnStepRC = clickAndVerify("pgeAccountOverview","lnkLogout","pgeMyAccountLogin","txtUserName");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}
