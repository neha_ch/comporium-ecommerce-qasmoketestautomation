package TestScript.CustomerSummary;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import libraries.WebFunclib;
public class VerifyINTCoretypeForFTMLByAddingBundle_SHOPM798 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;
	
	@Test
	public void VerifyINTCoretypeForFTMLByAddingBundle(ITestContext testContext) throws InterruptedException 
	{
		
		// ########################################################################################################
		// # Test Case ID: SHOPM798
		// # Test Case: VerifyINTCoretypeForFTMLByAddingBundle
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify INT CoreType for Fort Mills location for a customer, by adding a bundle plan.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################

		//Getting data from master test data file
		String strURL = dicTestData.get("strURL");
		String strStreetAddress = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strExstUsername = dicTestData.get("strExstUsername");
		String strExstPassword = dicTestData.get("strExstPassword");
		String strPopUpText = dicTestData.get("strPopUpText");
		String strTVTile = dicTestData.get("strText");
		String strServices = dicTestData.get("strServices");
		String strBundlePlans = dicTestData.get("strUpdatedPlanName");
		String strInternetPlan = dicTestData.get("strInternetPlan");
		String strTVPlan = dicTestData.get("strTVPlan");
		String strPhonePlan = dicTestData.get("strPhonePlan");
		String CardNumber = dicTestData.get("CardNumber");
		String ExpiryDate = dicTestData.get("ExpiryDate");
		String CardHolderName = dicTestData.get("CardHolderName");
		String CVV = dicTestData.get("CVV");
		boolean blnSelection = true;
		
		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> Address Search, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address Search' Page is displayed.";
		blnStepRC = navigateToURL(strURL,"pgeProfilePage","txtStreetAddress");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium account exists, Street Address: '<b>"+strStreetAddress+ "</b>'";
		strActualResult = "'Zip code: '<b>"+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddress, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 4 - Click on 'Yes I live here' from the pop-up
		//Click on 'Yes I live here' from the pop-up
		// ########################################################################################################
		strStepDesc = "Click on 'Yes I live here' from the pop-up.";
		strActualResult = "User is navigated to 'My Account Login' page.";
		blnStepRC = clickOnAddressConformationPopup("btnYesLiveHere","");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 5 - Enter Account username and password
		//Enter Account username and password
		// ########################################################################################################
		strStepDesc = "Login as '<b>FTML existing customer with INT coretype</b>' from My Account login page.";
		strActualResult = "User is navigated to 'My Account Overview' page for account number '<b>"+strExstUsername+"</b>'.";
		blnStepRC = loginExistingCustomer(strExstUsername,strExstPassword);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 6 - Click on 'Add/Upgrade Service'
		//Click on 'Add/Upgrade Service'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Add/Upgrade Service</b>' link and verify navigated page.";
		strActualResult = "User is navigated to 'Customer Summary' page.";
		blnStepRC = verifyExistsAndClick("pgeAccountOverview","lnkAddUpgradeService","pgeCustomerSummary","btnTVAddService");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 7 - Verify Television Tile
		// Verify Television Tile
		// ########################################################################################################
		strStepDesc = "Verify Television Tile on 'Customer Summary' page.";
		strActualResult = "Television Tile with text '"+strTVTile+"' is displayed.";
		blnStepRC = verifyTextContains("pgeCustomerSummary","eleExpTheBestTvTile",strTVTile);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 8 - Click on 'Add Service'
		// Click on 'Add Service'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Add Service</b>' button for 'Television' and verify navigated page.";
		strActualResult = "User is navigated to 'Browse' page.";
		blnStepRC = clickAndVerify("pgeCustomerSummary","btnTVAddService","pgeBrowse","chkTVPlan");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 9 - Click on 'Bundle Plans'
		// Click on 'Bundle Plans'
		// ########################################################################################################
		strStepDesc = "Click on '<bBundle Plans</b>' on 'Browse' page.";
		strActualResult = "'View All Bundles' checkbox is displayed on 'Browse' page.";
		blnStepRC = clickAndVerify("pgeBrowse","btnBundlePlans","pgeBrowse","chkViewAllBundles");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 10 - Click on 'View All Bundles' check-box
		// Click on 'View All Bundles' check-box
		// ########################################################################################################
		strStepDesc = "Click on 'View All Bundles' check-box and verify 'I am looking for:' section.";
		strActualResult = "Check-boxes for each bundle type '<b>TV,Internet,Phone</b>' and '<b>Security</b>' are displayed in 'I am looking for' section.";
		blnStepRC = selectViewAllBundles();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 11 - Click on 'Customize' button
		// Click on 'Customize' button for the bundle Plan
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button for bundle plan '<b>Triple Play Bundle Zipstream Internet, Moxi HD Digital Variety TV & Home Phone</b>'.";
		strActualResult = "User is navigated to 'Customize Order' page.";
		blnStepRC=clickAndVerify("pgeBrowse","btnCustomizeZipstrBundle","pgeCustomizeOrder","eleInternetPlanHeader","elePhonePlanHeader");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 12 - Verify Internet Section
		// Verify Internet Section
		// ########################################################################################################
		strStepDesc = "Verify selection of '<b>Zipstream - 1 Gigabit</b>' under 'Internet' section.";
		strActualResult = "'Zipstream - 1 Gigabit' is selected under 'Internet' section.";
		blnStepRC=verifyCheckboxSelection("pgeCustomizeOrder","chkZipstream1GigaByte",true);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 13 - Verify Television Section
		// Verify Television Section
		// ########################################################################################################
		strStepDesc = "Verify selection of '<b>Moxi Whole Home HD DVR Digital Variety</b>' under 'Television' section.";
		strActualResult = "'Moxi Whole Home HD DVR Digital Variety' is selected under 'Television' section.";
		blnStepRC=verifyCheckboxSelection("pgeCustomizeOrder","chkTVmaxiHomeHDDVR",true);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 14 - Verify Telephone Section
		// Verify Telephone Section
		// ########################################################################################################
		strStepDesc = "Verify selection of '<b>Voice Plus</b>' under 'Telephone' section.";
		strActualResult = "'Voice Plus' is selected under 'Telephone' section.";
		blnStepRC=verifyCheckboxSelection("pgeCustomizeOrder","chkVoicePlus",blnSelection);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 15 - Verify Telephone Section
		// Verify Telephone Section
		// ########################################################################################################
		strStepDesc = "Verify '<b>Upgrade to Voice Plus Unlimited</b>' plan under 'Telephone' section.";
		strActualResult = "'Upgrade to Voice Plus Unlimited' plan is present as an option under 'Telephone' section.";
		blnStepRC=verifyCheckboxSelection("pgeCustomizeOrder","chkPhonePlanAddOns",false);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 16 - Verify selected internet plan
		// Verify selected plan
		// ########################################################################################################
		strStepDesc = "Verify '<b>Zipstream Internet</b>' selected as 'Internet' plan under 'Additional Monthly Charges'.";
		strActualResult = "'Zipstream Internet' plan is selected as 'Internet' Plan under 'Internet' section.";
		blnStepRC=verifyTextContains("pgeCustomizeOrder","elePlanUnderAMCharges",strInternetPlan);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 17 - Verify selected plan charges
		// Verify selected plan charges
		// ########################################################################################################
		strStepDesc = "Verify selected Internet plan's charges under 'Additional Monthly Charges'.";
		strActualResult = "Selected 'Internet' Plan's charges are displayed in $ amount under 'Internet' section.";
		blnStepRC=verifyTextContains("pgeCustomizeOrder","elePlan$UnderAMCharges","$");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 18 - Verify selected Television plan
		// Verify selected plan
		// ########################################################################################################
		strStepDesc = "Verify '<b>Moxi Whole Home HD DVR Digital Variety</b>' selected as 'TV' plan under 'Additional Monthly Charges'.";
		strActualResult = "'Moxi Whole Home HD DVR Digital Variety' plan is selected as 'TV' Plan under 'Television' section.";
		blnStepRC=verifyTextContains("pgeCustomizeOrder","eleSelectedTVPlan",strTVPlan);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 19 - Verify selected plan charges
		// Verify selected plan charges
		// ########################################################################################################
		strStepDesc = "Verify selected TV plan's charges under 'Additional Monthly Charges'.";
		strActualResult = "Selected 'TV' Plan's Charges are displayed in $ amount under 'Internet' section.";
		blnStepRC=verifyTextContains("pgeCustomizeOrder","eleSelectedTVPlan$","$");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 20 - Verify selected Phone plan
		// Verify selected plan
		// ########################################################################################################
		strStepDesc = "Verify '<b>Voice Plus</b>' selected as 'Phone' plan under 'Additional Monthly Charges'.";
		strActualResult = "'Voice Plus' plan is selected as 'Phone' Plan under 'Phone' section.";
		blnStepRC=verifyTextContains("pgeCustomizeOrder","eleVoicePlusPlan",strPhonePlan);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 21 - Verify selected plan charges
		// Verify selected plan charges
		// ########################################################################################################
		strStepDesc = "Verify selected Phone plan's charges under 'Additional Monthly Charges'.";
		strActualResult = "Selected 'Phone' Plan's Charges are displayed in $ amount under 'Phone' section.";
		blnStepRC=verifyTextContains("pgeCustomizeOrder","eleVoicePlusCharges","$");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 22 - Click on 'Continue' button
		// Click on 'Continue' button
		// ########################################################################################################
		strStepDesc = "Click on 'Continue' button on 'Customized Order' page.";
		strActualResult = "User is navigated to 'Schedule' page.";
		blnStepRC=clickAndVerify("pgeCheckout","btnContinue","pgeServiceSchedule","eleSchedulePageTitle");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 23 - Select Installation Date and Time
		// Select Installation Date and Time
		// ########################################################################################################
		strStepDesc = "Click on 'Select Date and Time' button,select an available date and Preferred Time of Day and click on OK.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC =selectDateAndTimeForInstallation();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 24 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on Continue button on 'Service & Installation' page.";
		strActualResult = "'Confirm Order' page is displayed with fields 'Highlighted Confirm breadcrum,Total Due and Account Details' sections.";
		blnStepRC =verifyConfirmOrderPage();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 25 - Verify selected feature services
		//  Verify selected feature services
		// ########################################################################################################
		strStepDesc = "Verify services populated under Additional Monthly Charges section on 'Confirm Order' page.";
		strActualResult = "Services '<b>"+strServices+"</b>' are displayed under 'Additional Monthly Charges' section.";
		blnStepRC=verifyBundlePlanServicesAndCharges(strBundlePlans,"AddPlan");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 26 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on 'I agree checkbox' and click 'continue' button on 'Confirm Order' page.";
		strActualResult = "'Set Up Payment' page is displayed with following fields: 'What am I paying today?' ,'Credit Card/Debit Card Payment section','Submit Order button' and 'Monthly Charges section'.";
		blnStepRC =selectIAgreeCheckBoxAndContinue();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		/*// ########################################################################################################
		// Step 27 - Fill 'Credit card' details
		// Fill 'Credit card' details
		// ########################################################################################################
		strStepDesc = "Fill 'Credit card' details and click on Submit Order button.";
		strActualResult = "'Thank you' page is displayed.";
		blnStepRC =fillCardDetails(CardNumber, ExpiryDate, CardHolderName, CVV);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,dicTestData.get("strOrderTxt&Number"), "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}*/
				
		// ########################################################################################################
		// Step 28 - Click on 'Go To My Account'.
		//  Click on 'Go To My Account' button.
		// ########################################################################################################
		strStepDesc = "Click on 'Go To My Account' button on 'Customer Summary' page.";
		strActualResult = "Pop-Up '<b>"+strPopUpText+"</b>' is displayed.";
		blnStepRC = clickAndVerify("pgeBrowse","btnGoToMyAccount","pgeCustomerSummary","eleAreYouSureToLeavePopUp");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 29 - Click on 'Leave Page'.
		//  Click on 'Leave Page' button.
		// ########################################################################################################
		strStepDesc = "Click on 'Leave Page' button from the pop-up.";
		strActualResult = "'My Account Overview' page is displayed.";
		blnStepRC = clickAndVerify("pgeCustomerSummary","btnLeavePage","pgeAccountOverview","lnkAddUpgradeService");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 30 - Logout from the session.
		//  Logout from the application.
		// ########################################################################################################
		strStepDesc = "Click on 'Logout' button to logout from the application.";
		strActualResult = "Account is logged out successfully.";
		blnStepRC = clickAndVerify("pgeAccountOverview","lnkLogout","pgeMyAccountLogin","txtUserName");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}
