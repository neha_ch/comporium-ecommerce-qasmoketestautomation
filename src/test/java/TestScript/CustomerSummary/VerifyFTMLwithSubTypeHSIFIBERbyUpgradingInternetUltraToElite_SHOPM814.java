package TestScript.CustomerSummary;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import libraries.WebFunclib;
public class VerifyFTMLwithSubTypeHSIFIBERbyUpgradingInternetUltraToElite_SHOPM814 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;
	
	@Test
	public void VerifyFTMLwithSubTypeHSIFIBERbyUpgradingInternetUltraToElite(ITestContext testContext) throws InterruptedException 
	{
		
		// ########################################################################################################
		// # Test Case ID: SHOPM814
		// # Test Case: VerifyFTMLwithSubTypeHSIFIBERbyUpgradingInternetUltraToElite
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify FTML customers with subtype HSI-FIBER can upgrade Internet service, by upgrading an 
		//#   Ultra Plan to Elite plan.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################

		//Getting data from master test data file
		String strURL = dicTestData.get("strURL");
		String strStreetAddress = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strExstUsername = dicTestData.get("strExstUsername");
		String strExstPassword = dicTestData.get("strExstPassword");
		String strPopUpText = dicTestData.get("strPopUpText");
		String strInternetTile = dicTestData.get("strText");
		String strUpdatedPlanName = dicTestData.get("strUpdatedPlanName");
		
		// ########################################################################################################
		// Deleting all the cookies before executing the script on IE browser
		// Delete All Cookies
		// ########################################################################################################
			deleteAllCookies();

		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> Address Search, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address Search' Page is displayed.";
		blnStepRC = navigateToURL(strURL,"pgeProfilePage","txtStreetAddress");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium account exists, Street Address: '<b>"+strStreetAddress+ "</b>'";
		strActualResult = "'Zip code: '<b>"+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddress, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 4 - Click on 'Yes I live here' from the pop-up
		//Click on 'Yes I live here' from the pop-up
		// ########################################################################################################
		strStepDesc = "Click on 'Yes I live here' from the pop-up.";
		strActualResult = "User is navigated to 'My Account Login' page.";
		blnStepRC = clickOnAddressConformationPopup("btnYesLiveHere","");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 2 - Enter Account username and password
		//Enter Account username and password
		// ########################################################################################################
		strStepDesc = "Login as '<b>FTML customer with subtype HSI-FIBER</b>' from My Account login page.";
		strActualResult = "User is navigated to 'My Account Overview' page for account number '<b>"+strExstUsername+"</b>'.";
		blnStepRC = loginExistingCustomer(strExstUsername,strExstPassword);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 3 - Click on 'Add/Upgrade Service'
		//Click on 'Add/Upgrade Service'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Add/Upgrade Service</b>' link and verify navigated page.";
		strActualResult = "User is navigated to 'Customer Summary' page.";
		blnStepRC = verifyExistsAndClick("pgeAccountOverview","lnkAddUpgradeService","pgeAccountOverview","btnInternetShopUpgrades");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 4 - Verify Internet Tile
		// Verify Internet Tile
		// ########################################################################################################
		strStepDesc = "Verify Internet Tile on 'Customer Summary' page.";
		strActualResult = "Internet Tile with text '<b>"+strInternetTile+"</b>' is displayed.";
		blnStepRC = verifyTextContains("pgeCustomerSummary","eleInternetTile",strInternetTile);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 5 - Click on 'Shop Upgrades'
		// Click on 'Shop Upgrades'
		// ########################################################################################################
		strStepDesc = "Click on '<b>Shop Upgrades</b>' button for 'Internet' and verify navigated page.";
		strActualResult = "User is navigated to 'Customize Order' page.";
		blnStepRC = clickAndVerify("pgeAccountOverview","btnInternetShopUpgrades","pgeCustomizeOrder","eleInternetPlanHeader");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 6 - Upgrade the Internet Plan
		//  Upgrade the Internet Plan
		// ########################################################################################################
		strStepDesc = "Select '<b>Upgrade to Elite</b>' to upgrade the Standard Internet Plan.";
		strActualResult = "'Upgrade to Elite' add-on is selected under 'Internet' plan section.";
		blnStepRC=selectCheckBox("pgeCustomizeOrder","chkUpgradeUltraToElite","elePlanUnderAMCharges","id",false);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 7 - Verify Plan Name
		// Verify Plan Name
		// ########################################################################################################
		strStepDesc = "Verify selected plan's name under 'Additional Monthly Charges' section.";
		strActualResult = "Selected plan's name '<b>Ultra Internet</b>' is displayed under 'Additional Monthly Charges' section.";
		blnStepRC=verifyTextContains("pgeCustomizeOrder","elePlanUnderAMCharges",strUpdatedPlanName);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 8 - Verify Plan Charges
		// Verify Plan Charges
		// ########################################################################################################
		strStepDesc = "Verify plan charges under 'Additional Monthly Charges' section.";
		strActualResult = "Selected Plan's Charges are displayed under 'Additional Monthly Charges' section.";
		blnStepRC=verifyTextContains("pgeCustomizeOrder","elePlan$UnderAMCharges","$");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 9 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on Continue button on 'Customize Order' page.";
		strActualResult = "User is navigated to 'Confirm Order' page.";
		blnStepRC =clickAndVerify("pgeCustomizeOrder","btnContinue","pgeConfirmOrder","eleConfirmHighlighted");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
	
		// ########################################################################################################
		// Step 10 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on 'I agree checkbox' and click 'continue' button on 'Confirm Order' page.";
		strActualResult = "'Thank you' page is displayed.";
		//blnStepRC =CheckIAgreeCheckBoxAndClickOnContinue();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			//return;
		}
				
		// ########################################################################################################
		// Step 11 - Click on 'Go To My Account'.
		//  Click on 'Go To My Account' button.
		// ########################################################################################################
		strStepDesc = "Click on 'Go To My Account' button on 'Customer Summary' page.";
		strActualResult = "Pop-Up '<b>"+strPopUpText+"</b>' is displayed.";
		blnStepRC = clickAndVerify("pgeBrowse","btnGoToMyAccount","pgeCustomerSummary","eleAreYouSureToLeavePopUp");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 12 - Click on 'Leave Page'.
		//  Click on 'Leave Page' button.
		// ########################################################################################################
		strStepDesc = "Click on 'Leave Page' button from the pop-up.";
		strActualResult = "'My Account Overview' page is displayed.";
		blnStepRC = clickAndVerify("pgeCustomerSummary","btnLeavePage","pgeAccountOverview","lnkAddUpgradeService");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 13 - Logout from the session.
		//  Logout from the application.
		// ########################################################################################################
		strStepDesc = "Click on 'Logout' button to logout from the application.";
		strActualResult = "Account is logged out successfully.";
		blnStepRC = clickJSAndVerify("pgeAccountOverview","lnkLogout","pgeMyAccountLogin","txtUserName");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}
