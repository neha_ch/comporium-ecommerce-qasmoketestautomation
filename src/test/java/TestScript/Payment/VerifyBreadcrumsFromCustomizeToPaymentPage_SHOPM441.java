package TestScript.Payment;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import Launcher.Launcher;
import libraries.WebFunclib;
public class VerifyBreadcrumsFromCustomizeToPaymentPage_SHOPM441 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;

	@Test
	public void VerifyBreadcrumsFromCustomizeToPaymentPage(ITestContext testContext) throws InterruptedException 
	{
		// ########################################################################################################
		// # Test Case ID: SHOPM-441
		// # Test Case: VerifyBreadcrumsFromCustomizeToPaymentPage
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify breadcrums from Customize to Payment Page for new customers in shopq application.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################
		
		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strFirstName = dicTestData.get("strFirstName");
		String strLastName = dicTestData.get("strLastName");
		String strDOB = dicTestData.get("strDOB");
		String strPhoneNumber = dicTestData.get("strPhoneNumber");
		String strEmail = dicTestData.get("strEmail");
		String strSSN = dicTestData.get("strSSN");
		
		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium account exists, Street Address: '<b>"+strStreetAddressData+"</b>'";
		strActualResult = "'Zip code: '<b>"+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		strActualResult = "Fields 'Selected Service Address','Change Address','highlighted Bundle Plans','Individual Plans' and 'Cart section' are displayed.";
		blnStepRC = clickOnSubmitAddressSearch(true,false);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
	
		// ########################################################################################################
		// Step 5 - Click on 'Customize' button
		// Click on 'Customize' button to add a plan
		// ########################################################################################################
		strStepDesc = "Select a Bundle Plan by clicking on 'Customize' button on the 'Browse' page.";
		strActualResult = "'Customize' page is displayed as first highlighted 'Customize' bread crum.";
		blnStepRC = clickAndVerify("pgeBrowse","btnCustomizeBundle","pgeCustomizeOrder","eleBrdcrum1Highlighted","brdCustomizeHighlighted");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 6 - Click on 'Continue' button
		// Click on 'Continue' button
		// ########################################################################################################
		strStepDesc = "Click on 'Continue' button on 'Customized Order' page.";
		strActualResult = "'Checkout' page is displayed as second highlighted 'Checkout' bread crum.";
		blnStepRC = clickAndVerify("pgeCheckout","btnContinue","pgeCheckout","eleBrdcrum2Highlighted","brdCheckoutHighlighted");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
	
		// ########################################################################################################
		// Step 7 - Enter Billing Address details
		// Enter Billing Address details for existing customer
		// ########################################################################################################
		strStepDesc = "Enter all details in Primary Account Holder section, select 'No Thanks' checkbox and then Click Continue.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC =enterCheckoutInformation(strFirstName, strLastName,strPhoneNumber,strDOB, strSSN, strEmail,true);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 8 - Verify Breadcrum Stage
		// Verify Breadcrum Stage
		// ########################################################################################################
		strStepDesc = "Verify third highlighted 'Schedule' bread crum.";
		strActualResult = "'Schedule' bread crum is displayed as third highlighted breadcrum.";
		blnStepRC =verifyElementExist("pgeServiceSchedule","eleBrdcrum3Highlighted");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 9 - Verify 'Schedule' breadcrum
		// Verify 'Schedule' breadcrum
		// ########################################################################################################
		strStepDesc = "Verify highlighted 'Schedule' breadcrum.";
		strActualResult = "Highlighted 'Schedule' breadcrum is displayed on Schedule page.";
		blnStepRC =verifyElementExist("pgeServiceSchedule","brdScheduleHighlighted");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 10 - Select Installation Date and Time
		// Select Installation Date and Time
		// ########################################################################################################
		strStepDesc = "Click on 'Select Date and Time' button,select an available date and Preferred Time of Day and click on OK.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC =selectDateAndTimeForInstallation();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	
		// ########################################################################################################
		// Step 11 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on Continue button on 'Service & Installation' page.";
		strActualResult = "'Confirm Order' page is displayed as fourth highlighted 'Confirm' bread crum.";
		blnStepRC = clickAndVerify("pgeServiceSchedule","btnContinue","pgeConfirmOrder","eleBrdcrum4Highlighted","brdConfirmHighlighted");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 12 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on 'I agree checkbox' and click 'continue' button on 'Confirm Order' page.";
		strActualResult = "'Set Up Payment' page is displayed with following fields: 'What am I paying today?' ,'Credit Card/Debit Card Payment section','Submit Order button' and 'Monthly Charges section'.";
		blnStepRC =selectIAgreeCheckBoxAndContinue();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 13 - Verify Breadcrum Stage
		// Verify Breadcrum Stage
		// ########################################################################################################
		strStepDesc = "Verify fifth highlighted 'Payment' bread crum.";
		strActualResult = "'Payment' bread crum is displayed as fifth highlighted breadcrum.";
		blnStepRC =verifyElementExist("pgeSetUpPayment","eleBrdcrum5Highlighted");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 14 - Verify 'Payment' breadcrum
		// Verify 'Payment' breadcrum
		// ########################################################################################################
		strStepDesc = "Verify highlighted 'Payment' breadcrum.";
		strActualResult = "Highlighted 'Payment' breadcrum is displayed on 'Set Up Payment' page.";
		blnStepRC =verifyElementExist("pgeSetUpPayment","brdPaymentHighlighted");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 15 - "Click on 'Customize' breadcrum
		// Click on 'Customize' breadcrum
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' breadcrum on 'Set Up Payment' page.";
		strActualResult = "'Customize' page is displayed as first highlighted 'Customize' bread crum.";
		blnStepRC = clickElementAndVerify("pgeCustomizeOrder","eleCustomizeBreadcrum","pgeCustomizeOrder","eleBrdcrum1Highlighted","brdCustomizeHighlighted");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 16 - Click on 'Continue' button
		// Click on 'Continue' button
		// ########################################################################################################
		strStepDesc = "Click on 'Continue' button on 'Customized Order' page.";
		strActualResult = "'Checkout' page is displayed as second highlighted 'Checkout' bread crum.";
		blnStepRC = clickAndVerify("pgeCheckout","btnContinue","pgeCheckout","eleBrdcrum2Highlighted","brdCheckoutHighlighted");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 17 - Enter SSN
		// Enter SSN and continue
		// ########################################################################################################
		strStepDesc = "Enter SSN and click on 'Continue'.";
		strActualResult = "'Service & Installation' page is displayed as third highlighted 'Schedule' bread crum.";
		blnStepRC = enterDataAndClick("pgeCheckout","txtSSN",strSSN,"btnContinueSubmit","pgeServiceSchedule","eleBrdcrum3Highlighted");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 21 - Verify 'Schedule' breadcrum
		// Verify 'Schedule' breadcrum
		// ########################################################################################################
		strStepDesc = "Verify highlighted 'Schedule' breadcrum.";
		strActualResult = "Highlighted 'Schedule' breadcrum is displayed on 'Service & Installation' page.";
		blnStepRC =verifyElementExist("pgeServiceSchedule","brdScheduleHighlighted");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 18 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on Continue button on 'Service & Installation' page.";
		strActualResult = "'Confirm Order' page is displayed as fourth highlighted 'Confirm' bread crum.";
		blnStepRC = clickAndVerify("pgeServiceSchedule","btnContinue","pgeConfirmOrder","eleBrdcrum4Highlighted","brdConfirmHighlighted");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 19 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on 'I agree checkbox' and click 'continue' button on 'Confirm Order' page.";
		strActualResult = "'Set Up Payment' page is displayed with following fields: 'What am I paying today?' ,'Credit Card/Debit Card Payment section','Submit Order button' and 'Monthly Charges section'.";
		blnStepRC =selectIAgreeCheckBoxAndContinue();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 20 - Verify Breadcrum Stage
		// Verify Breadcrum Stage
		// ########################################################################################################
		strStepDesc = "Verify fifth highlighted 'Payment' bread crum.";
		strActualResult = "'Payment' bread crum is displayed as fifth highlighted breadcrum.";
		blnStepRC =verifyElementExist("pgeSetUpPayment","eleBrdcrum5Highlighted");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 21 - Verify 'Payment' breadcrum
		// Verify 'Payment' breadcrum
		// ########################################################################################################
		strStepDesc = "Verify highlighted 'Payment' breadcrum.";
		strActualResult = "Highlighted 'Payment' breadcrum is displayed on 'Set Up Payment' page.";
		blnStepRC =verifyElementExist("pgeSetUpPayment","brdPaymentHighlighted");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}
