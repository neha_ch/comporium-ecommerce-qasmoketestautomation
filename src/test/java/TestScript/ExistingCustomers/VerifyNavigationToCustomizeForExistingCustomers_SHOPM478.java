package TestScript.ExistingCustomers;
import org.testng.ITestContext;
import org.testng.annotations.Test;
import libraries.WebFunclib;
public class VerifyNavigationToCustomizeForExistingCustomers_SHOPM478 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;
	
	@Test
	public void VerifyNavigationToCustomizeForExistingCustomers(ITestContext testContext) throws InterruptedException 
	{
		
		// ########################################################################################################
		// # Test Case ID: SHOPM478
		// # Test Case: VerifyNavigationToCustomizeForExistingCustomers
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify 'navigation to Customize' flow for existing customers.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################

		//Getting data from master test data file
		String strURL = dicTestData.get("strURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strExstUsername = dicTestData.get("strExstUsername");
		String strExstPassword = dicTestData.get("strExstPassword");
		String strPopUpText = "Are you sure you want to leave this page? All your shopping changes will be lost.";
		
		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> Address Search, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address Search' Page is displayed.";
		blnStepRC = navigateToURL(strURL,"pgeProfilePage","txtStreetAddress");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter an address where No Comporium Service exists
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where No Comporium Service exists, Street Address: '<b>"+ strStreetAddressData + "</b>'";
		strActualResult = "'Zip code: '<b>"+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}

		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 4 - Click on 'Yes I live here' from the pop-up
		//Click on 'Yes I live here' from the pop-up
		// ########################################################################################################
		strStepDesc = "Click on 'Yes I live here' from the pop-up.";
		strActualResult = "User is navigated to 'My Account Login' page.";
		blnStepRC = clickOnAddressConformationPopup("btnYesLiveHere","");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 5 - Enter Account username and password
		//Enter Account username and password
		// ########################################################################################################
		strStepDesc = "Enter Account User Nname and Password and click on 'Login' button.";
		strActualResult = "User is navigated to 'My Account Overview' page for account number '<b>"+strExstUsername+"</b>'.";
		blnStepRC = loginExistingCustomer(strExstUsername,strExstPassword);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 6 - Click on 'Shop Upgrades'
		//Click on 'Shop Upgrades'
		// ########################################################################################################
		strStepDesc = "Click on 'Shop Upgrades' link and verify navigated page.";
		strActualResult = "User is navigated to 'Customize Order' page.";
		blnStepRC = clickAndVerify("pgeAccountOverview","btnShopUpgrades","pgeCustomizeOrder","lnkReturnToAccSummary");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 7 - Click on 'Return To Account Summary'
		//Click on 'Return To Account Summary'
		// ########################################################################################################
		strStepDesc = "Click on 'Return To Account Summary' link and verify navigated page.";
		strActualResult = "User is navigated to 'Customer Summary' page.";
		blnStepRC = clickAndVerify("pgeCustomizeOrder","lnkReturnToAccSummary","pgeAccountOverview","btnShopUpgrades");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 8 - Click on 'Shop Upgrades'
		//Click on 'Shop Upgrades'
		// ########################################################################################################
		strStepDesc = "Click on 'Shop Upgrades' link and verify navigated page.";
		strActualResult = "User is navigated to 'Customize Order' page.";
		blnStepRC = clickAndVerify("pgeAccountOverview","btnShopUpgrades","pgeCustomizeOrder","lnkReturnToAccSummary");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 9 - Click on 'Go To My Account'.
		//  Click on 'Go To My Account' button.
		// ########################################################################################################
		strStepDesc = "Click on 'Go To My Account' button on 'Customer Summary' page.";
		strActualResult = "Pop-Up '<b>"+strPopUpText+"</b>' is displayed.";
		blnStepRC = clickAndVerify("pgeBrowse","btnGoToMyAccount","pgeCustomerSummary","eleAreYouSureToLeavePopUp");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 10 - Click on 'Leave Page'.
		//  Click on 'Leave Page' button.
		// ########################################################################################################
		strStepDesc = "Click on 'Leave Page' button from the pop-up.";
		strActualResult = "'My Account Overview' page is displayed.";
		blnStepRC = clickAndVerify("pgeCustomerSummary","btnLeavePage","pgeAccountOverview","lnkAddUpgradeService");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 11 - Logout from the session.
		//  Logout from the application.
		// ########################################################################################################
		strStepDesc = "Click on 'Logout' button to logout from the application.";
		strActualResult = "Account is logged out successfully.";
		blnStepRC = clickAndVerify("pgeAccountOverview","lnkLogout","pgeMyAccountLogin","txtUserName");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}
