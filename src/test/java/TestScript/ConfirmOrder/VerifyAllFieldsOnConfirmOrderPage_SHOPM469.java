package TestScript.ConfirmOrder;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import Launcher.Launcher;
import libraries.WebFunclib;
public class VerifyAllFieldsOnConfirmOrderPage_SHOPM469 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;

	@Test
	public void VerifyAllFieldsOnConfirmOrderPage(ITestContext testContext) throws InterruptedException 
	{
		// ########################################################################################################
		// # Test Case ID: SHOPM-469
		// # Test Case: VerifyAllFieldsOnConfirmOrderPage
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify all the fields on Confirm Order page.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################
		
		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strFirstName = dicTestData.get("strFirstName");
		String strLastName = dicTestData.get("strLastName");
		String strDOB = dicTestData.get("strDOB");
		String strPhoneNumber = dicTestData.get("strPhoneNumber");
		String strEmail = dicTestData.get("strEmail");
		String strSSN = dicTestData.get("strSSN");

		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium account exists, Street Address: '<b>"+strStreetAddressData+"</b>'";
		strActualResult = "'Zip code: '<b>"+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 4 - "Click on 'No, I'm Moving here'
		// "Click on 'No, I'm Moving here'
		// ########################################################################################################
		strStepDesc = "Click on 'No, I'm Moving here' from the pop-up.";
		strActualResult = "Fields 'Selected Service Address', 'Change Address','highlighted Bundle Plans','Individual Plans' and 'Cart section' are displayed.";
		blnStepRC = clickOnAddressConformationPopup("btnIamMovingHere",strStreetAddressData);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 5 - Click on 'Customize' button
		// Click on 'Customize' button to add a plan
		// ########################################################################################################
		strStepDesc = "Select a Bundle Plan by clicking on 'Customize' button on the 'Browse' page.";
		strActualResult = "'Customize Order' Page is displayed with following fields : highlighted 'Customize' breadcrum with Step-Number,Checkout,Confirm,Payment breadcrums,"
				+ "Recommended Upgraded Equipment Section,Monthly Charges Section,Product Section(Internet) and Continue,Continue Shopping buttons.";
		blnStepRC = customizePlanAndVerify(true);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 6 - Click on 'Continue' button
		// Click on 'Continue' button
		// ########################################################################################################
		strStepDesc = "Click on 'Continue' button on 'Customized Order' page.";
		strActualResult = "'Checkout Page' is displayed with following fields 'Highlighted 'Checkout' section,'Primary Account Holder','Billing Address' and 'Credit Check Information' sections.";
		blnStepRC = checkoutPlanAndVerify();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
	
		// ########################################################################################################
		// Step 7 - Enter Billing Address details
		// Enter Billing Address details for existing customer
		// ########################################################################################################
		strStepDesc = "Enter all details in Primary Account Holder section, select 'No Thanks' checkbox and then Click Continue.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC =enterCheckoutInformation(strFirstName, strLastName, strPhoneNumber,strDOB,strSSN, strEmail,true);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 8 - Select Installation Date and Time
		// Select Installation Date and Time
		// ########################################################################################################
		strStepDesc = "Click on 'Select Date and Time' button,select an available date and Preferred Time of Day and click on OK.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC =selectDateAndTimeForInstallation();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 9 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on Continue button on 'Service & Installation' page.";
		strActualResult = "'Confirm Order' page is displayed with fields 'Highlighted Confirm breadcrum,Total Due and Account Details' sections.";
		blnStepRC =verifyConfirmOrderPage();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 10 - Click on + icon
		// Click on + icon
		// ########################################################################################################
		strStepDesc = "Click on + icon to expand all the 'Total Due' Section.";
		strActualResult = "'Total Due' Section is expanded on 'Confirm Order' page.";
		blnStepRC =clickAndVerify("pgeConfirmOrder","btnPlusIcon","pgeConfirmOrder","eleTotalDueToday");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 11 - verify Monthly Charges
		// verify Monthly Charges
		// ########################################################################################################
		strStepDesc = "Verify Monthly Charges in expanded 'Total Due' Section.";
		strActualResult = "Amount Bifurcation is divided into 'Monthly Charges'.";
		blnStepRC =page("pgeConfirmOrder").element("eleMonthlyCharges").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 12 - verify Total One Time Charges
		// verify Total One Time Charges
		// ########################################################################################################
		strStepDesc = "Verify Total One Time Charges in expanded 'Total Due' Section.";
		strActualResult = "Amount Bifurcation is divided into 'Total One Time Charges'.";
		blnStepRC =page("pgeConfirmOrder").element("eleTotalOneTimeCharges").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 13 - verify Taxes & Fees
		// verify Taxes & Fees
		// ########################################################################################################
		strStepDesc = "Verify Taxes & Fees in expanded 'Total Due' Section.";
		strActualResult = "Amount Bifurcation is divided into 'Taxes & Fees'.";
		blnStepRC =page("pgeConfirmOrder").element("eleTaxes&Fees").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 14 - verify Plans 
		// verify added Plans 
		// ########################################################################################################
		strStepDesc = "Verify added Plans under 'Monthly Charges' section.";
		strActualResult = "Selected Plans are displayed under 'Monthly Charges' section.";
		blnStepRC =page("pgeConfirmOrder").element("eleSelectedPlans").exist();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 15 - verify Plans Price
		// verify added Plans Price
		// ########################################################################################################
		strStepDesc = "Verify Price of the added Plans under 'Monthly Charges' section.";
		strActualResult = "Selected Plans with price are displayed under 'Monthly Charges' section.";
		blnStepRC = verifyTextContains("pgeConfirmOrder", "elePlansPrice","$");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 16 - verify Total Due Amount,Total Due Today
		// verify Total Due Amount,Total Due Today
		// ########################################################################################################
		strStepDesc = "Verify that Total Due Amount matches Total Due Today Amount.";
		strActualResult = "'Total Due' Amount is equal to 'Total Due Today' Amount.";
		blnStepRC = verifyTextMatches("pgeConfirmOrder","eleTotalDueAmount","pgeConfirmOrder","eleTotalDueToday$");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 17 - Verify Continue button
		// Verify Continue button
		// ########################################################################################################
		strStepDesc = "Verify that Continue button is disabled.";
		strActualResult = "Continue button is disabled till 'I agree with Terms & Conditions' checkbox is checked.";
		blnStepRC = verifyEnabled("pgeConfirmOrder","btnContinue",false);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 18 - Now click on "I agree with  Terms & Conditions" checkbox
		// Now click on "I agree with  Terms & Conditions" checkbox
		// ########################################################################################################
		strStepDesc = "Click on 'I agree with Terms & Conditions' checkbox.";
		strActualResult = "'I agree with Terms & Conditions' is checked.";
		blnStepRC = clickAndVerify("pgeConfirmOrder","chkIagreeTermsConditions","pgeConfirmOrder","btnContinue");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 19 - Verify Continue button
		// Verify Continue button
		// ########################################################################################################
		strStepDesc = "Verify that Continue button is enabled.";
		strActualResult = "Continue button is enabled on clicking 'I agree with Terms & Conditions' checkbox.";
		blnStepRC = verifyEnabled("pgeConfirmOrder","btnContinue",true);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 20 - Verify "I agree with  Terms & Conditions" checkbox
		// Verify "I agree with  Terms & Conditions" checkbox
		// ########################################################################################################
		strStepDesc = "Verify 'I agree with Terms & Conditions' checkbox.";
		strActualResult = "'I agree with Terms & Conditions' checkbox is disabled once user checks it.";
		blnStepRC = verifyEnabled("pgeConfirmOrder","chkIagreeTermsConditions",false);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
}
	}
