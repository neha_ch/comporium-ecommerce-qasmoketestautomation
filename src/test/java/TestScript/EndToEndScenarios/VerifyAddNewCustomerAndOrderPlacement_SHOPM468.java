package TestScript.EndToEndScenarios;
import java.util.HashMap;
import java.util.List;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import libraries.WebFunclib;
public class VerifyAddNewCustomerAndOrderPlacement_SHOPM468 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;

	@Test
	public void VerifyAddNewCustomerAndOrderPlacement(ITestContext testContext) throws InterruptedException 
	{
		// ########################################################################################################
		// # Test Case ID: SHOPM-468
		// # Test Case: VerifyAddNewCustomerAndOrderPlacement
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify end to end flow, till order placement for a new customer.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################
		
		//Getting data from master test data file
		String strURL = dicTestData.get("strURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strParentPlan = dicTestData.get("strParentPlan");
		String strSubPlan  = dicTestData.get("strSubPlan");
		String strCustomizePlan  = dicTestData.get("strCustomizePlan");
		String strSelectValue = dicTestData.get("strSelectValue");
		String strSelectType = dicTestData.get("strSelectType");
		String deselect = dicTestData.get("deSelect");
		boolean deSelect = Boolean.parseBoolean(deselect);
		String strMulltipleIterations = dicTestData.get("strMulltipleIterations");
		boolean blnMulltipleIterations = Boolean.parseBoolean(strMulltipleIterations);
		String strFirstName = dicTestData.get("strFirstName");
		String strLastName = dicTestData.get("strLastName");
		String strDOB = dicTestData.get("strDOB");
		String strPhoneNumber = dicTestData.get("strPhoneNumber");
		String strEmail = dicTestData.get("strEmail");
		String strSSN = dicTestData.get("strSSN");
		String CardNumber = dicTestData.get("CardNumber");
		String ExpiryDate = dicTestData.get("ExpiryDate");
		String CardHolderName = dicTestData.get("CardHolderName");
		String CVV = dicTestData.get("CVV");
		HashMap<String,List<String>> dicTestOrders = new HashMap<String,List<String>>();
		String strDataFile = dicTestData.get("strDataFilePath");
		String strFilePath = strDataFile+"TestDataOrders.xlsx";
		String strZipFile = strDataFile+"TestDataOrders.zip";
		
		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC=navigateToURL(strURL,"pgeProfilePage","txtStreetAddress");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium account exists, Street Address: '<b>"+strStreetAddressData+"</b>'";
		strActualResult = "'Zip code: '<b>"+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 4 - "Click on 'No, I'm Moving here'
		// "Click on 'No, I'm Moving here'
		// ########################################################################################################
		strStepDesc = "Click on 'No, I'm Moving here' from the pop-up.";
		strActualResult = "Fields 'Selected Service Address', 'Change Address','highlighted Bundle Plans','Individual Plans' and 'Cart section' are displayed.";
		blnStepRC = clickOnAddressConformationPopup("btnIamMovingHere",strStreetAddressData);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 5 - Customize selected plan
		// Cuatomize specified plan
		// ########################################################################################################
		strStepDesc = "Select plan '<b>"+strParentPlan+"</b>' and click on 'Customize' button on 'Browse' page.";
		strActualResult = "User is navigated to 'Customize Order' Page.";
		blnStepRC = customizeBundleIndividualPlan(strParentPlan,strSubPlan,strCustomizePlan);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 6 - Verify If Security Plan is selected
		// If Yes, select security equipment
		// ########################################################################################################
		strStepDesc = "Select a Security equipment if a Security Bundle/Indivudal Plan is selected on 'Customize Order' page.";
		strActualResult = "Security Equipment is selected if a Security plan exists.";
		blnStepRC = verifyExistsAndSelect("pgeCustomizeOrder","lstDoorLockSecurityEquip",strSelectValue,strSelectType,deSelect,"pgeCustomizeOrder","eleDoorLockService$");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 7 - Verify If Security Plan is selected
		// If Yes, select security equipment
		// ########################################################################################################
		strStepDesc = "Select a Security equipment if a Security Bundle/Indivudal Plan is selected on 'Customize Order' page.";
		strActualResult = "Security Equipment is selected if a Security plan exists.";
		blnStepRC = verifyExistsAndSelect("pgeCustomizeOrder","lstSmokeDetSecurityEquip",strSelectValue,strSelectType,deSelect,"pgeCustomizeOrder","eleSmokeDectService$");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		/*// ########################################################################################################
		// Step 5 - Click on 'Customize' button
		// Click on 'Customize' button to add a plan
		// ########################################################################################################
		strStepDesc = "Select a Bundle Plan by clicking on 'Customize' button on the 'Browse' page.";
		strActualResult = "'Customize Order' Page is displayed with following fields : highlighted 'Customize' breadcrum with Step-Number,Checkout,Confirm,Payment breadcrums,"
				+ "Recommended Upgraded Equipment Section,Monthly Charges Section,Product Section(Internet) and Continue,Continue Shopping buttons.";
		blnStepRC = comporium_CustomizeYourPlan(true);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}*/

		// ########################################################################################################
		// Step 8 - Click on 'Continue' button
		// Click on 'Continue' button
		// ########################################################################################################
		strStepDesc = "Click on 'Continue' button on 'Customized Order' page.";
		strActualResult = "'Checkout Page' is displayed with following fields 'Highlighted 'Checkout' section,'Primary Account Holder','Billing Address' and 'Credit Check Information' sections.";
		blnStepRC = checkoutPlanAndVerify();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
	
		// ########################################################################################################
		// Step 9 - Enter Billing Address details
		// Enter Billing Address details for existing customer
		// ########################################################################################################
		strStepDesc = "Enter all details in Primary Account Holder section, select 'No Thanks' checkbox and then Click Continue.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		if(blnMulltipleIterations)
			blnStepRC =fill_BillingAddress(strFirstName, strLastName, strDOB, strPhoneNumber, strSSN, strEmail,true,blnMulltipleIterations);
		else
			blnStepRC =enterCheckoutInformation(strFirstName, strLastName, strDOB, strPhoneNumber, strSSN, strEmail,true);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 10 - Select Installation Date and Time
		// Select Installation Date and Time
		// ########################################################################################################
		strStepDesc = "Click on 'Select Date and Time' button,select an available date and Preferred Time of Day and click on OK.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC = selectDateAndTimeForInstallation("pgeServiceSchedule","btnSelectDateForSecurity","btnSelectDateTime","eleSelectedDateTime");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 11 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on Continue button on 'Service & Installation' page.";
		strActualResult = "'Confirm Order' page is displayed with fields 'Highlighted Confirm breadcrum,Total Due and Account Details' sections.";
		blnStepRC =verifyConfirmOrderPage();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 12 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on 'I agree checkbox' and click 'continue' button on 'Confirm Order' page.";
		strActualResult = "'Set Up Payment' page is displayed with following fields: 'What am I paying today?' ,'Credit Card/Debit Card Payment section','Submit Order button' and 'Monthly Charges section'.";
		blnStepRC =selectIAgreeCheckBoxAndContinue();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 13 - Fill 'Credit card' details
		// Fill 'Credit card' details
		// ########################################################################################################
		strStepDesc = "Fill 'Credit card' details and click on Submit Order button.";
		strActualResult = "'Thank you' page is displayed.";
		blnStepRC =fillCardDetails(CardNumber, ExpiryDate, CardHolderName, CVV);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,dicTestData.get("strOrderTxt&Number"), "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		if(blnMulltipleIterations)
		{
			// ########################################################################################################
			// Step 14 - Fetch Order Number
			// Fetch Order Number
			// ########################################################################################################
			strStepDesc = "Fetch Order Number and location for the logged-in customer.";
			String strOrderNumWithLocation = dicTestData.get("strOrderNumber")+" "+dicTestData.get("strLocation");
			strActualResult = "Order Number '<b>"+dicTestData.get("strOrderNumber")+"</b>' and location '<b>"+dicTestData.get("strLocation")+" fetched successfully.";
			blnStepRC=addDictionaryValues(dicTestOrders,strOrderNumWithLocation,"strOrderNumber");
			if (blnStepRC) 
			{
				reporter.reportStep(strStepDesc,strActualResult, "Pass");
			} 
			else 
			{
				reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			}
			
			// ########################################################################################################
			// Step 15 - Update Order Number and Location in Data File
			// Update Order Number and Location in Data File
			// ########################################################################################################
			strStepDesc = "Update generated Order Number with Location in Data File.";
			strActualResult = "Generated Order Number with location is updated in Data File.";
			blnStepRC=writeDataToExcel(strFilePath,dicTestOrders);
			if (blnStepRC) 
			{
				reporter.reportStep(strStepDesc,strActualResult, "Pass");
			} 
			else 
			{
				reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			}
			
			// ########################################################################################################
			// Step 16 - Compress Data File
			// Compress Data File
			// ########################################################################################################
			strStepDesc = "Compress the Data File after data is updated.";
			strActualResult = "Data File is successfully compressed.";
			blnStepRC=compressFile(strFilePath,strZipFile);
			if (blnStepRC) 
			{
				reporter.reportStep(strStepDesc,strActualResult, "Pass");
			} 
			else 
			{
				reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			}
		}
	}
}
