package TestScript.EndToEndScenarios;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.nimbusds.oauth2.sdk.ParseException;

import Launcher.Launcher;
import libraries.WebFunclib;
public class VerifyFieldsValidationOnPaymenttPageAndPlaceOrder_SHOPM479 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;
	
	@Test
	public void VerifyFieldsValidationOnPaymenttPageAndPlaceOrder(ITestContext testContext) throws InterruptedException, ParseException, java.text.ParseException 
	{

		// ########################################################################################################
		// # Test Case ID: SHOPM-479
		// # Test Case: VerifyFieldsValidationOnPaymenttPageAndPlaceOrder
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify fields and and fields validations on 'Set Up Payment' page and then place order.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################

		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strFirstName = dicTestData.get("strFirstName");
		String strLastName = dicTestData.get("strLastName");
		String strPhoneNumber = dicTestData.get("strPhoneNumber");;
		String strSSN = dicTestData.get("strSSN");
		String strDOB = dicTestData.get("strDOB");
		String strEmail = dicTestData.get("strEmail");
		//String strInvalidCharacters = dicTestData.get("strInvalidCharacters");
		String strCardType1 = dicTestData.get("strCardType1");
		String strCardType2 = dicTestData.get("strCardType2");
		String strCardType3 = dicTestData.get("strCardType3");
		String strCardType4 = dicTestData.get("strCardType4");
		String strVisaCardNum = dicTestData.get("strVisaCardNum");
		String strMasterCardNum = dicTestData.get("strMasterCardNum");
		String strAmexCardNum = dicTestData.get("strAmexCardNum");
		String strDiscoverCardNum = dicTestData.get("strDiscoverCardNum");
		String CardNumber = dicTestData.get("CardNumber");
		String ExpiryDate = dicTestData.get("ExpiryDate");
		String CardHolderName = dicTestData.get("CardHolderName");
		String CVV = dicTestData.get("CVV");

		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter a street address where Comporium Service exists, Street Address: '<b>"+ strStreetAddressData + "</b>'";
		strActualResult = "'Zip code: '<b>"+""+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 4 - Click on 'No, I'm Moving here' from the pop-up
		// Click on 'No, I'm Moving here' from the pop-up
		// ########################################################################################################
		strStepDesc = "Click on 'No, I'm Moving here' from the pop-up.";
		strActualResult = "User is navigated to 'Browse' page.";
		blnStepRC = clickOnAddressConformationPopup("btnIamMovingHere",strStreetAddressData);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 5 - Click on 'Individual Plans' on 'Browse' page.
		// Click on 'Individual Plans'
		// ########################################################################################################
		strStepDesc = "Click on 'Individual Plans' on 'Browse' page.";
		strActualResult = "All the individual plans are displayed.";
		blnStepRC=selectBundleAndVerifyPlanTypes("pgeBrowse","btnIndividualPlans");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 6 - Click on Customize button for any Plan
		// Click on Customize button for any Plan
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button for any Plan on 'Browse' page.";
		strActualResult = "User is navigated to 'Customize Order' page.";
		blnStepRC=clickAndVerify("pgeBrowse","btnCustomize","pgeCustomizeOrder","lnkContinueShopping");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 7 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on Continue button on 'Customize Order' page.";
		strActualResult = "User is navigated to 'Checkout' page.";
		blnStepRC =checkoutPlanAndVerify();
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 8 - Enter Billing Address details
		// Enter Billing Address details for existing customer
		// ########################################################################################################
		strStepDesc = "Enter all details in Primary Account Holder section, select 'No Thanks' checkbox and then Click Continue.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC =enterCheckoutInformation(strFirstName, strLastName, strDOB, strPhoneNumber, strSSN, strEmail,true);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 9 - Select Installation Date and Time
		// Select Installation Date and Time
		// ########################################################################################################
		strStepDesc = "Click on 'Select Date and Time' button,select an available date and Preferred Time of Day and click on OK.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC =selectDateAndTimeForInstallation();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 10 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on Continue button on 'Service & Installation' page.";
		strActualResult = "'Confirm Order' page is displayed with fields 'Highlighted Confirm breadcrum,Total Due and Account Details' sections.";
		blnStepRC =verifyConfirmOrderPage();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 11 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on 'I agree checkbox' and click 'continue' button on 'Confirm Order' page.";
		strActualResult = "User is navigated to 'Set Up Payment' page.";
		blnStepRC =selectIAgreeCheckBoxAndContinue();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 12 - Enter Alphanumeric and Special Characters
		// Enter Alphanumeric and Special Characters
		// ########################################################################################################
		strStepDesc = "Enter Alphanumeric and Special Characters in 'Card Number' field.";
		strActualResult = "User is not be able to enter anything other than numbers in 'Card Number' field.";
		//blnStepRC=EnterDataAndVerifyErrorMessage("pgeSetUpPayment","txtCardNumber",CardNumber,strInvalidCharacters,"","","InvalidData");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 13 - Verify the card type logo for 'VISA'
		// Verify the card type logo for 'VISA
		// ########################################################################################################
		strStepDesc = "Verify card type logo for '<b>"+strCardType1+"</b>' Card.";
		strActualResult = "Logo for '<b>"+strCardType1+"</b>' card type is displayed on entering card number, starting with 4.";
		blnStepRC=EnterCardAndVerifyLogo("pgeSetUpPayment","txtCardNumber","eleVISACardLogo",strVisaCardNum,strCardType1);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
		
		// ########################################################################################################
		// Step 14 - Verify the card type logo for 'MASTERCARD'
		// Verify the card type logo for 'MASTERCARD'
		// ########################################################################################################
		strStepDesc = "Verify card type logo for '<b>"+strCardType2+"</b>' Card.";
		strActualResult = "Logo for '<b>"+strCardType2+"</b>' card type is displayed on entering card number, starting with 5.";
		blnStepRC=EnterCardAndVerifyLogo("pgeSetUpPayment","txtCardNumber","eleMasterCardLogo",strMasterCardNum,strCardType2);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");		
		}
		
		// ########################################################################################################
		// Step 15 - Verify the card type logo for 'AMEX'
		// Verify the card type logo for 'AMEX'
		// ########################################################################################################
		strStepDesc = "Verify card type logo for '<b>"+strCardType3+"</b>' Card.";
		strActualResult = "Logo for '<b>"+strCardType3+"</b>' card type is displayed on entering card number, starting with 3.";
		blnStepRC=EnterCardAndVerifyLogo("pgeSetUpPayment","txtCardNumber","eleAmexCardLogo",strAmexCardNum,strCardType3);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
		
		// ########################################################################################################
		// Step 16 - Verify the card type logo for 'Discover'
		// Verify the card type logo for 'Discover'
		// ########################################################################################################
		strStepDesc = "Verify card type logo for '<b>"+strCardType4+"</b>' Card.";
		strActualResult = "Logo for '<b>"+strCardType4+"</b>' card type is displayed on entering card number, starting with 6.";
		blnStepRC=EnterCardAndVerifyLogo("pgeSetUpPayment","txtCardNumber","eleDiscoverCardLogo",strDiscoverCardNum,strCardType4);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
		
		// ########################################################################################################
		// Step 17 - Enter Alphanumeric and Special Characters
		// Enter Alphanumeric and Special Characters
		// ########################################################################################################
		strStepDesc = "Enter Alphanumeric and Special Characters in 'Expiration Date' field.";
		strActualResult = "User is not be able to enter anything other than numbers in 'Expiration Date' field.";
		//blnStepRC=EnterDataAndVerifyErrorMessage("pgeSetUpPayment","txtExpiryDate",ExpiryDate,strInvalidCharacters,"","","InvalidData");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
		
		// ########################################################################################################
		// Step 18 - Enter Expiration Date
		// Enter Expiration Date
		// ########################################################################################################
		strStepDesc = "Enter Expiration Date and verify the format.";
		strActualResult = "Expiration Date is entered only in '<b>mm/yy</b>' format.";
		blnStepRC=verifyExpirationDateFormat(ExpiryDate);
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
		
		// ########################################################################################################
		// Step 19 - Enter Alphanumeric and Special Characters
		// Enter Alphanumeric and Special Characters
		// ########################################################################################################
		strStepDesc = "Enter Alphanumeric and Special Characters in 'Cardholder Name' field.";
		strActualResult = "User is not be able to enter anything other than alphabets in 'Cardholder Name' field.";
		//blnStepRC=EnterDataAndVerifyErrorMessage("pgeSetUpPayment","txtCardHolderName",CardHolderName,strDiscoverCardNum,"","","InvalidData");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
		
		// ########################################################################################################
		// Step 20 - Enter Alphanumeric and Special Characters
		// Enter Alphanumeric and Special Characters
		// ########################################################################################################
		strStepDesc = "Enter Alphanumeric and Special Characters in 'CVV' field.";
		strActualResult = "User is not be able to enter anything other than numbers in 'CVV' field.";
		//blnStepRC=EnterDataAndVerifyErrorMessage("pgeSetUpPayment","txtCVV",CVV,CardHolderName,"","","InvalidData");
		if (blnStepRC)
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");	
		}
		
		// ########################################################################################################
		// Step 21 - Fill 'Credit card' details
		// Fill 'Credit card' details
		// ########################################################################################################
		strStepDesc = "Fill 'Credit card' details and click on Submit Order button.";
		strActualResult = "'Thank you' page is displayed.";
		blnStepRC =fillCardDetails(CardNumber, ExpiryDate, CardHolderName, CVV);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,dicTestData.get("strOrderTxt&Number"), "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}
