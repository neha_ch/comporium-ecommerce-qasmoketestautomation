package TestScript.EndToEndScenarios;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import libraries.WebFunclib;
public class VerifyQuadPlayBundleWithAllServicesOnCustomizeOrder_SHOPM1167 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;

	@Test
	public void VerifyQuadPlayBundleWithAllServicesOnCustomizeOrder(ITestContext testContext) throws InterruptedException 
	{
		// ########################################################################################################
		// # Test Case ID: SHOPM-1167
		// # Test Case: VerifyQuadPlayBundleWithAllServicesOnCustomizeOrder
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify end to end flow, till order placement by customizing a Quad Play Bundle with all services.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################
		
		//Getting data from master test data file
		String strURL = dicTestData.get("strURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strPlans = dicTestData.get("strCustomizePlan");
		String strServices  = dicTestData.get("strServices");
		String strSelectValue = dicTestData.get("strSelectValue");
		String strSelectType = dicTestData.get("strSelectType");
		String strFirstName = dicTestData.get("strFirstName");
		String strLastName = dicTestData.get("strLastName");
		String strDOB = dicTestData.get("strDOB");
		String strPhoneNumber = dicTestData.get("strPhoneNumber");
		String strEmail = dicTestData.get("strEmail");
		String strSSN = dicTestData.get("strSSN");
		String CardNumber = dicTestData.get("CardNumber");
		String ExpiryDate = dicTestData.get("ExpiryDate");
		String CardHolderName = dicTestData.get("CardHolderName");
		String CVV = dicTestData.get("CVV");
		
		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC=navigateToURL(strURL,"pgeProfilePage","txtStreetAddress");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter an address where Comporium account exists, Street Address: '<b>"+strStreetAddressData+"</b>'";
		strActualResult = "'Zip code: '<b>"+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 4 - "Click on 'No, I'm Moving here'
		// "Click on 'No, I'm Moving here'
		// ########################################################################################################
		strStepDesc = "Click on 'No, I'm Moving here' from the pop-up.";
		strActualResult = "Fields 'Selected Service Address', 'Change Address','highlighted Bundle Plans','Individual Plans' and 'Cart section' are displayed.";
		blnStepRC = clickOnAddressConformationPopup("btnIamMovingHere",strStreetAddressData);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 5 - Click on 'View All Bundles' check-box
		// Click on 'View All Bundles' check-box
		// ########################################################################################################
		strStepDesc = "Click on 'View All Bundles' check-box and verify 'I am looking for:' section.";
		strActualResult = "Check-boxes for each bundle type '<b>TV,Internet,Phone</b>' and '<b>Security</b>' are displayed in 'I am looking for' section.";
		blnStepRC = selectViewAllBundlesAndVerify();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 6 - Click on 'Customize' button
		// Click on 'Customize' button
		// ########################################################################################################
		strStepDesc = "Click on 'Customize' button for '<b>Quad Play Bundle Zipstream Internet, Moxi HD Digital Variety, Phone & Security Plan</b>' on 'Browse' page.";
		strActualResult = "'Telephone' services are displayed on 'Customize Order' page.";
		blnStepRC=clickAndVerify("pgeBrowse","btnCustomizeQPlayBundle","pgeCustomizeOrder","elePhonePlanHeader");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 7 - Verify Security Services 
		// Verify Security Services
		// ########################################################################################################
		strStepDesc = "Verify Security Services on 'Customize Order' page.";
		strActualResult = "'Security' services are displayed on 'Customize Order' page.";
		blnStepRC=verifyElementExist("pgeCustomizeOrder","eleSecurityPlanHeader");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 8 - Verify TV Services 
		// Verify TV Services
		// ########################################################################################################
		strStepDesc = "Verify Television Services on 'Customize Order' page.";
		strActualResult = "'Television' services are displayed on 'Customize Order' page.";
		blnStepRC=verifyElementExist("pgeCustomizeOrder","eleTVPlanHeader");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 9 - Verify Internet Services 
		// Verify Internet Services
		// ########################################################################################################
		strStepDesc = "Verify Internet Services on 'Customize Order' page.";
		strActualResult = "'Internet' services are displayed on 'Customize Order' page.";
		blnStepRC=verifyElementExist("pgeCustomizeOrder","eleInternetPlanHeader");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 10 - Verify $ amount for each
		// Verify $ amount for each service
		// ########################################################################################################
		strStepDesc = "Verify $ amount for all services under 'Register'.";
		strActualResult = "All the services with $ amount are displayed under 'Register' on 'Customize Order' page.";
		blnStepRC=verifyMultipleElementsText("pgeCustomizeOrder","eleSelectedPlans",strPlans);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 11 - Verify accordions
		// Verify expanded accordions
		// ########################################################################################################
		strStepDesc = "Verify all the '<b>accordians</b>' are expanded on 'Customize Order' page.";
		strActualResult = "All the accordians are expanded.";
		blnStepRC=verifyMultipleElements("pgeCustomizeOrder","eleExpandedAccordians");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// #######################################################################################################
		// Step 12 - Verify Internet services
		// Verify Internet services
		// ########################################################################################################
		strStepDesc = "Verify Internet services under expanded Internet accordian.";
		strActualResult = "Internet services are displayed under expanded Internet accordian.";
		blnStepRC=verifyElementExist("pgeCustomizeOrder","chkUpgradeEquipment");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// #######################################################################################################
		// Step 13 - Verify TV services
		// Verify TV services
		// ########################################################################################################
		strStepDesc = "Verify TV services under expanded Television accordian.";
		strActualResult = "TV services are displayed under expanded Television accordian.";
		blnStepRC=verifyElementExist("pgeCustomizeOrder","elePremiumChannels");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// #######################################################################################################
		// Step 14 - Verify TV services
		// Verify TV services
		// ########################################################################################################
		strStepDesc = "Verify Security services under expanded Security accordian.";
		strActualResult = "Security services are displayed under expanded Security accordian.";
		blnStepRC=verifyElementExist("pgeCustomizeOrder","eleSecurityEquipments");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// #######################################################################################################
		// Step 15 - Verify Phone services
		// Verify Phone services
		// ########################################################################################################
		strStepDesc = "Verify Phone services under expanded Telephone accordian.";
		strActualResult = "Phone services are displayed under expanded Telephone accordian.";
		blnStepRC=verifyElementExist("pgeCustomizeOrder","elePhonePlanFeatures");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
				
		// ########################################################################################################
		// Step 16 - Collapse Internet
		// Collapse Internet
		// ########################################################################################################
		strStepDesc = "Click on '<b>Collapse</b>' button for Internet' on 'Customize Order' page.";
		strActualResult = "Internet Services are collapsed.";
		blnStepRC = clickAndVerify("pgeCustomizeOrder","btnCollapseInternet","pgeCustomizeOrder","eleCollapsedInternet");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 17 - Expand Internet
		// Expand Internet
		// ########################################################################################################
		strStepDesc = "Click on '<b>Expand</b>' button for Internet' on 'Customize Order' page.";
		strActualResult = "Internet Services are expanded.";
		blnStepRC = clickAndVerify("pgeCustomizeOrder","btnCollapseInternet","pgeCustomizeOrder","eleExpandedInternet");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 18 - Select security features
		// Select security features
		// ########################################################################################################
		strStepDesc = "Select Security features '<b>"+strServices+"</b>' on 'Customize Orde' page.";
		strActualResult = "Security features are selected from 'Features' section.";
		blnStepRC=selectFromMultipleFeatures("pgeCustomizeOrder",strServices,strSelectType,strSelectValue);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 19 - Click on 'Continue' button
		// Click on 'Continue' button
		// ########################################################################################################
		strStepDesc = "Click on 'Continue' button on 'Customized Order' page.";
		strActualResult = "'Checkout Page' is displayed with following fields 'Highlighted 'Checkout' section,'Primary Account Holder','Billing Address' and 'Credit Check Information' sections.";
		blnStepRC = checkoutPlanAndVerify();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
	
		// ########################################################################################################
		// Step 20 - Enter Billing Address details
		// Enter Billing Address details for existing customer
		// ########################################################################################################
		strStepDesc = "Enter all details in Primary Account Holder section, select 'No Thanks' checkbox and then Click Continue.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC =fill_BillingAddress(strFirstName, strLastName, strDOB, strPhoneNumber, strSSN, strEmail,true,false);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 21 - Select Installation Date and Time
		// Select Installation Date and Time
		// ########################################################################################################
		strStepDesc = "Click on 'Select Date and Time' button,select an available date and Preferred Time of Day and click on OK.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC = selectDateAndTimeForInstallation();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
				
		// ########################################################################################################
		// Step 22 - Select Installation Date and Time
		// Select Installation Date and Time
		// ########################################################################################################
		strStepDesc = "Click on 'Select Date and Time' button,select an available date and Preferred Time of Day and click on OK.";
		strActualResult = "'Service & Installation' page is displayed with 'Select Date & Time' section to schedule installation.";
		blnStepRC = selectDateAndTimeForInstallation("pgeServiceSchedule","btnSelectDateForSecurity","btnSelectDateTime","eleSelectedDateTime");
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 23 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on Continue button on 'Service & Installation' page.";
		strActualResult = "'Confirm Order' page is displayed with fields 'Highlighted Confirm breadcrum,Total Due and Account Details' sections.";
		blnStepRC =verifyConfirmOrderPage();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 24 - Click on Continue button
		// Click on Continue button
		// ########################################################################################################
		strStepDesc = "Click on 'I agree checkbox' and click 'continue' button on 'Confirm Order' page.";
		strActualResult = "'Set Up Payment' page is displayed with following fields: 'What am I paying today?' ,'Credit Card/Debit Card Payment section','Submit Order button' and 'Monthly Charges section'.";
		blnStepRC =selectIAgreeCheckBoxAndContinue();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 25 - Fill 'Credit card' details
		// Fill 'Credit card' details
		// ########################################################################################################
		strStepDesc = "Fill 'Credit card' details and click on Submit Order button.";
		strActualResult = "'Thank you' page is displayed.";
		blnStepRC =fillCardDetails(CardNumber, ExpiryDate, CardHolderName, CVV);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,dicTestData.get("strOrderTxt&Number"), "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}
