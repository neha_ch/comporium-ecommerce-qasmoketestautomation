package TestScript.EndToEndScenarios;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.nimbusds.oauth2.sdk.ParseException;

import Launcher.Launcher;
import libraries.WebFunclib;
public class VerifyCreditCheckInfoUncheckedAndNavigateToThankYouPage_SHOPM444 extends WebFunclib 
{
	String strStepDesc = "";
	String strActualResult = "";
	boolean blnStepRC = true;
	
	@Test
	public void VerifyCreditCheckInfoUncheckedAndNavigateToThankYouPage(ITestContext testContext) throws InterruptedException, ParseException, java.text.ParseException 
	{

		// ########################################################################################################
		// # Test Case ID: SHOPM-444
		// # Test Case: VerifyCreditCheckInfoUncheckedAndNavigateToThankYouPage
	    // #-------------------------------------------------------------------------------------------------------
		// # Description: Verify navigation to Thank You page after entering SSN that has 
		// #          copy write violation on Checkout page.
		// #-------------------------------------------------------------------------------------------------------
		// # Pre-conditions: ShopM URL is accessible to the user.
		// # Post-conditions: NA
		// # Limitations: NA
		// #-------------------------------------------------------------------------------------------------------
		// # Owner: Neha Chauhan
		// # Created on: 10-05-2018
		// #-------------------------------------------------------------------------------------------------------
		// # Reviewer: 
		// # Review Date:
		// #-------------------------------------------------------------------------------------------------------
		// # History Notes:
		// ########################################################################################################

		//Getting data from master test data file
		String strURL = Launcher.dicCommValues.get("strApplicationURL");
		String strStreetAddressData = dicTestData.get("strStreetAddress");
		String strZip = dicTestData.get("strZip");
		String strFirstName = dicTestData.get("strFirstName");
		String strLastName = dicTestData.get("strLastName");
		String strPhoneNumber = dicTestData.get("strPhoneNumber");;
		String strSSN = dicTestData.get("strSSN");
		String strDOB = dicTestData.get("strDOB");
		String strEmail = dicTestData.get("strEmail");

		// ########################################################################################################
		// Step 1 - Launching the URL
		// Open application URL.
		// ########################################################################################################
		strStepDesc = "Navigate to <b> ShopM New customers site, </b> application URL : <b>'"+ strURL + "'</b>.";
		strActualResult = "'Address search' Page with 'Street Address' text box and 'Submit' button in disabled state, are displayed.";
		blnStepRC = launchURL();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult,"Pass");
		}
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 2 - Enter your Location/Street Address
		// Enter street address to be located
		// ########################################################################################################
		strStepDesc = "Enter a street address where Comporium Service exists, Street Address: '<b>"+ strStreetAddressData + "</b>'";
		strActualResult = "'Zip code: '<b>"+""+strZip+"</b>' is displayed and 'Submit' button is enabled.";
		blnStepRC = enterAnAddress(strStreetAddressData, 0,true,strZip);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 3 - Click on 'Submit' button.
		// Click on Submit after entering street address
		// ########################################################################################################
		strStepDesc = "Click on 'Submit' button on 'Address Search' page.";
		blnStepRC = clickOnSubmitAddressSearch(true,true);
		strActualResult = "A Pop up with message '<b>"+dicTestData.get("strPopUpText")+"</b>' is displayed.";
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 4 - Click on 'No, I'm Moving here' from the pop-up
		// Click on 'No, I'm Moving here' from the pop-up
		// ########################################################################################################
		strStepDesc = "Click on 'No, I'm Moving here' from the pop-up.";
		strActualResult = "User is navigated to 'Browse' page.";
		blnStepRC = clickOnAddressConformationPopup("btnIamMovingHere",strStreetAddressData);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 5 - Click on 'Customize' button
		// Click on 'Customize' button to add a plan
		// ########################################################################################################
		strStepDesc = "Select a Bundle Plan by clicking on 'Customize' button on the 'Browse' page.";
		strActualResult = "'Customize Order' Page is displayed with following fields : highlighted 'Customize' breadcrum with Step-Number,Checkout,Confirm,Payment breadcrums,"
				+ "Recommended Upgraded Equipment Section,Monthly Charges Section,Product Section(Internet) and Continue,Continue Shopping buttons.";
		blnStepRC = customizePlanAndVerify(true);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}
		
		// ########################################################################################################
		// Step 6 - Click on 'Continue' button
		// Click on 'Continue' button
		// ########################################################################################################
		strStepDesc = "Click on 'Continue' button on 'Customized Order' page.";
		strActualResult = "'Checkout Page' is displayed with following fields 'Highlighted 'Checkout' section,'Primary Account Holder','Billing Address' and 'Credit Check Information' sections.";
		blnStepRC = checkoutPlanAndVerify();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
			return;
		}

		// ########################################################################################################
		// Step 7 - Enter Billing Address details
		// Enter Billing Address details for new customer
		// ########################################################################################################
		strStepDesc = "Enter all details in Primary Account Holder section along-with <b>SSN that has copy write violation</b>.";
		strActualResult = "All the Primary Account Holder details along-with SSN with copy write violation, is entered on Checkout page.";
		blnStepRC =enterCheckoutInformation(strFirstName, strLastName, strPhoneNumber,strDOB, strSSN, strEmail,false);
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
		
		// ########################################################################################################
		// Step 8 - Verify 'Thank you' note
		// Verify 'Thank you' note
		// ########################################################################################################
		strStepDesc = "Verify 'Thank You! We have received your request' note on 'Thank You' page.";
		strActualResult = "'<b>Thank You! We have received your request!.We will contact you within 24 business hours "
				+ "to complete your request</b>' is displayed on 'Thank You' page.";
		blnStepRC =navigateToThankyouPageWithUncheckCredit();
		if (blnStepRC) 
		{
			reporter.reportStep(strStepDesc,strActualResult, "Pass");
		} 
		else 
		{
			reporter.reportStep(strStepDesc,ErrDescription, "Fail");
		}
	}
}
